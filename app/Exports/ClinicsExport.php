<?php

namespace App\Exports;

use App\DoctorAppointment;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class ClinicsExport implements FromCollection, ShouldAutoSize, WithHeadings, WithEvents
{
    use Exportable;

    public $start;
    public $end;

    public function __construct($start, $end){
        $this->start = $start;
        $this->end = date('Y-m-d', strtotime($end) + 24 * 60 * 60);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DoctorAppointment::select('first_name', 'last_name', 'phone_number', 'clinic', 'doctor', 'reason', 'insured', 'card_number', 'date', 'updated_at')
            ->whereBetween('updated_at', [$this->start, $this->end])
            ->get();
    }

    public function headings(): array
    {
        return [
            'Имя',
            'Фамилия',
            'Телефон',
            'Клиника',
            'Доктор',
            'Причина',
            'Застрахован',
            'Номер страховки',
            'Предполагаемая дата',
            'Дата обращения',
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }

}
