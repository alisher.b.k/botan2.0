<?php

namespace App\Exports;

use App\StaffTelegramUsers;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\BotanDialogHistories;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class NewUsersExport implements FromCollection, ShouldAutoSize, WithHeadings, WithEvents
{
    use Exportable;

    public $start;
    public $end;
    public function __construct($start, $end){
        $this->start = $start;
        $this->end = $end;
    }

    public function collection()
    {
        return StaffTelegramUsers::select('botan_staff.FIO', 'botan_staff.WORKPLACE', 'stafftelegramusers.created_at')
            ->whereBetween('stafftelegramusers.created_at', [$this->start, $this->end])
            ->join('botan_staff', function($join)
            {
                $join->on('stafftelegramusers.staffEmail', '=', 'botan_staff.ISN')
                    ->orOn('stafftelegramusers.staffEmail', '=', 'botan_staff.email');
            })
            ->get();
    }

    public function headings(): array
    {
        return [
            'ФИО',
            'Компания',
            'Дата регистрации',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
