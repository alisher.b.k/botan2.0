<?php

namespace App\Exports;

use App\BotanStaff;
use App\Quiz;
use App\QuizQuestion;
use App\StaffTelegramUsers;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;

class QuizExport implements WithMultipleSheets {
    use Exportable;

    private $id;

    public function __construct($id) {
        $this->id = $id;
    }

    public function headings(): array {
        return [
            'ФИО',
            'Правильный ответ',
            'Неправильный ответ',
            'Дата'
        ];
    }

    public function sheets(): array {
        $quiz = Quiz::where('id', '=', $this->id)->first();

        return $quiz->questions->map(function ($question) use ($quiz) {
            return new class($question) implements FromCollection, WithTitle {
                /**
                 * @var QuizQuestion
                 */
                private $question;

                public function __construct($question) {
                    $this->question = $question;
                }

                public function title(): string {
                    $result = $this->question->text;
                    $limit = 28;
                    if (strlen($result) > $limit) {
                        $result = substr($result, 0, $limit) . '...';
                    }
                    return $result;
                }

                public function collection() {
                    $headings = [
                        'ФИО',
                        'Правильный ответ',
                        'Неправильный ответ',
                        'Дата'
                    ];

                    $result = [$headings];

                    foreach ($this->question->results as $quizResult) {
                        $telegramUser = StaffTelegramUsers::where('telegramId', $quizResult->chatId)->first();
                        if ($telegramUser) {
                            $user = BotanStaff::where('ISN', $telegramUser->staffEmail)
                                ->orWhere('email', $telegramUser->staffEmail)
                                ->first();
                            if ($user) {
                                $date = new \DateTime($quizResult->updated_at);
                                if ($this->question['option' . $quizResult->answer] === $this->question->correctOption) {
                                    $result[] = [$user->FIO, '+', '', $date->format('Y-m-d H:i:s')];
                                } else if (! is_null($quizResult->answer)) {
                                    $result[] = [$user->FIO, '', '+', $date->format('Y-m-d H:i:s')];
                                }
                            }
                        }
                    }
                    return collect($result);
                }
            };
        })->toArray();
    }
}
