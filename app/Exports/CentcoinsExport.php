<?php

namespace App\Exports;

use App\DoctorAppointment;
use App\StaffBalance;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class CentcoinsExport implements FromCollection, ShouldAutoSize, WithHeadings, WithEvents
{
    use Exportable;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return StaffBalance::select('botan_staff.FIO', 'botan_staff.WORKPLACE', DB::raw('cast(staff_balance.balance as char)'))
            ->join('botan_staff', function($join)
            {
                $join->on('staff_balance.isn', '=', 'botan_staff.ISN')
                    ->orOn('staff_balance.isn', '=', 'botan_staff.email');
            })
            ->get();
    }

    public function headings(): array
    {
        return [
            'ФИО',
            'Компания',
            'Кол-во',
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:C1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }}
