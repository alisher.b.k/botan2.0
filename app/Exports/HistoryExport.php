<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\BotanDialogHistories;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class HistoryExport implements FromCollection, ShouldAutoSize, WithHeadings, WithEvents
{
    public function collection()
    {
        return BotanDialogHistories::select('id', 'userEmail', 'request', 'response', 'created_at')->latest()->take(300)->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Email/ISN',
            'Запрос пользователя',
            'Ответ бота',
            'Дата',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}