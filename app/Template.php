<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Template
 * @package App
 * @property int $id
 * @property string $template
 * @property int $category
 * @property string $description
 * @property string $file
 * @property $created_at
 * @property $updated_at
 */
class Template extends Model
{
    //
}
