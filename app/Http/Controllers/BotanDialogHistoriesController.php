<?php

namespace App\Http\Controllers;

use App\BotanDialogHistories;
use App\BotanStaff;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HistoryExport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\StaffTelegramUsers;
use App\Companies;

class BotanDialogHistoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showHistory(){
        // $history = BotanDialogHistories::latest()->take(300)->get();

        if(Auth::user()->role->role_id == 3 || Auth::user()->role->role_id == 4){
            $users = StaffTelegramUsers::all();
            $companies = Companies::all();
            return view('sendMessagesForm', ['companies' => $companies, 'users' => $users]);
        }else{
            $history = BotanDialogHistories::select(DB::raw('chatId, telegramLastName,telegramFirstName, max(created_at) as date'))
          ->groupBy('chatId')
          ->orderBy('date', 'desc')
          ->get();

        return view('dialog_history',
            ['histories' => $history]);
        }
    }

    public function showHistoryForChat(Request $request) {
      $history = BotanDialogHistories::where('chatId', $request->chatId)
        ->get();
      return view('dialog_history_chat',
          ['histories' => $history]);
    }

    public function exportToExcel(){
        return Excel::download(new HistoryExport, 'history.xls');
    }

    public function showStaff()
    {
        $staff = BotanStaff::all();
        $last = BotanStaff::orderBy('id','desc')->first();
        return view('staff', ['staff' => $staff, 'last' => $last->id+1]);
    }
}
