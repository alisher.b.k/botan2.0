<?php

namespace App\Http\Controllers;

use App\BotanStaff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StaffController extends Controller
{
    public $companyList = [
        'ТОО "Сентрас Капитал"',
        '"АО Сентрас Секьюритиз"',
        'АО "Сентрас Секьюритиз"',
        'ТОО "Венчурный Фонд Сентрас"',
        'ТОО «Центр цифровых технологий»',
        'ТОО «Сентрас Инвест»',
        'ТОО «3Е Технологии»',
        'ТОО «Centrica»',
        'АО АИФРИ «Фонд краткосрочной ликвидности»',
        'ТОО «Сентрас Консалтинг»',
        'ТОО «Микрофинансовая организация «Сентрас Кредит»',
        'КФ  «Kazakhstan Growth Forum»',
        'ТОО «Smart Partners Consulting»',
        'ТОО "Коммеск Недвижимость"',
        'ТОО "Сентрас Брэндс"'
    ];
    public function updateStaff(Request $request){
        DB::beginTransaction();
        $this->deleteOldData();
        $res = true;
        $data = json_decode($request->mData);
        foreach ($data as $empl){
            $user = new BotanStaff();
            $user->FIO = "$empl->LASTNAME $empl->FIRSTNAME $empl->PARENTNAME";
            $user->position = $empl->DUTY;
            $user->photo = null;
            $user->workPhone = null;
            $user->intPhone = $empl->INNPHONE;
            $user->mobPhone = $empl->MOBILEPHONE;
            $user->email = $empl->EMAIL;
            $user->bday = $empl->BIRTHDAY;
            $user->workEx = $empl->WORKEXP;
            $user->ISN = $empl->ISN;
            $user->NAMELAT = $empl->NAMELAT;
            $user->WORKPLACE = $empl->WORKPLACE;
            $user->MAINDEPT = $empl->MAINDEPT;
            $user->SUBDEPT = $empl->SUBDEPT;
            $user->VACATIONBEG = $empl->VACATIONBEG;
            $user->VACATIONEND = $empl->VACATIONEND;
            $user->TRIPBEG = $empl->TRIPBEG;
            $user->TRIPEND = $empl->TRIPEND;
            $user->LEAVEDATE = $empl->LEAVEDATE;
            try{
                if(!$user->save()){
                    $res = false;
                }
            }catch (\Exception $exception){
                Log::debug('NEW USER CESEC');
                Log::debug($exception->getMessage());
                DB::rollback();
            }
        }
        if($res){
            DB::commit();
        }else{
            DB::rollback();
        }
        return [
            'success' => $res
        ];
    }

    public function deleteOldData(){
        $oldStaff = BotanStaff::whereIn('WORKPLACE', $this->companyList)->get();
        foreach ($oldStaff as $old){
            $old->delete();
        }
    }

    public $companyListSOSMA = [
        'SOS Medical Assistance ТОО'
    ];

    public function updateStaffSOSMA(Request $request){
        DB::beginTransaction();
        $this->deleteOldDataSOSMA();
        $res = true;
        $data = json_decode($request->mData);
        foreach ($data as $empl){
            $user = new BotanStaff();
            $user->FIO = "$empl->LASTNAME $empl->FIRSTNAME $empl->PARENTNAME";
            $user->position = $empl->DUTY;
            $user->photo = null;
            $user->workPhone = null;
            $user->intPhone = $empl->INNPHONE;
            $user->mobPhone = $empl->MOBILEPHONE;
            $user->email = $empl->EMAIL;
            $user->bday = $empl->BIRTHDAY;
            $user->workEx = $empl->WORKEXP;
            $user->ISN = $empl->ISN;
            $user->NAMELAT = $empl->NAMELAT;
            $user->WORKPLACE = $empl->WORKPLACE;
            $user->MAINDEPT = $empl->MAINDEPT;
            $user->SUBDEPT = $empl->SUBDEPT;
            $user->VACATIONBEG = $empl->VACATIONBEG;
            $user->VACATIONEND = $empl->VACATIONEND;
            $user->TRIPBEG = $empl->TRIPBEG;
            $user->TRIPEND = $empl->TRIPEND;
            $user->LEAVEDATE = $empl->LEAVEDATE;
            try{
                if(!$user->save()){
                    $res = false;
                }
            }catch (\Exception $exception){
                Log::debug('NEW USER CESEC');
                Log::debug($exception->getMessage());
                DB::rollback();
            }
        }
        if($res){
            DB::commit();
        }else{
            DB::rollback();
        }
        return [
            'success' => $res
        ];
    }

    public function deleteOldDataSOSMA(){
        $oldStaff = BotanStaff::whereIn('WORKPLACE', $this->companyListSOSMA)->get();
        foreach ($oldStaff as $old){
            $old->delete();
        }
    }

    public static function getVacation($ISN){
        try{
            $client = self::getClient($ISN);
            $result = $client->iin_string([
                'Input_IIN' => $ISN
            ]);
        }catch (\SoapFault $exception){
            Log::debug($exception);
            return [
                'success' => false,
                'text' => 'Извини, но информация о твоем отпуске засекречена. 🤭 Тебе нужно обратиться в HR-службу.'
            ];
        }
        return [
            'success' => true,
            'result' => $result->return
        ];
    }

    protected static function getClient($ISN){
        $client = new \SoapClient('http://192.168.1.8/zup_security/ws/ws_botan.1cws?wsdl', [
            'cache_wsdl' => WSDL_CACHE_NONE,
            'trace'      => 1,
            'login'      => 'Administrator',
            'password'   => 'ghfdf',
        ]);
        return $client;
    }

    public static function getVacationSOSMA($ISN){
        try{
            $client = self::getClientSOSMA($ISN);
            $result = $client->iin_string([
                'Input_IIN' => $ISN
            ]);
        }catch (\SoapFault $exception){
            Log::debug($exception);
            return [
                'success' => false,
                'text' => 'Извини, но информация о твоем отпуске засекречена. 🤭 Тебе нужно обратиться в HR-службу.'
            ];
        }
        return [
            'success' => true,
            'result' => $result->return
        ];
    }

    protected static function getClientSOSMA($ISN){
        $client = new \SoapClient('http://185.182.217.14:90/zup2/ws/ws_botan.1cws?wsdl', [
            'cache_wsdl' => WSDL_CACHE_NONE,
            'trace'      => 1,
            'login'      => 'Admin',
            'password'   => 'ghfdf',
        ]);
        return $client;
    }
}
