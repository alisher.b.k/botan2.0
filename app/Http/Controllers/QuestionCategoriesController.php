<?php

namespace App\Http\Controllers;

use App\QuestionCategories;
use Illuminate\Http\Request;

class QuestionCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCategories(){
        $categories = QuestionCategories::all();
        $last = QuestionCategories::count();
        return view('questionCategories', ['categories' => $categories,
            'last' => $last+1]);
    }

    public function editCategory($id){
        $category = QuestionCategories::find($id);
        if(!$category){
            $category = null;
        }
        return view('editCategory', ['category' => $category, 'last'=>$id]);
    }

    public function saveCategory(Request $request){
        $category = QuestionCategories::find($request->categoryId);
        if(!$category){
            $category = new QuestionCategories();
            $category->id = $request->categoryId;
        }
        if($request->parent_id){
            $category->parent_id = $request->parent_id;
        } else if($request->parent_id == "0"){
            $category->parent_id = 0;
        }
        $category->description = $request->description;
        $category->categoryName = $request->category;
        $category->save();

        $categories = QuestionCategories::all();
        $last = QuestionCategories::count();
        return redirect()->route('questionsCategories', ['categories' => $categories, 'last' => $last+1]);
    }

    public function deleteCategory($id)
    {
        QuestionCategories::destroy($id);
        $last = QuestionCategories::count();
        $i = 1;
        $categories = QuestionCategories::all();
        foreach($categories as $category){
            $category->id = $i;
            $category->save();
            $i++;
        }

        return redirect()->route('questionsCategories', ['categories' => $categories, 'last' => $last+1]);
    }
}
