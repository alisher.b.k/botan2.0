<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function send(){
        Mail::send(['text' => 'mail'], ['name' => 'test'], function($message){
            $message->to('moreno.anita96@gmail.com', 'to Anita')->subject('test message');
            $message->from('moreno.anita96@gmail.com', 'to Anita');
        });
    }
}
