<?php

namespace App\Http\Controllers;

use App\Roles;
use App\User;
use App\UserRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRolesController extends Controller
{
    public function getAllUsersRoles(){
        $users = User::all();
        return view('user_roles',['users' => $users]);
    }

    public function assignRole()
    {
        $role = new UserRoles();
        $role->user_id = Auth::user()->id;
        $role->role_id = 3;
        $role->save();
        return redirect()->route('home') ;
    }

    public function userInfo($id)
    {
        $user = User::find($id);
        $roles = Roles::all();
        return view('editUser', ['user' => $user, 'roles' => $roles]);
    }

    public function userSaveInfo(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        if($request->password != null){
            $user->password = Hash::make($request->password);
            $user->save();
        }
        $userRole = UserRoles::where('user_id', $user->id)->first();
        $userRole->role_id = $request->role;
        $userRole->save();
        return redirect()->route('usersRoles');
    }

    public function createUserView(){
        return view('createUser' , ['roles' => Roles::all()]);
    }

    public function createUser(Request $request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->remember_token = Str::random(10);
        $user->save();

        $userRoles = new UserRoles();
        $userRoles->user_id = $user->id;
        $userRoles->role_id = $request->role;
        $userRoles->save();

        return redirect()->route('usersRoles');
    }

    public function deleteUser($id){
        User::findOrFail($id)->delete();
        UserRoles::where('user_id', $id)->first()->delete();
        return redirect()->route('usersRoles');
    }
}
