<?php

namespace App\Http\Controllers;

use App\Companies;
use App\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProducts()
    {
        $products = Products::all();
        return view('products', ['products' => $products]);
    }

    public function editProduct($id)
    {
        $companies = Companies::all();
        $product = Products::where('id', $id)->first();
        if ($product) {
            return view('productEdit', ['product' => $product, 'companies' => $companies]);
        } else {
            return view('productEdit', ['product' => null, 'companies' => $companies]);
        }
    }
    public function saveProduct(Request $request) {
        if ($request->id) {
            $product = Products::where('id', $request->id)->first();
            if ($product) {
                $product->title = $request->title;
                $product->productContent = $request->productContent;
                $product->companyName = $request->companyName;
                $product->save();
                return redirect('/botan/products');
            } else {
                $product = new Products();
                $product->title = $request->title;
                $product->productContent = $request->productContent;
                $product->companyName = $request->companyName;
                $product->save();
                return redirect('/botan/products');
            }
        } else {
            $product = new Products();
            $product->title = $request->title;
            $product->productContent = $request->productContent;
            $product->companyName = $request->companyName;
            $product->save();
            return redirect('/botan/products');
        }
    }

    public function deleteProduct($id) {
        $product = Products::where('id', $id)->first();
        $product->delete();
        return redirect('/botan/products');
    }
}
