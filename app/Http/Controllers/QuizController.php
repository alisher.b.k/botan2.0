<?php

namespace App\Http\Controllers;

use App\Exports\QuizExport;
use App\Http\Controllers\SimpleXLSXGen;
use Illuminate\Http\Request;
use App\Quiz;
use App\QuizQuestion;
use App\QuizResults;
use App\StaffTelegramUsers;
use App\BotanStaff;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Excel;

class QuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showQuizzes()
    {
        $quiz = Quiz::all();

        return view('quiz', ['quizzes' => $quiz]);
    }

    public function downloadExcel($id) {
        $export = new QuizExport($id);

        return $export->download('test' . '.xlsx');
    }

    public function downloadExcel_old($id)
    {

        $quizMain = Quiz::where('id', $id)->first();
        $quizQuestions = QuizQuestion::where('quizId', $id)->get();

        $xlsx = new SimpleXLSXGen();

        if (count($quizQuestions)) {
            foreach ($quizQuestions as $quizQuestion) {
                $answersExcel = [['ФИО', 'Правильный ответ', 'Неправильный ответ', 'Дата']];
                $quizResults = QuizResults::where('quizId', $quizQuestion->id)
                    ->orderBy('created_at', 'desc')
                    ->get();
                foreach ($quizResults as $quizResult) {
                    $telegramUser = StaffTelegramUsers::where('telegramId', $quizResult->chatId)->first();
                    if ($telegramUser) {
                        $user = BotanStaff::where('ISN', $telegramUser->staffEmail)->orWhere('email', $telegramUser->staffEmail)->first();
                        if ($user) {
                            $date = new \DateTime($quizResult->updated_at);
                            if ($quizQuestion['option' . $quizResult->answer] === $quizQuestion->correctOption) {
                                array_push($answersExcel, [$user->FIO, '+', '', $date->format('Y-m-d H:i:s')]);
                            } else if (!is_null($quizResult->answer)) {
                                array_push($answersExcel, [$user->FIO, '', '+', $date->format('Y-m-d H:i:s')]);
                            }
                        }
                    }
                }
                $xlsx->addSheet($answersExcel, $quizQuestion->text);
            }
        }


        // $xlsx = SimpleXLSXGen::fromArray($answersExcel);

        $xlsx->downloadAs($quizMain->name . '.xlsx');

        $quizzes = Quiz::all();
        return view('quiz', ['quizzes' => $quizzes]);
    }
}
