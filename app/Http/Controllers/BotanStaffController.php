<?php

namespace App\Http\Controllers;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\Events;
use App\QuestionCategories;
use App\QuestionsAnswers;
use App\StaffTelegramUsers;
use App\StaffBalance;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\DB;

class BotanStaffController extends Controller
{
    // public function rechargeBalance(Request $request) {
    //     $staff = BotanStaff::where('id', $request->id)->first();
    //     if ($staff) {
    //         $telegramUser = StaffTelegramUsers::where('staffEmail', $staff->ISN ? $staff->ISN : $staff->email)->first();
    //         $userBalance = StaffBalance::where('isn', $staff->ISN ? $staff->ISN : $staff->email)->first();
    //         if(!$userBalance) {
    //             $userBalance = new StaffBalance();
    //             $userBalance->telegramId = '';
    //             $userBalance->balance = 0;
    //             $userBalance->isn = $staff->ISN ? $staff->ISN : $staff->email;
    //         }
    //         $userBalance->balance += $request->balance;
    //         if ($userBalance->balance >= 0) {
    //             $transaction = new Transaction();
    //             $transaction->telegramId = '';
    //             $transaction->isn = $userBalance->isn;
    //             $transaction->balanceBefore = $userBalance->balance - $request->balance;
    //             $transaction->balanceAfter = $userBalance->balance;
    //             $transaction->description = $request->description;
    //             try{
    //                 $userBalance->save();
    //                 $transaction->save();
    //                 DB::commit();
    //             }catch (\Exception $ex){
    //                 DB::rollBack();
    //                 abort(500, 'Произошла ошибка при начислении Сенткоинов');
    //             }
    //         }

            
    //     }
    //     return redirect()->route('showStaff');
    // }

    // public function withdrawBalance(Request $request) {
    //     $staff = BotanStaff::where('id', $request->id)->first();
    //     if ($staff) {
    //         $telegramUser = StaffTelegramUsers::where('staffEmail', $staff->ISN ? $staff->ISN : $staff->email)->first();
    //         $userBalance = StaffBalance::where('isn', $staff->ISN ? $staff->ISN : $staff->email)->first();
    //         if(!$userBalance) {
    //             $userBalance = new StaffBalance();
    //             $userBalance->telegramId = '';
    //             $userBalance->balance = 0;
    //             $userBalance->isn = $staff->ISN ? $staff->ISN : $staff->email;
    //         }
    //         $userBalance->balance -= $request->balance;

    //         if ($userBalance->balance >= 0) {
    //             $transaction = new Transaction();
    //             $transaction->telegramId = '';
    //             $transaction->isn = $userBalance->isn;
    //             $transaction->balanceBefore = $userBalance->balance + $request->balance;
    //             $transaction->balanceAfter = $userBalance->balance;
    //             $transaction->description = $request->description;
    //             try{
    //                 $userBalance->save();
    //                 $transaction->save();
    //                 DB::commit();
    //             }catch (\Exception $ex){
    //                 DB::rollBack();
    //                 abort(500, 'Произошла ошибка при начислении Сенткоинов');
    //             }
    //         }
    //     }
    //     return redirect()->route('showStaff');
    // }

    public function searchQuestions(Request $request)
    {
        $history = new BotanDialogHistories();
        $history->request = "Поиск вопросов (слова: " . $request->keywords . ")";
        $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
        if ($telegramUser) {
            $history->userEmail = $telegramUser->staffEmail;
            $user = BotanStaff::where("email", strtolower($telegramUser->staffEmail))->first();
            if (!$user) {
                $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
            }
            if ($user->LEAVEDATE) {
                $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                $history->save();
                return "2";
            } else {
                $company = Companies::where("companyName", $user->WORKPLACE)->first();
                $keyWordsArray = explode(" ", strtolower($request->keywords));
                //                SELECT * FROM `questions-_answers`
//                WHERE (`companyId` =0 OR`companyId` = 1)
//                AND `question` LIKE  '%документ%'
                $str = "(companyId = 0 OR companyId = " . $company->id . ") AND ( question LIKE '%" . strtolower($request->keywords) . "%' ";

//                foreach($keyWordsArray as $keyword){
//                    $str .= " OR question LIKE '%" .strtolower($keyword). "%'";
//                }
                $str .= ")";
                $questions = QuestionsAnswers::whereRaw($str)->get();
                $result = "";
                $i = 1;
                $array = [];
                if ($questions) {
                    foreach ($questions as $question) {
                        $result .= $question->id;
                        $result .= ". ";
                        $result .= $question->question . "\n";
                        if ($i % 4 === 0 || $i === count($questions)) {
                            $array[] = $result;
                            $result = "";
                        }
                        $i++;
                    }
                    $history->response = "Количество выведеных вопросов:" . count($questions) . "\n" . $result;
                    $history->save();
                    return $array;
                } else {
                    $history->userEmail = '';
                    $history->response = "Вопросы не найдены";
                    $history->save();
                    return response()->json(['status' => 'Error'], 404);
                }
            }
        } else {
            $history->userEmail = '';
            $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
            $history->save();
            return "0";
        }


    }

    public function search(Request $request)
    {
        $history = new BotanDialogHistories();
        $history->request = "Поиск сотрудников по имени " . $request->fullname;
        $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
        if ($telegramUser) {
            $history->userEmail = $telegramUser->staffEmail;
            $user = BotanStaff::where("email", strtolower($telegramUser->staffEmail))->first();
            if (!$user) {
                $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
            }
            if ($user->LEAVEDATE) {
                $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                $history->save();
                return "2";
            } else {
                $users = BotanStaff::where('FIO', 'LIKE', '%' . strtolower($request->fullname) . '%')->get();
                if (count($users) > 0) {
                    $str = "";
                    $i = 0;
                    $array = [];
                    foreach ($users as $user) {
//                        if ($user->photo) {
//                            $str .= "\n" . $user->photo;
//                        }
                        $str .= "\n ФИО: " . $user->FIO;
                        if ($user->position) {
                            $str .= "\n Должность: " . $user->position;
                        }
                        if ($user->WORKPLACE) {
                            $str .= "\n Место работы: " . $user->WORKPLACE;
                        }
                        if ($user->MAINDEPT) {
                            $str .= "\n Департамент: " . $user->MAINDEPT;
                        }
                        if ($user->SUBDEPT) {
                            $str .= "\n Отдел: " . $user->SUBDEPT;
                        }
                        if ($user->workPhone) {
                            $str .= "\n Рабочий телефон: " . $user->workPhone;
                        }
                        if ($user->intPhone) {
                            $str .= "(вн. " . $user->intPhone . ")";
                        }
                        if ($user->mobPhone) {
                            $str .= "\n Мобильный телефон: " . $user->mobPhone;
                        }
                        if ($user->email) {
                            $str .= "\n Email: " . $user->email;
                        }
                        if ($user->workEx) {
                            $str .= "\n Стаж: " . $user->workEx;
                        }
                        if ($user->bday) {
                            $str .= "\n Дата рождения: " . strval($user->bday);
                        }
                        if ($user->VACATIONBEG) {
                            $str .= "\n Дата начала отпуска: " . strval($user->VACATIONBEG);
                        }
                        if ($user->VACATIONEND) {
                            $str .= "\n Дата окончания отпуска: " . strval($user->VACATIONEND);
                        }
                        if ($user->TRIPBEG) {
                            $str .= "\n TRIPBEG: " . strval($user->TRIPBEG);
                        }
                        if ($user->TRIPEND) {
                            $str .= "\n TRIPEND: " . strval($user->TRIPEND);
                        }
                        if ($user->LEAVEDATE) {
                            $str .= "\n Дата увольнения: " . strval($user->LEAVEDATE) . "\n";
                        }

                        if ($i % 6 === 0 || $i === count($users) - 1) {
                            array_push($array, $str);
                            $str = "";
                        }
                        $i++;
                    }
                    $history->response = implode($array);
                    $history->save();
                    return $array;
                }
                $history->response = "Сотрудники с данным именем не найдены";
                $history->save();
                return response()->json(['status' => 'Error'], 404);
            }
        }
        $history->userEmail = '';
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return "0";
    }

    public function getQuestions(Request $request)
    {
        $history = new BotanDialogHistories();
        $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
        if ($telegramUser) {
            $history->userEmail = $telegramUser->staffEmail;
            $history->request = "Запрос на просмотр всех вопросов";
            $user = BotanStaff::where("email", strtolower($telegramUser->staffEmail))->first();
            if (!$user) {
                $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
            }
            if ($user->LEAVEDATE) {
                $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                $history->save();
                return "2";
            } else {
                $company = Companies::where('companyName', $user->WORKPLACE)->first();
                $questions = QuestionsAnswers::where('companyId', $company->id)
                    ->orWhere('companyId', 0)
                    ->get();
                if (count($questions) > 0) {
                    $result = '';
                    $i = 1;
                    $array = [];
                    foreach ($questions as $question) {
                        $result .= $question->id;
                        $result .= ". ";
                        $result .= $question->question . "\n";
                        if ($i % 4 === 0 || $i === count($questions)) {
                            $array[] = $result;
                            $result = "";
                        }
                        $i++;
                    }
                    $history->response = "Количество выведеных вопросов:" . count($questions);
                    $history->save();
                    return $array;
                } else {
                    $history->response = "Вопросы не найдены";
                    $history->save();
                }
                return response()->json(['status' => 'Error'], 404);
            }
        }
        $history->userEmail = "";
        $history->request = "Запрос на просмотр всех вопросов";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return "0";
    }

    public function getAnswer(Request $request)
    {
        $history = new BotanDialogHistories();
        $telegramUser = StaffTelegramUsers::where('telegramId', $request->userId)->first();
        if ($telegramUser) {
            $history->userEmail = $telegramUser->staffEmail;
            $user = BotanStaff::where("email", strtolower($telegramUser->staffEmail))->first();
            if (!$user) {
                $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
            }
            if ($user->LEAVEDATE) {
                $history->request = "Запрос на просмотр ответа на вопрос(" . $request->id . ")";
                $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                $history->save();
                return "2";
            } else {
                $answer = QuestionsAnswers::find($request->id);
                if ($answer) {
                    $history->request = "Запрос на просмотр ответа на вопрос(" . $request->id . "): " . $answer->question;
                    $history->response = "Был выведен ответ: " . $answer->answer;
                    $history->save();
                    return $answer->answer;
                }
                $history->request = "Запрос на просмотр ответа на вопрос: " . $request->id;
                $history->response = "Вопрос не найден";
                $history->save();
                return response()->json(['status' => 'Error'], 404);
            }
        }
        $history->userEmail = 0;
        $history->request = "Запрос на просмотр ответа на вопрос №" . $request->id;
        $history->response = "id(" . $request->userId . ") Пользователь не авторизован ";
        $history->save();
        return "0";
    }

    public function getCategoriesAmount()
    {
        $count = QuestionCategories::count();
        return $count;
    }

    public function getCategories(Request $request)
    {
        $history = new BotanDialogHistories();
        $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
        if ($telegramUser) {
            $history->request = "Запрос на просмотр категорий";
            $user = BotanStaff::where('email', strtolower($telegramUser->staffEmail))->first();
            if (!$user) {
                $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
            }
            if ($user->LEAVEDATE) {
                $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                $history->save();
                return "2";
            } else {
                if ($user) {
                    if ($user->email) {
                        $history->userEmail = $user->email;
                    } else {
                        $history->userEmail = $user->ISN;
                    }
                    $history->request = "Запрос на просмотр категорий";
                    $history->response = "Был выведен ответ: ";
                    $categories = QuestionCategories::where("parent_id", 0)->get();
                    if (count($categories) > 0) {
                        $str = "";
                        $i = 1;
                        $array = [];
                        foreach ($categories as $category) {
                            $str .= $category->id . ". ";
                            $history->response .= $category->id . ". ";
                            $str .= $category->categoryName . "\n";
                            $history->response .= $category->categoryName;
                            if ($i % 10 == 0 || $i == count($categories)) {
                                $array[] = $str;
                                $str = "";
                            }
                            $i++;
                        }

                        $history->save();
                        return $array;
                    }
                    $history->response .= "Нет категорий";
                    $history->save();
                }
                $history->userEmail = "";

                $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
                $history->save();
                return response()->json(['status' => 'Error'], 404);
            }
        }
        $history->userEmail = "";
        $history->request = "Запрос на просмотр категорий";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return "0";
    }

    public function getCategoryQuestions(Request $request)
    {
        Log::notice($request->categoryId);
        $history = new BotanDialogHistories();
        if ($request->id) {
            $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
            if ($telegramUser) {
                $history->userEmail = $telegramUser->staffEmail;
                $user = BotanStaff::where('email', strtolower($telegramUser->staffEmail))->first();
                if (!$user) {
                    $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
                }
                if ($user->LEAVEDATE) {
                    $history->request = "Запрос на просмотр категории №" . $request->categoryId;
                    $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                    $history->save();
                    return "2";
                } else {
                    $category = QuestionCategories::find($request->categoryId);
                    if ($category) {
                        $history->request = "Запрос на просмотр категории №" . $request->categoryId . "(" . $category->categoryName . ")";
                        $history->response = "Был выведен ответ: ";
                        $company = Companies::where('companyName', $user->WORKPLACE)->first();
//                        $questions = QuestionsAnswers::where('companyId', $company->id)
//                            ->orWhere('companyId', 0)
//                            ->get();
                        $questions = QuestionsAnswers::whereRaw('categoryId = ' . $request->categoryId . ' AND (companyId = ' . $company->id . " OR companyId = 0)")
                            ->get();
                        if (count($questions) > 0) {
                            $result = '';
                            $i = 1;
                            $array = [];
                            foreach ($questions as $question) {
                                $result .= $question->id;
                                $history->response .= $question->id . ". ";
                                $result .= ". ";
                                $result .= $question->question . "\n";
                                $history->response .= $question->question;
                                if ($i % 4 == 0 || $i == count($questions)) {
                                    $array[] = $result;
                                    $result = "";
                                }
                                $i++;
                            }
                            $history->save();
                            return $array;
                        } else {
                            $history->response .= "В данной категории нет вопросов";
                            $history->save();
                            return "3";
                        }
                        return response()->json(['status' => 'Error'], 404);
                    } else {
                        $history->response .= "Данной категории не существует";
                        $history->save();
                    }
                    return response()->json(['status' => 'Error'], 404);
                }
            }
            $history->userEmail = "";
            $history->request = "Запрос на просмотр категории";
            $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
            $history->save();
            return "0";
        }
        $history->userEmail = "";
        $history->request = "Запрос на просмотр категории";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return response()->json(['status' => 'Error'], 404);
    }

    public function getSubcategories(Request $request)
    {
        Log::notice($request->categoryId);
        $history = new BotanDialogHistories();
        if ($request->id) {
            $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
            if ($telegramUser) {
                $history->userEmail = $telegramUser->staffEmail;
                $user = BotanStaff::where('email', strtolower($telegramUser->staffEmail))->first();
                if (!$user) {
                    $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
                }
                if ($user->LEAVEDATE) {
                    $history->request = "Запрос на просмотр категории №" . $request->categoryId;
                    $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                    $history->save();
                    return "2";
                } else {
                    $category = QuestionCategories::find($request->categoryId);
                    if ($category) {
                        $history->request = "Запрос на просмотр категории №" . $request->categoryId . "(" . $category->categoryName . ")";
                        $history->response = "Был выведен ответ: ";
                        $subcategories = QuestionCategories::where('parent_id', $request->categoryId)->get();
                        if (count($subcategories) > 0) {
                            $result = '';
                            $i = 1;
                            $array = [];
                            foreach ($subcategories as $subcategory) {
                                $result .= $subcategory->id;
                                $history->response .= $subcategory->id . ". ";
                                $result .= ". ";
                                $result .= $subcategory->categoryName . "\n";
                                $history->response .= $subcategory->categoryName;
                                if ($i % 4 == 0 || $i == count($subcategories)) {
                                    $array[] = $result;
                                    $result = "";
                                }
                                $i++;
                            }
                            $history->save();
                            return $array;
                        }
                        $history->response .= "В данной категории нет подкатегорий";
                        $history->save();
                        return "3";
                    }
                    $history->response .= "Данной категории не существует";
                    $history->save();
                    return response()->json(['status' => 'Error'], 404);
                }
            }
            $history->userEmail = "";
            $history->request = "Запрос на просмотр категории";
            $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
            $history->save();
            return "0";
        }
        $history->userEmail = "";
        $history->request = "Запрос на просмотр категории";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return response()->json(['status' => 'Error'], 404);
    }

    public function calculateInsurance(Request $request)
    {
        $history = new BotanDialogHistories();
        if ($request->id) {
            Log::notice('here1');
            $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
            if ($telegramUser) {
                Log::notice('here2');
                $history->userEmail = $telegramUser->staffEmail;
                $user = BotanStaff::where('email', strtolower($telegramUser->staffEmail))->first();
                if (!$user) {
                    $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
                }
                Log::notice('here3');
                if ($user->LEAVEDATE) {
                    $history->request = "Запрос на получение суммы страховки";
                    $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                    $history->save();
                    return "2";
                } else {
                    Log::notice('here4');
//                    iins[0]=920901400452&plates[0]=194AYA02&exp[0]=1
                    $url = 'http://api.kupipolis.kz/bot/SearchEsbd?';
                    $iins = explode(" ", $request->iins);
                    for ($k = 0; $k < count($iins); $k++) {
                        if ($k != 0) {
                            $url .= '&';
                        }
                        $url .= 'iins[' . $k . ']=' . $iins[$k];
                    }
                    $plates = explode(" ", $request->plates);
                    for ($l = 0; $l < count($plates); $l++) {
                        $url .= '&';
                        $url .= 'plates[' . $l . ']=' . $plates[$l];
                    }
                    $exp = explode(" ", $request->experience);
                    for ($l = 0; $l < count($exp); $l++) {
                        $url .= '&';
                        $expVal = $exp[$l] < 2 ? 2 : 1;
                        $url .= 'exp[' . $l . ']=' . $expVal;
                    }
                    $getCalc = Curl::to($url)
                        ->withHeader('authcode: k5DYwMNtGvahEN7s')
                        ->withContentType('application/json')
                        ->withTimeout(60 * 20)
                        ->get();
                    Log::notice($getCalc);
                    return $getCalc;
                }
            }
        }
        $history->userEmail = "";
        $history->request = "Запрос на получение суммы страховки";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return response()->json(['status' => 'Error'], 404);
    }

    public function getVacationRemainingDays(Request $request)
    {
        $history = new BotanDialogHistories();
        if ($request->id) {
            $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
            if ($telegramUser) {
                $history->userEmail = $telegramUser->staffEmail;
                $user = BotanStaff::where('email', strtolower($telegramUser->staffEmail))->first();
                if (!$user) {
                    $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
                }
                if ($user->LEAVEDATE) {
                    $history->request = "Запрос на получение информации об отпуске";
                    $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                    $history->save();
                    return "2";
                } else {
                    if ($user->WORKPLACE === '"АО СК Коммеск-Омир"') {
                        $url = 'http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdb.php?ISN=' . $user->ISN;
                        $vacation = Curl::to($url)
                            ->withContentType('application/json')
                            ->withTimeout(60 * 20)
                            ->get();
                        $str = '';
                        $array = json_decode($vacation, true);
//                        if ($array['MAINDAYOFFS']) {
//                            $str .= 'Вых. дни: ' . $array['MAINDAYOFFS'] . "\r\n";
//                        }
//                        if ($array['USEDDAYOFFS']) {
//                            $str .= 'Исп. вых. дни: ' . $array['USEDDAYOFFS'] . "\r\n";
//                        }
                        if ($array['REMAININGDAYOFFS']) {
                            $str .= 'Ост. вых. дни: ' . $array['REMAININGDAYOFFS'] . "\r\n";
                        }
                        if ($array['ADMDAYS'] && ($array['USEDADMDAYS'] || intval($array['USEDADMDAYS']) == 0)) {
                            $admDays = intval($array['ADMDAYS']) - intval($array['USEDADMDAYS']);
                            $str .= 'Ост. адм. дни: ' . $admDays . "\r\n";
                        }
//                        if ($array['USEDADMDAYS']) {
//                            $str .= 'Исп. адм. вых. дни: ' . $array['USEDADMDAYS'] . "\r\n";
//                        }
                        if (($array['SENATDAYS'] || $array['SENATDAYS'] == 0) && ($array['USEDSENATDAYS'] || $array['USEDSENATDAYS'] == 0)) {
                            $senatDays = intval($array['SENATDAYS']) - intval($array['USEDSENATDAYS']);
                            $str .= 'Ост. cенат. дни: ' . $senatDays . "\r\n";
                        }
//                        if ($array['USEDSENATDAYS']) {
//                            $str .= 'Исп сенат. вых. дни: ' . $array['USEDSENATDAYS'] . "\r\n";
//                        }
                        if (($array['FOOTBALLDAYS'] || $array['FOOTBALLDAYS'] == 0) && ($array['USEDFOOTBALLDAYS'] || $array['USEDFOOTBALLDAYS'] == 0)) {
                            $footballDays = intval($array['FOOTBALLDAYS']) - intval($array['USEDFOOTBALLDAYS']);
                            $str .= 'Ост. футб. дни: ' . $footballDays . "\r\n";
                        }
//                        if ($array['USEDFOOTBALLDAYS']) {
//                            $str .= 'Исп футб. дни: ' . $array['USEDFOOTBALLDAYS'] . "\r\n";
//                        }
                        return $str;
                    } else {
                        $str = 'Нет информации по выходным дням';
                        return $str;
                    }

                }
            }
        }
        $history->userEmail = "";
        $history->request = "Запрос на получение информации об отпуске";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return response()->json(['status' => 'Error'], 404);
    }

    public function aboutUsView()
    {
        $aboutUs = DB::table('about_us')->first();
        return view('aboutUsView', ['aboutUs' => $aboutUs]);
    }

    public function aboutUsViewGetContent(Request $request)
    {
        $history = new BotanDialogHistories();
        if ($request->id) {
            Log::notice('here1');
            $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
            if ($telegramUser) {
                Log::notice('here2');
                $history->userEmail = $telegramUser->staffEmail;
                $user = BotanStaff::where('email', strtolower($telegramUser->staffEmail))->first();
                if (!$user) {
                    $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
                }
                if ($user->LEAVEDATE) {
                    $history->request = "Запрос на получение информации о нас";
                    $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                    $history->save();
                    return "2";
                } else {
                    $aboutUs = DB::table('about_us')->first();
//                    $content = strip_tags($aboutUs->content);
                    return $aboutUs->contentv2;
                }
            }
        }
        $history->userEmail = "";
        $history->request = "Запрос на получение информации о нас";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return response()->json(['status' => 'Error'], 404);
    }

    public function getEvents(Request $request)
    {
        $history = new BotanDialogHistories();
        if ($request->id) {
            Log::notice('here1');
            $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
            if ($telegramUser) {
                Log::notice('here2');
                $history->userEmail = $telegramUser->staffEmail;
                $user = BotanStaff::where('email', strtolower($telegramUser->staffEmail))->first();
                if (!$user) {
                    $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
                }
                if ($user->LEAVEDATE) {
                    $history->request = "Запрос на получение информации о мероприятих";
                    $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                    $history->save();
                    return "2";
                } else {
                    $str = "";
                    $allEvents = Events::whereDate('eventDate', '>=', date("Y-m-d"))->get();
                    foreach ($allEvents as $eventElement) {
                        $str .= $eventElement->title . "\n";
                        $str .= $eventElement->eventDate . "\n";
                        $str .= $eventElement->content . "\n\n";
                    }
                    return $str;
                }
            }
        }
        $history->userEmail = "";
        $history->request = "Запрос на получение информации о мероприятих";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return response()->json(['status' => 'Error'], 404);
    }
}
