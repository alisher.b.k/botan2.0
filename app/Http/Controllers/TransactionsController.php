<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\BotanStaff;
use App\StaffTelegramUsers;
use App\StaffBalance;
use Illuminate\Support\Facades\DB;


class TransactionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function rechargeBalance(Request $request) {
        $staff = BotanStaff::where('id', $request->id)->first();
        if ($staff) {
            $telegramUser = StaffTelegramUsers::where('staffEmail', $staff->ISN ? $staff->ISN : $staff->email)->first();
            $userBalance = StaffBalance::where('isn', $staff->ISN ? $staff->ISN : $staff->email)->first();
            if(!$userBalance) {
                $userBalance = new StaffBalance();
                $userBalance->telegramId = '';
                $userBalance->balance = 0;
                $userBalance->isn = $staff->ISN ? $staff->ISN : $staff->email;
            }
            $userBalance->balance += $request->balance;
            if ($userBalance->balance >= 0) {
                $transaction = new Transaction();
                $transaction->telegramId = '';
                $transaction->isn = $userBalance->isn;
                $transaction->balanceBefore = $userBalance->balance - $request->balance;
                $transaction->balanceAfter = $userBalance->balance;
                $transaction->description = $request->description;
                $transaction->symbol = '+';
                $transaction->style = 'text-success';
                $transaction->result = $transaction->balanceAfter - $transaction->balanceBefore;
                try{
                    $userBalance->save();
                    $transaction->save();
                    DB::commit();
                }catch (\Exception $ex){
                    DB::rollBack();
                    abort(500, 'Произошла ошибка при начислении Сенткоинов');
                }
            }

            
        }
        return redirect()->route('showTransactions',['id'=>$staff->id]);
    }

    public function withdrawBalance(Request $request) {
        $staff = BotanStaff::where('id', $request->id)->first();
        if ($staff) {
            $telegramUser = StaffTelegramUsers::where('staffEmail', $staff->ISN ? $staff->ISN : $staff->email)->first();
            $userBalance = StaffBalance::where('isn', $staff->ISN ? $staff->ISN : $staff->email)->first();
            if(!$userBalance) {
                $userBalance = new StaffBalance();
                $userBalance->telegramId = '';
                $userBalance->balance = 0;
                $userBalance->isn = $staff->ISN ? $staff->ISN : $staff->email;
            }
            $userBalance->balance -= $request->balance;

            if ($userBalance->balance >= 0) {
                $transaction = new Transaction();
                $transaction->telegramId = '';
                $transaction->isn = $userBalance->isn;
                $transaction->balanceBefore = $userBalance->balance + $request->balance;
                $transaction->balanceAfter = $userBalance->balance;
                $transaction->description = $request->description;
                $transaction->symbol = '-';
                $transaction->style = 'text-danger';
                $transaction->result = $transaction->balanceBefore - $transaction->balanceAfter;
                try{
                    $userBalance->save();
                    $transaction->save();
                    DB::commit();
                }catch (\Exception $ex){
                    DB::rollBack();
                    abort(500, 'Произошла ошибка при начислении Сенткоинов');
                }
            }
        }
        return redirect()->route('showTransactions',['id'=>$staff->id]);
    }
    public function showTransactions($id)
    {
        $botan_user = BotanStaff::find($id);
        $isnOrEmail = $botan_user->ISN ?? $botan_user->email;
        $transaction = Transaction::where('isn', $isnOrEmail)->orderBy('created_at','desc')->get();
        return view('transactions',['data' => $transaction,'user'=>$botan_user]);
    }
}