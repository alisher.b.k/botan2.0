<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poll;
use App\PollResults;

class PollController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function showPolls() {
      $pollsInfo = [];

      $polls = Poll::all();
      foreach ($polls as $poll) {
        $pollRes1 = PollResults::where('pollId', $poll->id)
          ->where('answer', $poll->option1)
          ->count();
        $pollRes2 = PollResults::where('pollId', $poll->id)
          ->where('answer', $poll->option2)
          ->count();
        $pollInfo["id"] = $poll->id;
        $pollInfo["created_at"] = $poll->created_at;
        $pollInfo["question"] = $poll->question;
        $pollInfo["option1"] = $poll->option1;
        $pollInfo["option2"] = $poll->option2;
        $pollInfo["option1Res"] = $pollRes1;
        $pollInfo["option2Res"] = $pollRes2;
        array_push($pollsInfo, $pollInfo);
      }

      return view('polls',
          ['polls' => $pollsInfo]);
    }
}
