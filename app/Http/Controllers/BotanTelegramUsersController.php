<?php

namespace App\Http\Controllers;

use App\BotanCodes;
use App\BotanDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class BotanTelegramUsersController extends Controller
{
    public static function generateCode($digits = 4){
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while($i < $digits){
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public function prelogin(Request $request){
        $history = new BotanDialogHistories();
        $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
        if($telegramUser){
            $user = BotanStaff::where('email', $telegramUser->staffEmail)->first();
            if(!$user){
                $user = BotanStaff::where('ISN', $telegramUser->staffEmail)->first();
            }
            $history->request = "Пользователь с email (".$telegramUser->staffEmail.") начал диалог";
            if ($user->LEAVEDATE) {
                $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                $history->save();
                return "2";
            } else {
                $history->userEmail = $telegramUser->staffEmail;
                $history->response = "Выберите действие";
                $history->save();
                return "1";
            }
        } else {
            $history->userEmail = "";
            $history->request = "id(".$request->id.") Неизвестный пользователь начал диалог";
            $history->response = "Пожалуйста авторизуйтесь";
            $history->save();
            return "0";
        }
    }

    public function login(Request $request){
        $history = new BotanDialogHistories();
        $user = BotanStaff::where('email', strtolower($request->email))->first();
        if(!$user){
            $email = StaffEmail::where("email", strtolower($request->email))->first();
            if($email){
                $user = BotanStaff::find($email->userId);
            } else {
                $user = BotanStaff::where('ISN', $request->email)->first();
                if($user) {
                    $history->userEmail = $user->ISN;
                    $history->response = "Авторизация прошла успешно";
                    $telegramUser = StaffTelegramUsers::where('telegramId', $request->message)->first();
                    if(!$telegramUser){
                        $history->request = "Авторизация пользователя с ISN: ".$request->email;
                        $telegramUser = new StaffTelegramUsers();
                        $telegramUser->staffEmail = $user->ISN;
                    } else {
                        $history->request = "Повторная авторизация пользователя с ISN: ".$request->email;
                    }
                    $telegramUser->telegramId = $request->message;
                    $telegramUser->save();
                    $history->save();
                    return "ISN";
                }
                $history->userEmail = "";
                $history->request = "Авторизация пользователя с ISN: ".$request->email;
                $history->response = "Email или ISN введен неверно или у вас нет доступа для просмотра информации";
                $history->save();
                return response()->json(['status' => 'Error'], 404);
            }
        }
        if($user){
            $history->userEmail = $request->email;
            $code = $this->generateCode();
            session()->put('code', $code);
            Mail::send(['text' => 'mail'], ['name' => 'Код для авторизации'], function($message) use ($user, $request){
                $message->to($request->email, $user->FIO)->subject('Код для авторизации');
                $message->from('bot@kommesk.kz', 'Botan');
            });
            $history->response = "Письмо с кодом отправлено вам на почту. (Код:".$code.")";
            $newCode = new BotanCodes();
            $newCode->botan_user_id = $user->id;
            $newCode->code = $code;
            $newCode->save();
            $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
            if(!$telegramUser){
                $history->request = "Авторизация пользователя с Email: ".$request->email;
                $telegramUser = new StaffTelegramUsers();
                $telegramUser->staffEmail = $user->email;
            } else {
                $history->request = "Повторная авторизация пользователя с Email: ".$request->email;
            }
            $telegramUser->telegramId = $request->message;
            $telegramUser->save();
            $history->save();
            return ", ".$user->FIO;
        }
        $history->userEmail = "";
        $history->request = "Авторизация пользователя с Email: ".$request->email;
        $history->response = "Email введен неверно или у вас нет доступа для просмотра информации";
        $history->save();
        return response()->json(['status' => 'Error'], 404);
    }

    public function checkCode(Request $request)
    {
        $history = new BotanDialogHistories();
        $user = BotanStaff::where('email', strtolower($request->email))->first();
        if(!$user) {
            $email = StaffEmail::where("email", strtolower($request->email))->first();
            if ($email) {
                $user = BotanStaff::find($email->userId);
            }
        }
        if ($user) {
            $history->userEmail = $request->email;
            $history->request = "Проверка кода для авторизации пользователя с email. Код(".$request->code.")";
            $code = BotanCodes::where('botan_user_id', $user->id)
                ->where('code', $request->code)
                ->first();
            if(!$code){
                $history->response = "Код введен неверно";
                $history->save();
                return response()->json(['status' => 'Error'], 404);
            }
            $history->response = "Код введен верно";
            $history->save();
            return;
        }
        $history->userEmail = "";
        $history->request = "Проверка кода: ".$request->code;
        $history->response = "Email введен неверно или у вас нет доступа для просмотра информации (".$request->email.")";
        $history->save();
        return response()->json(['status' => 'Error'], 404);
    }
}
