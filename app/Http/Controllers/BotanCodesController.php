<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BotanCodesController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showImage(Request $request)
    {
        return view('showPhoto', ['filepath' => '/photos/'.$request->fileName]);
    }
}
