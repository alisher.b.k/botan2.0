<?php

namespace App\Http\Controllers;

use App\GroupUser;
use App\MessageGroup;
use App\StaffTelegramUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    public function index(){
        $groups = MessageGroup::all();
        return view('groupList', compact('groups'));
    }

    public function delete(MessageGroup $group){
        $group->delete();
        return redirect(route('group'));
    }

    public function addView(){
        $users = StaffTelegramUsers::all();
        return view('groupAdd', compact('users'));
    }

    public function add(Request $request){
        DB::beginTransaction();
        try{
            $group = new MessageGroup();
            $group->name = $request->name;
            $group->save();
            foreach ($request->users as $userId){
                $user = new GroupUser();
                $user->telegramId = $userId;
                $user->group_id = $group->id;
                $user->save();
            }
            DB::commit();
            return redirect(route('group'));
        }catch (\Exception $exception){
            DB::rollBack();
            return $exception->getMessage();
        }
    }

    public function editView(MessageGroup $group){
        $usersList = $group->getUsersArray();
        $users = StaffTelegramUsers::all();
        return view('groupEdit', compact(['group', 'users', 'usersList']));
    }

    public function edit(MessageGroup $group, Request $request){
        DB::beginTransaction();
        try{
            $group->name = $request->name;
            $group->save();
            $userList = $group->getUsersArray();
            foreach ($request->users as $userId){
                if(!in_array($userId, $userList)){
                    $user = new GroupUser();
                    $user->telegramId = $userId;
                    $user->group_id = $group->id;
                    $user->save();
                }
            }

            foreach ($userList as $userId){
                if(!in_array($userId,$request->users)){
                    GroupUser::where('telegramId', $userId)->where('group_id', $group->id)->first()->delete();
                }
            }
            DB::commit();
            return redirect(route('group'));
        }catch (\Exception $exception){
            DB::rollBack();
            return $exception->getMessage();
        }
    }
}
