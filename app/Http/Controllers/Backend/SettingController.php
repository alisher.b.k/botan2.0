<?php

namespace App\Http\Controllers\Backend;

use App\CentMessage;
use App\CentPartner;
use App\Console\Commands\AddStaffCommand;
use App\Console\Commands\getCentProducts;
use App\Reconciliation;
use App\StaffTelegramUsers;
use App\Telegram\DefaultKeyboard;
use App\Telegram\MenuCommand;
use Artisan;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Quiz;
use App\QuizQuestion;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\FileUpload\InputFile;
use App\Companies;
use App\BotanStaff;
use App\Poll;
use App\TelegramRequestLog;
use App\BotanDialogHistories;
use App\QuizResults;
use App\News;
use App\NewsReactions;
use Illuminate\Support\Facades\File;
use App\Jobs\SendQuizJob;
use App\NewsImage;
use App\TypeMessages;
use function Psy\debug;
use App\Jobs\SendNotificationJob;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sendMessagesFormView()
    {
        $users = StaffTelegramUsers::all();
        $companies = Companies::all();
        return view('sendMessagesForm', ['companies' => $companies, 'users' => $users]);
    }

    public function sendQuizMessagesFormView()
    {
        $users = StaffTelegramUsers::all();
        $companies = Companies::all();
        return view('sendQuizMessagesForm', ['companies' => $companies, 'users' => $users]);
    }

    public function sendCentMessagesFormView()
    {
        $users = StaffTelegramUsers::all();
        $companies = Companies::all();
        $partners = CentPartner::all();
        $partnersArray = [];
        foreach ($partners as $partner) {
            $partnersArray[(string)$partner->partner_id] = $partner->message_text;
        }
        return view('sendCentMessagesForm', [
            'companies' => $companies,
            'users' => $users,
            'partners' => $partners,
            'partnersArray' => $partnersArray
        ]);
    }

    public function index()
    {
        return view('backend.setting', Setting::getSettings());
    }

    public function store(Request $request)
    {
        Setting::where('key', '!=', null)->delete();
        foreach ($request->except('_token') as $key => $value) {
            $setting = new Setting;
            $setting->key = $key;
            $setting->value = $request->$key;
            $setting->save();
        }
        return redirect()->route('admin.setting.index');
    }

    public function setwebhook(Request $request)
    {
        $result = $this->sendTelegramData('setwebhook', [
            'query' => ['url' => $request->url . '/' . \Telegram::getAccessToken()]
        ]);
        return redirect()->route('admin.setting.index')->with('status', $result);
    }

    public function getwebhookinfo(Request $request)
    {
        $result = $this->sendTelegramData('getWebhookInfo');
        return redirect()->route('admin.setting.index')->with('status', $result);
    }

    public function sendTelegramData($route = '', $params = [], $method = 'POST')
    {
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.telegram.org/bot' . \Telegram::getAccessToken() . '/']);
        $result = $client->request($method, $route, $params);
        return (string)$result->getBody();
    }

    public function uploadPrizes(Request $request)
    {
        if ($request->hasFile('photoUpload')) {
            $dir = public_path();
            $files1 = scandir($dir);
            foreach ($files1 as $old) {
                if (preg_match("/prizes/i", $old)) {
                    File::delete(public_path() . '/' . $old);
                }
            }

            $file = $request->file('photoUpload');
            $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName()) - strlen($file->getClientOriginalExtension()) - 1);
            $date = date('m/d/Y h:i:s a', time());
            $fileName = 'prizes' . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/', $fileName);
        }
        return redirect()->route('prizes');
    }

    public function sendNotifications(Request $request)
    {
        // $this->checkExistOrMake('pictures');

        $fileNames = [];
        $telegramUsers = [];
        $reply_markup = [];
        if ($request->messageType === 'poll') {
            $poll = new Poll();
            $poll->question = $request->message;
            $poll->option1 = 'Да';
            $poll->option2 = 'Нет';
            $poll->save();

            $logArr = [
                "command" => 'poll' . $poll->id,
            ];
        } else if ($request->messageType === 'message-reactions') {
            $content = preg_replace("/\r|\n/", "", $request->message);

            $news = new News();
            $news->content = $content;
            $news->link = $request->link;
            $news->title = $request->title;
            $news->save();
        }else if ($request->messageType === 'reactions-without-links') {
            $content = preg_replace("/\r|\n/", "", $request->message);

            $news = new News();
            $news->content = $content;
            $news->title = $request->title;
            $news->save();
        }
        if ($request->hasFile('photoUpload')) {
            $files = $request->file('photoUpload');
            foreach($files as $file){
                $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName()) - strlen($file->getClientOriginalExtension()) - 1);
                $date = date('m/d/Y h:i:s a', time());
                $fileName =
                // md5($date . $originFileName) . '.'
                $originFileName. '.'
                . $file->getClientOriginalExtension();
                $file->move(public_path() . '/pictures', $fileName);
                if ($request->messageType === 'message-reactions') {
                    $newsFile = new NewsImage();
                    $newsFile->news_id = $news->id;
                    $newsFile->path = '/pictures/' . $fileName;
                    $newsFile->save();
                }else if ($request->messageType === 'reactions-without-links') {
                    $newsFile = new NewsImage();
                    $newsFile->news_id = $news->id;
                    $newsFile->path = '/pictures/' . $fileName;
                    $newsFile->save();
                }
                array_push($fileNames, $fileName);
            }
        }

//------------------------------------------------------------start 15.08.2022----------------------------------------------------------------
        if($request->company === "user"){
            $telegramUsers = StaffTelegramUsers::whereIn('telegramId', $request->users)->get();
        } elseif($request->company === "companies"){
            $companyList = $request->companies;
            $users = StaffTelegramUsers::all();
            foreach ($users as $user) {
                $staffUser = BotanStaff::whereIn('WORKPLACE', $companyList)
                    ->where('email', $user->staffEmail)
                    ->orWhere(function ($query) use ($user, $companyList){
                        $query->where('ISN', $user->staffEmail)
                            ->whereIn('WORKPLACE', $companyList);
                    })
                    ->first();
                if ($staffUser !== null) {
                    $telegramUsers[] = $user;
                }
            }
        } else {
            $telegramUsers = StaffTelegramUsers::all();
        }

//------------------------------------------------------------end 15.08.2022---------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------
        // if ($request->check === "yes") {
        //     if ($request->company === "user") {
        //         $telegramUsers = StaffTelegramUsers::whereIn('telegramId', $request->users)->get();
        //         foreach($telegramUsers as $user){
        //             if ($user->getStaff === null)
        //                 continue;
        //             $telegramUsers[] = $user;
        //         }
        //     } elseif ($request->company === "companies") {
        //         $companyList = $request->companies;
        //         $users = StaffTelegramUsers::all();
        //         foreach ($users as $user) {
        //             $staffUser = BotanStaff::whereIn('WORKPLACE', $companyList)
        //                 ->where('email', $user->staffEmail)
        //                 ->orWhere(function ($query) use ($user, $companyList) {
        //                     $query->where('ISN', $user->staffEmail)
        //                         ->whereIn('WORKPLACE', $companyList);
        //                 })
        //                 ->first();
        //             if ($staffUser !== null) {
        //                 $telegramUsers[] = $user;
        //             }
        //         }
        //     } else {
        //         $telegramUsers = StaffTelegramUsers::all();
        //         foreach($telegramUsers as $user){
        //             if ($user->getStaff === null)
        //                 continue;
        //             $telegramUsers[] = $user;
        //         }
        //     }
        // } else {
        //     if ($request->company === "user") {
        //         $users = StaffTelegramUsers::whereIn('telegramId', $request->users)->get();
        //         foreach ($users as $user) {
        //             if ($user->getStaff === null)
        //                 continue;
        //             $types = $user->getStaff->typeMessages;
        //             $result = 0;
        //             if ($types !== null) {
        //                 foreach ($types as $type) {
        //                     if ($type->key !== $request->messageType) {
        //                         $result += 0;
        //                     } else {
        //                         $result += 1;
        //                     }
        //                 }
        //                 if ($result !== 1) {
        //                     array_push($telegramUsers, $user);
        //                 }
        //             } else {
        //                 array_push($telegramUsers, $user);
        //             }
        //         }
        //     } elseif ($request->company === "companies") {
        //         $companyList = $request->companies;
        //         $AllUsers = StaffTelegramUsers::all();
        //         foreach ($AllUsers as $user) {
        //             $staffUser = BotanStaff::whereIn('WORKPLACE', $companyList)
        //                 ->where('email', $user->staffEmail)
        //                 ->orWhere(function ($query) use ($user, $companyList) {
        //                     $query->where('ISN', $user->staffEmail)
        //                         ->whereIn('WORKPLACE', $companyList);
        //                 })
        //                 ->first();
        //             if ($staffUser !== null) {
        //                 $types = $staffUser->typeMessages;
        //                 $result = 0;
        //                 if ($types !== null) {
        //                     foreach ($types as $type) {
        //                         if ($type->key !== $request->messageType) {
        //                             $result += 0;
        //                         } else {
        //                             $result += 1;
        //                         }
        //                     }
        //                     if ($result !== 1) {
        //                         array_push($telegramUsers, $user);
        //                     }
        //                 } else {
        //                     array_push($telegramUsers, $user);
        //                 }
        //             }
        //         }
        //     } else {
        //         $users = StaffTelegramUsers::all();
        //         foreach ($users as $user) {
        //                 if ($user->getStaff === null)
        //                     continue;
        //                 $types = $user->getStaff->typeMessages;
        //                 $result = 0;
        //                 if ($types !== null) {
        //                     foreach ($types as $type) {
        //                         if ($type->key !== $request->messageType) {
        //                             $result += 0;
        //                         } else {
        //                             $result += 1;
        //                         }
        //                     }
        //                     if ($result !== 1) {
        //                         array_push($telegramUsers, $user);
        //                     }
        //                 } else {
        //                     array_push($telegramUsers, $user);
        //                 }
        //         }
        //     }
        // }
//-----------------------------------------------------------------------------------------------------------------------
        $keyboard = $request->messageType === 'poll'
            ? [['Да', 'Нет']]
            : DefaultKeyboard::getMenuKeyboard('');
        $bookingButton = $request->messageType !== 'poll' && $request->messageType !== 'message-reactions' && $request->messageType !== 'reactions-without-links';


        if ($request->messageType === 'message-reactions') {
            $reply_markup = Keyboard::make([
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
                'inline_keyboard' => [
                    [
                        ['text' => '👍', 'callback_data' => '👍#poll' . $news->id],
                        ['text' => '🤔', 'callback_data' => '🤔#poll' . $news->id],
                        ['text' => '👎', 'callback_data' => '👎#poll' . $news->id],
                    ], [['text' => 'Получить ссылку', 'callback_data' => 'Получить ссылку#poll' . $news->id]]
                ]]);
        }elseif($request->messageType === 'reactions-without-links') {
            $reply_markup = Keyboard::make([
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
                'inline_keyboard' => [
                    [
                        ['text' => '👍', 'callback_data' => '👍#poll' . $news->id],
                        ['text' => '🤔', 'callback_data' => '🤔#poll' . $news->id],
                        ['text' => '👎', 'callback_data' => '👎#poll' . $news->id],
                    ]
                ]]);
        } else {
            $reply_markup = Keyboard::make(['keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
        }

        foreach ($telegramUsers as $telegramUser) {
            if ($telegramUser->updatesSubscription === 1) {
                try {
                    SendNotificationJob::dispatch($fileNames, $request->message, $request->messageType, $telegramUser, $bookingButton);
                } catch (\Exception $e) {
                    Log::debug('Выброшено исключение: ' . $e->getMessage() . " " . $telegramUser->telegramId);
                }
            }
        }
        return redirect()->route('sendMessagesForm');
    }

    // public function checkExistOrMake(String $path)
    // {
    //     if(!file_exists(($path))) {
    //         File::makeDirectory(public_path($path), 0777, true);
    //     }
    // }

    public function sendCentNotifications(Request $request)
    {
        $partner = CentPartner::where('partner_id', $request->partner)->first();
        if ($partner === null) {
            abort(404, 'Product not found');
        }
        if ($request->company === "user") {
            $telegramUsers = StaffTelegramUsers::whereIn('telegramId', $request->users)->get();
        } elseif ($request->company === "companies") {
            $users = StaffTelegramUsers::all();
            $companyList = $request->companies;
            foreach ($users as $user) {
                $staffUser = BotanStaff::whereIn('WORKPLACE', $companyList)
                    ->where('email', $user->staffEmail)
                    ->orWhere(function ($query) use ($user, $companyList) {
                        $query->where('ISN', $user->staffEmail)
                            ->whereIn('WORKPLACE', $companyList);
                    })
                    ->first();
                if ($staffUser) {
                    $telegramUsers[] = $user;
                }
            }
        } else {
            $telegramUsers = StaffTelegramUsers::all();
        }
        $reply_markup = Keyboard::make([
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'inline_keyboard' => [
                [[
                    'text' => 'Консультация',
                    'callback_data' => "cent_{$partner->partner_id}"
                ]]
            ]
        ]);
        foreach ($telegramUsers as $telegramUser) {
            if ($telegramUser->updatesSubscription === 1) {
                try {
                    $fileName = explode('/', $partner->image_url);
                    $fileName = end($fileName);
                    Telegram::sendPhoto([
                        'chat_id' => $telegramUser->telegramId,
                        'photo' => new InputFile($partner->image_url, $fileName),
                    ]);
                    $resp = Telegram::sendMessage([
                        'chat_id' => $telegramUser->telegramId,
                        'text' => $request->message,
                        'reply_markup' => $reply_markup
                    ]);
                    $historyArr = [
                        "chatId" => $telegramUser->telegramId,
                        "response" => "Рассылка CENT.KZ, Партнер {$partner->id}",
                        "userEmail" => $telegramUser->staffEmail,
                        "telegramUsername" => $resp->chat->username,
                        "telegramFirstName" => $resp->chat->first_name,
                        "telegramLastName" => $resp->chat->last_name,
                    ];
                    BotanDialogHistories::create($historyArr);
                } catch (\Exception $exception) {
                    Log::debug('CENT Рассылка : ' . $exception->getMessage());
                }

            }
        }
        return redirect()->route('sendCentMessagesForm');
    }

    public function sendQuiz(Request $request)
    {
        $newQuiz = new Quiz();
        $newQuiz->name = $request->quizName;
        $newQuiz->save();

        $questionsNum = $request->questionsNum;
        for ($i = 0; $i < $questionsNum; $i++) {
            $fileName = '';
            $telegramUsers = [];
            if ($request->hasFile('photoUpload' . $i)) {
                $file = $request->file('photoUpload' . $i);
                $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName()) - strlen($file->getClientOriginalExtension()) - 1);
                $date = date('m/d/Y h:i:s a', time());
                $fileName = md5($date . $originFileName) . '.' . $file->getClientOriginalExtension();
                $file->move(public_path() . '/quiz', $fileName);
            }

            if ($request->company === "user") {
                $telegramUsers = StaffTelegramUsers::whereIn('telegramId', $request->users)->get();
            } elseif ($request->company === 'companies') {
                $companyList = $request->companies;
                $users = StaffTelegramUsers::all();
                foreach ($users as $user) {
                    $staffUser = BotanStaff::whereIn('WORKPLACE', $companyList)
                        ->where('email', $user->staffEmail)
                        ->orWhere(function ($query) use ($user, $companyList) {
                            $query->where('ISN', $user->staffEmail)
                                ->whereIn('WORKPLACE', $companyList);
                        })
                        ->first();
                    if ($staffUser) {
                        $telegramUsers[] = $user;
                    }
                }
            } else {
                $telegramUsers = StaffTelegramUsers::all();
            }

            $options = array(
                $request['option1' . $i],
                $request['option2' . $i],
                $request['option3' . $i],
                $request['option4' . $i],
            );

            $quiz = new QuizQuestion();
            $quiz->quizId = $newQuiz->id;
            $quiz->text = $request['message' . $i];
            if ($fileName) {
                $quiz->file = $fileName;
            }
            $quiz->option1 = $request['option1' . $i];
            $quiz->option2 = $request['option2' . $i];
            $quiz->option3 = $request['option3' . $i];
            $quiz->option4 = $request['option4' . $i];
            $quiz->correctOption = $options[$request['correctAnswer' . $i]];
            $quiz->save();
            $token = getenv('TELEGRAM_BOT_TOKEN');
            foreach ($telegramUsers as $telegramUser) {
                if ($telegramUser->updatesSubscription === 1) {
                    try {
                        $data = [
                            'chat_id' => $telegramUser->telegramId,
                            'question' => $request['message' . $i],
                            'options' => json_encode($options),
                            'is_anonymous' => 'False',
//                            'type' => 'quiz',
//                            'correct_option_id' => $request['correctAnswer'.$i],
                        ];
                        if ($fileName) {
                            $resp = Telegram::sendPhoto([
                                'chat_id' => $telegramUser->telegramId,
                                'photo' => new InputFile(public_path() . '/quiz/' . $fileName, $fileName),
                            ]);
                        }


                        $curl_handle = curl_init();
                        curl_setopt($curl_handle, CURLOPT_URL, "https://api.telegram.org/bot$token/sendPoll?" . http_build_query($data));
                        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($curl_handle);
                        curl_close($curl_handle);

                        $respObject = json_decode($response, true);

                        $quizResult = new QuizResults();
                        $quizResult->chatId = $telegramUser->telegramId;
                        $quizResult->quizId = $quiz->id;
                        $quizResult->pollId = $respObject["result"]["poll"]["id"];
                        $quizResult->save();

                        $historyArr = [
                            "chatId" => $telegramUser->telegramId,
                            "response" => $request['message' . $i],
                            "userEmail" => $telegramUser->staffEmail,
                        ];
                        BotanDialogHistories::create($historyArr);
                    } catch (\Exception $e) {
                        Log::debug('Выброшено исключение quiz: ' . $e->getMessage() . " " . $telegramUser->telegramId);
                    }
                }
            }
        }
        return redirect()->route('sendMessagesForm');
    }

    public static function saveQuizAnswer($update)
    {
        $quizResult = QuizResults::where('pollId', $update["poll_answer"]["poll_id"])->first();
        $quizResult->answer = intval($update["poll_answer"]["option_ids"][0]) + 1;
        $quizResult->save();
    }

    public static function saveCentMessage($update)
    {
        $partner_id = explode("_", $update["callback_query"]["data"]);
        $partner_id = $partner_id[1];
        $user = StaffTelegramUsers::where('telegramId', $update["callback_query"]["from"]["id"])->first();
        $userData = BotanStaff::where('email', $user->staffEmail)
            ->orWhere('isn', $user->staffEmail)
            ->first();
        $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getMenuKeyboard($userData->WORKPLACE ?? ""),
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $messageData = CentMessage::where('product_id', $partner_id)
            ->where('telegram_id', $update["callback_query"]["from"]["id"])
            ->first();
        if ($userData !== null && $messageData === null) {
            // TODO send message to partner
            $fullname = explode(' ', $userData->FIO);
            $data = [
                'email' => $userData->email,
                'phone' => $userData->mobPhone,
                'first_name' => $fullname[1],
                'last_name' => $fullname[0],
                'product_id' => $partner_id
            ];
            $centMessage = new CentMessage();
            $centMessage->product_id = $partner_id;
            $centMessage->telegram_id = $update["callback_query"]["from"]["id"];
            $centMessage->save();
            $response = Curl::to('https://cent.kz/utls/sendorderfrombotan')
                ->withData($data)
                ->asJson()
                ->post();
            $id = (string)$response->id;
            try {
                $resp = Telegram::sendMessage([
                    'chat_id' => $update["callback_query"]["from"]["id"],
                    'text' => "Спасибо! \nВаша заявка успешно оформлена! Мы свяжемся с Вами в ближайшее время. \nНомер вашей заявки: $id",
                    'parse_mode' => 'html',
                    'reply_markup' => $reply_markup
                ]);
            } catch (\Exception $exception) {
                Log::debug("CENT Message : Error {$exception->getMessage()}");
            }
        } else {
            $resp = Telegram::sendMessage([
                'chat_id' => $update["callback_query"]["from"]["id"],
                'text' => 'Произошла ошибка напишите в раздел сообщить о проблеме',
                'reply_markup' => $reply_markup
            ]);
            Log::debug("CENT Message : User not found {$update['callback_query']['from']['id']}");
        }
    }

    public static function saveReaction($update)
    {
        $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getMenuKeyboard(StaffTelegramUsers::getWorkPlaceByChatId($update["callback_query"]["from"]["id"])),
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $content = preg_replace("/\r|\n/", "", $update["callback_query"]["message"]["text"]);
        $pieces = explode("#poll", $update["callback_query"]["data"]);
        $newsList = News::where('id', $pieces[1])->get();
        foreach ($newsList as $news) {
            if ($pieces[0] !== 'Получить ссылку') {
                $existingReaction = NewsReactions::where('telegramId', $update["callback_query"]["from"]["id"])
                    ->where('newsId', $news->id)
                    ->where('reaction', '!=', 'Получить ссылку')
                    ->first();
            } else {
                $existingReaction = NewsReactions::where('telegramId', $update["callback_query"]["from"]["id"])
                    ->where('newsId', $news->id)
                    ->where('reaction', $pieces[0])
                    ->first();
            }

            if ($pieces[0] === 'Получить ссылку' && $news->link) {
                $resp = Telegram::sendMessage([
                    'chat_id' => $update["callback_query"]["from"]["id"],
                    'text' => $news->link,
                    'reply_markup' => $reply_markup,
                    'disable_web_page_preview' => 'true',
                ]);
            }

            if (!$existingReaction) {
                $news_reactions = new NewsReactions();
                $news_reactions->reaction = $pieces[0];
                $news_reactions->telegramId = $update["callback_query"]["from"]["id"];
                $news_reactions->newsId = $news->id;
                $news_reactions->save();

                if ($pieces[0] !== 'Получить ссылку') {
                    $resp = Telegram::sendMessage([
                        'chat_id' => $update["callback_query"]["from"]["id"],
                        'text' => 'Спасибо за вашу оценку',
                        'reply_markup' => $reply_markup
                    ]);
                }
            }
        }
    }

    public function changeCompanyVVP($company)
    {
        $staffVVP = BotanStaff::where('ISN', 19000)->first();
        switch (strtolower($company)) {
            case 'kommesk' :
                $staffVVP->WORKPLACE = "\"АО СК Коммеск-Омир\"";
                break;
            case 'insurance' :
                $staffVVP->WORKPLACE = "АО \"СК \"Сентрас Иншуранс\"";
                break;
        }
        $staffVVP->save();
        return redirect('/');
    }

    public function updateCentProducts()
    {
        $data = new getCentProducts();
        $data->handle();
        return redirect()->route('sendCentMessagesForm');
    }

    public function sendTypeOfMessage(Request $request)
    {
        $message = TypeMessages::query()->where('key', $request->type)->first();
        $countUsers = StaffTelegramUsers::with('getStaff')->count();
        if ($message === null)
            return response()->json(['success' => $countUsers]);
        $types = $message->botanStaff;
        $result = $countUsers - count($types);
        return response()->json(['success' => $result]);
    }

    public function logs()
    {
        $files = scandir(storage_path('/logs'));
        return view('logreader.index', compact('files'));
    }

    public function read($path)
    {
        $result = explode("\n", file_get_contents(storage_path("/logs/{$path}")));
        return view('logreader.log', compact('result'));
    }

    public function addUsersInfo()
    {
        try {
            Artisan::call('botan:addUsersInfo');
            return view('logreader.index');
        } catch (\Exception $e) {
            return view('logreader.index')->withErrors(['error_message' => $e->getMessage()]);
        }
    }
    public static function sendToKiasReconcilationResult($update, $messageId)
    {
        $reconciliation = Reconciliation::where('message_id', $messageId)->first();
        if ($update["callback_query"]["data"] == "more_") {
            Telegram::editMessageText([
                'text' => Reconciliation::getMessage($reconciliation, true),
                'chat_id' => $update["callback_query"]["from"]["id"],
                'message_id' => $messageId,
                'reply_markup' => Keyboard::make([
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true,
                    'inline_keyboard' => [
                        [
                            ['text' => 'Согласовать', 'callback_data' => 'r#yes'],
                            ['text' => 'Отказать', 'callback_data' => 'r#no'],
                        ]
                    ]
                ])
            ]);
        } else {
            $answer = $update["callback_query"]["data"] == "r#yes" ? 1 : 0;
            $company = Reconciliation::COMPANIES[$reconciliation->company];
            try {

                $connectResult = Curl::to('https://connect.cic.kz/centras/aida/set_coordination')
                    ->withData([
                        "token" => "wesvk345sQWedva55sfsd*g",
                        "company" => $company,
                        "emplISN" => $reconciliation->isn,
                        "coordinationISN" => $reconciliation->reconciliation_isn,
                        "answer" => $answer,
                        "partner_id" => 9
                    ])
                    ->asJson()
                    ->post();
                try {
                    Log::channel('telegram')->debug("connect rec result");
                    Log::channel('telegram')->debug($connectResult);
                } catch (\Exception $e) {
                    Log::debug("somethin went wrong when sending reconciliation to kias");
                    Log::debug($e->getMessage());
                }
                if(isset($connectResult->result)&&$connectResult->result == 1){
                    $message = "$reconciliation->contract_number' ";
                    $message = $answer == 1 ? $message.'Согласовано' : $message.'Отказано';
                    Telegram::editMessageText([
                        'message_id' => $messageId,
                        'chat_id' => $update["callback_query"]["from"]["id"],
                        'text' => $message,
                    ]);
                }else{
                    $result = json_decode(json_encode($connectResult), true);
                    Telegram::sendMessage([
                        'chat_id' => $update["callback_query"]["from"]["id"],
                        'text' => "Произошла ошибка.\n".$result['result'],
                        'reply_markup' => Keyboard::make([
                            'keyboard' => [['На главную']],
                            'resize_keyboard' => true,
                            'one_time_keyboard' => true
                        ])
                    ]);
                    Log::debug($result);
//                Telegram::sendMessage([
//                    'chat_id' => $update["callback_query"]["from"]["id"],
//                    'text' => $connectResult->result,
//                    'reply_markup' => Keyboard::make([
//                        'keyboard' => [['На главную']],
//                        'resize_keyboard' => true,
//                        'one_time_keyboard' => true
//                    ])
//                ]);
                }
            } catch (\Exception $e) {
                Log::debug("setCoordination error");
                Log::debug($e->getMessage());
            }
        }
    }
}
