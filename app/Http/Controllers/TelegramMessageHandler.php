<?php

namespace App\Http\Controllers;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\QuestionCategories;
use App\QuestionsAnswers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\Telegram\AboutUsCommand;
use App\Telegram\CalculatorCommand;
use App\Telegram\CentKzCommand;
use App\Telegram\CheckUser;
use App\Telegram\DefaultKeyboard;
use App\Telegram\FacebookCommand;
use App\Telegram\FeedbackCommand;
use App\Telegram\AuthCommand;
use App\Telegram\HelpDeskCommand;
use App\Telegram\HRCommand;
use App\Telegram\LimitsCommand;
use App\Telegram\MenuCommand;
use App\Telegram\MethodologyCommand;
use App\Telegram\PasswordResetCommand;
use App\Telegram\ProductsCommand;
use App\Telegram\QuestionsCategoriesCommand;
use App\Telegram\QuestionsCommand;
use App\Telegram\SearchQuestionsCommand;
use App\Telegram\UpdatesSubscription;
use App\Telegram\OfficeCommand;
use App\Telegram\ZhanabayCommand;
use App\Telegram\TechStaffCommand;
use App\Telegram\LibraryCommand;
use App\Telegram\PartiesCommand;
use App\Telegram\HoldingCommand;
use App\Telegram\PollCommand;
use App\Telegram\SenateCommand;
use App\Telegram\VacationCommand;
use App\Telegram\ReservationCommand;
use App\Telegram\ProblemCommand;
use App\Telegram\BossCommand;
use App\Telegram\BalanceCommand;
use App\Telegram\BookingCommand;
use App\TelegramRequestLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Commands\Command;
use App\Telegram\DoctorCommand;
use App\Telegram\RecruitingCommand;

class TelegramMessageHandler extends Controller
{
    public static function index($messageText, $chatId, $username, $telegramRequestLog, $firstname, $lastname, $result, $message, $callback)
    {
        // 381528038 - тестовый чат
        // массив для избежания отрпавления сообщения
        // по умолчанию в групповые чаты
        $chatIdArr = ["-381528038", "-333170792", '-1001151800226'];



          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "request" => $messageText,
          ];

          // запись запроса в историю диалогов
          $historyArr["userEmail"] = $result['user']
            ? $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN
            : '';
          BotanDialogHistories::create($historyArr);

          Log::debug('check message');
          Log::debug($messageText);
          Log::debug($callback);
          $log = TelegramRequestLog::getLog($chatId);
          if (isset($callback) && $messageText === 'Нажмите нужную кнопку'){
              TelegramRequestLog::deleteRows($chatId);
              $messageText = $callback;
          }
            Log::debug($messageText);
            if($messageText === 'На главную' || $messageText === 'на главную' || $messageText === '🔚 На главную') {
                MenuCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
                return;
            }elseif($messageText === 'Еще'){
                MenuCommand::expandMenu($chatId, $username, $firstname, $lastname, $result);
            }elseif($messageText === '📑 Внутренний рекрутинг'){
                RecruitingCommand::executeCommand($chatId);
                return;
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'recruiting' && isset($callback)){
                RecruitingCommand::chooseVacancy($chatId, $message, $callback);
                return;
            }elseif($messageText == '💻 Cent.kz'){
                (new CentKzCommand)->handleMessage($messageText, $chatId, $callback);
            }elseif (CentKzCommand::isCentMessage($messageText, $chatId, $callback)){
                (new CentKzCommand())->handleMessage($messageText, $chatId, $callback);
            }elseif (PasswordResetCommand::isPwResetMessage($messageText, $chatId, $callback)){
                (new PasswordResetCommand())->handleMessage($messageText, $chatId, $callback);
            }
            elseif($messageText === '👩‍⚕️ Запись на прием к врачу' || $messageText === 'Запись на прием к врачу' || $messageText === 'doctor' || $messageText === 'Doctor'){
                DoctorCommand::executeCommand($chatId,$username,$firstname,$lastname);
            }elseif(isset($telegramRequestLog->command) && $messageText === '🔚 На главную'){
                MenuCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
                return;
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:answerYesOrNo' && $messageText === '✅ Да'){
                Doctorcommand::answerYes($chatId,$username,$firstname,$lastname,$messageText);
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:answerYesOrNo' && $messageText === '❌ Нет'){
                Doctorcommand::answerNo($chatId,$username,$firstname,$lastname,$messageText);
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:numberOfcard' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                if(strlen($messageText) >= 8){
                    DoctorCommand::numberOfcard($chatId,$username,$firstname,$lastname,$messageText);
                }else{
                    DoctorCommand::answerYesAgain($chatId,$username,$firstname,$lastname,$messageText);
                }
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:PhoneNumber' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                if(strlen($messageText) === 12){
                    Doctorcommand::chooseClinic($chatId,$username,$firstname,$lastname,$messageText);
                }else{
                    Doctorcommand::phoneNumberOfInsured($chatId,$username,$firstname,$lastname,$messageText);
                }
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:PhoneNumberOfUninsured' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                if(strlen($messageText) === 12){
                    Doctorcommand::chooseClinic($chatId,$username,$firstname,$lastname,$messageText);
                }else{
                    Doctorcommand::phoneNumberOfUninsured($chatId,$username,$firstname,$lastname,$messageText);
                }
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:chooseClinic' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                DoctorCommand::chooseDoctor($chatId,$username,$firstname,$lastname,$messageText);
            }elseif(isset($callback) && isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:chooseDoctor' && $callback){
                DoctorCommand::reason($chatId,$username,$firstname,$lastname,$messageText,$callback);
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:writeReason' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                DoctorCommand::finalMessage($chatId,$username,$firstname,$lastname,$messageText);
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:finalMessage' && $messageText === 'Отправить'){
                DoctorCommand::sendToCoordinators($chatId);
            }elseif(isset($telegramRequestLog->command) && $telegramRequestLog->command === 'doc:finalMessage' && $messageText === 'Отменить'){
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                MenuCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
                //DoctorCommand::deleteAllData($chatId);
            }elseif($messageText === '👩‍⚕️ Запись на прием к врачу' || $messageText === 'Запись на прием к врачу' || $messageText === 'doctor' || $messageText === 'Doctor'){
                DoctorCommand::executeCommand($chatId,$username,$firstname,$lastname);
            }elseif ($messageText === '🤑 cенткоины'){
                BalanceCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            }elseif ($messageText === '/balance' || $messageText === '💰 Мой баланс'){
                BalanceCommand::balance($chatId, $username, $firstname, $lastname, $result);
            }elseif ($messageText === '/prizes' || $messageText === '🎁 Призы'){
                BalanceCommand::prizes($chatId, $username, $firstname, $lastname, $result);
            }
            // elseif ($messageText === 'О мероприятиях') {
            //     PartiesCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // }
            elseif ($messageText === '🏦 О холдинге' || $messageText === 'О холдинге') {
                HoldingCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === 'Заявить о проблеме' || $messageText === '💡 Заявить о проблеме') {
                ProblemCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === 'История моих обращений' || $messageText == '/history' || $messageText == "\/history") {
                ProblemCommand::problemHistory($chatId, $messageText, $username, $firstname, $lastname, $result);
            } elseif ($messageText === '🏦 О нас') {
                AboutUsCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === '👨‍💼 Найду коллегу' || $messageText === 'Найду коллегу') {
                FacebookCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === 'Мой отпуск' || $messageText === '🏝 Мой отпуск') {
                VacationCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === '⌚️ Бронирование') {
                BookingCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === '✉️ Улучшить Ботан') {
                FeedbackCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === 'Методология') {
                MethodologyCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === 'Вопросы') {
                QuestionsCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === 'Категории вопросов') {
                QuestionsCategoriesCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === 'Поиск вопросов') {
                SearchQuestionsCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === 'Шишки'|| $messageText === '😎 Шишки') {
                BossCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            } elseif ($messageText === '/work' || $messageText === '/system' || $messageText === '/error' || $messageText === '/another') {
                ProblemCommand::handleButton($chatId, $messageText, $username, $firstname, $lastname, $result, true);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'problem') {
                if ($messageText == 'Нет' || $messageText == 'нет') {
                    ProblemCommand::sendThanks($chatId, $messageText, $username, $firstname, $lastname, $result, true);
                } else {
                    ProblemCommand::handleQuestion($chatId, $messageText, $username, $firstname, $lastname, $result, $message, true);
                }
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'problem_category') {
                ProblemCommand::handleButton($chatId, $messageText, $username, $firstname, $lastname, $result, $message, true);
            } elseif($telegramRequestLog && $telegramRequestLog->command === 'place_for_booking'){
                BookingCommand::chooseCompany($chatId, $messageText, $username, $firstname, $lastname, $result);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking') {
                BookingCommand::chooseRoom($chatId, $messageText, $username, $firstname, $lastname, $result);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom' && $messageText === 'Забронировать') {
                BookingCommand::showWeek($chatId, $messageText, $username, $firstname, $lastname, $result, 'create');
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom' && $messageText === 'Удалить броню') {
                BookingCommand::showWeek($chatId, $messageText, $username, $firstname, $lastname, $result, 'delete');
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom_showWeek') {
                BookingCommand::showTime($chatId, $messageText, $username, $firstname, $lastname, $result, $message, true);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom_showWeekDelete') {
                BookingCommand::showTimeForDelete($chatId, $messageText, $username, $firstname, $lastname, $result);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom_showWeekDelete_showTimeForDelete') {
                BookingCommand::confirmDeleteBooking($chatId, $messageText, $username, $firstname, $lastname, $result);
            }  elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom_showWeekDelete_showTimeForDelete_confirmDeleteBooking') {
                if ($messageText == 'Да') {
                    BookingCommand::deleteBooking($chatId, $messageText, $username, $firstname, $lastname, $result);
                } else {
                    MenuCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
                }
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom_showWeek_showTime') {
                BookingCommand::chooseStartTime($chatId, $messageText, $username, $firstname, $lastname, $result, $message, true);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom_showWeek_showTime_chooseStartTime') {
                BookingCommand::chooseEndTime($chatId, $messageText, $username, $firstname, $lastname, $result, $message, true);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'answerOnProblemSave') {
                if ($messageText == 'Нет' || $messageText == 'нет') {
                    ProblemCommand::closeProblem($chatId, $messageText, $username, $firstname, $lastname, $result, false);
                } else {
                    ProblemCommand::handleButton($chatId, $messageText, $username, $firstname, $lastname, $result, false);
                }
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'problem_append') {
                if ($messageText == 'Нет' || $messageText == 'нет') {
                    ProblemCommand::sendThanks($chatId, $messageText, $username, $firstname, $lastname, $result, false);
                } else {
                    ProblemCommand::handleQuestion($chatId, $messageText, $username, $firstname, $lastname, $result, $message, false);
                }
            } else if (substr($messageText, 0, 4) == '/add') {
                $id = substr($messageText, 4, 5);

                $log = new TelegramRequestLog();
                $log->telegramId = $chatId;
                $log->command = 'answerOnProblemSave';
                $log->data = json_encode(array('problemId' => $id));
                $log->save();

                if ($messageText == 'Нет' || $messageText == 'нет') {
                    ProblemCommand::closeProblem($chatId, $messageText, $username, $firstname, $lastname, $result, false);
                } else {
                    ProblemCommand::handleButton($chatId, $messageText, $username, $firstname, $lastname, $result, false);
                }
            }
            // elseif ($messageText === 'Сенат') {
            //     SenateCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // }

            // elseif ($messageText === 'Продавцам') {
            //     ProductsCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // }
            // elseif ($messageText === 'Бронирование') {
            //     ReservationCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // }
            // elseif ($telegramRequestLog && $telegramRequestLog->command === 'reservation') {
            //     ReservationCommand::handleRoomChoice($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }
            // elseif ($messageText === 'Подписка-отписка от обновлений') {
            //     UpdatesSubscription::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // } elseif ($messageText === 'Калькулятор') {
            //     CalculatorCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // } elseif ($messageText === 'hr') {
            //     HRCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // } elseif ($messageText === 'Офис') {
            //     OfficeCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // } elseif ($messageText === 'Жанабай') {
            //     ZhanabayCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // } elseif ($messageText === 'Тех. персонал') {
            //     TechStaffCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // }
            // elseif ($messageText === 'Библиотека' || $messageText === '📚 Библиотека') {
            //     LibraryCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // }
            // elseif ($messageText === 'Опросы') {
            //     PollCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
            // } elseif (substr($messubstrsageText, 0, 10) === 'ответ' || substr($messageText, 0, 10) === 'Ответ') {
            //     TelegramMessageHandler::getAnswer($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }
            elseif (substr($messageText, 0, 18) === 'категория' || substr($messageText, 0, 18) === 'Категория') {
                TelegramMessageHandler::getSubcategories($chatId, $messageText, $username, $firstname, $lastname, $result);
            }
            elseif (substr($messageText, 0, 23) === 'найти вопрос' || substr($messageText, 0, 23) === 'Найти вопрос') {
                TelegramMessageHandler::searchQuestion($chatId, $messageText, 24, $username, $firstname, $lastname, $result);
            } elseif (substr($messageText, 0, 25) === 'Поиск вопроса' || substr($messageText, 0, 25) === 'поиск вопроса') {
                TelegramMessageHandler::searchQuestion($chatId, $messageText, 26, $username, $firstname, $lastname, $result);
            } elseif($telegramRequestLog && $telegramRequestLog->command === 'limits') {
                LimitsCommand::getLimits($chatId, $messageText);
            } elseif ($messageText === '👩‍⚕️ Лимиты по ДМС') {
                LimitsCommand::executeCommand($chatId);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'boss_companies') {
                BossCommand::companyBosses($chatId, $messageText, $username, $firstname, $lastname, $result);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'boss_photo') {
                BossCommand::bossPhoto($chatId, $messageText, $username, $firstname, $lastname, $result);
            } elseif ($telegramRequestLog && $telegramRequestLog->command === 'feedback_message') {
                FeedbackCommand::saveFeedback($chatId, $messageText, $username, $firstname, $lastname, $result, $message);
            }
            //  elseif ($telegramRequestLog && $telegramRequestLog->command === 'zhanabay') {
            //     ZhanabayCommand::sendRequest($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'help_desk') {
            //     HelpDeskCommand::sendCommandContent($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator') {
            //     CalculatorCommand::insuranceType($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator1') {
            //     CalculatorCommand::calculateNS($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator2') {
            //     CalculatorCommand::getFlightType($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator2flight_type') {
            //     CalculatorCommand::getTripType($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator2flight_typetrip_type') {
            //     CalculatorCommand::calculateDKS($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator3') {
            //     CalculatorCommand::getPrice($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator3_get_price') {
            //     CalculatorCommand::departureDate($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator3_get_price_departure_date') {
            //     CalculatorCommand::arrivalDate($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator3_get_price_departure_date_arrival_date') {
            //     CalculatorCommand::getDiscountList($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator3_get_price_departure_date_arrival_date_discount') {
            //     CalculatorCommand::getLoadingList($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator3_get_price_departure_date_arrival_date_discount_loading') {
            //     CalculatorCommand::iin($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'calculator3_get_price_departure_date_arrival_date_discount_loading_iin') {
            //     CalculatorCommand::calculateMst($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }
            elseif ($telegramRequestLog && $telegramRequestLog->command === 'search staff') {
                TelegramMessageHandler::searchStaff($chatId, $messageText, $username, $firstname, $lastname, $result);
            } elseif ($telegramRequestLog && $telegramRequestLog->command == 'holding') {
                HoldingCommand::sendCommandContent($chatId, $messageText, $username, $firstname, $lastname, $result);
            }
            // elseif ($telegramRequestLog && $telegramRequestLog->command === 'tech_staff') {
            //     TechStaffCommand::sendRequest($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }elseif ($telegramRequestLog && $telegramRequestLog->command === 'waiting poll') {
            //     PollCommand::showPollAnswers($chatId, $messageText, $username, $firstname, $lastname, $result);
            // }
            elseif ($telegramRequestLog && substr($telegramRequestLog->command, 0, 4) === 'poll') {
                $pollId = substr($telegramRequestLog->command, 4, 5);
                PollCommand::saveResult($chatId, $messageText, $username, $firstname, $lastname, $result, $pollId);
            }
            else {
                if (!in_array($chatId, $chatIdArr)) {
                    $reply_markup = Keyboard::make([
                        'keyboard' => [['На главную']],
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true
                    ]);
                    $userEmail = $result['user'] ? $result['user']->email
                        ? $result['user']->email
                        : $result['user']->ISN
                        : '';
                    $historyArr = [
                      "chatId" => $chatId,
                      "telegramUsername" => $username,
                      "telegramFirstName" => $firstname,
                      "telegramLastName" => $lastname,
                      "userEmail" => $userEmail,
                    ];
                    if (substr($chatId, 0, 1) === '-') {
                        $text = "Привет! Я корпоративный бот группы Компаний Сентрас.";
                    } else {
                        $text = "Я еще не знаю таких команд, напишите в разделе 💡 Заявить о проблеме /problem\nВ следующий раз я постараюсь ответить";
                    }
                    Telegram::sendMessage(['chat_id' => $chatId,
                      'reply_markup' => $reply_markup,
                      'text' => $text
                    ]);
                    $historyArr["response"] = $text;
                    BotanDialogHistories::create($historyArr);
                }
            }

    }

    public static function searchStaff($chatId, $text, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result && $result['user']) {

                $reply_markup = Keyboard::make([
                    'keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
                if (preg_match('/[а-яА-Я\s]{1,}$/u',$text)) {
                    if (strlen($text)<6) {
                        Telegram::sendMessage(['chat_id' => $chatId, 'text' => 'Количество символов должно быть больше 3', 'reply_markup' => $reply_markup]);
                        return;
                    }
                } else {
                    if (substr($text, 0, 2) === "+7") {
                        $text = substr($text, 2, strlen($text));
                    } else if (substr($text, 0, 1) === '8') {
                        $text = substr($text, 1, strlen($text));
                    }

                    if (!preg_match('/^[0-9]{10}$/',$text)) {
                        Telegram::sendMessage(['chat_id' => $chatId, 'text' => 'Поиск доступен только по полному номеру', 'reply_markup' => $reply_markup]);
                        return;
                    }
                }
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "userEmail" => $userEmail,
                ];

                $permutations = array();
                $allPermutations = pc_permute(explode(' ', $text), array(), $permutations);

                $users = array();

                foreach ($allPermutations as $perm) {
                    $perm_users = BotanStaff::where('FIO', 'LIKE', '%' . strtolower($perm) . '%')
                        ->orWhere('mobPhone', 'LIKE', '%' . strtolower($perm) . '%')
                        ->get();
                    foreach ($perm_users as $user) {
                        array_push($users, $user);
                    }
                }


                if (count($users) > 0) {
                    $res = "";
                    $i = 0;
                    foreach ($users as $user) {
                        $res .= "\n ФИО: " . $user->FIO;
                        if ($user->position) {
                            $res .= "\n Должность: " . $user->position;
                        }
                        if ($user->WORKPLACE) {
                            $res .= "\n Место работы: " . $user->WORKPLACE;
                        }
                        if ($user->MAINDEPT) {
                            $res .= "\n Департамент: " . $user->MAINDEPT;
                        }
                        if ($user->SUBDEPT) {
                            $res .= "\n Отдел: " . $user->SUBDEPT;
                        }
                        if ($user->workPhone) {
                            $res .= "\n Рабочий телефон: " . $user->workPhone;
                        }
                        if ($user->intPhone) {
                            $res .= "(вн. " . $user->intPhone . ")";
                        }
                        if ($user->mobPhone) {
                            $res .= "\n Мобильный телефон: " . $user->mobPhone;
                        }
                        if ($user->email) {
                            $res .= "\n Email: " . $user->email;
                        }
                        if ($user->workEx) {
                            $res .= "\n Стаж: " . $user->workEx;
                        }
                        if ($user->bday) {
                            $res .= "\n Дата рождения: " . strval($user->bday);
                        }
                        if ($user->VACATIONBEG) {
                            $res .= "\n Дата начала отпуска: " . strval($user->VACATIONBEG);
                        }
                        if ($user->VACATIONEND) {
                            $res .= "\n Дата окончания отпуска: " . strval($user->VACATIONEND);
                        }
                        if ($user->TRIPBEG) {
                            $res .= "\n TRIPBEG: " . strval($user->TRIPBEG);
                        }
                        if ($user->TRIPEND) {
                            $res .= "\n TRIPEND: " . strval($user->TRIPEND);
                        }
                        if ($user->LEAVEDATE) {
                            $res .= "\n Дата увольнения: " . strval($user->LEAVEDATE) . "\n";
                        }
                        if ($i % 6 === 0 || $i === count($users) - 1) {
                            Telegram::sendMessage(['chat_id' => $chatId, 'text' => $res, 'reply_markup' => $reply_markup]);
                            $historyArr["response"] = $res;
                            BotanDialogHistories::create($historyArr);
                            $res = "";
                        }
                        $i++;
                        $res .= "\n";
                    }
                } else {
                    $text = "Сотрудники с данным именем или телефоном не найдены";
                    $historyArr["response"] = $text;
                    Telegram::sendMessage(['chat_id' => $chatId, 'text' => $text, 'reply_markup' => $reply_markup]);
                    BotanDialogHistories::create($historyArr);
                }
                $log = new TelegramRequestLog();
                $log->telegramId = $chatId;
                $log->command = 'search staff';
                $log->save();
          }
        } catch (\Exception $e) {
            Log::debug('/searchstaff ' . $e->getMessage());
        }
    }

    public static function searchQuestion($chatId, $text, $num, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result["user"]) {
              $userEmail = $result['user']->email
                  ? $result['user']->email
                  : $result['user']->ISN;
              $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "userEmail" => $userEmail,
              ];

              $company = Companies::where("companyName", $result['user']->WORKPLACE)->first();
              $str = "(companyId = 0 OR companyId = " . $company->id . ") AND ( question LIKE '%" . strtolower(substr($text, $num, strlen($text))) . "%' ";
              $str .= ")";
              $questions = QuestionsAnswers::whereRaw($str)->get();
              $res = "";
              $i = 1;
              if ($questions) {
                  foreach ($questions as $question) {
                      $res .= $question->id;
                      $res .= ". ";
                      $res .= $question->question . "\n";
                      if ($i % 10 === 0 || $i === count($questions)) {
                          Telegram::sendMessage(['chat_id' => $chatId, 'text' => $res]);
                          $historyArr["response"] = $res;
                          BotanDialogHistories::create($historyArr);
                          $res = "";
                      }
                      $i++;
                  }
                  $text = "Выберите номер вопроса и отправьте команду 'ответ номер_вопроса'\nНапример: ответ 10";
                  Telegram::sendMessage(['chat_id' => $chatId,
                      'text' => $text]);
                  $historyArr["response"] = $text;
                  BotanDialogHistories::create($historyArr);
              } else {
                  $res = "Вопросы не найдены";
              }
              if ($res) {
                  Telegram::sendMessage(['chat_id' => $chatId, 'text' => $res]);
                  $historyArr["response"] = $res;
                  BotanDialogHistories::create($historyArr);
              }
            }
        } catch (\Exception $e) {
            Log::debug('/searchquestions ' . $e->getMessage());
        }
    }

    public static function getSubcategories($chatId, $text, $username, $firstname, $lastname, $result)
    {
        try {
            $isCategory = true;
            $foundCategories = false;
            $foundQuestions = false;
            if ($result['user']) {
              $res = "";
              $userEmail = $result['user']->email
                  ? $result['user']->email
                  : $result['user']->ISN;
              $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "userEmail" => $userEmail,
              ];
                $category = QuestionCategories::find(substr($text, 19, strlen($text)));
                if ($category) {
                    $subcategories = QuestionCategories::where('parent_id', substr($text, 19, strlen($text)))->get();
                    if (count($subcategories) > 0) {
                        $foundCategories = true;
                        $res = "Категории:\n";
                        foreach ($subcategories as $subcategory) {
                            $res .= $subcategory->id;
                            $res .= ". ";
                            $res .= $subcategory->categoryName . "\n";
                        }
                    } else {
                        $isCategory = false;
                        $category = QuestionCategories::find(substr($text, 18, strlen($text)));
                        if ($category) {
                            $company = Companies::where('companyName', $result['user']->WORKPLACE)->first();
                            $questions = QuestionsAnswers::whereRaw('categoryId = ' . substr($text, 19, strlen($text)) . " AND (companyId = " . $company->id . " OR companyId = 0)")
                                ->get();
                            if (count($questions) > 0) {
                                $foundQuestions = true;
                                $res .= "Вопросы: \n";
                                $i = 0;
                                foreach ($questions as $question) {
                                    $res .= $question->id;
                                    $res .= ". ";
                                    $res .= $question->question . "\n";
                                    $i++;
                                    if ($i === 10) {
                                        Telegram::sendMessage(['chat_id' => $chatId, 'text' => $res]);
                                        $historyArr["response"] = $res;
                                        BotanDialogHistories::create($historyArr);
                                        $res = '';
                                        $i = 0;
                                    }
                                }
                            } else {
                                $res = "В данной категории нет вопросов";
                            }
                        } else {
                            $res = "Данной категории не существует";
                        }
                    }
                } else {
                    $res = "Данной категории не существует";
                }
                if ($res) {
                    Telegram::sendMessage(['chat_id' => $chatId, 'text' => $res]);
                    $historyArr["response"] = $res;
                    BotanDialogHistories::create($historyArr);
                }
                if ($isCategory && $res && $foundCategories) {
                  $text = "Выберите номер категории и отправьте команду 'категория номер_категории'\nНапример: категория 10";
                    Telegram::sendMessage(['chat_id' => $chatId,
                        'text' => $text]);
                    $historyArr["response"] = $text;
                    BotanDialogHistories::create($historyArr);
                } elseif (!$isCategory && $res && $foundQuestions) {
                    $text = "Выберите номер вопроса и отправьте команду 'ответ номер_вопроса'\nНапример: ответ 10";
                    Telegram::sendMessage(['chat_id' => $chatId,
                        'text' => $text]);
                    $historyArr["response"] = $text;
                    BotanDialogHistories::create($historyArr);
                }
            }

        } catch (\Exception $e) {
            Log::debug('/getSubcategories ' . $e->getMessage());
        }
    }

    public static function getAnswer($chatId, $text, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result['user']) {
                  $company = Companies::where('companyName', $result['user']->WORKPLACE)->first();
                  $answer = QuestionsAnswers::where('id', substr($text, 10, strlen($text)))->where('companyId', $company->id)->first();
                  if (!$answer) {
                      $answer = QuestionsAnswers::where('id', substr($text, 10, strlen($text)))->where('companyId', 0)->first();
                  }
                  if ($answer) {
                      $res = $answer->answer;
                  } else {
                      $res = "Вопрос не найден";
                  }
                  $userEmail = $result['user']->email
                      ? $result['user']->email
                      : $result['user']->ISN;
                  $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $res,
                    "userEmail" => $userEmail,
                  ];
                  Telegram::sendMessage(['chat_id' => $chatId, 'text' => $res]);
                  BotanDialogHistories::create($historyArr);

                }

        } catch (\Exception $e) {
            Log::debug('getAnswer ' . $e->getMessage());
        }
    }
}

function pc_permute($items, $perms = array(), &$permutations) {
    if (empty($items)) {
        array_push($permutations, implode(' ', $perms));
    } else {
        for ($i = count($items) -1; $i >= 0; --$i) {
            $newitems = $items;
            $newperms = $perms;
            list($foo) = array_splice($newitems, $i, 1);
            array_unshift($newperms, $foo);
            pc_permute($newitems, $newperms, $permutations);
            if ($i == 0) {
                return $permutations;
            }
        }
    }
}
