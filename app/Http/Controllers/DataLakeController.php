<?php

namespace App\Http\Controllers;

use App\NewsReactions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DataLakeController extends Controller
{
    public function getUsers(){
        $result = DB::connection('mysql')->select('SELECT b.ISN, s.created_at dateReg, b.mobPhone, max(h.created_at) lastActivityDate, b.email, b.FIO
FROM stafftelegramusers s
join botan_staff b on s.staffEmail = b.ISN or s.staffEmail = b.email
join botan_dialog_histories h on h.chatId = s.telegramId
group by b.isn');
        $this->exportCsv($result, 'users');
    }

    public function getUserActivity(Request $request){
        $updated = date('d.m.Y h:i:s', strtotime($request->updated));
        $result = DB::connection('mysql')->select('SELECT s.isn, a.created_at, a.request, s.FIO, s.mobPhone, s.email FROM userActions a
left join botan_staff s on a.userEmail = s.email or a.userEmail = s.isn');
        $this->exportCsv($result, 'user_activity');
    }

    public function getUserReactions(Request $request){
//        $updated = date('d.m.Y h:i:s', strtotime($request->updated));
        $result = DB::connection('mysql')->select("SELECT b.ISN userISN, n.created_at datePublish,
substr(n.content,instr(n.content, '#')+1) tag1,
null tag2,
null tag3,
r.reaction reaction,(
		select
			case when (count(rr.id) > 0)
			then 1 else 0 end
			from
				news_reactions rr
			where rr.telegramId = r.telegramId and
			rr.id <> r.id and
			rr.reaction = 'Получить ссылку'
	) linkRequested, n.id newsId,r.created_at dateReaction, 	b.FIO FIO, 	b.mobPhone phone, b.email email
FROM news_reactions r
-- LEFT JOIN news_reactions rr on r.telegramId = rr.telegramId and r.id <> rr.id and r.newsId = rr.newsId
JOIN news n ON r.newsId = n.id
JOIN stafftelegramusers u ON u.telegramId = r.telegramId
JOIN botan_staff b ON u.staffEmail = b.ISN OR u.staffEmail = b.email
WHERE r.reaction <> 'Получить ссылку'
and n.content like '%#%';");
        foreach ($result as $key => $value){
            $a =explode('#', $value->tag1);
            $result[$key]->tag1 = $a[0];
            $result[$key]->tag2 = $a[1];
            $result[$key]->tag3 = $a[2];
        }
//AND r.created_at > $updated
        $this->exportCsv($result, 'reactions');
    }

    public function getProblemReport(){
        $result = DB::connection('mysql')->select('select
	p.created_at,
	p.id,
	b.FIO,
	b.WORKPLACE,
	p.category,
	p.question,
	case when p.questionFile is null then 0 else 1 end questionFileExists ,
	p.answer,
	case when p.answerFile is null then 0 else 1 end answerFileExists,
	p.status
from
	problem p
join stafftelegramusers s on p.telegramId = s.telegramId
join botan_staff b on (b.isn = s.staffEmail or b.email = s.staffEmail)');
        $this->exportCsv($result, 'problem');
    }

    public function getBookingReport(){
        $result = DB::connection('mysql')
            ->select('
                select * from book_record
            ');
        $this->exportCsv($result, 'booking');
    }

    public function getDoctorReport(){
        $result = DB::select('select first_name, last_name, phone_number, clinic, doctor, reason, insured, card_number, updated_at
            from doctor_appointments');
        $this->exportCsv($result, 'booking');
    }

    public function exportCsv($data, $type){
        $filename = $type."_data_export_" . date("Y-m-d") . ".csv";
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys((array)$data[0]), ';');
        foreach ($data as $row) {
            fputcsv($df, $this->getRow($row),';');
        }
        fclose($df);
        die();
    }

    public function getRow($row){
        $data = [];
        foreach ($row as $r){
            array_push($data, $r);
        }
        return $data;
    }
}
