<?php

namespace App\Http\Controllers;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\Http\Controllers\TelegramMessageHandler;
use App\QuestionCategories;
use App\QuestionsAnswers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\Telegram\CheckUser;
use App\Telegram\DefaultKeyboard;
use App\Telegram\FacebookCommand;
use App\Telegram\FeedbackCommand;
use App\Telegram\AuthCommand;
use App\Telegram\HRCommand;
use App\Telegram\MenuCommand;
use App\Telegram\MethodologyCommand;
use App\Telegram\ProductsCommand;
use App\Telegram\QuestionsCategoriesCommand;
use App\Telegram\QuestionsCommand;
use App\Telegram\SearchQuestionsCommand;
use App\Telegram\UpdatesSubscription;
use App\Telegram\OfficeCommand;
use App\Telegram\ZhanabayCommand;
use App\Telegram\BookingCommand;
use App\TelegramRequestLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Commands\Command;
use App\Http\Controllers\Backend\SettingController;

class TelegramHandlerController extends Controller
{
    public function index()
    {
        // получение обновление с телеграма
        $update = Telegram::commandsHandler(true);

        Log::debug('$update');
        Log::debug($update);


        $message = $update->getMessage();
        $callback = null;
        Log::debug('$message');
        Log::debug($message);
        try {
            $messageId = $message->getMessageId();
        } catch (\BadMethodCallException $exception) {
            Log::debug("Bad method call exception: ");
            Log::debug($exception);
            return 1;
        }
        try {

            if (isset($update["poll_answer"])) {
                SettingController::saveQuizAnswer($update);
            } else if (isset($update['callback_query'])) {
                if (str_contains($update["callback_query"]["data"], '#poll')) {
                    SettingController::saveReaction($update);
                } elseif (str_contains($update["callback_query"]["data"], 'cent')) {
                    SettingController::saveCentMessage($update);
                } elseif (str_contains($update["callback_query"]["data"], 'r#') || $update["callback_query"]["data"] == 'more_') {
                    SettingController::sendToKiasReconcilationResult($update, $messageId);
                    return 1;
                } else {
                    $callbackRoute = explode('-', $update["callback_query"]["data"]);
                    $data = $update["callback_query"]["data"];
                    if ($data == "null_callback") {
                        return 1;
                    } else if (count($callbackRoute)) {
                        if (array_key_exists(1, $callbackRoute) && $callbackRoute[1] == 'months_list') {
                            return 1;
                        } else if ($callbackRoute[0] === 'calendar') {
                            BookingCommand::validateCalendar($update, $message);
                        } else if ($callbackRoute[0] === 'timepicker') {
                            BookingCommand::validateTimePicker($update, $message);
                        } else {
                            $chatId = $message->getChat()->getId();
                            $username = $message->getChat()->getUsername();
                            $firstname = $message->getChat()->first_name;
                            $lastname = $message->getChat()->last_name;
                            $messageText = strtolower($message->getText());
                            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                            $result = CheckUser::index($chatId);
                            if ($result['user']) {
                                TelegramMessageHandler::index($messageText, $chatId, $username, $telegramRequestLog, $firstname, $lastname, $result, $message, $data);
                            }
                        }
                    }
                }
            } else if (!count($message)) {
                Log::debug('empty message in TelegramHandlerController.php');
                return 1;
            } else {
                $chatId = $message->getChat()->getId();
                $username = $message->getChat()->getUsername();
                $firstname = $message->getChat()->first_name;
                $lastname = $message->getChat()->last_name;


                $messageText = strtolower($message->getText());

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "request" => $messageText,
                ];

                $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)
                    ->first();

                if ($telegramUser) {
                    $telegramUser->telegramUsername = $username;
                    $telegramUser->telegramFirstName = $firstname;
                    $telegramUser->telegramLastName = $lastname;
                    $telegramUser->save();
                }

                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                $log = TelegramRequestLog::where('command', 'zhanabay waiting answer')->first();

                if (isset($update['callback_query'])) {
                    $callbackQuery = $update->getCallbackQuery();
                    $callbackQueryId = $callbackQuery->getId();
                    $chatId = $callbackQuery->getMessage()->getChat()->id;
//            $messageId = $callbackQuery->getMessage()->getMessageId();
                    $callback = $callbackQuery->getData();
                }

                $res = TelegramRequestLog::where('telegramId', $chatId)->first();
                Log::channel('telegram')->debug('$telegramRequestLog132');
                Log::channel('telegram')->debug($telegramRequestLog);

                // $historyArr["userEmail"] = $result['user']
                //   ? $result['user']->email
                //     ? $result['user']->email
                //     : $result['user']->ISN
                //   : '';
                Log::channel('telegram')->debug(gettype($update));
                if (!in_array($messageText, DefaultKeyboard::getCommands())) {
                    Log::debug('$update->poll_answer dont exist');
                    //Log::channel('telegram')->debug($telegramRequestLog->command);
                    if ($telegramRequestLog && $telegramRequestLog->command === 'auth: enter email') {
                        BotanDialogHistories::create($historyArr);
                        AuthCommand::login($chatId, $messageText, $username, $firstname, $lastname);
                    } elseif ($telegramRequestLog && substr($telegramRequestLog->command, 0, 35) === 'auth: waiting for code verification') {
                        BotanDialogHistories::create($historyArr);
                        $email = substr($telegramRequestLog->command, 36, strlen($telegramRequestLog->command));
                        Log::debug('check' . $email . 'email');
                        AuthCommand::checkCode($chatId, $messageText, $email, $username, $firstname, $lastname);
                    } elseif ($messageText === 'Авторизация') {
                        BotanDialogHistories::create($historyArr);
                        AuthCommand::executeCommand($chatId, $username, $firstname, $lastname);
                    } elseif ($log && ($messageText == 'Буду в течении часа' || $messageText == 'Буду в течении дня' || $messageText == 'Сегодня не получится, повторите запрос завтра')) {
                        ZhanabayCommand::sendZhanabayAnswerToUser($chatId, $messageText, $username, $firstname, $lastname);
                    } else {
                        // проверка пользователя и запись запроса в историю диалогов
                        $result = CheckUser::index($chatId, $username, $firstname, $lastname, $message);
                        if ($result['user']) {
                            Log::debug($chatId . " " . $username . " " . $message);
                            TelegramMessageHandler::index($messageText, $chatId, $username, $telegramRequestLog, $firstname, $lastname, $result, $message, $callback, $messageId);
                        }
                    }
                }
            }
        }catch (\Exception $e) {
            Log::channel('telegram')->debug("TelegramHadlerController: ". $e->getMessage());
            return 1;
        }
        return 1;
    }

}
