<?php

namespace App\Http\Controllers;
use App\Boss;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class BossController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function list() {
    $bosses = Boss::all();
    return view('bosses', ['bosses' => $bosses]);
  }

  public function importExcel(Request $request) {
    if ($request->hasFile('excelFile')) {
      Boss::truncate();
      $file = $request->file('excelFile');
      $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName())- strlen($file->getClientOriginalExtension()) -1);
      $fileName = $originFileName.'.'.$file->getClientOriginalExtension();
      $file->move(public_path() . '/excel', $fileName);
      $path = public_path()."/excel/".$fileName;
      $xlsx = SimpleXLSX::parse($path);

      $sheetsCount = $xlsx->sheetsCount();
      $bosses = array();
      for ($i = 0; $i < $sheetsCount; $i++) {
        foreach ( $xlsx->rows($i) as $r => $row ) {
          if ($r!=0 && $row[1] && $row[1] != 'ФИО') {
            //          0 - id
            //          1 - FIO
            //          2 - workplace
            //          3 - position

            $boss = new Boss();
            $boss->FIO = $row[1];
            $boss->workplace = $row[2];
            $boss->position = $row[3];
            array_push($bosses, $boss);

            $existingBoss = Boss::where('FIO', $row[1])->first();
            // if ($existingBoss) {
            //   $existingBoss->workplace = $row[2];
            //   $existingBoss->position = $row[3];
            //   $existingBoss->save();
            // } else {
              $boss->save();
            // }
          }
        }
      }

      $existingBosses = Boss::all();
      foreach ($existingBosses as $obj) {
        $fio = $obj->FIO;
        $neededObject = array_filter(
          $bosses,
          function ($e) use (&$fio) {
              return $e->FIO == $fio;
          }
        );
        if (!$neededObject) {
          $obj->delete();
        }
      }
    }
    return redirect()->route('bosses');
  }

  public function uploadPhoto (Request $request) {
    if ($request->hasFile('photo')) {
      $file = $request->file('photo');
      $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName())- strlen($file->getClientOriginalExtension()) -1);
      $fileName = $originFileName.'.'.$file->getClientOriginalExtension();
      $file->move(public_path() . '/bosses', $fileName);

      $boss = Boss::where('id', $request->id)->first();
      $boss->photo = $fileName;
      $boss->save();

      return redirect()->route('bosses');
    }
  }

  public function downloadPhoto($uuid)
    {
      $boss = Boss::where('id', $uuid)->first();
      $pathToFile = public_path() . '/bosses/' . $boss->photo;
      return response()->download($pathToFile);
    }

    public function create()
    {
        return view('bossCreate');
    }

    public function store(Request $request)
    {
        $input = $request->except(['_token']);
        $result = Boss::create($input);
        return redirect(route('bosses'));
    }

    public function edit($id)
    {
        $boss = Boss::find($id);
        return view('bossesEdit', compact('boss'));
    }

    public function update(Request $request)
    {
        $input = $request->except(['_token']);
        $result = Boss::updateOrCreate(['id'=>$input['id']],$input);
        return redirect(route('bosses'));
    }
}
