<?php

namespace App\Http\Controllers;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\StaffTelegramUsers;
use App\Telegram\DefaultKeyboard;
use App\Template;
use App\TemplateCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class TemplateController extends Controller
{
    public function index()
    {
        $templates = TemplateCategory::all();
        return view('templates', ['templates' => $templates]);
    }

    public function addCategory(){
        return view('templatesCategoryAdd');
    }

    public function saveCategory(Request $request){
        $category = new TemplateCategory();
        $category->category = $request->category;
        $category->description = $request->description;
        $category->save();
        return redirect()->route('templates');
    }

    public function getTemplatesByCategory(TemplateCategory $category){
        $templates = $category->templates;
        return view('templatesByCategory', ['templates' => $templates]);
    }

    public function addTemplate(){
        $categoryList = TemplateCategory::all();
        return view('templatesAdd', ['templates' => $categoryList]);
    }

    public function saveTemplate(Request $request){
        $template = new Template();
        $template->template = $request->template;
        $template->category = $request->category;
        $template->description = $request->description ? $request->description : null;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName()) - strlen($file->getClientOriginalExtension()) - 1);
            $fileName = time().'_'.$originFileName . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/template', $fileName);
            $template->file = $fileName;
        }else{
            $template->file = null;
        }
        $template->save();
        return redirect()->route('templates.category.get', $request->category);
    }

    public function deleteTemplateFile(Template $template){
        $template->file = null;
        $template->save();
        return redirect()->route('templates.category.get', $template->category);
    }

    public function editTemplate(Template $template){
        $categoryList = TemplateCategory::all();
        return view('templatesEdit', ['categories' => $categoryList, 'template' => $template]);
    }

    public function saveEditedTemplate(Template $template, Request $request){
        $template->template = $request->template;
        $template->category = $request->category;
        $template->description = $request->description ? $request->description : null;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName()) - strlen($file->getClientOriginalExtension()) - 1);
            $fileName = time().'_'.$originFileName . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/template', $fileName);
            $template->file = $fileName;
        }else{
            $template->file = null;
        }
        $template->save();
        return redirect()->route('templates.category.get', $request->category);
    }

    public function downloadFile(Template $template){
        $pathToFile = public_path() . '/template/' . $template->file;
        return response()->download($pathToFile);
    }

    public function sendTemplate(Template $template, Request $request){
        $telegramUsers = [];
        if($request->company === "user"){
            $telegramUsers = StaffTelegramUsers::whereIn('telegramId', $request->users)->get();
        }elseif ($request->company) {
            $users = StaffTelegramUsers::all();
            foreach ($users as $user) {
                $staffUser = BotanStaff::where('WORKPLACE', $request->company)
                    ->where('email', $user->staffEmail)
                    ->orWhere('ISN', $user->staffEmail)
                    ->first();
                if ($staffUser && $staffUser->WORKPLACE === $request->company) {
                    array_push($telegramUsers, $user);
                }
            }
        } else {
            $telegramUsers = StaffTelegramUsers::all();
        }


        foreach ($telegramUsers as $telegramUser) {
            if ($telegramUser->updatesSubscription === 1) {
                try {
                    if ($template->file) {
                        Telegram::sendPhoto([
                            'chat_id' => $telegramUser->telegramId,
                            'photo' => new InputFile(base_path() . '/public/template/' . $template->file, substr($template->file, 11)),
                        ]);
                    }
                    $keyboard = DefaultKeyboard::getMenuKeyboard(StaffTelegramUsers::getWorkPlaceByChatId($telegramUser->telegramId));

                    $reply_markup = Keyboard::make(['keyboard' => $keyboard,
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true]);
                    if ($template->description){
                        $resp = Telegram::sendMessage([
                            'chat_id' => $telegramUser->telegramId,
                            'text' => $template->description,
                            'reply_markup' => $reply_markup
                        ]);
                    }


                    $historyArr = [
                        "chatId" => $telegramUser->telegramId,
                        "response" => $request->messageType === 'poll'
                            ? "Опрос \n".$request->message
                            : $request->message,
                        "userEmail" => $telegramUser->staffEmail,
                        "telegramUsername" => $resp->chat->username,
                        "telegramFirstName" => $resp->chat->first_name,
                        "telegramLastName" => $resp->chat->last_name,
                    ];
                    BotanDialogHistories::create($historyArr);
                } catch (\Exception $e) {
                    Log::debug('Выброшено исключение sendTemplate: ' . $e->getMessage() . " " . $telegramUser->telegramId);
                }
            }
        }
        return redirect()->route('templates.send', $template->id);
    }

    public function sendTemplateView(Template $template){
        $companies = Companies::all();
        $users = StaffTelegramUsers::all();
        return view('sendTemplateForm', ['companies' => $companies, 'template' => $template, 'users' => $users]);
    }

    public function deleteCategory(TemplateCategory $category){
        $category->delete();
        return redirect()->route('templates');
    }

    public function deleteTemplate(Template $template){
        $template->delete();
        return redirect()->route('templates.category.get', $template->category);
    }
}
