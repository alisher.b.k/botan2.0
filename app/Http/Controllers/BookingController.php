<?php

namespace App\Http\Controllers;

use App\BookingBuilding;
use App\BookRecord;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rooms;

class BookingController extends Controller
{
    public function buildingList(){
        return view('bookingList', ['list' => BookingBuilding::all()]);
    }
    
    public function buildingAddView(){
        return view('bookingBuilding');
    }

    public function buildingAdd(Request $request){
        try {
            $building = new BookingBuilding();
            $building->building = $request->building;
            $building->save();
            return redirect(route('booking.building'));
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function buildingEditView($id){
        return view('bookingEdit', ['building' => BookingBuilding::findOrFail($id)]);
    }

    public function buildingEdit(Request $request, $id){
        try{
            $building = BookingBuilding::findOrFail($id);
            $building->building = $request->building;
            $building->save();
            return redirect(route('booking.building'));
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public function buildingDelete($id){
        foreach(BookingBuilding::findOrFail($id)->rooms as $room){
            $records = BookRecord::where('room', $room->room)->get();
            foreach($records as $record){
                $record->delete();
            }
            $room->delete();
        }
        BookingBuilding::findOrFail($id)->delete();
        return redirect(route('booking.building'));
    }

    public function roomList($id){
        return view('bookingRoomList', ['rooms' => BookingBuilding::findOrFail($id)->rooms, 'building' => BookingBuilding::findOrFail($id)]);
    }

    public function roomAddView($id){
        return view('bookingRoom', ['building' => BookingBuilding::findOrFail($id)]);
    }

    public function roomAdd(Request $request, $id){
        try {
            $room = new Rooms();
            $room->room = $request->room;
            $room->booking_building_id = $id;
            $room->color = $request->color; 
            $room->save();
            return redirect(route('booking.room', [$id]));
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function roomEditView($id, $buildingId){
        return view('bookingRoomEdit', ['room' => Rooms::findOrFail($id), 'buildingId' => $buildingId]);
    }

    public function roomEdit(Request $request, $id, $buildingId){
        try{
            $room = Rooms::findOrFail($id);
            $room->room = $request->building;
            $room->color = $request->color;
            $room->save();
            return redirect(route('booking.room', [$buildingId]));
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public function roomDelete($id, $building){
        $records = BookRecord::where('room', Rooms::findOrFail($id)->room)->get();
        foreach($records as $record){
            $record->delete();
        }
        Rooms::findOrFail($id)->delete();
        return redirect(route('booking.room', [$building]));
    }

}
