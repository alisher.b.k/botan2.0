<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vacancy;

class RecruitingController extends Controller
{
    public function getVacancy() {
        return view('recruiting', ['vacancies' => Vacancy::all()]);
    }

    public function createVacancyView() {
        return view('createVacancy');
    }

    public function createVacancy(Request $request){

        $validated = $request->validate([
            'name' => 'required',
            'company' => 'required',
            'department' =>  'required',
            'city' => 'required',
            'requirements' => 'required',
            'duties' => 'required'
        ]);

        Vacancy::create($validated);
        return redirect()->route('getVacancy');
    }

    public function deleteVacancy($id){
        Vacancy::find($id)->delete();
        return redirect()->route('getVacancy');
    }

    public function editVacancyView($id){
        return view('editVacancyView',['vacancy' => Vacancy::find($id)]);
    }

    public function editVacancy($id, Request $request) {
        $vacancy = Vacancy::find($id);
        $validated = $request->validate([
            'name' => 'required',
            'company' => 'required',
            'department' =>  'required',
            'city' => 'required',
            'requirements' => 'required',
            'duties' => 'required'
        ]);
        $vacancy->update($validated);
        return redirect()->route('getVacancy');
    }

    public function importVacancyExcel(Request $request){
        if($request->hasFile('excelFile')) {
            $file = $request->file('excelFile');
            $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName())- strlen($file->getClientOriginalExtension()) -1);
            $fileName = $originFileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/excel', $fileName);
            $path = public_path()."/excel/".$fileName;
            $xlsx = SimpleXLSX::parse($path);
            foreach ( $xlsx->rows() as $r => $row ) {
                if ($r!=0) {
//          0 - name
//          1 - company
//          2 - department
//          3 - city
//          4 - requirements
//          5 - duties
                    $user = new Vacancy();
                    $user->name = $row[0];
                    $user->company = $row[1];
                    $user->department = $row[2];
                    $user->city = $row[3];
                    $user->requirements = $row[4];
                    $user->duties = $row[5];
                    $user->save();
                }
            }
        }
        return redirect()->route('getVacancy');
    }
}
