<?php

namespace App\Http\Controllers;

use App\Exports\CentcoinsExport;
use App\Exports\ClinicsExport;
use App\Exports\NewUsersExport;
use App\StaffBalance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Excel;
use App\News;
use App\NewsReactions;
use App\ResultOfReaction;
use App\StaffTelegramUsers;
use App\BotanStaff;
use App\Exports\ReactionsExport;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ExportController extends Controller
{
    public function getNewUsersView(){
        return view('export.users');
    }

    public function exportNewUsers(Request  $request){
//        $export = new NewUsersExport();
//        $export->start = $request->dateStart;
//        $export->end = $request->dateEnd;
//        return Excel::download($export, 'new_users.xlsx');

        return (new NewUsersExport($request->dateStart, $request->dateEnd))->download('new_users.xlsx');
    }

    public function exportCentcoins(){
        return (new CentcoinsExport())->download('centcoins.xlsx');
    }

    public function getClinicsView(){
        return view('export.clinics');
    }

    public function getReactionsView(){
        return view('export.reactions');
    }

    public function getReactionsForExport(Request $request){
        $data = News::select('id', 'content', 'title', 'updated_at')->whereBetween('updated_at',  [Carbon::parse($request->dateStart)->startOfDay()->toDateTimeString(), Carbon::parse($request->dateEnd)->endOfDay()->toDateTimeString()])->get();
        return $data;
    }

    public function exportReaction(Request $request){
        $reaction = NewsReactions::where('newsId', $request->id)->get();
        $users = [];
        foreach ($reaction as $item) {
            array_push($users, $item->telegramId);
        }
        $users = array_unique($users);
        foreach($users as $user){
            $telegramUser = StaffTelegramUsers::where('telegramId', $user)->first();
            $User = BotanStaff::where('email', $telegramUser->staffEmail)->orWhere('isn', $telegramUser->staffEmail)->first();
            $name = new ResultOfReaction();
            $name->news_reactions_id = $request->id;
            $name->name = $User->FIO ?? null;
            $data = NewsReactions::where('telegramId', $user)->get();
            $resultEmoji = [];
                foreach($data as $dat){
                    if($dat->newsId == $request->id){
                        switch ($dat->reaction) {
                            case '👍':
                                $name->good = "✅";
                                array_push($resultEmoji,'👍');
                                break;
                            case "👎":
                                $name->bad = "✅";
                                array_push($resultEmoji,'👎');
                                break;
                            case "🤔":
                                $name->whatever = "✅";
                                array_push($resultEmoji,"🤔");
                                break;
                            case "Получить ссылку":
                                $name->getLink = "✅";
                                array_push($resultEmoji,"Получить ссылку");
                                break;
                        }
                    }
                }
                if(!in_array('👍', $resultEmoji)){
                    $name->good = $name->good = "❌";
                }
                if(!in_array("🤔", $resultEmoji)){
                    $name->whatever = "❌";
                }
                if(!in_array('👎', $resultEmoji)){
                    $name->bad = "❌";
                }
                if(!in_array("Получить ссылку", $resultEmoji)){
                    $name->getLink = "❌";
                }

            $name->save(); 
        }

            $contentName = News::select('content', 'title')->where('id', $request->id)->first();
            if($contentName['title'] == null){
                $result = (new ReactionsExport($request->id))->download(mb_substr($contentName['content'],0,20, 'UTF-8').'.xlsx');
            }else{
                $result = (new ReactionsExport($request->id))->download(mb_substr($contentName['title'],0,20, 'UTF-8').'.xlsx');
            }
            ResultOfReaction::where('news_reactions_id', $request->id)->delete();
            return $result;
    }

    public function exportClinics(Request $request){
        return (new ClinicsExport($request->dateStart, $request->dateEnd))->download('doctor_appointments.xlsx');
    }
}
