<?php

namespace App\Http\Controllers;

use App\BotanStaff;
use App\Reconciliation;
use App\StaffTelegramUsers;
use App\Telegram\CheckUser;
use App\TelegramRequestLog;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class NotificationController extends Controller
{
    private $rules = [
        'isn' => 'required',
        'reconciliation_isn' => 'required',
        'message' => 'nullable|string',
        'fio' => 'required',
        'contract_number' => 'required',
        'date' => 'required',
        'kurator' => 'nullable|string',
        'company' => 'nullable|string',
        'participant' => 'nullable|string',
        'note' => 'nullable|string'
    ];

    public function storeAndSend(Request $request){
        $validate = Validator::make($request->all(), $this->rules);
        if ($validate->fails()) {
            return response()->json(['success'=>false, 'message' =>$validate->errors()],200,['Content-type'=>'application/json;charset=utf-8'],JSON_UNESCAPED_UNICODE);
        }
        $array = $request->all();
        try {
            Log::channel("telegram")->debug("согласование ");
            Log::channel("telegram")->debug($array);
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
        }
        $array['date'] = date('d.m.Y H:i:s', strtotime($array['date']));
        $reconciliation = Reconciliation::updateOrCreate($array);
        $user = BotanStaff::where('FIO', $array['fio'])->first();
        if ($user==null) {
            return response()->json(['success' => false, 'message' => 'Не найден пользователь в базе Ботан'],200,['Content-type'=>'application/json;charset=utf-8'],JSON_UNESCAPED_UNICODE);
        }
        $telegramUser = StaffTelegramUsers::where('staffEmail', $user->ISN)->orWhere('staffEmail', $user->email)->first();
        if ($telegramUser==null) {
            return response()->json(['success' => false, 'message' => 'Не найден телеграм пользователя'],200,['Content-type'=>'application/json;charset=utf-8'],JSON_UNESCAPED_UNICODE);
        }
        $reconciliation->telegram_id = $telegramUser->telegramId;
        $reconciliation->save();
        try {
            $result = Telegram::sendMessage([
                'chat_id' => $telegramUser->telegramId,
                'text' => Reconciliation::getMessage($reconciliation, false),
                'reply_markup' => Keyboard::make([
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true,
                    'inline_keyboard' => [
                        [
                            ['text' => 'Подробнее', 'callback_data' => 'more_'],
                        ],
                        [
                            ['text' => 'Согласовать', 'callback_data' => 'r#yes'],
                            ['text' => 'Отказать', 'callback_data' => 'r#no'],
                        ]
                    ]
                ])
            ]);
            $reconciliation->message_id = $result->message_id;
            $reconciliation->update();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Согласование не отправлено ' . $e->getMessage()],200,['Content-type'=>'application/json;charset=utf-8'],JSON_UNESCAPED_UNICODE);
        }
        return response()->json(['success'=>true, 'message' => 'Согласование отправлено'],200,['Content-type'=>'application/json;charset=utf-8'],JSON_UNESCAPED_UNICODE);
    }

    public function sendMail(Request $request)
    {
//        if ($request->hash != '#12k0asp'){
//            abort(403);
//        }
        $messageText = $request->message;
        $fio = $request->fio;
        try {
            Mail::send(['text' => 'mail'], ['name' => 'Код для авторизации'], function ($message) use ($fio, $messageText) {
                $message->to($messageText, $fio)->subject('Код для авторизации');
                $message->from('bot@centras.kz', 'Botan');
            });

            return response()->json(['success' => true]);
        } catch (\Swift_TransportException $e) {
            Log::debug($request);
            Log::debug('Notification Email');
            Log::debug($e->getMessage());
            return response()->json($e->getMessage());
        }
    }
}
