<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\BotanStaff;
use App\Companies;
use App\CustomCommand;
use App\Events;
use App\Feedback;
use App\Library;
use App\Problem;
use App\ProblemDialogHistories;
use App\Telegram\DefaultKeyboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Ixudra\Curl\Facades\Curl;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\TelegramRequestLog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getStaffInfo_request = Curl::to('http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdb.php')
            ->returnResponseObject()
//            ->withContentType('application/json')
            ->withTimeout(60*10)
            ->get();
        $array = json_decode(json_encode($getStaffInfo_request), True);
        return view('home',['arr'=>$array]);
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

    public function editStaff($id)
    {
        $staff = BotanStaff::find($id);
        if(!$staff){
            $staff = null;
        }

        return view('editStaff', ['staff' => $staff, 'last'=>$id]);
    }

    public function answerOnProblem($id){
        $problem = Problem::find($id);
        if(!$problem){
            $staff = null;
        }
        return view('answerOnProblem', ['problem' => $problem]);
    }

    public function downloadProblemFile($uuid)
    {
        $problem = Problem::where('id', $uuid)->firstOrFail();
        $pathToFile = public_path() . '/problem/' . $problem->questionFile;
        return response()->download($pathToFile);
    }

    public function downloadProblemAnswerFile($uuid)
    {
        $problem = Problem::where('id', $uuid)->firstOrFail();
        $pathToFile = public_path() . '/problem/' . $problem->answerFile;
        return response()->download($pathToFile);
    }

    public function downloadFeedbackFile($uuid)
    {
        $feedback = Feedback::where('id', $uuid)->firstOrFail();
        $pathToFile = public_path() . '/feedback/' . $feedback->file;
        return response()->download($pathToFile);
    }

    public function downloadProblemFileDialog(ProblemDialogHistories $dialog)
    {
        $pathToFile = public_path() . '/problem/' . $dialog->questionFile;
        return response()->download($pathToFile);
    }

    public function downloadProblemAnswerFileDialog(ProblemDialogHistories $dialog)
    {
        $pathToFile = public_path() . '/problem/' . $dialog->answerFile;
        return response()->download($pathToFile);
    }

    public function closeProblem(Request $request) {
        $id = $request->id;

        $problem = Problem::where('id', '=', $id)->first();
        $problem->status = "closed";
        $problem->save();

        return redirect()->route('problems');
    }

    public function answerOnProblemSave(Request $request) {
        $id=$request->id;
        $problem = Problem::where('id', '=', $id)->first();

        $problemDialogHistory = new ProblemDialogHistories();

        if ($problem->answer || $problem->answerFile) {
            $problemDialogHistory->answer = $request->answer;
            $problemDialogHistory->telegramId = $problem->telegramId;
            $problemDialogHistory->problemId = $problem->id;
            $problemDialogHistory->email = $problem->email;
            $problemDialogHistory->save();
        } else {
            $problem->answer = $request->answer;
        }

        $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);

        Telegram::sendMessage(['chat_id' => $problem->telegramId,
            'text' => "Ответ на твое обращение №".$id.": \n".$request->answer,
            'reply_markup' => $reply_markup]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName())- strlen($file->getClientOriginalExtension()) -1);
            $fileName = $originFileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/problem', $fileName);
            if ($problem->answer || $problem->answerFile) {
                $problemDialogHistory->answerFile = $fileName;
                $problemDialogHistory->save();
            } else {
                $problem->answerFile = $fileName;
            }

            $token = getenv('TELEGRAM_BOT_TOKEN');
            define('BOTAPI', 'https://api.telegram.org/bot' . $token . '/');
            $data2 = [
                'chat_id' => $problem->telegramId,
                'document' => new \CURLFile(public_path() . '/problem/'. $fileName),
                'reply_markup' => $reply_markup
            ];
            $ch = curl_init(BOTAPI . 'sendDocument');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            $output = curl_exec($ch);
            curl_close($ch);
        }

        Telegram::sendMessage(['chat_id' => $problem->telegramId,
            'text' => "Хочешь дополнить проблему?",
            'reply_markup' => Keyboard::make(['keyboard' => [
                ['Да', 'Нет']],
            'resize_keyboard' => true,
            'one_time_keyboard' => true])]);

        TelegramRequestLog::where('telegramId', $problem->telegramId)->delete();
        $log = new TelegramRequestLog();
        $log->telegramId = $problem->telegramId;
        $log->command = 'answerOnProblemSave';
        $log->data = json_encode(array('problemId' => $id));
        $log->save(); 


        $problem->save();
        return redirect()->route('problems');
    }

    public function deleteStaff($id){
        BotanStaff::destroy($id);
        $last = BotanStaff::orderBy('id','desc')->first();
        $i = 1;
        $allStaff = BotanStaff::all();
        foreach($allStaff as $staff){
            $staff->id = $i;
            $staff->save();
            $i++;
        }

        return redirect()->route('showStaff');

    }

    public function importExcel(Request $request){
        if($request->hasFile('excelFile')) {
            $file = $request->file('excelFile');
            $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName())- strlen($file->getClientOriginalExtension()) -1);
            $fileName = $originFileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/excel', $fileName);
            $path = public_path()."/excel/".$fileName;
            $xlsx = SimpleXLSX::parse($path);
            $workplace = $xlsx->rows()[1][3];
            BotanStaff::where('WORKPLACE', $workplace)->delete();
            foreach ( $xlsx->rows() as $r => $row ) {
                if ($r!=0) {
                    Companies::updateOrCreate(['companyName' => $row[3]]);
//          0 - FIO
//          1 - position
//          2 - MAINDEPT
//          3 - WORKPLACE
//          4 - mobPhone
//          5 - workPhone
//          6 - intPhone
//          7 - email
                    $user = new BotanStaff();
                    $user->FIO = $row[0];
                    $user->position = $row[1];
                    $user->MAINDEPT = $row[2];
                    $user->WORKPLACE = $row[3];
                    $user->mobPhone = str_replace('-', '', $row[4]);
                    $user->workPhone = str_replace('-', '', $row[5]);
                    $user->intPhone = str_replace('-', '', $row[6]);
                    $user->email = $row[7];
                    $user->save();
                }
            }
        }
        return redirect()->route('showStaff');
    }

    public function saveStaff(Request $request)
    {
        Log::notice($request->WORKPLACE);
        $staff = BotanStaff::find($request->id);
        if(!$staff){
            $staff = new BotanStaff();
            $staff->id = $request->id;
        }
        $staff->photo = $request->photo;
        if($request->hasFile('photoUpload')) {
            $file = $request->file('photoUpload');
            $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName())- strlen($file->getClientOriginalExtension()) -1);
            $date = date('m/d/Y h:i:s a', time());
            $fileName = $request->ISN.'_'.md5($date.$originFileName).'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/photos', $fileName);
            $staff->photo = 'https://bots.n9.kz/staff/photo/'.$fileName;
        }
        $staff->FIO = $request->lastname." ".$request->firstname." ".$request->parentname;
        $staff->ISN = $request->ISN;
        $staff->NAMELAT = $request->NAMELAT;
        $staff->position = $request->position;
        if ($request->WORKPLACE) {
            $staff->WORKPLACE = $request->WORKPLACE;
        }
        $staff->MAINDEPT = $request->MAINDEPT;
        $staff->SUBDEPT = $request->SUBDEPT;
        $staff->workPhone = $request->workPhone;
        $staff->intPhone = $request->intPhone;
        $staff->mobPhone = $request->mobPhone;
        $staff->email = $request->email;
        $staff->bday = $request->bday;
        $staff->workEx = $request->workEx;
        $staff->VACATIONBEG = $request->VACATIONBEG;
        $staff->VACATIONEND = $request->VACATIONEND;
        $staff->TRIPBEG = $request->TRIPBEG;
        $staff->TRIPEND = $request->TRIPEND;
        $staff->LEAVEDATE = $request->LEAVEDATE;
        $staff->save();

        return redirect()->route('showStaff');
    }

    public function aboutUs() {
        $aboutUs = AboutUs::all();
        $commands = CustomCommand::where('type', 'about_us')->get();
        if ($aboutUs && count($aboutUs) > 0) {
            return view('aboutUsEdit', ['aboutUs' => $aboutUs, 'commands' => $commands]);
        } else {
            return view('aboutUsEdit', ['aboutUs' => [], 'commands' => $commands]);
        }
    }

    public function saveAboutUs(Request $request) {
        $aboutUs = AboutUs::all();
        if ($aboutUs && count($aboutUs) > 0) {
          if($request->aboutUsContent){
            $aboutUs[0]->content = $request->aboutUsContent;
            $aboutUs[0]->save();
          }
          if ($request->aboutUsContentV2 || $request->aboutUsContentV2 == ""){
            $aboutUs[0]->contentv2 = $request->aboutUsContentV2;
            $aboutUs[0]->save();
          }
        } else {
            $aboutUs = new AboutUs();
            $aboutUs->content = $request->aboutUsContent;
            $aboutUs->contentv2 = $request->aboutUsContentV2;
            $aboutUs->save();
        }
        $aboutUs = AboutUs::all();
        return redirect()->route('aboutUs');
    }

    public function saveSenateAboutUs(Request $request) {
        $aboutUs = AboutUs::all();
        if ($aboutUs && count($aboutUs) > 0) {
          if ($request->senateCompanies || $request->senateCompanies == "") {
            if (count($aboutUs) > 1) {
              $aboutUs[1]->contentv2 = $request->senateCompanies;
              $aboutUs[1]->save();
            } else {
              $aboutUs = new AboutUs();
              $aboutUs->contentv2 = $request->senateCompanies;
              $aboutUs->save();
            }
          } 
        } 
        $aboutUs = AboutUs::all();
        return redirect()->route('aboutUs');
    }

    public function getEventCalendar() {
        $events = [];
        $allEvents = Events::all();
        foreach ($allEvents as $eventElement) {
            $events[] = Calendar::event(
                $eventElement->title, // title
                true, //full day event
                $eventElement->eventDate, //start time
                $eventElement->eventDate, //start time
                $eventElement->id, //event id
                [
                    'url' => 'https://bots.n9.kz/botan/events/edit/'.$eventElement->id,
//                    'url' => 'http://127.0.0.1:8000/botan/events/edit/'.$eventElement->id,
                    //any other full-calendar supported parameters
                ]
            );
        }

//        $events[] = Calendar::event(
//            'Event one', // title
//            true, //full day event
//            '2019-04-21T0900', //start time
//            '2019-04-21T1100', //end time
//            0, //event id
//            [
//                'url' => 'http://full-calendar.io',
//                //any other full-calendar supported parameters
//            ]
//        );
        $calendar = Calendar::addEvents($events)
        ->setOptions([
            'firstDay' => 1,
            'locale' => 'ru'
        ])->setCallbacks([]);
        return view('eventCalendar', ['calendar' => $calendar]);
    }

    public function saveEvent(Request $request) {
        if ($request->id) {
            $event = Events::where('id', $request->id)->first();
            $event->title = $request->title;
            $event->content = $request->eventContent;
            $event->eventDate = $request->eventDate;
            $event->eventType = $request->eventType;
            $event->save();
        } else {
            $event = new Events();
            $event->title = $request->title;
            $event->content = $request->eventContent;
            $event->eventDate = $request->eventDate;
            $event->eventType = $request->eventType;
            $event->save();
        }
        return redirect('/botan/events');
    }

    public function editEvent($id) {
        if ($id) {
            $event = Events::where('id', $id)->first();
            return view('eventCalendarEdit', ['event' => $event]);
        } else {
            return view('eventCalendarEdit', ['event' => null]);
        }
    }

    public function deleteEvent(Request $request) {
        if ($request->id) {
            $event = Events::where('id', $request->id)->first();
            $event->delete();
            return redirect('/botan/events');
        } else {
            return redirect('/botan/events');
        }
    }

    public function helpDesk() {
        $commands = CustomCommand::where('type', 'help_desk')->get();
        return view('helpDesk', ['commands' => $commands]);
    }

    public function editHelpDesk($id) {
        $command = CustomCommand::find($id);
        if(!$command){
            $command = null;
        }
        return view('helpDesk_edit', ['command' => $command]);
    }

    public function editAboutUsCommand($id) {
        $command = CustomCommand::find($id);
        if(!$command){
            $command = null;
        }
        return view('aboutUsEditCommand', ['command' => $command]);
    }

    
    public function saveAboutUsCommand(Request $request) {
        $command = CustomCommand::find($request->commandId);
        if(!$command){
            $command = new CustomCommand();
        }
        $command->command = $request->command;
        $command->commandDescription = $request->commandDescription;
        $command->content = $request->commandContent;
        $command->type = 'about_us';
        if ($request->hasFile('commandFile')) {
            $file = $request->file('commandFile');
            $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName())- strlen($file->getClientOriginalExtension()) -1);
            $fileName = $originFileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/about_us', $fileName);
            $command->file = $fileName;
        }
       
        $command->save();

        return redirect()->route('aboutUs');
    }

    public function deleteCommand($id) {
        CustomCommand::destroy($id);
        $commands = CustomCommand::all();
        return redirect()->route('helpDesk',['commands' => $commands]);
    }

    public function problems() {
        $problems = Problem::orderBy('id', 'desc')->get();
        return view('problem', ['problems' => $problems]);
    }

    public function deleteAboutCommand($id) {
        CustomCommand::destroy($id);
        return redirect()->route('aboutUs');
    }

    public function library(){
        $library = Library::all();
        return view('library', ['library' => $library]);
    }

    public function editLibrary($id) {
        $library = Library::find($id);
        if(!$library){
            $command = null;
        }
        return view('library_edit', ['library' => $library]);
    }

    public function saveLibrary(Request $request) {
        $library = Library::find($request->libraryId);
        if(!$library){
            $library = new Library();
        }
        $library->description = $request->description;
        $library->link = $request->link;
        $library->save();

        $library = Library::all();
        return redirect()->route('library', ['library' => $library]);
    }

    public function deleteLibrary($id) {
        Library::destroy($id);
        $library = Library::all();
        return redirect()->route('library',['library' => $library]);
    }
}
