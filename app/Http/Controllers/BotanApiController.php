<?php

namespace App\Http\Controllers;

use App\DoctorAppointment;
use App\Clinics;
use App\BookingBuilding;
use App\BookRecord;
use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\Console\Commands\AddStaffCommand;
use App\CustomCommand;
use App\Library;
use App\MessageGroup;
use App\Products;
use App\QuestionsAnswers;
use App\Rooms;
use App\StaffTelegramUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use StaffContoller;

class BotanApiController extends Controller
{
    public function sendNotification(Request $request) {
        if ($request->isn) {
            $isn = $request->isn;
            $docType = $request->docType;
            $docNum = $request->docNum;
            $success = false;

            $user = BotanStaff::where('ISN', $isn)
                ->first();
            if ($user) {
                $telegramUser = StaffTelegramUsers::where('staffEmail', $user->email)->first();
                if (!$telegramUser) {
                    $telegramUser = StaffTelegramUsers::where('staffEmail', $user->ISN)->first();
                    if (!$telegramUser) {

                    } else {
                        if ($telegramUser->updatesSubscription) {
                            $success = true;
                            BotanApiController::sendDocumentNotification($telegramUser->telegramId, $docType, $docNum);
                        }
                    }
                } else {
                    if ($telegramUser->updatesSubscription) {
                        $success = true;
                        BotanApiController::sendDocumentNotification($telegramUser->telegramId, $docType, $docNum);
                    }
                }
            }

            $data = ['success' => $success ];

            return json_encode( $data );
        }
    }

    public function sendServiceNotification(Request $request) {
        Log::debug('service center notification');
        Log::debug($request->all());
        if ($request->isn) {
            $isn = $request->isn;
            $serviceCenter = $request->serviceCenter;
            $serviceType = $request->serviceType;
            $customer = $request->customer;
            $customerDept = $request->customerDept;
            $requestNo = $request->requestNo;
            $status = $request->status;
            $subject = $request->subject;

            $success = false;
            $message = "<b>Новая заявка в ЦО</b> : {$serviceCenter}\n
<b>Тип заявки</b> : {$serviceType}\n
<b>Заказчик</b> : {$customer}\n
<b>Подразделение заказчика</b> : {$customerDept}\n
<b>Номер заявки</b> : {$requestNo}\n
<b>Статус</b> : {$status}\n
<b>Текст заявки</b> : {$subject}";
            $user = BotanStaff::where('ISN', $isn)
                ->first();
            if ($user) {
                $telegramUser = StaffTelegramUsers::where('staffEmail', $user->email)->first();
                if (!$telegramUser) {
                    $telegramUser = StaffTelegramUsers::where('staffEmail', $user->ISN)->first();
                    if (!$telegramUser) {
                        return json_encode([
                            'success' => false,
                            'error' => 'Пользователь не найден'
                        ]);
                    } else {
                        $success = true;
                        try{
                            BotanApiController::sendServiceCenterNotification($telegramUser->telegramId, $message);
                        }catch (\Exception $ex){
                            return json_encode([
                                'success' => false,
                                'error' => 'Пользователь не зарегистрирован или ограничил доступ к чат боту'
                            ]);
                        }
                    }
                } else {
                    $success = true;
                    try{
                        BotanApiController::sendServiceCenterNotification($telegramUser->telegramId, $message);
                    }catch (\Exception $ex){
                        return json_encode([
                            'success' => false,
                            'error' => 'Пользователь не зарегистрирован или ограничил доступ к чат боту'
                        ]);
                    }
                }
            }

            $data = ['success' => $success ];

            return json_encode( $data );
        }elseif($request->id){
            $id = $request->id;
            $serviceCenter = $request->serviceCenter;
            $serviceType = $request->serviceType;
            $customer = $request->customer;
            $customerDept = $request->customerDept;
            $requestNo = $request->requestNo;
            $status = $request->status;
            $subject = $request->subject;
            $success = false;
            $message = "<b>Новая заявка в ЦО</b> : {$serviceCenter}\n
<b>Тип заявки</b> : {$serviceType}\n
<b>Заказчик</b> : {$customer}\n
<b>Подразделение заказчика</b> : {$customerDept}\n
<b>Номер заявки</b> : {$requestNo}\n
<b>Статус</b> : {$status}\n
<b>Текст заявки</b> : {$subject}";
            try{
                $group = MessageGroup::findOrFail($id);
            }catch (ModelNotFoundException $exception){
                return [
                    'success' => false,
                    'error' => 'Group not found'
                ];
            }

            $staffEmails = $group->getUsersArray();

            if(count($staffEmails) === 0){
                return [
                    'success' => false,
                    'error' => 'Group not found'
                ];
            }
            $errorUsers = [];
            foreach ($staffEmails as $staffEmail){
                try{
                    $telegramUser = StaffTelegramUsers::where('staffEmail', $staffEmail)->first();
                    BotanApiController::sendServiceCenterNotification($telegramUser->telegramId, $message);
                    $success = true;
                }catch (\Exception $exception){
                    array_push($errorUsers, $staffEmail);
                }
            }

            if(count($errorUsers) > 0) {
                $data = [
                    'success' => $success,
                    'error' => implode(',', $errorUsers)
                ];
            }else{
                $data = ['success' => $success];
            }

            return json_encode( $data );
        }else{
            return json_encode([
                'success' => false,
                'error' => 'Не указан ISN'
            ]);
        }
    }

    public static function sendDocumentNotification($chatId, $docType, $docNum) {
        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => "Вам на согласование поступил документ ".$docType." №".$docNum,
        ]);
    }

    public static function sendServiceCenterNotification($chatId, $message){
        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => $message,
            'parse_mode' => 'html'
        ]);
    }

    public function viewProduct($id)
    {
        $product = Products::where('id', $id)->first();
        return view('productView', ['product' => $product]);
    }


    public function getProducts(Request $request)
    {
        $history = new BotanDialogHistories();
        $telegramUser = StaffTelegramUsers::where('telegramId', $request->id)->first();
        if ($telegramUser) {
            $history->userEmail = $telegramUser->staffEmail;
            $history->request = "Запрос на просмотр продуктов";
            $user = BotanStaff::where("email", strtolower($telegramUser->staffEmail))->first();
            if (!$user) {
                $user = BotanStaff::where("ISN", $telegramUser->staffEmail)->first();
            }
            if ($user->LEAVEDATE) {
                $history->response = "У вас нет доступа. Вы не являетесь сотрудником компании " . $telegramUser->WORKPLACE;
                $history->save();
                return "2";
            } else {
                $company = Companies::where('companyName', $user->WORKPLACE)->first();
                $products = Products::where('companyName', $company->companyName)->get();
                if (count($products) > 0) {
                    $result = '';
                    $i = 1;
                    $array = [];
                    foreach ($products as $product) {
                        $result .= $product->id . ". " . $product->title . "\n";
                        $result .= $product->companyName . "\n\n";
                        if ($i % 4 === 0 || $i === count($products)) {
                            $array[] = $result;
                            $result = "";
                        }
                        $i++;
                    }
                    $history->response = "Количество выведеных продуктов:" . count($products);
                    $history->save();
                    return $array;
                } else {
                    $history->response = "Продукты не найдены";
                    $history->save();
                    return "Продукты не найдены";
                }
                return response()->json(['status' => 'Error'], 404);
            }
        }
        $history->userEmail = "";
        $history->request = "Запрос на просмотр продуктов";
        $history->response = "id(" . $request->id . ") Пользователь не авторизован ";
        $history->save();
        return "0";
    }

    public function schedule() {
        $addStaff = new AddStaffCommand();
        $addStaff->addCommeskOmirStaff();
        $addStaff->addSentrasInsuranceStaff();
        return 'done';
    }

    public function getBookingData(Request $request){

        $today = date('d.m.Y');
        $rooms = [];
        $building = BookingBuilding::where('building', 'like', '%'.'Коммеск')->first();
        foreach($building->rooms as $build){
            $rooms[$build->room] = $build->id;
        }
        $events = [];
        $i = 1;
        $roomEvents = BookRecord::where('created_at', '>', $today)->get();

        foreach ($roomEvents as $event){
            if(array_key_exists($event->room, $rooms)) {
                $fioArray = explode(' ', $event->fio);
                $fio = "$fioArray[0] $fioArray[1]";
                $events[] = [
                    'id' => $i,
                    'resourceId' => $rooms[$event->room],
                    'start' => date("Y-m-d\TH:i:s", strtotime($event->date . ' ' . $event->start)),
                    'end' => date("Y-m-d\TH:i:s", strtotime($event->date . ' ' . $event->end)),
                    'title' => "$fio $event->room"
                ];
            }
        }

        $resources = [];

        foreach($building->rooms as $room){
            $resources[] = [
                'id' => $rooms[$room->room],
                'title' => $room->room,
                'eventColor' => $room->color
            ];
        }

        return response()->json([
                    'success' => true,
                    'data' => [
                    'resources' => $resources,
                    'events' => $events
                ]
            ]);
    }


    public function sendOgpoVtsNotification(Request $request){
        $user = BotanStaff::where('ISN', $request->isn)->first();
        if($user === null){
            return response()->json([
                'success' => false,
                'error' => 'User not found'
            ]);
        }
        $telegramUser = StaffTelegramUsers::whereIn('staffEmail', [$user->ISN,$user->email])->first();
        if($telegramUser === null){
            return response()->json([
                'success' => false,
                'error' => 'Telegram user not found'
            ]);
        }
        $messageText = $request->message;
        $buttonText = $request->button;
        $imageUrl = $request->image;
        $link = $request->link;
        try {
            $fileName = explode('/', $request->image);
            $fileName = end($fileName);
            Telegram::sendPhoto([
                'chat_id' => $telegramUser->telegramId,
                'photo' => new InputFile($imageUrl, $fileName),
            ]);
            $reply_markup = Keyboard::make([
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
                'inline_keyboard' => [
                    [[
                        'text' => $buttonText,
                        'url' => $link,
                        'one_time_keyboard' => true,
                    ]]
                ]
            ]);
            $resp = Telegram::sendMessage([
                'chat_id' => $telegramUser->telegramId,
                'text' => $messageText,
                'reply_markup' => $reply_markup
            ]);
            $historyArr = [
                "chatId" => $telegramUser->telegramId,
                "response" => "Рассылка ВТС",
                "userEmail" => $telegramUser->staffEmail,
                "telegramUsername" => $resp->chat->username,
                "telegramFirstName" => $resp->chat->first_name,
                "telegramLastName" => $resp->chat->last_name,
            ];
            BotanDialogHistories::create($historyArr);
        }catch (\Exception $e){
            Log::debug("VTS Notify error : $telegramUser->telegramId ".$e->getMessage());
            return response()->json([
                'success' => false,
                'error' => 'Bot was blocked by user'
            ]);
        }
        return response()->json([
            'success' => true
        ]);
    }

    public function sendDoctorNotificationAida(Request $request){
        try{
            $regNo = $request->reg_no;
            $fullname = $request->fullname;
            $phone = $request->phone;
            $insCard = $request->ins_card;
            $age = $request->age;
            $reason = $request->reason;
            $street = $request->street;
            $building = $request->building;
            $apartmentNo = $request->apartment_no;
            $messageText = "<b>Новая заявка №$regNo</b> : \n
<b>ФИО</b> : $fullname\n
<b>Телефон</b> : $phone\n
<b>Номер страховки</b> : $insCard\n
<b>Возраст</b> : $age\n
<b>Причина</b> : $reason\n
<b>Улица</b> : $street\n
<b>Дом</b> : $building\n
<b>Квартира</b> : $apartmentNo\n";
            $group = MessageGroup::findOrFail(2);
            $staffEmails = $group->getUsersArray();
            foreach ($staffEmails as $staffEmail){
                $telegramUser = StaffTelegramUsers::where('staffEmail', $staffEmail)->first();
                Telegram::sendMessage([
                    'chat_id' => $telegramUser->telegramId,
                    'text' => $messageText,
                    'parse_mode' => 'html'
                ]);
            }
            return response()->json([
                'success' => true
            ]);
        }catch (\Exception $exception){
            Log::debug("AIDA DOCTOR EXCEPTION : ".$exception->getMessage()." ".$exception->getLine());
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage(),
            ]);
        }
    }

    public function getOlData(Request $request){
        try{
            [$success, $error] = $this->verifyOlRequest($request);
            if (!$success){
                return $error;
            }

            $chatId = StaffTelegramUsers::getChatIdByIsn($request->userIsn);
            if ($chatId === 0) {
                return response()->json([
                    'success' => false,
                    'error' => 'User not found'
                ]);
            }
            $solutionText = (int)$request->solution === 1 ? "Согласовал ": "Отказал";

            $message = "Обходной лист № : $request->docNum\n";
            $message .= $solutionText." ".$request->emplName;
            if (($remark = $request->get('remark', null)) !== null) {
                $message .= "\nПримечание : $remark";
            }
            if ( count($request->get('emplList', [])) > 0 ) {
                $message .= "\nНе согласовали : \n";
                $employees = [];
                foreach ($request->emplList as $empl){
                    $employees[] = $empl['name'];
                }
                $message .= implode(",\n", $employees);
            }
            Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => $message,
                'parse_mode' => 'html'
            ]);
            return response()->json(['success' => true]);
        } catch (\Exception $exception) {
          Log::debug("OL Message error : ".$exception->getMessage());
          return response()->json([
              'success' => false,
              'error' => 'Uncaught Exception'
          ]);
        }
    }

    public function verifyOlRequest($request): array
    {
        if ($request->get('userIsn', null) === null){
            return [false, $this->responseNotFilled('userIsn')];
        }
        if ($request->get('solution', null) === null){
            return [false, $this->responseNotFilled('solution')];
        }
        if ($request->get('emplName', null) === null){
            return [false, $this->responseNotFilled('emplName')];
        }
        if ($request->get('docNum', null) === null){
            return [false, $this->responseNotFilled('docNum')];
        }
        return [true, null];
    }
    public function responseNotFilled($field){
        return response()->json([
            'success' => false,
            'error' => "{ $field } is not filled"
        ]);
    }

    public function getBuildings()
    {
        try {
            $buildings = BookingBuilding::get();
            return response()->json([
                'success' => true,
                'data' => [
                    'buildings' => $buildings
                ],
                'error' => [
                    'code' => null,
                    'message' => null
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            ]);
        }
    }

    public function getBuildingByIdWithRooms(Request $request)
    {
        try {
            $buildingWithRooms = BookingBuilding::with('rooms')->find($request->id);
            return response()->json([
                'success' => true,
                'data' => [
                    'building' => $buildingWithRooms
                ],
                'error' => [
                    'code' => null,
                    'message' => null
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            ]);
        }
    }

    public function getBookingRecords(Request $request)
    {
        try {
            $bookRecords = BookRecord::where('room', $request->room)
                ->where('date', $request->date)
                ->orderBy('start')
                ->get();
            return response()->json([
                'success' => true,
                'data' => [
                    'book_records' => $bookRecords
                ],
                'error' => [
                    'code' => null,
                    'message' => null
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ],
            ]);
        }
    }

    public function getRooms(Request $request)
    {
        try {
            $rooms = Rooms::where('building_id', $request->id)->get();
            return response()->json([
                'success' => true,
                'data' => [
                    'rooms' => $rooms
                ],
                'error' => [
                    'code' => null,
                    'message' => null
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            ]);
        }
    }

    public function createRecord(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'date' => 'required',
                'start' => 'required',
                'end' => 'required',
                'room' => 'required',
                'fio' => 'required',
                'outsystemsUserId' => 'required',
                'buildingName' => 'required'
            ]);

            $records = BookRecord::where('date', $validatedData['date'])->where('room', $validatedData['room'])->get();

            $isBusyForStartTime = false;
            $isBusyForEndTime = false;

            if(count($records)){
                $startTime;
                $customStart = strtotime($validatedData['start']);
                foreach ($records as $record) {
                    $start = strtotime($record->start);
                    $end = strtotime($record->end);
                    if ($customStart >= $start && $customStart <= $end) {
                        $isBusyForStartTime = true;
                    }
                }

                if($isBusyForStartTime) {
                    return response()->json([
                        'success' => false,
                        'error' => [
                            'code' => null,
                            'message' => 'Данное время занято. Попробуйте заново'
                        ]
                    ]);
                } else {
                    $startTime = $customStart;
                }

                $customEnd = strtotime($validatedData['end']);
                foreach ($records as $record) {
                    $startRecord = strtotime($record->start);
                    $endRecord = strtotime($record->end);
                    $startTimeTelegramRequestLog = $startTime;
                    if($startTimeTelegramRequestLog <= $endRecord){
                      if ($customEnd >= $startRecord && $customEnd <= $endRecord || $customEnd >= $endRecord) {
                           $isBusyForEndTime = true;
                        }
                    }
                }

                if($isBusyForEndTime) {
                    return response()->json([
                        'success' => false,
                        'error' => [
                            'code' => null,
                            'message' => 'Данное время занято. Попробуйте заново'
                        ]
                    ]);
                }
            }

            BookRecord::create($validatedData);

            return response()->json([
                'success' => true,
                'data' => [
                    "message" => 'created'
                ],
                'error' => [
                    'code' => null,
                    'message' => null
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            ]);
        }
    }

    public function getUserRecords(Request $request)
    {

        try {
            
            $validatedData = $request->validate([
                'outsystemsUserId' => 'required'
            ]);

            $userRecords = BookRecord::where('outsystemsUserId', $validatedData['outsystemsUserId'])
                                      ->groupBy('date','start')
                                      ->get();
            
            return response()->json([
                'success' => true,
                'data' => [
                    "userRecords" => $userRecords
                ],
                'error' => [
                    'code' => null,
                    'message' => null
                ]
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            ]);
        }
    }

     public function deleteRecordById(Request $request)
        {
            try {
                $validatedData = $request->validate([
                    'id' => "required"
                ]);
                BookRecord::find($validatedData['id'])->delete();
                return response()->json([
                    'success' => true,
                    'data' => [
                        "message" => "deleted"
                    ],
                    'error' => [
                        'code' => null,
                        'message' => null
                    ]
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]
                ]);
            }
        }

        public function getClinics() 
        {
            try {
                $clinics = Clinics::all();
                return response()->json([
                    'success' => true,
                    'data' => [
                        "clinics" => $clinics
                    ],
                    'error' => [
                        'code' => null,
                        'message' => null
                    ]
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]
                ]);
            }
        }

        public function getDoctors(Request $request)
        {
            try {
                $validatedData = $request->validate([
                    'name' => 'required'
                ]);
                $doctors = Clinics::where('name', $validatedData['name'])->first()->doctors;
                return response()->json([
                    'success' => true,
                    'data' => [
                        "doctors" => $doctors
                    ],
                    'error' => [
                        'code' => null,
                        'message' => null
                    ]
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]
                ]);
            }
        }

        public function sendToCoordinators(Request $request)
        {
            try {
                $validatedData = $request->validate([
                    'FIO' => 'required',
                    'phoneNumber' => 'required',
                    'selectedClinic' => 'required',
                    'selectedDoctor' => 'required',
                    'reason' => 'required',
                    'insuredOrNot' => 'required',
                    'date' => 'required',
                    'cardNumber' => 'nullable'
                ]);
                $userFIO = explode(' ', $validatedData['FIO']);
                $text = "\n Имя: ".$userFIO[1].
                    "\nФамилия: ".$userFIO[0].
                    "\nКонтактный номер : ".$validatedData['phoneNumber'].
                    "\nКлиника : ".$validatedData['selectedClinic'].
                    "\nВрач : ".$validatedData['selectedDoctor'].
                    "\nПричина обращения : ".$validatedData['reason'].
                    "\nПредполагаемая дата : ".$validatedData['date'].
                    "\nЗастрахован(a) : ".$validatedData['insuredOrNot'];

                    if(isset($validatedData['cardNumber'])){
                        $text.= "\nНомер страховой карточки : ".$validatedData['cardNumber'];
                    }
                DoctorAppointment::create([
                    'first_name' => $userFIO[1],
                    'last_name' => $userFIO[0],
                    'phone_number' => $validatedData['phoneNumber'],
                    'clinic' => $validatedData['selectedClinic'],
                    'doctor' => $validatedData['selectedDoctor'],
                    'reason' => $validatedData['reason'],
                    'insured' => $validatedData['insuredOrNot'],
                    'date' => $validatedData['date'],
                    'card_number' => $validatedData['cardNumber'] ?? null
                ]);

                $group = MessageGroup::findOrFail(10);
                $staffEmails = $group->getUsersArray();
                foreach ($staffEmails as $staffEmail){
                        $telegramUser = StaffTelegramUsers::where('staffEmail', $staffEmail)->first();
                        Telegram::sendMessage([
                            'chat_id' => $telegramUser->telegramId,
                            'text' => $text,
                            'parse_mode' => 'html'
                        ]);
                }

                return response()->json([
                    'success' => true,
                    'data' => [
                        "message" => "your form has been submitted"
                    ],
                    'error' => [
                        'code' => null,
                        'message' => null
                    ]
                ]);
                
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]
                ]);
            }
        }

        public function getEmployees(){
            try {
                $employees = BotanStaff::all();
                return response()->json([
                    'success' => true,
                    'data' => [
                        "employees" => $employees
                    ],
                    'error' => [
                        'code' => null,
                        'message' => null
                    ]
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]
                ]);
            }
        }

        public function getOutsystemsVacation(Request $request) {
            try {

                $validatedData = $request->validate([
                    'ISN' => 'required',
                    'companyName' => 'required',
                ]);

                if($validatedData['companyName'] == "SOS Medical Assistance ТОО"){
                    $vacationData = StaffController::getVacationSOSMA($validatedData['ISN']);
                    if($vacationData['success'] == true){
                        return response()->json([
                            'success' => true,
                            'data' => [
                                "vacation" => $vacationData["result"]
                            ],
                            'error' => [
                                'code' => null,
                                'message' => null
                            ]
                        ]);  
                    }else{
                        return response()->json([
                            'success' => false,
                            'error' => [
                                'message' => $vacationData['text']
                        ]]);
                    }
                }else{
                    $vacationData = StaffController::getVacation($validatedData['ISN']);
                    if($vacationData['success'] == true){
                        return response()->json([
                            'success' => true,
                            'data' => [
                                "vacation" => $vacationData["result"]
                            ],
                            'error' => [
                                'code' => null,
                                'message' => null
                            ]
                        ]);  
                    }else{
                        return response()->json([
                            'success' => false,
                            'error' => [
                                'message' => $vacationData['text']
                        ]]);
                    }
                }
            } catch(\Exception $e){
                return response()->json([
                    'success' => false,
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage()
                    ]
                ]);
            }
        }
}
