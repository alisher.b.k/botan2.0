<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $feedbackMessages = Feedback::all();
        return view('feedback', ['feedbackMessages' => $feedbackMessages]);
    }
}
