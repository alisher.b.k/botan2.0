<?php

namespace App\Http\Controllers;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\QuestionCategories;
use App\QuestionsAnswers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionsAnswersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAllQuestionsAndAnswers(){
        $questions = QuestionsAnswers::all();
        $last = QuestionsAnswers::count();
        return view('questions_answers', ['questions' => $questions, 'last' => $last+1]);
    }

    public function editQuestion($id)
    {
        $question = QuestionsAnswers::find($id);
        if(!$question){
            $question = null;
        }
        $categories = QuestionCategories::where('parent_id', '!=', 0)->get();
        $companies = Companies::all();
        return view('editQuestion', ['question' => $question, 'last'=>$id, 'categories' => $categories, 'companies'=>$companies]);
    }

    public function saveQuestion(Request $request)
    {
        $question = QuestionsAnswers::find($request->questionId);
        if(!$question){
            $question = new QuestionsAnswers();
            $question->id = $request->questionId;
        }
        $question->companyId = $request->companyId;
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->categoryId = $request->category;
        $question->save();

        $questions = QuestionsAnswers::all();
        $last = QuestionsAnswers::count();
        return redirect()->route('questionsAnswers', ['questions' => $questions, 'last' => $last+1]);
    }

    public function deleteQuestion($id)
    {
        QuestionsAnswers::destroy($id);
        $last = QuestionsAnswers::count();
        $i = 1;
        $questions = QuestionsAnswers::all();
        foreach($questions as $question){
            $question->id = $i;
            $question->save();
            $i++;
        }

        return redirect()->route('questionsAnswers', ['questions' => $questions, 'last' => $last+1]);
    }
}
