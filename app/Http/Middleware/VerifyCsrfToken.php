<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'botan/login',
        'botan/users/search',
        'botan/login/code',
        'botan/dialogues/history',
        'botan/answer/get',
        'botan/users/search',
        'botan/telegram/calc',
        '519488420:*',
        '692018914:*',
        'notification',
        'serviceCenterNotify',
        'updateStaff',
        'updateStaffSOSMA',
        'booking/data',
        'booking/buildings',
        'booking/records',
        'booking/rooms',
        'ogpovts/notification',
        'aida/notification/doctor',
        'api/get-ol-data',
        'api/send-reconciliation',
        'send/mail'
    ];
}
