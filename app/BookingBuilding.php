<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingBuilding extends Model
{
    function rooms(){
        return $this->hasMany('App\Rooms');
    }
}
