<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultOfReaction extends Model
{
    protected $table = 'result_of_reaction';
}
