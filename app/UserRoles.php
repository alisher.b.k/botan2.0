<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    public function roleName()
    {
        return $this->hasOne('App\Roles', 'id', 'role_id');
    }
}
