<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageGroup extends Model
{
    public function users(){
        return $this->hasMany('App\GroupUser', 'group_id', 'id');
    }

    public function getUsersArray(){
        $usersList = [];
        foreach ($this->users as $user) {
            array_push($usersList, $user->telegramId);
        }
        return $usersList;
    }
}