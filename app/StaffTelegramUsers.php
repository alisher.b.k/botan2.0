<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffTelegramUsers extends Model
{
    protected $table = 'stafftelegramusers';

    public $FIO = null;
    public function getFullName (){
        $user = BotanStaff::where('ISN', $this->staffEmail)->orWhere('email', $this->staffEmail)->first();
        $this->FIO = $user->FIO ?? $user;
        return $this->FIO;
    }

    public function getStaff(){
        if (is_numeric($this->staffEmail))
            return $this->hasOne(BotanStaff::class, 'ISN', 'staffEmail');
        return $this->hasOne(BotanStaff::class, 'email', 'staffEmail');
    }

    public static function getWorkPlaceByChatId($chatId){
        $user = self::getUser($chatId);
        return $user === null ? "" : ($user->WORKPLACE ?? "");
    }

    public static function getUserIsnByChatId($chatId){
        $user = self::getUser($chatId);
        return $user === null ? "" : ($user->ISN ?? "");
    }

    public static function getUserIsnAndWorkPlaceByChatId($chatId){
        $user = self::getUser($chatId);
        $isn = $user === null ? "" : ($user->ISN ?? "");
        $workplace =  $user === null ? "" : ($user->WORKPLACE ?? "");
        return [$isn, $workplace];
    }

    public static function getChatIdByIsn($isn){
        $staffUser = BotanStaff::where('ISN', $isn)->first();
        if ($staffUser === null)
            return 0;
        $user = self::whereIn('staffEmail', [$staffUser->ISN, $staffUser->email])->first();
        return $user === null ? 0 : $user->telegramId;
    }

    public static function getUser($chatId){
        $authData = self::where('telegramId', $chatId)->first();
        if($authData === null){
            return null;
        }
        $user = BotanStaff::where('ISN', $authData->staffEmail)->orWhere('email', $authData->staffEmail)->first();
        return $user;
    }
}
