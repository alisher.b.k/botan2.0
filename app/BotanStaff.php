<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BotanStaff extends Model
{
    protected $table = 'botan_staff';

    public static function boot()
    {
        parent::boot();

        BotanStaff::deleting(function($model)
        {
            $emails = StaffEmail::where('userId', $model->id)->get();
            if(count($emails) > 0){
                foreach ($emails as $email)
                {
                    $email->delete();
                }
            }
        });
    }

    public function balanceByIsn()
    {
        return $this->hasOne('App\StaffBalance', 'isn', 'ISN');
    }

    public function balanceByEmail()
    {
        return $this->hasOne('App\StaffBalance', 'isn', 'email');
    }
        public function transactionByEmail()
    {
        return $this->hasOne('App\Transaction', 'isn', 'email');
    }   
    public function transactionByIsn()
    {
        return $this->hasOne('App\Transaction', 'isn', 'ISN');
    }

    public function typeMessages(){
        return $this->belongsToMany("App\TypeMessages");
    }
}