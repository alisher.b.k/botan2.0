<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Ixudra\Curl\Facades\Curl;

class CalculatorCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "calculator";

    /**
     * @var string Command Description
     */
    protected $description = "";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $response = "Выберите номер вида страхования:
                \n 1. НС
                \n 2. ДКС
                \n 3. МСТ
               ";
//              \n 4. ОГПО

              $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true]);

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;

                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $response,
                  "userEmail" => $userEmail,
                ];

                BotanDialogHistories::create($historyArr);

              $log = new TelegramRequestLog();
              $log->telegramId = $chatId;
              $log->command = 'calculator';
              $log->save();
            }
        } catch (\Exception $e) {
            Log::debug('/calculator ' . $e->getMessage());
        }
    }

    public static function insuranceType($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                $logArr = [
                    "command" => 'calculator' . $messageText,
                    "telegramId" => $chatId,
                ];

                $response = "";

                if ($messageText === '1') {
                    $response = "Выберите тип поездки: \n";
                    $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                               <soapenv:Header/>
                               <soapenv:Body>
                                  <urn:getIproductList soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>
                               </soapenv:Body>
                            </soapenv:Envelope>";
                    $IproductList = Curl::to('http://api.kupipolis.kz/passins/index/ws/1/scope/api')
                        ->withContentType('application/xml')
                        ->withTimeout(60 * 10)
                        ->withData($xml)
                        ->post();
                    $IproductList = str_replace('ns1:', '', $IproductList);
                    $result = simplexml_load_string($IproductList);
                    $result = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/getIproductListResponse/return/item');
                    for ($i = 0; $i < count($result); $i++){
                        $response .= $result[$i]->value.' /'.$result[$i]->key."\n";
                    }

                } elseif ($messageText === '2') {
                    $response = "Выберите пакет страхования или покрытие: \n\n";
                    $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                               <soapenv:Header/>
                               <soapenv:Body>
                                  <urn:getPackageList soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>
                               </soapenv:Body>
                            </soapenv:Envelope>";
                    $packageList = Curl::to('http://api.kupipolis.kz/v1/complexpass/index/ws/1/scope/api')
                        ->withContentType('application/xml')
                        ->withTimeout(60 * 10)
                        ->withData($xml)
                        ->post();
                    $packageList = str_replace('ns1:', '', $packageList);
                    $result = simplexml_load_string($packageList);
                    $result = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/getPackageListResponse/return/item');
                    for ($i = 0; $i < count($result); $i++){
                        $response .= $result[$i]->value.' /'.$result[$i]->key."\n";
                    }

                    $response .= "\n";

                    $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                               <soapenv:Header/>
                               <soapenv:Body>
                                  <urn:getCoverTypeList soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>
                               </soapenv:Body>
                            </soapenv:Envelope>";
                    $coverList = Curl::to('http://api.kupipolis.kz/v1/complexpass/index/ws/1/scope/api')
                        ->withContentType('application/xml')
                        ->withTimeout(60 * 10)
                        ->withData($xml)
                        ->post();
                    $coverList = str_replace('ns1:', '', $coverList);
                    $result = simplexml_load_string($coverList);
                    $result = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/getCoverTypeListResponse/return/item');
                    for ($i = 0; $i < count($result); $i++){
                        $response .= $result[$i]->value.' /'.$result[$i]->key."\n";
                    }
                } elseif ($messageText === '3') {
                    $response = "Выберите территорию страхования: \n\n";
                    $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <urn:getTerritoryList soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>
                       </soapenv:Body>
                    </soapenv:Envelope>";
                    $packageList = Curl::to('http://api.kupipolis.kz/tour/index/ws/1/scope/api')
                        ->withContentType('application/xml')
                        ->withTimeout(60 * 10)
                        ->withData($xml)
                        ->post();
                    $packageList = str_replace('ns1:', '', $packageList);
                    $result = simplexml_load_string($packageList);
                    $result = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/getTerritoryListResponse/return/item');
                    for ($i = 0; $i < count($result); $i++){
                        $response .= $result[$i]->value.' /'.$result[$i]->key."\n";
                    }
                }

                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                TelegramRequestLog::create($logArr);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $userEmail,
                ];
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/insuranceType' . $e->getMessage());
        }
    }

    public static  function calculateNS($chatId, $messageText, $username, $firstname, $lastname, $result){
        try {
            if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                $messageText = strtoupper(substr($messageText, 1));
                    $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                               <soapenv:Header/>
                               <soapenv:Body>
                                  <urn:calculate soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">
                                     <iproduct xsi:type=\"xsd:string\">".$messageText."</iproduct>
                                  </urn:calculate>
                               </soapenv:Body>
                            </soapenv:Envelope>";
                    $calculateResponse = Curl::to('http://api.kupipolis.kz/passins/index/ws/1/scope/api')
                        ->withContentType('application/xml')
                        ->withTimeout(60 * 10)
                        ->withData($xml)
                        ->post();
                    $calculateResponse = str_replace('ns1:', '', $calculateResponse);
                    $result = simplexml_load_string($calculateResponse);
                    $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/calculateResponse/return');
                    if (!$response) {
                        $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring');
                    }
                    $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true]);
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $response[0].'',
                        'reply_markup' => $reply_markup,
                    ]);
                    $historyArr = [
                        "chatId" => $chatId,
                        "telegramUsername" => $username,
                        "telegramFirstName" => $firstname,
                        "telegramLastName" => $lastname,
                        "response" => $response[0],
                        "userEmail" => $result['user']->email,
                    ];
                    BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/getPrice' . $e->getMessage());
        }
    }

    public static  function getFlightType($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                $logArr = [
                    "command" => $telegramRequestLog->command . 'flight_type',
                    "telegramId" => $chatId,
                ];
                $messageText = strtoupper(substr($messageText, 1));

                $isPackage = substr($messageText, 0,4) == 'PACK';
                if ($isPackage) {
                    $new_array = array('data'=>json_encode(array('package' => $messageText)));
                } else {
                    $new_array = array('data'=>json_encode(array('cover_type' => $messageText)));
                }
                $logArr=array_merge($logArr, $new_array);

                $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <urn:getFlightTypeList soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>
                           </soapenv:Body>
                        </soapenv:Envelope>";
                $flightTypeList = Curl::to('http://api.kupipolis.kz/v1/complexpass/index/ws/1/scope/api')
                    ->withContentType('application/xml')
                    ->withTimeout(60 * 10)
                    ->withData($xml)
                    ->post();
                $flightTypeList = str_replace('ns1:', '', $flightTypeList);
                $result = simplexml_load_string($flightTypeList);
                $result = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/getFlightTypeListResponse/return/item');
                if (!$result) {
                    $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring');
                } else{
                    $response = "Выберите тип перелета:\n";
                    for ($i = 0; $i < count($result); $i++){
                        $response .= $result[$i]->value.' /'.$result[$i]->key."\n";
                    }
                }
                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                TelegramRequestLog::create($logArr);

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/getFlightType' . $e->getMessage());
        }
    }

    public static function getTripType($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                $logArr = [
                    "command" => $telegramRequestLog->command . 'trip_type',
                    "telegramId" => $chatId,
                    "data" => json_decode($telegramRequestLog->data, true),
                ];
                $messageText = strtoupper(substr($messageText, 1));

                $new_array = array('flight_type'=>$messageText);
                $logArr['data']=json_encode(array_merge($logArr['data'], $new_array));

                $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <urn:getTripTypeList soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>
                           </soapenv:Body>
                        </soapenv:Envelope>";
                $tripTypeList = Curl::to('http://api.kupipolis.kz/v1/complexpass/index/ws/1/scope/api')
                    ->withContentType('application/xml')
                    ->withTimeout(60 * 10)
                    ->withData($xml)
                    ->post();
                $tripTypeList = str_replace('ns1:', '', $tripTypeList);
                $result = simplexml_load_string($tripTypeList);
                $result = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/getTripTypeListResponse/return/item');
                if (!$result) {
                    $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring');
                } else{
                    $response = "Выберите тип поездки:\n";
                    for ($i = 0; $i < count($result); $i++){
                        $response .= $result[$i]->value.' /'.$result[$i]->key."\n";
                    }
                }
                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                TelegramRequestLog::create($logArr);

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/getTripType' . $e->getMessage());
        }
    }

    public static function calculateDKS($chatId, $messageText, $username, $firstname, $lastname, $result){
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $messageText = strtoupper(substr($messageText, 1));

                $data = json_decode($telegramRequestLog->data, true);

                $package = array_key_exists('package', $data) ? $data['package'] : '';
                $cover_type = array_key_exists('cover_type', $data) ? $data['cover_type'] : '';
                $flight_type = array_key_exists('flight_type', $data) ? $data['flight_type'] : '';
                $trip_type = $messageText;


                $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <urn:calculate soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">
                                 <package xsi:type=\"xsd:string\">".$package."</package>
                                 <cover_type xsi:type=\"xsd:anyType\">".$cover_type."</cover_type>
                                 <flight_type xsi:type=\"xsd:string\">".$flight_type."</flight_type>
                                 <trip_type xsi:type=\"xsd:string\">".$trip_type."</trip_type>
                              </urn:calculate>
                           </soapenv:Body>
                        </soapenv:Envelope>";
                $calculateResponse = Curl::to('http://api.kupipolis.kz/v1/complexpass/index/ws/1/scope/api')
                    ->withContentType('application/xml')
                    ->withTimeout(60 * 10)
                    ->withData($xml)
                    ->post();
                $calculateResponse = str_replace('ns1:', '', $calculateResponse);
                $result = simplexml_load_string($calculateResponse);
                $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/calculateResponse/return');
                if (!$response) {
                    $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring');
                }
                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response[0].'',
                    'reply_markup' => $reply_markup,
                ]);
                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response[0],
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/calculateDKS' . $e->getMessage());
        }
    }

    public static  function getPrice($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $messageText = substr($messageText, 1);

                $logArr = [
                    "command" => $telegramRequestLog->command . '_get_price',
                    "telegramId" => $chatId,
                    'data' => json_encode(array('territory' => $messageText))
                ];

                $response = "Выберите страховую сумму:\n";

                if ($messageText === "first") {
                    $response .= "10000 /10000 \n20000 /20000 \n30000 /30000";
                } elseif ($messageText === "second") {
                    $response .= "30000 /30000 \n50000 /50000";
                } elseif ($messageText === "third" || $messageText === "world") {
                    $response .= "70000 /70000 \n100000 /100000";
                }

                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                TelegramRequestLog::create($logArr);

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/getPrice' . $e->getMessage());
        }
    }

    public static  function departureDate($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $messageText = substr($messageText, 1);

                $logArr = [
                    "command" => $telegramRequestLog->command . '_departure_date',
                    "telegramId" => $chatId,
                    'data' => json_decode($telegramRequestLog->data, true)
                ];

                $new_array = array('price' => $messageText);
                $logArr['data']=json_encode(array_merge($logArr['data'], $new_array));

                $response = "Введите дату отъезда в формате ДД.ММ.ГГГГ\n";

                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                TelegramRequestLog::create($logArr);

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/getPrice' . $e->getMessage());
        }
    }

    public static  function arrivalDate($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();

                $date = $dateService->validateDate($messageText);
                if (!$date) {
                    $response = "Вы ввели некорректную дату, введите в формате ДД.ММ.ГГГГ\n";
                } else {
                    TelegramRequestLog::where('telegramId', $chatId)->delete();

                    $logArr = [
                        "command" => $telegramRequestLog->command . '_arrival_date',
                        "telegramId" => $chatId,
                        'data' => json_decode($telegramRequestLog->data, true)
                    ];

                    $new_array = array('departure_date' => $messageText);
                    $logArr['data'] = json_encode(array_merge($logArr['data'], $new_array));

                    $response = "Введите дату возвращения в формате ДД.ММ.ГГГГ\n";

                    TelegramRequestLog::create($logArr);
                }
                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/arrivalDate' . $e->getMessage());
        }
    }

    public static  function getDiscountList($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();

                $date = validateDate($messageText);
                if (!$date) {
                    $response = "Вы ввели некорректную дату, введите в формате ДД.ММ.ГГГГ\n";
                } else {
                    TelegramRequestLog::where('telegramId', $chatId)->delete();

                    $logArr = [
                        "command" => $telegramRequestLog->command . '_discount',
                        "telegramId" => $chatId,
                        'data' => json_decode($telegramRequestLog->data, true)
                    ];

                    $new_array = array('arrival_date' => $messageText);
                    $logArr['data'] = json_encode(array_merge($logArr['data'], $new_array));

                    $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                               <soapenv:Header/>
                               <soapenv:Body>
                                  <urn:getDiscountList soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>
                               </soapenv:Body>
                            </soapenv:Envelope>";
                    $discountList = Curl::to('http://api.kupipolis.kz/tour/index/ws/1/scope/api')
                        ->withContentType('application/xml')
                        ->withTimeout(60 * 10)
                        ->withData($xml)
                        ->post();
                    $discountList = str_replace('ns1:', '', $discountList);
                    $result = simplexml_load_string($discountList);
                    $result = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/getDiscountListResponse/return/item');
                    if (!$result) {
                        $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring');
                    } else{
                        $response = "Выберите скидку:\n";
                        for ($i = 0; $i < count($result); $i++){
                            $response .= $result[$i]->value.' /'.$result[$i]->key."\n";
                        }
                    }

                    TelegramRequestLog::create($logArr);
                }

                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);



                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/getDiscountList' . $e->getMessage());
        }
    }

    public static  function getLoadingList($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();

                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $messageText = substr($messageText, 1);

                $logArr = [
                    "command" => $telegramRequestLog->command . '_loading',
                    "telegramId" => $chatId,
                    'data' => json_decode($telegramRequestLog->data, true)
                ];

                $new_array = array('discount' => $messageText);
                $logArr['data'] = json_encode(array_merge($logArr['data'], $new_array));

                $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <urn:getLoadingList soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>
                           </soapenv:Body>
                        </soapenv:Envelope>";
                $loadingList = Curl::to('http://api.kupipolis.kz/tour/index/ws/1/scope/api')
                    ->withContentType('application/xml')
                    ->withTimeout(60 * 10)
                    ->withData($xml)
                    ->post();
                $loadingList = str_replace('ns1:', '', $loadingList);
                $result = simplexml_load_string($loadingList);
                $result = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/getLoadingListResponse/return/item');
                if (!$result) {
                    $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring');
                } else{
                    $response = "Выберите надбавку:\n";
                    for ($i = 0; $i < count($result); $i++){
                        $response .= $result[$i]->value.' /'.$result[$i]->key."\n";
                    }
                }

                TelegramRequestLog::create($logArr);


                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);



                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/getLoadingList' . $e->getMessage());
        }
    }

    public static function iin($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $messageText = substr($messageText, 1);

                $logArr = [
                    "command" => $telegramRequestLog->command . '_iin',
                    "telegramId" => $chatId,
                    'data' => json_decode($telegramRequestLog->data, true)
                ];

                $new_array = array('loading' => $messageText);
                $logArr['data']=json_encode(array_merge($logArr['data'], $new_array));

                $response = "Введите ИИН\n";

                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                TelegramRequestLog::create($logArr);

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $response,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/iin' . $e->getMessage());
        }
    }

    public static function calculateMst($chatId, $messageText, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);

                $iin=trim(strip_tags($messageText)); // отделяем лишние пробелы и предполагаемые посторонние данные
                if (strlen($iin)<12) { // Если введенный ИИН меньше 12 цифр
                    $response = 'Номер ИИН введен неверно, он должен состоять из двенадцати цифр. Введите заново';
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $response,
                        'reply_markup' => $reply_markup,
                    ]);
                    $historyArr = [
                        "chatId" => $chatId,
                        "telegramUsername" => $username,
                        "telegramFirstName" => $firstname,
                        "telegramLastName" => $lastname,
                        "response" => $response,
                        "userEmail" => $result['user']->email,
                    ];
                } elseif (ctype_digit($iin) != TRUE) { // Если введенный ИИН не состоит только из цифр
                    $response = 'Номер ИИН введен неверно, он должен состоять только из цифр. Введите заново';
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $response,
                        'reply_markup' => $reply_markup,
                    ]);
                    $historyArr = [
                        "chatId" => $chatId,
                        "telegramUsername" => $username,
                        "telegramFirstName" => $firstname,
                        "telegramLastName" => $lastname,
                        "response" => $response,
                        "userEmail" => $result['user']->email,
                    ];
                } else {
                    $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();

                    TelegramRequestLog::where('telegramId', $chatId)->delete();

                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => "Пожалуйста подождите, ваш запрос обрабатывается",
                        'reply_markup' => $reply_markup,
                    ]);

                    $data = json_decode($telegramRequestLog->data, true);

                    $territory = array_key_exists('territory', $data) ? $data['territory'] : '';
                    $price = array_key_exists('price', $data) ? $data['price'] : '';
                    $departure_date = array_key_exists('departure_date', $data) ? $data['departure_date'] : '';
                    $arrival_date = array_key_exists('arrival_date', $data) ? $data['arrival_date'] : '';
                    $discount = array_key_exists('discount', $data) ? $data['discount'] : '';
                    $loading = array_key_exists('loading', $data) ? $data['loading'] : '';
                    $iin = $messageText;

                    $xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:kupipolisApiOrderWsdl\">
                               <soapenv:Header/>
                               <soapenv:Body>
                                  <urn:calculate soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">
                                    <authcode xsi:type=\"xsd:string\"></authcode>
                                     <territory xsi:type=\"xsd:string\">" . $territory . "</territory>
                                     <price xsi:type=\"xsd:string\">" . $price . "</price>
                                     <departure_date xsi:type=\"xsd:date\">" . $departure_date . "</departure_date>
                                     <arrival_date xsi:type=\"xsd:date\">" . $arrival_date . "</arrival_date>
                                     <discount xsi:type=\"xsd:string\">" . $discount . "</discount>
                                     <loading xsi:type=\"xsd:string\">" . $loading . "</loading>
                                     <iin xsi:type=\"xsd:string\">" . $iin . "</iin>
                                  </urn:calculate>
                               </soapenv:Body>
                            </soapenv:Envelope>";

                    $calculateResponse = Curl::to('http://api.kupipolis.kz/tour/index/ws/1/scope/api')
                        ->withContentType('application/xml')
                        ->withTimeout(60 * 10)
                        ->withData($xml)
                        ->post();
                    $calculateResponse = str_replace('ns1:', '', $calculateResponse);
                    $result = simplexml_load_string($calculateResponse);
                    $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/calculateResponse/return');
                    if (!$response) {
                        $response = $result->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring');
                    }
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $response[0].' ',
                        'reply_markup' => $reply_markup,
                    ]);
                    $historyArr = [
                        "chatId" => $chatId,
                        "telegramUsername" => $username,
                        "telegramFirstName" => $firstname,
                        "telegramLastName" => $lastname,
                        "response" => $response[0].'',
                        "userEmail" => $result['user']->email,
                    ];
                }

                BotanDialogHistories::create($historyArr);

            }
        } catch (\Exception $e) {
            Log::debug('/calculateMst' . $e->getMessage());
        }
    }
}

function validateDate($date, $format = 'd.m.Y')
{
    $d = \DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}
