<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\Problem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\CustomCommand;
use Ixudra\Curl\Facades\Curl;


class ReservationCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "reservation";

    /**
     * @var string Command Description
     */
    protected $description = "";

    /**
     * @inheritdoc
     */
    public function handle()
    {
      $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
      $chatId = $chat->getId();
      $username = $chat->getUsername();
      $firstname = $chat->first_name;
      $lastname = $chat->last_name;
      $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
      $messageText = strtolower($message->getText());

      $result = CheckUser::index($chatId);

      $historyArr = [
        "chatId" => $chatId,
        "telegramUsername" => $username,
        "telegramFirstName" => $firstname,
        "telegramLastName" => $lastname,
        "request" => $messageText,
      ];

      // запись запроса в историю диалогов
      $historyArr["userEmail"] = $result['user']
        ? $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN
        : '';
      BotanDialogHistories::create($historyArr);

      $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
    }

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          $reply_markup = Keyboard::make(['keyboard' => 
            [
              // ['Комната №1', 'Комната №2', 'Комната №3'],
              ['На главную'],
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);


          TelegramRequestLog::where('telegramId', $chatId)->delete();
          
          $res = "Команда находится в разработке"; //Выберите переговорную


          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup,
              'parse_mode'=> 'markdown']);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $res,
            "userEmail" => $userEmail,
          ];
          BotanDialogHistories::create($historyArr);

          $log = new TelegramRequestLog();
          $log->telegramId = $chatId;
          $log->command = 'reservation';
          $log->save();
        }
      } catch (\Exception $e) {
          Log::debug('/reservation ' . $e->getMessage());
      }
    }
    
    public static function handleRoomChoice($chatId, $messageText, $username, $firstname, $lastname, $result) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          $reply_markup = Keyboard::make(['keyboard' => 
            [
              ['Забронировать', 'Удалить броню'],
              ['На главную'],
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);


          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          $telegramRequestLog->command = 'reservation_handleRoomChoice';
          $telegramRequestLog->data = json_encode(array('room' => $messageText));
          $telegramRequestLog->save();

          $res = "Если хотите забронировать комнату переговоров нажмите на кнопку *«Забронировать»*
                \nЕсли хотите удалить ранее сделанную броню нажмите на кнопку *«Удалить броню»*";


          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup,
              'parse_mode'=> 'markdown']);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $res,
            "userEmail" => $userEmail,
          ];
          BotanDialogHistories::create($historyArr);
        }
      } catch (\Exception $e) {
          Log::debug('/reservation handleRoomChoice' . $e->getMessage());
      }
    } 
}
