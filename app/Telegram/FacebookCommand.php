<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\QuestionsAnswers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;

class FacebookCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "facebook";

    /**
     * @var string Command Description
     */
    protected $description = "Найду коллегу";


    /**
     * @inheritdoc
     */
    public function handle()
    {
        $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
        $chatId = $chat->getId();
        $username = $chat->getUsername();
        $firstname = $chat->first_name;
        $lastname = $chat->last_name;
        $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
        $messageText = strtolower($message->getText());

        $result = CheckUser::index($chatId);

        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "request" => $messageText,
        ];

        // запись запроса в историю диалогов
        $historyArr["userEmail"] = $result['user']
          ? $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN
          : '';
        BotanDialogHistories::create($historyArr);

        $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
    }

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
        try {
          if ($result['user']) {
            $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                'resize_keyboard' => true,
                'one_time_keyboard' => true]);
            TelegramRequestLog::where('telegramId', $chatId)->delete();

          $log = new TelegramRequestLog();
          $log->telegramId = $chatId;
          $log->command = 'search staff';
          $log->save();

            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;

            $text = "Введите имя или телефон сотрудника";

            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => $text
            ]);
            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $text,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
          }
        } catch (\Exception $e) {
            Log::debug('/facebook ' . $e->getMessage());
        }
    }
}
