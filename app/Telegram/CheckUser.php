<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\QuestionsAnswers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\TelegramRequestLog;

class CheckUser
{
    public static function index($chatId) //$username, $firstname, $lastname
    {
        try {
            $res = '';

            // $historyArr = [
            //   "chatId" => $chatId,
            //   "telegramUsername" => $username,
            //   "telegramFirstName" => $firstname,
            //   "telegramLastName" => $lastname,
            // ];

            $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)
              ->first();
            $user = null;
            if ($telegramUser) {
                $userEmail = strtolower($telegramUser->staffEmail);
                // $historyArr["userEmail"] = $userEmail;

                $user = BotanStaff::where("email", $userEmail)->first();
                if (!$user) {
                    $user = BotanStaff::where("ISN", $userEmail)->first();
                }
                if (!$user) {
                    $userId = StaffEmail::where('email', $userEmail)->first();
                    if ($userId) {
                        $user = BotanStaff::where("id", $userId->userId)->first();
                    } else {
                        $res = "Email не найден";
                    }
                }
                if (!$user) {
                    $res = "Пользователь не найден";
                } elseif ($user && $user->LEAVEDATE) {
                  // $historyArr["response"] = "У вас нет доступа. Вы не являетесь сотрудником компании " .
                  // $telegramUser->WORKPLACE;
                }
            } else {
                $res = 'Вы не авторизованы';
            }
//            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
//            !$telegramRequestLog
            $sub = substr($chatId, 0, 1);
            if (!$user && $sub != '-') {
              $reply_markup = Keyboard::make([
                  'keyboard' => [['/auth']],
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true
              ]);
                Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => $res,
                'reply_markup' => $reply_markup,
              ]);
              // запись ответа в историю диалогов
              // $historyArr["response"] = $res;
              // BotanDialogHistories::create($historyArr);
            } else {
                return array('res' => $res, 'user' => $user ? $user : null);
            }
        } catch (\Exception $e) {
            Log::debug('checkUser@index ' . $e->getMessage());
        }
    }
}
