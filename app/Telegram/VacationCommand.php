<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\Companies;
use App\Products;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\Http\Controllers\StaffController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use App\Telegram\ConfigClass;


class VacationCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "vacation";

    /**
     * @var string Command Description
     */
    protected $description = "Мой отпуск";

    /**
     * @inheritdoc
     */
    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if ($result['user']) {
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            $res = 'Нет информации по выходным дням';
            $reply_markup = Keyboard::make([
                'keyboard' => DefaultKeyboard::getKeyboard(),
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);

            if ($result['user']->WORKPLACE === '"АО СК Коммеск-Омир"') {
                  $url = 'http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdb.php?ISN=' . $result['user']->ISN;
                  $vacation = Curl::to($url)
                      ->withContentType('application/json')
                      ->withHeader('mypwd: 313f5d4d9262f11eaa0077a02bbbf764')
                      ->withTimeout(60 * 20)
                      ->get();
                  $str = '';
                  $array = json_decode($vacation, true);
                  if ($array['REMAININGDAYOFFS']) {
                      $str .= 'Ост. вых. дни: ' . $array['REMAININGDAYOFFS'] . "\r\n";
                  }
                  if ($array['ADMDAYS'] && ($array['USEDADMDAYS'] || intval($array['USEDADMDAYS']) == 0)) {
                      $admDays = intval($array['ADMDAYS']) - intval($array['USEDADMDAYS']);
                      $str .= 'Ост. адм. дни: ' . $admDays . "\r\n";
                  }
                  if (($array['SENATDAYS'] || $array['SENATDAYS'] == 0) && ($array['USEDSENATDAYS'] || $array['USEDSENATDAYS'] == 0)) {
                      $senatDays = intval($array['SENATDAYS']) - intval($array['USEDSENATDAYS']);
                      $str .= 'Ост. cенат. дни: ' . $senatDays . "\r\n";
                  }
                  if (($array['FOOTBALLDAYS'] || $array['FOOTBALLDAYS'] == 0) && ($array['USEDFOOTBALLDAYS'] || $array['USEDFOOTBALLDAYS'] == 0)) {
                      $footballDays = intval($array['FOOTBALLDAYS']) - intval($array['USEDFOOTBALLDAYS']);
                      $str .= 'Ост. футб. дни: ' . $footballDays . "\r\n";
                  }
                  $res = $str;
              } else if ($result['user']->WORKPLACE === 'АО "СК "Сентрас Иншуранс"') {
                $url = 'https://api.kupipolis.kz/bot/getvacationdays?isn=' . $result['user']->ISN;
                $vacation = Curl::to($url)
                    ->withHeader('authcode: k5DYwMNtGvahEN7s')
                    ->withContentType('application/json')
                    ->withTimeout(60 * 20)
                    ->get();
                $str = '';
                $array = json_decode($vacation, true);
                if (array_key_exists('rest_vac_days', $array)) {
                    $str .= 'Ост. вых. дни: ' . $array['rest_vac_days'] . "\r\n";
                }
                if (array_key_exists('rest_adm_days', $array)) {
                    $str .= 'Ост. адм. дни: ' . $array['rest_adm_days'] . "\r\n";
                }
                $res = $str;
              } else if ($result['user']->WORKPLACE === "АО \"КСЖ Сентрас Коммеск Life\"") {
                $url = 'http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdblife.php?ISN=' . $result['user']->ISN;
                  $vacation = Curl::to($url)
                      ->withContentType('application/json')
                      ->withHeader('mypwd: 313f5d4d9262f11eaa0077a02bbbf764')
                      ->withTimeout(60 * 20)
                      ->get();
                  $str = '';
                  $array = json_decode($vacation, true);
                  if ($array['REMAININGDAYOFFS']) {
                      $str .= 'Ост. вых. дни: ' . $array['REMAININGDAYOFFS'] . "\r\n";
                  }
                  if ($array['ADMDAYS'] && ($array['USEDADMDAYS'] || intval($array['USEDADMDAYS']) == 0)) {
                      $admDays = intval($array['ADMDAYS']) - intval($array['USEDADMDAYS']);
                      $str .= 'Ост. адм. дни: ' . $admDays . "\r\n";
                  }
                  if (($array['SENATDAYS'] || $array['SENATDAYS'] == 0) && ($array['USEDSENATDAYS'] || $array['USEDSENATDAYS'] == 0)) {
                      $senatDays = intval($array['SENATDAYS']) - intval($array['USEDSENATDAYS']);
                      $str .= 'Ост. cенат. дни: ' . $senatDays . "\r\n";
                  }
                  if (($array['FOOTBALLDAYS'] || $array['FOOTBALLDAYS'] == 0) && ($array['USEDFOOTBALLDAYS'] || $array['USEDFOOTBALLDAYS'] == 0)) {
                      $footballDays = intval($array['FOOTBALLDAYS']) - intval($array['USEDFOOTBALLDAYS']);
                      $str .= 'Ост. футб. дни: ' . $footballDays . "\r\n";
                  }
                  $res = $str;
              }elseif ($result['user']->WORKPLACE === 'SOS Medical Assistance ТОО'){
                $vacationData = StaffController::getVacationSOSMA($result['user']->ISN);
                if (isset($vacationData["text"])) {
                    $res = $vacationData["text"];
                } else if (isset($vacationData["result"])) {
                    $res = 'Осталось дней отпуска: '.$vacationData["result"];
                }
              }elseif ($result['user']->WORKPLACE !== 'SOS Medical Assistance ТОО') {
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => "Считаю отпускные дни ...",
                    'reply_markup' => $reply_markup
                ]);
                $vacationData = StaffController::getVacation($result['user']->ISN);
                if (isset($vacationData["text"])) {
                    $res = $vacationData["text"];
                } else if (isset($vacationData["result"])) {
                    $res = 'Осталось дней отпуска: '.$vacationData["result"];
                }
              }
              Telegram::sendMessage([
                  'chat_id' => $chatId,
                  'text' => $res,
                  'reply_markup' => $reply_markup
              ]);
              $userEmail = $result['user']->email
                  ? $result['user']->email
                  : $result['user']->ISN;
              $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "response" => $res,
                "userEmail" => $userEmail,
              ];
              BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/vacation ' . $e->getMessage());
        }
    }
}
