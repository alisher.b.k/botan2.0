<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Events;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use App\Telegram\ConfigClass;

class SenatePartiesCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "senate_parties";

    /**
     * @var string Command Description
     */
    protected $description = "Мероприятия сената";

    public function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $str = "";
                $allEvents = Events::whereDate('eventDate', '>=', date("Y-m-d"))
                  ->where('eventType', 'senate')
                  ->get();
                foreach ($allEvents as $eventElement) {
                    $str .= $eventElement->title . "\n";
                    $str .= $eventElement->eventDate . "\n";
                    $str .= $eventElement->content . "\n\n";
                }
                if (!$str) {
                    $str = "Мероприятия отсутсвуют";
                }

                $reply_markup = Keyboard::make([
                  'keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true
                ]);

                Telegram::sendMessage([
                  'chat_id' => $chatId,
                  'text' => $str,
                  'reply_markup' => $reply_markup
                ]);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;
                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $str,
                  "userEmail" => $userEmail,
                ];
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/senate_parties ' . $e->getMessage());
        }
    }
}
