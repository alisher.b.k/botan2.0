<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\Telegram\MenuCommand;
use App\ProblemDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\Problem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\CustomCommand;
use Ixudra\Curl\Facades\Curl;


class ProblemCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "problem";

    /**
     * @var string Command Description
     */
    protected $description = "";

    /**
     * @inheritdoc
     */
    public function handle()
    {
      $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
      $chatId = $chat->getId();
      $username = $chat->getUsername();
      $firstname = $chat->first_name;
      $lastname = $chat->last_name;
      $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
      $messageText = strtolower($message->getText());

      $result = CheckUser::index($chatId);

      $historyArr = [
        "chatId" => $chatId,
        "telegramUsername" => $username,
        "telegramFirstName" => $firstname,
        "telegramLastName" => $lastname,
        "request" => $messageText,
      ];

      // запись запроса в историю диалогов
      $historyArr["userEmail"] = $result['user']
        ? $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN
        : '';
      BotanDialogHistories::create($historyArr);

      $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
    }

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          $reply_markup = Keyboard::make(['keyboard' =>
            [
              ['Это мешает мне в работе', 'Этого нет в системе'],
              ['Здесь ошибка', 'Иное'],
              ['История моих обращений'],
              ['На главную'],
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);


          TelegramRequestLog::where('telegramId', $chatId)->delete();

          $res = "Есть проблема на работе или знаешь, как улучшить работу компании? \n
            *Сообщи в соответствующем разделе:*\n
            • *«Это мешает мне в работе»* /work  - что-то *мешает твоей работе, не хватает* ресурсов и нужного оборудования\n
            • *«Этого нет в системе»* /system - чего-то *нет в в системах и программах:* КИАС, 1С, MyCIC и др. \n
            • *«Здесь ошибка»* /error - *ошибка* в данных, в информации, в документах и т.д.\n
            • *«Иное»* /another - если ни одна из перечисленных выше кнопок не подходит для описания твоей проблемы\n
            *Важно! Дополни свое обращение: прикрепи документ, фото, видео, аудио.*
            \nРазмер прикрепляемого файла не должен превышать 30 МБ";


          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup,
              'parse_mode'=> 'markdown']);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $res,
            "userEmail" => $userEmail,
          ];
          BotanDialogHistories::create($historyArr);

          $log = new TelegramRequestLog();
          $log->telegramId = $chatId;
          $log->command = 'problem_category';
          $log->save();
        }
      } catch (\Exception $e) {
          Log::debug('/problem' . $e->getMessage());
      }
    }

    public static function handleButton($chatId, $messageText, $username, $firstname, $lastname, $result, $isNew) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          if ($isNew) {
            TelegramRequestLog::where('telegramId', $chatId)->delete();
          } else {
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          }

          if ($messageText === '/another') {
            $messageText = 'Иное';
          } else if ($messageText === '/system') {
            $messageText = 'Этого нет в системе';
          } else if ($messageText === '/work') {
            $messageText = 'Это мешает мне в работе';
          } else if ($messageText === '/error') {
            $messageText = 'Здесь ошибка';
          }

          if(!$isNew || $messageText === 'Этого нет в системе' || $messageText === 'Это мешает мне в работе' ||  $messageText === 'Здесь ошибка' ||  $messageText === 'Иное'){
             $reply_markup = Keyboard::make(['keyboard' =>
            [
              ['На главную'],
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);
            $res = "Введите ваше сообщение";

            if (!$isNew) {
              $telegramRequestLog->command = 'problem_append';
              $telegramRequestLog->save();
            } else {
               $log = new TelegramRequestLog();
                $log->telegramId = $chatId;
                $log->command = 'problem';
                $log->data = json_encode(array('category' => $messageText));
                $log->save();
            }

          } else {
            $reply_markup = Keyboard::make(['keyboard' =>
            [
              ['Это мешает мне в работе', 'Этого нет в системе'],
              ['Здесь ошибка', 'Иное'],
              ['На главную'],
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);
            $res = "Забыли выбрать категорию обращения";

            $log = new TelegramRequestLog();
            $log->telegramId = $chatId;
            $log->command = 'problem_category';
            $log->save();
          }


          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup,
              'parse_mode'=> 'markdown']);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $res,
            "userEmail" => $userEmail,
          ];
          BotanDialogHistories::create($historyArr);
        }
      } catch (\Exception $e) {
          Log::debug('/problem handleButton' . $e->getMessage());
      }
    }

    public static function handleQuestion($chatId, $messageText, $username, $firstname, $lastname, $result, $message, $isNew) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;
          if ($messageText == 'Да' || $messageText == 'да') {
            Telegram::sendMessage(['chat_id' => $chatId,
            'text' => 'Дополните обращение',
            'hide_keyboard' => true]);
            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $res,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
            return;
          }
          // если в логе есть айди проблемы обновляем текущую
          // если нет то создаем новую
          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          $data = json_decode($telegramRequestLog->data, true);
          if (array_key_exists("problemId", $data)) {
            if (!$isNew) {
              $problemDialogHistory = new ProblemDialogHistories();
              $problemDialogHistory->problemId = $data["problemId"];
              $problemDialogHistory->email = $userEmail;
              $problemDialogHistory->telegramId = $chatId;
              $problemDialogHistory->save();
            } else {
              $problem = Problem::where('id', $data["problemId"])->first();
              if ($problem->question && $problem->questionFile) {
                self::sendThanks($chatId, $messageText, $username, $firstname, $lastname, $result, true);
                return;
              }
            }
          } else {
            DB::table('problem')->insert(
              [
                'email' => $userEmail,
                'telegramId' => $chatId,
                "category" => $data['category'],
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()'),
              ]
            );
            $problem = Problem::where('telegramId', $chatId)->orderBy('id', 'desc')->first();
            $telegramRequestLog->data = json_encode(array('category' => $data['category'], 'problemId' => $problem->id));
            $telegramRequestLog->save();
          }

          if ($message) {
            $messageDecoded = json_decode($message, true);
            $isCaption = false;
            $isFile = false;
            if (
              array_key_exists("document", $messageDecoded)
              || array_key_exists("photo", $messageDecoded)
              || array_key_exists("video", $messageDecoded)
              || array_key_exists("video_note", $messageDecoded)
              || array_key_exists("audio", $messageDecoded)
              || array_key_exists("voice", $messageDecoded)
            ) {
              $isFile = true;
              $key = '';
              if (array_key_exists("document", $messageDecoded)){
                $key = $messageDecoded["document"]["file_id"];
              } elseif (array_key_exists("photo", $messageDecoded)) {
                $key = $messageDecoded["photo"][0]["file_id"];
              } elseif (array_key_exists("video", $messageDecoded)) {
                $key = $messageDecoded["video"]["file_id"];
              } elseif (array_key_exists("video_note", $messageDecoded)) {
                $key = $messageDecoded["video_note"]["file_id"];
              } elseif (array_key_exists("audio", $messageDecoded)) {
                $key = $messageDecoded["audio"]["file_id"];
              } elseif (array_key_exists("voice", $messageDecoded)) {
                $key = $messageDecoded["voice"]["file_id"];
              }
              $token = getenv('TELEGRAM_BOT_TOKEN');
              $fileInfo = Curl::to('https://api.telegram.org/bot'.$token.'/getFile?file_id='.$key)
              ->withContentType('application/json')
              ->withTimeout(60 * 10)
              ->get();

              $fileInfoDecoded = json_decode($fileInfo, true);

              file_put_contents( public_path()."/problem/".$fileInfoDecoded["result"]["file_path"],file_get_contents('https://api.telegram.org/file/bot'.$token.'/'.$fileInfoDecoded["result"]["file_path"]));
              if ($isNew) {
                $problem->questionFile = $fileInfoDecoded["result"]["file_path"];
              } else {
                $problemDialogHistory->questionFile = $fileInfoDecoded["result"]["file_path"];
                $problemDialogHistory->save();
              }
            }
            if (array_key_exists("caption", $messageDecoded)) {
              $isCaption = true;
              if ($isNew) {
                $problem->question = $messageDecoded["caption"];
              } else {
                $problemDialogHistory->question = $messageDecoded["caption"];
                $problemDialogHistory->save();
              }
            }
            if ($isNew && (($isCaption && $isFile)||($problem->question && $isFile))) {
              $problem->save();
              self::sendThanks($chatId, $messageText, $username, $firstname, $lastname, $result, $isNew);
              return;
            } else if ($isFile) {
              $res = 'Хотите дополнить обращение сообщением?';
            } else {
              if ($isNew) {
                $problem->question = $messageText;
                $problem->save();
                if($problem->questionFile && $problem->question) {
                  self::sendThanks($chatId, $messageText, $username, $firstname, $lastname, $result, $isNew);
                  return;
                }
              } else {
                $problemDialogHistory->question = $messageText;
                $problemDialogHistory->save();
              }
              $res = 'Хотите прикрепить файл? Размер прикрепляемого файла не должен превышать 30 МБ';
            }
          }

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $res,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
            if ($isNew) {
              $problem->save();
            }
            $reply_markup = Keyboard::make(['keyboard' =>
            [
              ['Да', 'Нет'],
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);

            Telegram::sendMessage(['chat_id' => $chatId,
            'text' => $res,
            'reply_markup' => $reply_markup,
            'parse_mode'=> 'markdown']);
        }
      } catch (\Exception $e) {
          Log::debug('/problem handleQuestion' . $e->getMessage());
      }
    }

    public static function sendThanks($chatId, $messageText, $username, $firstname, $lastname, $result, $isNew) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          $data = json_decode($telegramRequestLog->data, true);
          $problemId = '';

          if (array_key_exists("problemId", $data)) {
            $problemId = $data["problemId"];
          }

          TelegramRequestLog::where('telegramId', $chatId)->delete();

          $reply_markup = Keyboard::make(['keyboard' =>
          [
            ['На главную'],
          ],
          'resize_keyboard' => true,
          'one_time_keyboard' => true]);

            $res = "*Спасибо, что не остался равнодушным!*
            Твое обращение №".$problemId." отправлено в наш центр поддержки для рассмотрения. В скором времени ты получишь на него ответ."
            // ."\nПоздравляю! За обращение тебе начислено 2 сенткоина!🤑"
            ;

          Telegram::sendMessage(['chat_id' => $chatId,
          'text' => $res,
          'reply_markup' => $reply_markup,
          'parse_mode'=> 'markdown']);

          // отправка обращения в групповой чат
          // -381528038 - тестовый чат
          // -333170792 - родители ботана
          // -1001151800226(-495584085) Botan 2.0
          $chatIdArr = ['-1001151800226'];
          Log::info("we're beginning to send thanks page");
          $usernameStr = $username ? "@".$username."\n" : "";
          if($problem = Problem::findOrFail($problemId)){
              $usernameStr = $problem->getUserFullName();
              $userEmail = "\nКомпания : $problem->company";
          }
          foreach ($chatIdArr as $id) {
            if ($isNew) {
              $messageContent = "В разделе «Проблема» появилось новое обращение. \n\nАвтор: ".$usernameStr.$userEmail;
            } else {
              $messageContent = "В разделе «Проблема» было дополнено обращение №".$problemId.". \n\nАвтор: ".$usernameStr.$userEmail;
            }
            $reply_markup = Keyboard::make([
              'hide_keyboard' => true]);

            $resp = Telegram::sendMessage([
              'chat_id' => $id,
              'text' => $messageContent,
              'reply_markup' => $reply_markup
            ]);
            Log::info("thanks message: ".$resp);
            $historyArr['response'] = $messageContent;
            $historyArr['chatId'] = $id;
            if ($resp->chat->title) {
              $historyArr['telegramUsername'] = "";
              $historyArr['telegramFirstName'] = $resp->chat->title;
              $historyArr['telegramLastName'] = "";
            } else if ($resp->chat->first_name || $resp->chat->last_name || $resp->chat->username) {
              $historyArr['telegramUsername'] = $resp->chat->username;
              $historyArr['telegramFirstName'] = $resp->chat->first_name;
              $historyArr['telegramLastName'] = $resp->chat->last_name;
            }
            BotanDialogHistories::create($historyArr);
          }
        }
      } catch (\Exception $e) {
        Log::debug('/problem sendThanks' . $e->getMessage());
      }
    }

    public static function closeProblem($chatId, $messageText, $username, $firstname, $lastname, $result, $isNew) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          $data = json_decode($telegramRequestLog->data, true);
          $problemId = '';

          if (array_key_exists("problemId", $data)) {
            $problemId = $data["problemId"];
          }

          TelegramRequestLog::where('telegramId', $chatId)->delete();
          $problem = Problem::where('id', $problemId)->first();
          $problem->status = "closed";
          $problem->save();

          MenuCommand::executeCommand($chatId, $username, $firstname, $lastname, $result);
        }
      } catch (\Exception $e) {
        Log::debug('/problem closeProblem' . $e->getMessage());
      }
    }

    public static function problemHistory ($chatId, $messageText, $username, $firstname, $lastname, $result) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          $reply_markup = Keyboard::make(['keyboard' =>
          [
            ['На главную'],
          ],
          'resize_keyboard' => true,
          'one_time_keyboard' => true]);

          $problems = Problem::where('telegramId', $chatId)->orderBy('id', 'DESC')->get();
          if (!count($problems)) {
            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => 'Обращения не найдены',
              'reply_markup' => $reply_markup
            ]);
            return;
          }
          foreach ($problems as $problem) {
            if ($problem->status == 'active') {
              $status = 'активное';
            } else if ($problem->status == 'closed') {
              $status = 'закрытое';
            } else {
              $status = $problem->status;
            }
            $text = "Обращение №".$problem->id."\nДата обращения: ".$problem->created_at.
            "\nДата обновления: ".$problem->updated_at.
            "\nКатегория: ".$problem->category.
            "\nСтатус: ".$status;
            if ($problem->status != 'closed') {
              $text .= "\nДополнить обращение: /add".$problem->id;
            }
            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => $text,
              'reply_markup' => $reply_markup
            ]);
          }


        }
      } catch (\Exception $e) {
        Log::debug('/problem closeProblem' . $e->getMessage());
      }
    }
}
