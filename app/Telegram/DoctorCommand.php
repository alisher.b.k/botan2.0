<?php
namespace App\Telegram;
use App\BotanStaff;
use App\DoctorAppointment;
use Telegram\Bot\Commands\Command;
use App\Telegram\DefaultKeyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use App\TelegramRequestLog;
use App\Clinics;
use App\StaffTelegramUsers;
use Illuminate\Support\Facades\Log;
use App\MessageGroup;
use App\TelegramUsers;
use Telegram\Bot\Actions;
class DoctorCommand extends Command
{

    protected $name = 'doctor';

    protected $description = 'Doctor command, Get a list of commands';

    public function handle(){
        $update = Telegram::getWebhookUpdates();
        $message = $update->getMessage();
        $chatId = $message->getChat()->getId();
        $username = $message->getChat()->getUsername();
        $firstname = $message->getChat()->first_name;
        $lastname = $message->getChat()->last_name;
        $messageText = $message->getText();
        $this->executeCommand($chatId,$username,$firstname,$lastname);
    }

    public static function executeCommand($chatId,$username,$firstname,$lastname){

        try {
        $log = new TelegramRequestLog();
        $log->telegramId = $chatId;
        $log->command = 'doc:answerYesOrNo';
        $log->save();

        $keyboard = DefaultKeyboard::getAnswer();
        $text = 'Вы застрахованы ?';
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        Telegram::sendMessage([
            'chat_id'=>$chatId,
            'text' => $text,
            'reply_markup' => $reply_markup,
        ]);
        } catch (\Exception $e) {
            Log::debug('/doctor executeCommand' . $e->getMessage());
        }


    }

    public static function answerYes($chatId,$username,$firstname,$lastname,$messageText){

        try {
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $logArr = [
                "command" => 'doc:numberOfcard',
                "telegramId" => $chatId,
                "data" => json_encode(["insuredOrNot" =>'Да']),
              ];

            $text = 'Введите номер карточки страхования:';
            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chatId,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Exception $e) {
            Log::debug('/doctor answerYes' . $e->getMessage());
        }
    }

    public static function answerYesAgain($chatId,$username,$firstname,$lastname,$messageText){
        try {
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $logArr = [
                "command" => 'doc:numberOfcard',
                "telegramId" => $chatId,
                "data" => json_encode(["insuredOrNot" =>'Да']),
              ];

            $text = 'Введите корректный номер страховой карточки:';
            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chatId,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        }   catch (\Exception $e) {
            Log::debug('/doctor answerYesAgain' . $e->getMessage());
        }
    }

    public static function numberOfcard($chatId,$username,$firstname,$lastname,$messageText){
        try {
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $logArr = [
                "command" => 'doc:PhoneNumber',
                "telegramId" => $chatId,
                "data" => json_decode($telegramRequestLog->data, true),
              ];

            $logArr['data'] = json_encode(array_merge($logArr['data'],["cardNumber" =>$messageText]));

            $text = 'Укажите свой контактный номер телефона в виде: +7XXXXXXXXXX';

            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chatId,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        }catch (\Exception $e) {
            Log::debug('/doctor numberOfcard' . $e->getMessage());
        }
    }

    public static function phoneNumberOfInsured($chatId,$username,$firstname,$lastname,$messageText){
        try {
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $logArr = [
                "command" => 'doc:PhoneNumber',
                "telegramId" => $chatId,
                "data" => json_decode($telegramRequestLog->data, true),
              ];

            $logArr['data'] = json_encode(array_merge($logArr['data'],['phoneNumber' => $messageText]));

            $text = 'Введите корректный номер телефона:';

            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chatId,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Exception $e) {
            Log::debug('/doctor phoneNumberOfInsured' . $e->getMessage());
        }
    }

    public static function answerNo($chatId,$username,$firstname,$lastname){
        try {
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $logArr = [
                "command" => 'doc:PhoneNumber',
                "telegramId" => $chatId,
                "data" => json_encode(["insuredOrNot" =>'Нет']),
              ];

            $text = 'Укажите свой контактный номер телефона в виде: +7XXXXXXXXXX';

            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chatId,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        }catch (\Exception $e) {
            Log::debug('/doctor answerNo' . $e->getMessage());
        }
    }
    public static function phoneNumberOfUninsured($chatId,$username,$firstname,$lastname,$messageText)
    {
        try {
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $logArr = [
                "command" => 'doc:PhoneNumberOfUninsured',
                "telegramId" => $chatId,
                "data" => json_encode(["insuredOrNot" =>'Нет']),
              ];

            $text = 'Введите корректный номер телефона:';

            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chatId,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Exception $e) {
            Log::debug('/doctor phoneNumberOfUninsured' . $e->getMessage());
        }
    }

    public static function chooseClinic($chatId,$username,$firstname,$lastname,$messageText){
        try {
          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          TelegramRequestLog::where('telegramId', $chatId)->delete();

          $logArr = [
            "command" => 'doc:chooseClinic',
            "telegramId" => $chatId,
            "data" => json_decode($telegramRequestLog->data, true),
          ];
          $logArr['data'] = json_encode(array_merge($logArr['data'],['phoneNumber' => $messageText]));

            $keyboard = DefaultKeyboard::getClinic();
            $text = 'Выберите клинику:';

            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
                'chat_id'=>$chatId,
                'text' => $text,
                'reply_markup' => $reply_markup,
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Exception $e) {
            Log::debug('/doctor chooseClinic' . $e->getMessage());
        }

    }

    public static function chooseDoctor($chatId,$username,$firstname,$lastname,$messageText){

        try {
        if($messageText === '🏥 МЦ на Шарипова'){
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $logArr = [
              "command" => 'doc:chooseDoctor',
              "telegramId" => $chatId,
              "data" => json_decode($telegramRequestLog->data, true),
            ];

            $logArr['data'] = json_encode(array_merge($logArr['data'],['selectedClinic'=> 'МЦ на Шарипова']));

            $clinic = Clinics::where('name','МЦ на Шарипова')->first();
            $text = "\nСпециалисты:\n";
            $keyboard = DefaultKeyboard::getDoctors($clinic);
         Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
            'reply_markup' => $keyboard
        ]);
            TelegramRequestLog::create($logArr);
        }elseif($messageText === '🏥 МЦ на Достык'){
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $logArr = [
              "command" => 'doc:chooseDoctor',
              "telegramId" => $chatId,
              "data" => json_decode($telegramRequestLog->data, true),
            ];

            $logArr['data'] = json_encode(array_merge($logArr['data'],['selectedClinic'=> 'МЦ на Достык']));
            $clinic = Clinics::where('name','МЦ на Достык')->first();
            $text = "\nСпециалисты:\n";
            $keyboard = DefaultKeyboard::getDoctors($clinic);
        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
            'reply_markup' => $keyboard
        ]);
            TelegramRequestLog::create($logArr);
        }
        } catch (\Exception $e) {
            Log::debug('/doctor chooseDoctor' . $e->getMessage());
        }

    }
    public static function reason($chatId,$username,$firstname,$lastname,$messageText,$callback){
            try {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $logArr = [
                  "command" => 'doc:writeReason',
                  "telegramId" => $chatId,
                  "data" => json_decode($telegramRequestLog->data, true),
                ];
                $logArr['data'] = json_encode(array_merge($logArr['data'],['selectedDoctor'=>$callback]));

                $text = 'Укажите причину вашего обращения к врачу:';
                $keyboard = DefaultKeyboard::MainKeyboard();
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $text,
                    'reply_markup' => $reply_markup
                ]);
                TelegramRequestLog::create($logArr);
            } catch (\Exception $e) {
                Log::debug('/doctor reason' . $e->getMessage());
            }

    }
    public static function finalMessage($chatId,$username,$firstname,$lastname,$messageText){

            try {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $logArr = [
                  "command" => 'doc:finalMessage',
                  "telegramId" => $chatId,
                  "data" => json_decode($telegramRequestLog->data, true),
                ];
                $logArr['data'] = json_encode(array_merge($logArr['data'],['reasonForTheRequest'=>$messageText]));
                $text = 'Если данные верны нажмите кнопку "Отправить"';

                $keyboard = [['Отправить', 'Отменить']];
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
                Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => $text,
                'reply_markup' => $reply_markup
            ]);
                TelegramRequestLog::create($logArr);
            }catch (\Exception $e) {
                Log::debug('/doctor finalMessage' . $e->getMessage());
            }
        }
    public static function sendToCoordinators($chatId){
        try {
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
        TelegramRequestLog::where('telegramId', $chatId)->delete();
        $data = json_decode($telegramRequestLog->data,true);
        $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)->first();
        $user = BotanStaff::where('email', $telegramUser->staffEmail)->orWhere('isn', $telegramUser->staffEmail)->first();
        $userFIO = explode(' ', $user->FIO);
        $text  =     "\n Имя: ".$userFIO[1].
                    "\nФамилия: ".$userFIO[0].
                    "\nКонтактный номер : ".$data['phoneNumber'].
                    "\nКлиника : ".$data['selectedClinic'].
                    "\nВрач : ".$data['selectedDoctor'].
                    "\nПричина обращения : ".$data['reasonForTheRequest'].
                    "\nЗастрахован(a) : ".$data['insuredOrNot'];

        if(isset($data['cardNumber'])){
            $text.= "\nНомер страховой карточки : ".$data['cardNumber'];
        }
        DoctorAppointment::create([
            'first_name' => $userFIO[1],
            'last_name' => $userFIO[0],
            'phone_number' => $data['phoneNumber'],
            'clinic' => $data['selectedClinic'],
            'doctor' => $data['selectedDoctor'],
            'reason' => $data['reasonForTheRequest'],
            'insured' => $data['insuredOrNot'],
            'card_number' => $data['cardNumber'] ?? null
        ]);
        $group = MessageGroup::findOrFail(10);
        $staffEmails = $group->getUsersArray();
        foreach ($staffEmails as $staffEmail){
                $telegramUser = StaffTelegramUsers::where('staffEmail', $staffEmail)->first();
                Telegram::sendMessage([
                    'chat_id' => $telegramUser->telegramId,
                    'text' => $text,
                    'parse_mode' => 'html'
                ]);
        }
        $keyboard  = DefaultKeyboard::getMenuKeyboard(StaffTelegramUsers::getWorkPlaceByChatId($chatId));
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $text_final =  "Спасибо за обращение! Пожалуйста, дождитесь звонка от сотрудника call-центра с информацией о вашей записи.";
        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => $text_final,
            'reply_markup' => $reply_markup
        ]);
        }catch (\Exception $e) {
            Log::debug('/doctor sendToCoordinators' . $e->getMessage());
        }
    }
}
