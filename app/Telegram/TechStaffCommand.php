<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;

class TechStaffCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "tech_staff";

    /**
     * @var string Command Description
     */
    protected $description = "";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $log = new TelegramRequestLog();
                $log->telegramId = $chatId;
                $log->command = 'tech_staff';
                $log->save();

                $reply_markup = Keyboard::make([
                    'keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);

                $response = "Опишите запрос";

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup
                ]);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;
                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $response,
                  "userEmail" => $userEmail,
                ];
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/tech_staff ' . $e->getMessage());
        }
    }

    public static function sendRequest($chatId, $messageText, $username, $firstname, $lastname, $result) {
      try {
          if ($result['user']) {
              TelegramRequestLog::where('telegramId', $chatId)->delete();

              $reply_markup = Keyboard::make([
                  'keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true
              ]);

              $response = "Спасибо за обращение";

              Telegram::sendMessage([
                  'chat_id' => $chatId,
                  'text' => $response,
                  'reply_markup' => $reply_markup
              ]);

              $userEmail = $result['user']->email
                  ? $result['user']->email
                  : $result['user']->ISN;
              $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "response" => $response,
                "userEmail" => $userEmail,
              ];
              BotanDialogHistories::create($historyArr);

              $usernameStr = $username ? "telegram: @".$username."\n" : "";

              // отправка запроса сотруднику Жанабай
              // -381528038 - тестовый чат
              // -336956371 - группа тех персонала
              $chatIdArr = ["-381528038", "-336956371"];
              foreach ($chatIdArr as $id) {
                $text =  "Запрос к тех. персоналу \n\n" .
                  $usernameStr. "" .$userEmail . "\nЗапрос: " . $messageText;
                $resp = Telegram::sendMessage([
                    'chat_id' => $id,
                    'text' => $text,
                ]);
                $historyArr['chatId'] = $id;
                if ($resp->chat->title) {
                  $historyArr['telegramUsername'] = "";
                  $historyArr['telegramFirstName'] = $resp->chat->title;
                  $historyArr['telegramLastName'] = "";
                } else if ($resp->chat->first_name || $resp->chat->last_name || $resp->chat->username) {
                  $historyArr['telegramUsername'] = $resp->chat->username;
                  $historyArr['telegramFirstName'] = $resp->chat->first_name;
                  $historyArr['telegramLastName'] = $resp->chat->last_name;
                }
                $historyArr["response"] = $text;
                BotanDialogHistories::create($historyArr);
              }
          }
      } catch (\Exception $e) {
          Log::debug('/tech_staff sendRequest' . $e->getMessage());
      }
    }
}
