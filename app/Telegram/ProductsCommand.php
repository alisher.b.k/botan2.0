<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\Companies;
use App\Products;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use App\Telegram\ConfigClass;

class ProductsCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "forsales";

    /**
     * @var string Command Description
     */
    protected $description = "Продукты";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if ($result["user"]) {
            $reply_markup = Keyboard::make([
              'keyboard' => DefaultKeyboard::getKeyboard(),
              'resize_keyboard' => true,
              'one_time_keyboard' => true
            ]);

            TelegramRequestLog::where('telegramId', $chatId)->delete();
            $products = Products::where('companyName', $result["user"]->WORKPLACE)->get();
            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;
            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "userEmail" => $userEmail,
            ];
            if (count($products) > 0) {
                $res = "";
                foreach ($products as $product) {
                    $result = '';
                    $result .= $product->id . ". " . $product->title . "\n";
                    $result .= $product->companyName . "\n";
                    // $result .= "https://bots.n9.kz/botan/api/products/view/" . $product->id . "\n\n";
                    $result .= $product->link."\n\n";
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $result,
                        'reply_markup' => $reply_markup
                    ]);
                    $historyArr["response"] = $result;
                    BotanDialogHistories::create($historyArr);
                }
            } else {
              $result = "Продукты не найдены";
              Telegram::sendMessage([
                  'chat_id' => $chatId,
                  'text' => $result,
                  'reply_markup' => $reply_markup
              ]);
              $historyArr["response"] = $result;
              BotanDialogHistories::create($historyArr);
            }
          }
        } catch (\Exception $e) {
            Log::debug('/products ' . $e->getMessage());
        }
    }
}
