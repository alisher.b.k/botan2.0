<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Services\DateService;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\BookRecord;
use App\TelegramCompanyLogs;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Ixudra\Curl\Facades\Curl;
use App\BookingBuilding;
require_once 'calendar.php';
require_once 'time.php';

class BookingCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "booking";

    /**
     * @var string Command description
     */
    protected $description = "Бронирование";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
        $chatId = $chat->getId();
        $username = $chat->getUsername();
        $firstname = $chat->first_name;
        $lastname = $chat->last_name;
        $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
        $messageText = strtolower($message->getText());

        $result = CheckUser::index($chatId);

        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "request" => $messageText,
        ];

        // запись запроса в историю диалогов
        $historyArr["userEmail"] = $result['user']
          ? $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN
          : '';
        BotanDialogHistories::create($historyArr);

        $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
    }

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if(isset($result['user'])){
            // $places = ['🏢 Коммеск', '🏬 Сентрас Иншуранс', '🏣 БЦ SАТ'];
            $places = ['🏬 Сентрас Иншуранс'];
            foreach(BookingBuilding::all() as $place){
              array_push($places, $place->building);
            }
            $reply_markup = Keyboard::make([
              'keyboard' => [
                  $places,
                  ['На главную']
              ],
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

            Telegram::sendMessage(['chat_id' => $chatId,
              'text' => "Выберите здание",
              'reply_markup' => $reply_markup]);


            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => 'Выберите здание',
              "userEmail" => $userEmail,
            ];

            BotanDialogHistories::create($historyArr);

            $log = new TelegramRequestLog();
            $log->telegramId = $chatId;
            $log->command = 'place_for_booking';
            $log->save();
          }
        }catch
            (\Exception $e) {
                Log::debug('/place_for_booking ' . $e->getMessage());
            }
    }

    public static function chooseCompany($chatId, $messageText, $username, $firstname, $lastname, $result){
      try{
          if (isset($result['user'])){
            $building = BookingBuilding::where('building', 'like', '%'. mb_substr($messageText, 2))->first();
              $rooms = [];
              if($building !== null){
                 $roomData = $building->rooms;
                 foreach($roomData as $room){
                    array_push($rooms, $room->room);
                 }
                 $reply_markup = Keyboard::make([
                  'keyboard' => [
                      $rooms,
                      ['На главную']
                  ],
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true]);
              }elseif($messageText === '🏬 Сентрас Иншуранс'){
                      $data = Curl::to('https://my.cic.kz/api/booking/officeList')
                      ->withContentType('application/json')
                      ->withTimeout(60 * 10)
                      ->post();
                  $arr = json_decode($data, true);
                  $arr = array_values($arr);
                  $rooms = [];
                  $temp = [];
                  for ($i = 0, $iMax = count($arr); $i < $iMax; $i++){
                      if($i % 2 === 0){
                          $temp[] = $arr[$i];
                      }else{
                          $temp[] = $arr[$i];
                          $rooms[] = $temp;
                          $temp = [];
                      }
                  }
                  if($iMax%2 !== 0){
                      $rooms[] = $temp;
                  }
                  $rooms[] = ['На главную'];
                  $reply_markup = Keyboard::make([
                      'keyboard' => $rooms,
                      'resize_keyboard' => true,
                      'one_time_keyboard' => true]);
                    }
            }

            TelegramRequestLog::where('telegramId', $chatId)->delete();
            $res = $result['res'];
            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;
            $res = "Выберите переговорную";

            Telegram::sendMessage(['chat_id' => $chatId,
                'text' => $res,
                'reply_markup' => $reply_markup]);

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $res,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);

            $log = new TelegramRequestLog();
            $log->telegramId = $chatId;
            $log->command = 'booking';
            $log->data = $messageText;
            $log->save();

            $logCompany = new TelegramCompanyLogs();
            $logCompany->telegramId = $chatId;
            $logCompany->company = $messageText;
            $logCompany->save();
            Log::debug("telegram company logs id: ".$logCompany->id);

        }catch
        (\Exception $e) {
            Log::debug('/booking ' . $e->getMessage());
        }
    }

    public static function chooseRoom($chatId, $messageText, $username, $firstname, $lastname, $result)
    {
      try {
        if ($result['user']) {
          $reply_markup = Keyboard::make([
            'keyboard' => [
                ['Забронировать', 'Удалить броню'],
                ['На главную']
              ],
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);
          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          TelegramRequestLog::where('telegramId', $chatId)->delete();
          $room = '';
          Log::debug("telegram_company_beg");
          Log::debug($telegramRequestLog->data);
          Log::debug($chatId);
          Log::debug("telegram_company_end");
          if ($telegramRequestLog->data == '🏬 Сентрас Иншуранс') {
            $data = Curl::to('https://my.cic.kz/api/booking/officeList')
                  ->withContentType('application/json')
                  ->withTimeout(60 * 10)
                  ->post();
            $arr = json_decode($data, true);
            foreach ($arr as $key => $value) {
              if ($messageText == $value) {
                $room = $key;
              }
            }
          } else {
            $room = $messageText;
          }

          $logArr = [
            "command" => $telegramRequestLog->command . "_chooseRoom",
            "telegramId" => $chatId,
            "data" => json_encode(["room" => $room, 'company' => $telegramRequestLog->data]),
            // "data" => json_decode($telegramRequestLog->data, true),
          ];
          $res = $result['res'];
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;
          $res = "Если хотите забронировать комнату переговоров нажмите на кнопку «Забронировать».
          \nЕсли хотите удалить ранее сделанную броню нажмите на кнопку «Удалить броню».";

          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup]);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $res,
            "userEmail" => $userEmail,
          ];
          BotanDialogHistories::create($historyArr);

          TelegramRequestLog::create($logArr);
        }
      } catch
      (\Exception $e) {
          Log::debug($e->getTraceAsString());
          Log::debug('/booking chooseRoom' . $e->getMessage());
      }
    }

    public static function showWeek($chatId, $messageText, $username, $firstname, $lastname, $result, $type) {
        $dateService = app(DateService::class);
      try {
        if ($result['user']) {
          $dates = [];

          // $stop_date = new \DateTime("now", new \DateTimeZone('Asia/Almaty'));
          // while(count($dates) < 5) {
          //   if (!isWeekend($stop_date)) {
          //     if (strtotime($stop_date->format('Y-m-d H:i'))<= strtotime($stop_date->setTime(20, 0)->format('Y-m-d H:i'))) {
          //       array_push($dates, $stop_date->format('d.m.Y'));
          //     }
          //   }
          //   $stop_date = $stop_date->modify('+1 day');
          // }


          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          TelegramRequestLog::where('telegramId', $chatId)->delete();
          $logArr = [
            "telegramId" => $chatId,
            'data' => json_decode($telegramRequestLog->data, true),
          ];
          if ($type == 'delete') {
            $logArr["command"] = $telegramRequestLog->command . "_showWeekDelete";
          } else {
            $logArr["command"] = $telegramRequestLog->command . "_showWeek";
          }
          $logArr['data']=json_encode(array_merge($logArr['data']));
          $res = $result['res'];
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;
          $res = "Выберите дату в календаре";

          $calendar = $dateService->get_calendar((int)date('m'), (int)date('Y'));
          $reply_markup = Keyboard::make([
            'inline_keyboard' => (array)$calendar,
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup]);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $res,
            "userEmail" => $userEmail,
          ];
          BotanDialogHistories::create($historyArr);

          TelegramRequestLog::create($logArr);
        }
      } catch
      (\Exception $e) {
          Log::debug('/booking showWeek' . $e->getMessage());
      }
    }

    public static function validateTimePicker($update, $message) {
        $dateService = app(DateService::class);
      $callbackRoute = explode('-', $update["callback_query"]["data"]);
      $chatId = $update["callback_query"]["from"]["id"];
      $messageId = $update["callback_query"]["message"]["message_id"];

      if ($callbackRoute[1] == 'all') {
        $username = $message->getChat()->getUsername();
        $firstname = $message->getChat()->first_name;
        $lastname = $message->getChat()->last_name;
        $result = CheckUser::index($chatId, $username, $firstname, $lastname, $message);
        $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();

        if ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom_showWeek_showTime') {
          self::chooseStartTime(
            $chatId,
            $callbackRoute[2].':'.$callbackRoute[3],
            $username,
            $firstname,
            $lastname,
            $result
          );
        } else if ($telegramRequestLog && $telegramRequestLog->command === 'booking_chooseRoom_showWeek_showTime_chooseStartTime') {
          self::chooseEndTime(
            $chatId,
            $callbackRoute[2].':'.$callbackRoute[3],
            $username,
            $firstname,
            $lastname,
            $result
          );
        }

        return;
      }

      $timepicker = $dateService->getTimePicker((int)$callbackRoute[2], (int)$callbackRoute[3]);

      $reply_markup = Keyboard::make([
        'inline_keyboard' => $timepicker,
          'resize_keyboard' => true,
          'one_time_keyboard' => true]);

      $token = getenv('TELEGRAM_BOT_TOKEN');
      define('BOTAPI', 'https://api.telegram.org/bot' . $token . '/');
      $data2 = [
          'chat_id' => $chatId,
          'reply_markup' => $reply_markup,
          'message_id' => $messageId
      ];
      $ch = curl_init(BOTAPI . 'editMessageReplyMarkup');
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      $output = curl_exec($ch);
      curl_close($ch);

      return;
    }

    public static function validateCalendar($update, $message) {
        $dateService = app(DateService::class);
      $callbackRoute = explode('-', $update["callback_query"]["data"]);
      $chatId = $update["callback_query"]["from"]["id"];
      $messageId = $update["callback_query"]["message"]["message_id"];

      if ($callbackRoute[0] === 'calendar' && $callbackRoute[1] === 'month') {
          $calendar = $dateService->get_calendar((int)$callbackRoute[2], (int)$callbackRoute[3]);
      } elseif ($callbackRoute[0] === 'calendar' && $callbackRoute[1] === 'year') {
          $months = $dateService->get_months_list((int)$callbackRoute[2]);
      } elseif($callbackRoute[0] === 'calendar' && $callbackRoute[1] === 'months_list') {
          $months = $dateService->get_months_list((int)$callbackRoute[2]);
      } elseif($callbackRoute[0] === 'calendar' && $callbackRoute[1] === 'years_list') {
          $months = $dateService->get_years_list((int)$callbackRoute[2]);
      } else {
        $username = $message->getChat()->getUsername();
        $firstname = $message->getChat()->first_name;
        $lastname = $message->getChat()->last_name;
        $result = CheckUser::index($chatId, $username, $firstname, $lastname, $message);
        $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
        if ($telegramRequestLog->command == 'booking_chooseRoom_showWeekDelete') {
          self::showTimeForDelete(
            $chatId,
            $callbackRoute[2].'.'.$callbackRoute[3].'.'.$callbackRoute[4],
            $username,
            $firstname,
            $lastname,
            $result
          );
        } else {
          self::showTime(
            $chatId,
            $callbackRoute[2].'.'.$callbackRoute[3].'.'.$callbackRoute[4],
            $username,
            $firstname,
            $lastname,
            $result
          );
        }
        return;
      }
      $reply_markup = Keyboard::make([
        'inline_keyboard' => $calendar,
          'resize_keyboard' => true,
          'one_time_keyboard' => true]);

      $token = getenv('TELEGRAM_BOT_TOKEN');
      define('BOTAPI', 'https://api.telegram.org/bot' . $token . '/');
      $data2 = [
          'chat_id' => $chatId,
          'reply_markup' => $reply_markup,
          'message_id' => $messageId
      ];
      $ch = curl_init(BOTAPI . 'editMessageReplyMarkup');
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      $output = curl_exec($ch);
      curl_close($ch);

      // Telegram::sendMessage([
      //   'chat_id' => $chatId,
      //   'text' => "Выберите день ближайшей недели или напишите дату в формате ДД.ММ.ГГГГ",
      //   'message_id' => $update["callback_query"]["message"]["message_id"],
      //   'reply_markup' => $reply_markup
      // ]);
      return;
}

    public static function showTime($chatId, $messageText, $username, $firstname, $lastname, $result) {
        $dateService = app(DateService::class);
      try {
        if ($result['user']) {

          $userEmail = $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN;

          $reply_markup = Keyboard::make([
            'keyboard' => [
                ['На главную']
              ],
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "userEmail" => $userEmail,
          ];

          Telegram::sendMessage(['chat_id' => $chatId,
                'text' => "Выбрана дата ".$messageText,
                'reply_markup' => $reply_markup]);

          // Формат времени правильный?
          // 1. Нет
          $calendar = $dateService->get_calendar((int)date('m'), (int)date('Y'));
          $reply_markup_for_catendar = Keyboard::make([
            'inline_keyboard' => (array)$calendar,
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);
            if (!$dateService->isNotEndedDate($messageText)) {
              $text = "Вы не можете выбрать дату, которая уже прошла! Попробуйте заново";
              Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $text,
              'reply_markup' => $reply_markup_for_catendar]);
              return;
            }
            $date = new \DateTime($messageText);
          if (!$dateService->validateDate($messageText)) {
            Telegram::sendMessage(['chat_id' => $chatId,
                'text' => "Неверный формат даты. Попробуйте заново",
                'reply_markup' => $reply_markup_for_catendar]);
            return;
          }

          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          TelegramRequestLog::where('telegramId', $chatId)->delete();
          $existingLogs = explode("_", $telegramRequestLog->command);
          if ($existingLogs[count($existingLogs) - 1] != 'showTime') {
            $telegramRequestLog->command .= "_showTime";
          }
          $oldData = json_decode($telegramRequestLog->data, true);
          $logArr = [
            "command" => $telegramRequestLog->command,
            "telegramId" => $chatId,
            'data' => $oldData,
          ];
          $room = $logArr['data']['room'];
          $logArr['data']=json_encode(array_merge($logArr['data'], ["date" => $messageText]));

          // 2. Да
          $res = "Комната переговоров свободна";
          $records = [];
          if ($oldData['company'] == '🏬 Сентрас Иншуранс') {
            $data = Curl::to('https://my.cic.kz/api/booking/get')
              ->withData(['dateStart' => $date->format('d.m.Y'), 'dateEnd' => $date->format('d.m.Y'), "office" => $room ])
              // ->withContentType('application/json')
              ->asJson()
              ->post();
            $arr = json_decode(json_encode($data, true),true);
            if ($arr && isset($arr[$room]) && $arr[$room][$date->format('d.m.Y')]) {
              $res = "Это время уже занято твоими коллегами:";
              foreach ($arr[$room][$date->format('d.m.Y')] as $key => $value) {
                // проверить комнату
                $user = BotanStaff::where("ISN", $value["author"])->first();
                $res .=
                "\n" .
                $dateService->formatDate($value["from"], 'H:i') .
                 "-" .
                $dateService->formatDate($value["to"], 'H:i') .
                  ", " .
                  $user->FIO ?? $value["author"];
              }
            }
          } else {
            $records = BookRecord::where('date', $date->format('Y-m-d'))
              ->where('room', $room)
              ->orderBy('start','asc')
              ->get();
            // Если брони нет
            if (count($records)) {
              $res = "Это время уже занято твоими коллегами:";
              foreach ($records as $record) {
                $res .= "\n" . $dateService->formatDate($record->start, 'H:i') . "-" . $dateService->formatDate($record->end, 'H:i') . ", " . $record->fio;
              }
            }
          }

          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup]);

          $currentTime = "08:00";
          if ($dateService->isToday($messageText)) {
            $today = new \DateTime("now", new \DateTimeZone('Asia/Almaty'));
            $currentTime = $today->format("H:i");
          }
          $time = explode(":",$currentTime);
          $minutes = $time[1];
          $hour = $time[0];
          while ($minutes % 10) {
            if ($minutes > 50){
              $minutes = 0;
              $hour += 1;
            } else {
              $minutes += 1;
            }
          }
          $timePicker = $dateService->getTimePicker($hour, $minutes);
          $reply_markup = Keyboard::make([
            'inline_keyboard' => (array)$timePicker,
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);
          Telegram::sendMessage(['chat_id' => $chatId,
          'text' => "Выберите время начала бронирования\n(Доступное время только с ".$currentTime." до 19:50)",
          // "Введите время начала бронирования в формате ЧЧ:ММ\n(Доступное время только с ".$currentTime." до 19:59)",
          'reply_markup' => $reply_markup]);

          $historyArr["response"] = $res;
          BotanDialogHistories::create($historyArr);

          TelegramRequestLog::create($logArr);
        }
      } catch
      (\Exception $e) {
          Log::debug('/booking showTime' . $e->getMessage());
      }
    }

    public static function showTimeForDelete($chatId, $messageText, $username, $firstname, $lastname, $result) {
        $dateService = app(DateService::class);
      try {
        if ($result['user']) {

          $userEmail = $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN;

          $reply_markup = Keyboard::make([
            'keyboard' => [
                ['На главную']
              ],
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "userEmail" => $userEmail,
          ];
          // Формат времени правильный?
          // 1. Нет
          $date = new \DateTime($messageText);
          if (!$dateService->validateDate($messageText)) {
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => "Введенная дата не подходит формату ДД.ММ.ГГГГ попробуйте заново",
              'reply_markup' => $reply_markup
            ]);
            return;
          }

          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          TelegramRequestLog::where('telegramId', $chatId)->delete();
          $oldData = json_decode($telegramRequestLog->data, true);
          $logArr = [
            "telegramId" => $chatId,
            'data' => $oldData,
          ];
          $room = $logArr['data']['room'];
          $logArr['data']=json_encode(array_merge($logArr['data'], ["date" => $messageText]));

          // 2. Да
          $buttons = [];
          $res = "";
          if ($oldData['company'] === '🏬 Сентрас Иншуранс') {
            $data = Curl::to('https://my.cic.kz/api/booking/get')
              ->withData(['dateStart' => $date->format('d.m.Y'), 'dateEnd' => $date->format('d.m.Y'), "office" => $room ])
              // ->withContentType('application/json')
              ->asJson()
              ->post();
            $arr = json_decode(json_encode($data, true),true);
            foreach ($arr as $room => $value) {
              foreach ($value as $date => $info) {
                foreach ($info as $record) {
                  if ($record["author"] == $result['user']->ISN) {
                    if ($res == "") {
                      $res = 'Выберите бронь, которую хотите удалить';
                    }
                    array_push($buttons, "\n" . $dateService->formatDate($record["from"], 'H:i') . "-" . $dateService->formatDate($record["to"], 'H:i'));
                  }
                }
              }
            }
            if (!count($buttons)){
              $res = "У вас нет брони на выбранную дату. Выберите другую дату или нажмите «На главную», чтобы вернуться в главное меню";
              $logArr["command"] = $telegramRequestLog->command;
            } else {
              $logArr["command"] = $telegramRequestLog->command . "_showTimeForDelete";
            }
          } else{
            $res = "У вас нет брони на выбранную дату. Выберите другую дату или нажмите «На главную», чтобы вернуться в главное меню";
            $records = BookRecord::where('date', $date->format('Y-m-d'))
              ->where('room', $room)
              ->where('telegramId', $chatId)
              ->orderBy('start','asc')
              ->get();
            if (count($records)) {
              $res = 'Выберите бронь, которую хотите удалить';
              for ($i = 0; $i < count($records); $i++) {
                array_push($buttons, "\n" . $dateService->formatDate($records[$i]->start, 'H:i') . "-" . $dateService->formatDate($records[$i]->end, 'H:i'));
              }
              $logArr["command"] = $telegramRequestLog->command . "_showTimeForDelete";
            } else {
              $logArr["command"] = $telegramRequestLog->command;
            }
          }
          $reply_markup = Keyboard::make([
            'keyboard' => [
                $buttons,
                ['На главную']
              ],
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);
          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup]);

          $historyArr["response"] = $res;
          BotanDialogHistories::create($historyArr);

          TelegramRequestLog::create($logArr);
        }
      } catch
      (\Exception $e) {
          Log::debug('/booking showTimeForDelete' . $e->getMessage());
      }
    }

    public static function chooseStartTime($chatId, $messageText, $username, $firstname, $lastname, $result) {
        $dateService = app(DateService::class);
      try {
        if ($result['user']) {

          $userEmail = $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN;

          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
            $oldData = json_decode($telegramRequestLog->data, true);
          $logArr = [
            "command" => $telegramRequestLog->command . "_chooseStartTime",
            "telegramId" => $chatId,
            'data' => $oldData,
          ];
          $room = $logArr['data']['room'];
          $date = $logArr['data']['date'];

          $currentTime = "08:00";
          if ($dateService->isToday($date)) {
            $today = new \DateTime("now", new \DateTimeZone('Asia/Almaty'));
            $minutes = (int)$today->format('i');
            $hour = (int)$today->format('H');
            while ($minutes % 10) {
              if ($minutes > 50){
                $minutes = 0;
                $hour += 1;
              } else {
                $minutes += 1;
              }
            }
            $today->setTime($hour, $minutes);
            $currentTime = $today->format("H:i");
          }

          $time = explode(":", $currentTime);
          $minutes = $time[1];
          $hour = $time[0];

          $timePicker = $dateService->getTimePicker($hour, $minutes);
          $reply_markup = Keyboard::make([
            'inline_keyboard' => (array)$timePicker,
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "userEmail" => $userEmail,
          ];
          // Формат времени правильный?
          // 1. Нет
          // if (!$dateService->validateDate($messageText, 'H:i', '19:59')) {
            // TelegramRequestLog::where('telegramId', $chatId)->delete();
          //   Telegram::sendMessage([
          //     'chat_id' => $chatId,
          //     'text' => "Неверный формат даты. Попробуйте заново",
          //     'reply_markup' => $reply_markup
          //   ]);
          //   return;
          // }

          $inputTimeArray = explode(":", $messageText);
          if (isset($inputTimeArray[1])) {
            if ((int)$inputTimeArray[1] < 10) {
              $inputTimeArray[1] = "0".$inputTimeArray[1];
              $messageText = $inputTimeArray[0].":".$inputTimeArray[1];
            }
          }
          $isNotValidatedRegex = !$dateService->validateTimeRegex($messageText);
          if ($isNotValidatedRegex) {
            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => "Неверно указан формат времени\n(Доступное время только с ".$hour.":".$minutes." до 19:50)\nПопробуйте заново",
              'reply_markup' => $reply_markup
            ]);
            return;
          }

          $isValidated = $dateService->validateTime($messageText, '07:59','20:00', $date);
          if (!$isValidated) {
            $currentTime = "08:00";
            if ($dateService->isToday($date)) {
              $today = new \DateTime("now", new \DateTimeZone('Asia/Almaty'));
              $minutes = (int)$today->format('i');
              $hour = (int)$today->format('H');
              while ($minutes % 10) {
                if ($minutes > 50){
                  $minutes = 0;
                  $hour += 1;
                } else {
                  $minutes += 1;
                }
              }
              $today->setTime($hour, $minutes);
              $currentTime = $today->format("H:i");
            }

            $time = explode(":", $currentTime);
            $timePicker = $dateService->getTimePicker((int)$time[0], (int)$time[1]);
            $reply_markup = Keyboard::make([
              'inline_keyboard' => (array)$timePicker,
                'resize_keyboard' => true,
                'one_time_keyboard' => true]);
            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => "Неверно указано время\n(Доступное время только с ".$currentTime." до 19:50)\nПопробуйте заново",
              'reply_markup' => $reply_markup
            ]);
            return;
          }

          $logArr['data']=json_encode(array_merge($logArr['data'], ["start" => $messageText]));

          $isBusy = false;
          $d = new \DateTime($date);
          $records = BookRecord::where('date', $d->format('Y-m-d'))->where('room', $room)->get();

          if (count($records)) {
            $customStart = strtotime($messageText);
            foreach ($records as $record) {
              $start = strtotime($record->start);
              $end = strtotime($record->end);
              if ($customStart >= $start && $customStart <= $end) {
                $isBusy = true;
              }
            }
          }

          if ($isBusy) {
            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => "Данное время занято. Попробуйте заново",
              'reply_markup' => $reply_markup
            ]);
            return;
          } else {
            TelegramRequestLog::where('telegramId', $chatId)->delete();
          }
          // 2. Да
          $dateTime = new \DateTime($messageText);
          $dateTime = $dateTime->modify('+1 minutes');

          $minutes = (int)$dateTime->format('i');
          $hour = (int)$dateTime->format('H');
          while ($minutes % 10) {
            if ($minutes > 50){
              $hour += 1;
              $minutes = 0;
            } else {
              $minutes += 1;
            }
          }
          $dateTime->setTime($hour, $minutes);

          $res = "Введите время окончания бронирования в формате ЧЧ:ММ\n(Доступное время только с ".$dateTime->format("H:i")." до
          20:00)";

          $time = explode(":", $dateTime->format('H:i'));
          $timePicker = $dateService->getTimePicker((int)$time[0], (int)$time[1]);
          $reply_markup = Keyboard::make([
            'inline_keyboard' => (array)$timePicker,
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);
          Telegram::sendMessage(['chat_id' => $chatId,
          'text' => $res,
          'reply_markup' => $reply_markup]);

          $historyArr["response"] = $res;
          BotanDialogHistories::create($historyArr);

          TelegramRequestLog::create($logArr);
        }
      } catch
      (\Exception $e) {
          Log::debug('/booking chooseStartTime' . $e->getMessage());
      }
    }

    public static function chooseEndTime($chatId, $messageText, $username, $firstname, $lastname, $result) {
        $dateService = app(DateService::class);
      try {
        if ($result['user']) {

          $userEmail = $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN;

          $minutes = (int)date('i');
          $hour = (int)date('H');
          while ($minutes % 10) {
            if ($minutes > 50){
              $minutes = 0;
              $hour += 1;
            } else {
              $minutes += 1;
            }
          }

          $timePicker = $dateService->getTimePicker($hour, (int)$minutes);
          $reply_markup = Keyboard::make([
            'inline_keyboard' => (array)$timePicker,
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "userEmail" => $userEmail,
          ];
          // Формат времени правильный?
          // 1. Нет
          // $date = new \DateTime($messageText);
          // if (!$dateService->validateDate($messageText, 'H:i')) {
            // TelegramRequestLog::where('telegramId', $chatId)->delete();
          //   Telegram::sendMessage([
          //     'chat_id' => $chatId,
          //     'text' => "Неверный формат даты. Попробуйте заново",
          //     'reply_markup' => $reply_markup
          //   ]);
          //   return;
          // }

          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
            $oldData = json_decode($telegramRequestLog->data, true);
          $logArr = [
            "command" => $telegramRequestLog->command . "_chooseEndTime",
            "telegramId" => $chatId,
            'data' => $oldData,
          ];
          $room = $logArr['data']['room'];
          $date = $logArr['data']['date'];
          $start = $logArr['data']['start'];

          $inputTimeArray = explode(":", $messageText);
          if (isset($inputTimeArray[1])) {
            if ((int)$inputTimeArray[1] < 10) {
              $inputTimeArray[1] = "0".$inputTimeArray[1];
              $messageText = $inputTimeArray[0].":".$inputTimeArray[1];
            }
          }
          $isNotValidatedRegex = !$dateService->validateTimeRegex($messageText);
          if ($isNotValidatedRegex) {
            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => "Неверно указан формат времени\n(Доступное время только с ".$hour.":".$minutes." до 20:00)\nПопробуйте заново",
              'reply_markup' => $reply_markup
            ]);
            return;
          }

          $isNotValidated = !$dateService->validateTime($messageText, $start, '20:01', $date);
          if ($isNotValidated) {
            $dateTime = new \DateTime($start);
            $dateTime = $dateTime->modify('+1 minutes');

            $minutes = (int)$dateTime->format('i');
            $hour = (int)$dateTime->format('H');
            while ($minutes % 10) {
              if ($minutes > 50){
                $minutes = 0;
                $hour += 1;
              } else {
                $minutes += 1;
              }
            }

            $timePicker = $dateService->getTimePicker((int)$hour, (int)$minutes);
            $reply_markup = Keyboard::make([
              'inline_keyboard' => (array)$timePicker,
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => "Неверно указано время\n(Доступное время только с ".$dateTime->format("H:i")." до 20:00)\nПопробуйте заново",
              'reply_markup' => $reply_markup
              ]);
              return;
            }
          $d = new \DateTime($logArr['data']['date']);
          if ($oldData['company'] == '🏬 Сентрас Иншуранс') {
            Log::debug(json_encode([
              'author' => $result['user']->ISN,
              'to' => $logArr['data']['date']." ".$messageText, // Дата окончания
              'from' => $logArr['data']['date']." ".$logArr['data']['start'], //Дата начала
              'title' => $result['user']->FIO, // Название брони Можно ФИО отправлять если нет такого поля для заполнения
              'office' => $logArr['data']['room'], // брать key с функции getMeetingRoomList
              'description' => null, //Можно отправлять null если нет такого поля для заполнения
          ]));
            $data = Curl::to('https://my.cic.kz/api/booking/add')
            ->withData([
                'author' => $result['user']->ISN,
                'to' => $logArr['data']['date']." ".$messageText, // Дата окончания
              'from' => $logArr['data']['date']." ".$logArr['data']['start'], //Дата начала
                'title' => $result['user']->FIO, // Название брони Можно ФИО отправлять если нет такого поля для заполнения
                'office' => $logArr['data']['room'], // брать key с функции getMeetingRoomList
                'description' => null, //Можно отправлять null если нет такого поля для заполнения
            ])
            ->asJson()
            // ->withContentType('application/json')
            ->post();
            Log::debug('$arr from my.cic');
            Log::debug(json_encode($data, true));
            if ($data) {
              if (!$data->success) {
                $reply_markup = Keyboard::make([
                  'keyboard' => [
                      ['На главную']
                  ],
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true]);
                Telegram::sendMessage(['chat_id' => $chatId,
                'text' => $data->error,
                'reply_markup' => $reply_markup]);
                return;
              }
            }

          } else {
              $isBusy = false;
              $d = new \DateTime($date);
              $records = BookRecord::where('date', $d->format('Y-m-d'))->where('room', $room)->get();
              if (count($records)) {
                $customStart = strtotime($messageText);
                foreach ($records as $record) {
                    $startRecord = strtotime($record->start);
                    $endRecord = strtotime($record->end);
                    $startTimeTelegramRequestLog = strtotime($start);
                    if($startTimeTelegramRequestLog <= $endRecord){
                      if ($customStart >= $startRecord && $customStart <= $endRecord || $customStart >= $endRecord) {
                          $isBusy = true;
                        }
                    }
                }
              }

              if ($isBusy) {
                $dateTime = new \DateTime($start);
                $dateTime = $dateTime->modify('+1 minutes');

                $minutes = (int)$dateTime->format('i');
                $hour = (int)$dateTime->format('H');
                while ($minutes % 10) {
                  if ($minutes > 50){
                    $minutes = 0;
                    $hour += 1;
                  } else {
                    $minutes += 1;
                  }
                }

                $timePicker = $dateService->getTimePicker((int)$hour, (int)$minutes);

                $reply_markup = Keyboard::make([
                  'inline_keyboard' => (array)$timePicker,
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true]);
                Telegram::sendMessage([
                  'chat_id' => $chatId,
                  'text' => "Данное время занято. Попробуйте заново",
                  'reply_markup' => $reply_markup
                ]);
                return;
              }
              $newRecord = new BookRecord();
              $newRecord->start = $logArr['data']['start'];
              $newRecord->end = $messageText;
              $newRecord->date = $d->format('Y-m-d');
              $newRecord->room = $logArr['data']['room'];
              $newRecord->telegramId = $chatId;
              $newRecord->isnOrEmail = $result['user']->ISN || $result['user']->email || "";
              $newRecord->fio = $result['user']->FIO;
              $newRecord->save();
          }

          TelegramRequestLog::where('telegramId', $chatId)->delete();
          // 2. Да


          $res = "Спасибо, ваше бронирование сохранено";
          $reply_markup = Keyboard::make([
            'keyboard' => [
                ['На главную']
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);
          Telegram::sendMessage(['chat_id' => $chatId,
          'text' => $res,
          'reply_markup' => $reply_markup]);

          $historyArr["response"] = $res;
          BotanDialogHistories::create($historyArr);
        }
      } catch
      (\Exception $e) {
          Log::debug('/booking chooseEndTime' . $e->getMessage());
      }
    }

    public static function confirmDeleteBooking($chatId, $messageText, $username, $firstname, $lastname, $result) {
        $dateService = app(DateService::class);
      try {
        if ($result['user']) {

          $userEmail = $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN;

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "userEmail" => $userEmail,
          ];

          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          TelegramRequestLog::where('telegramId', $chatId)->delete();
            $oldData = json_decode($telegramRequestLog->data, true);
          $logArr = [
            "command" => $telegramRequestLog->command,
            "telegramId" => $chatId,
            'data' => $oldData,
          ];
          $room = $logArr['data']['room'];
          $dates = explode("-", $messageText);



          // 2. Да
          $res = "У вас нет брони на выбранное время. Выберите другое время или нажмите «На главную», чтобы вернуться в главное меню";
          $buttons = [];
          $date = new \DateTime($logArr['data']['date']);
          if (count($dates) === 2) {
            $start = $dates[0];
            $end = $dates[1];
            if ($oldData['company'] === '🏬 Сентрас Иншуранс') {
              $exists = false;
              $data = Curl::to('https://my.cic.kz/api/booking/get')
              ->withData(['dateStart' => $date->format('d.m.Y'), 'dateEnd' => $date->format('d.m.Y'), "office" => $room ])
              // ->withContentType('application/json')
              ->asJson()
              ->post();
              $arr = json_decode(json_encode($data, true),true);
              foreach ($arr as $room => $value) {
                foreach ($value as $key => $info) {
                  foreach ($info as $record) {
                    Log::debug($record);
                    if ($record["author"] == $result['user']->ISN
                      && $record["from"] == $date->format('Y-m-d') . " " . $dateService->formatDate($start, 'H:i:s')
                      && $record["to"] == $date->format('Y-m-d') . " ". $dateService->formatDate($end, 'H:i:s')
                    ) {
                      $exists = true;
                    }
                  }
                }
              }
              if ($exists) {
                $res = "Вы уверены, что хотите удалить броню?";
                $buttons = ['Да', 'Нет'];
                $logArr["command"] = $telegramRequestLog->command . "_confirmDeleteBooking";
              }
            }
            else{
              $record = BookRecord::where('date', $date->format('Y-m-d'))
                ->where('room', $room)
                ->where('start', $start)
                ->where('end', $end)
                ->where('telegramId', $chatId)
                ->first();
              if ($record) {
                $res = "Вы уверены, что хотите удалить броню?";
                $buttons = ['Да', 'Нет'];
                $logArr["command"] = $telegramRequestLog->command . "_confirmDeleteBooking";
              }
            }

            // else {
            //   $records = BookRecord::where('date', $date->format('Y-m-d'))
            //     ->where('room', $room)
            //     ->where('telegramId', $chatId)
            //     ->orderBy('start','asc')
            //     ->get();
            //   if (count($records)) {
            //     $buttons = [];
            //     for ($i = 0; $i < count($records); $i++) {
            //       array_push($buttons, "\n" . $dateService->formatDate($records[$i]->start, 'H:i') . "-" . $dateService->formatDate($records[$i]->end, 'H:i'));
            //     }

            //     $reply_markup = Keyboard::make([
            //       'keyboard' => [
            //           $buttons,
            //           ['На главную']
            //         ],
            //         'resize_keyboard' => true,
            //         'one_time_keyboard' => true]);
            //   }
            //   $logArr['data']=json_encode($logArr['data']);
            // }
          }
          $logArr['data']=json_encode(array_merge($logArr['data'], ["start" => $start, "end" => $end]));


          $reply_markup = Keyboard::make([
            'keyboard' => [
                $buttons,
                ['На главную']
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);

          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup]);

          $historyArr["response"] = $res;
          BotanDialogHistories::create($historyArr);

          TelegramRequestLog::create($logArr);
        }
      } catch
      (\Exception $e) {
          Log::debug('/booking confirmDeleteBooking' . $e->getMessage());
      }
    }

    public static function deleteBooking($chatId, $messageText, $username, $firstname, $lastname, $result) {
        $dateService = app(DateService::class);
      try {
        if ($result['user']) {

          $userEmail = $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN;

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "userEmail" => $userEmail,
          ];

          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chatId)->first();
          TelegramRequestLog::where('telegramId', $chatId)->delete();
            $oldData = json_decode($telegramRequestLog->data, true);
          $logArr = [
            "command" => $telegramRequestLog->command,
            "telegramId" => $chatId,
            'data' => $oldData,
          ];
          $date = new \DateTime($logArr['data']['date']);
          $room = $logArr['data']['room'];
          if ($oldData['company'] === '🏬 Сентрас Иншуранс') {
              $id = null;
              $data = Curl::to('https://my.cic.kz/api/booking/get')
              ->withData(['dateStart' => $date->format('d.m.Y'), 'dateEnd' => $date->format('d.m.Y'), "office" => $room ])
              // ->withContentType('application/json')
              ->asJson()
              ->post();
              $arr = json_decode(json_encode($data, true),true);
              foreach ($arr as $room => $value) {
                foreach ($value as $key => $info) {
                  foreach ($info as $record) {
                    Log::debug($record);
                    if ($record["author"] == $result['user']->ISN
                      && $record["from"] == $date->format('Y-m-d') . " " . $dateService->formatDate($logArr['data']['start'], 'H:i:s')
                      && $record["to"] == $date->format('Y-m-d') . " ". $dateService->formatDate($logArr['data']['end'], 'H:i:s')
                    ) {
                      $id = $record["id"];
                    }
                  }
                }
              }
            $data = Curl::to('https://my.cic.kz/api/booking/delete')
              ->withData([
                  'id' => $id,
                  'author' => $result["user"]->ISN
              ])
              ->asJson()
              ->post();
          } else {
            $record = BookRecord::where('date', $date->format('Y-m-d'))
              ->where('room', $room)
              ->where('start', $logArr['data']['start'])
              ->where('end', $logArr['data']['end'])
              ->where('telegramId', $chatId)
              ->first();
            if ($record) {
              $record->delete();
            }
          }
          $reply_markup = Keyboard::make([
            'keyboard' => [
                ['На главную']
            ],
            'resize_keyboard' => true,
            'one_time_keyboard' => true]);

            $res = "Ваша броня удалена";
          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup]);

          $historyArr["response"] = $res;
          BotanDialogHistories::create($historyArr);
          TelegramCompanyLogs::where('telegramId', $chatId)->delete();
        }
      } catch (\Exception $e) {
          Log::debug('/booking deleteBooking' . $e->getMessage());
      }
    }
}

/**
 * @param $date
 * @return bool
 *
 * @deprecated
 */
function isWeekend($date) {
  // $inputDate = \DateTime::createFromFormat("d-m-Y", $date, new \DateTimeZone("Asia/Almaty"));
  return $date->format('N') >= 6;
}

/**
 * @param $date
 * @param string $format
 * @param string $customEnd
 * @return bool
 * @throws \Exception
 *
 * @deprecated
 */
function validateDate($date, $format = 'd.m.Y', $customEnd = '20:00') {
  $dt = \DateTime::createFromFormat($format, $date);
  if ($format == 'H:i') {
    if ($dt && $dt->format($format) == $date) {
      $start = new \DateTime('08:00');
      $end = new \DateTime($customEnd);
      $checkTime = new \DateTime($date, new \DateTimeZone('Asia/Almaty'));
      if ($checkTime >= $start && $checkTime <= $end) {
        return true;
      }
    }
    return false;
  }

  return $dt && $dt->format($format);
}

/**
 * @param $date
 * @return bool
 * @throws \Exception
 * @deprecated
 */
function isNotEndedDate($date) {
  $endOfDay = new \DateTime($date);
  $endOfDay->setTime(20, 00);
  $today = new \DateTime("now", new \DateTimeZone('Asia/Almaty'));

  $timestamp1 = strtotime($today->format('d.m.Y H:i'));
  $timestamp2 = strtotime($endOfDay->format('d.m.Y H:i'));
  $hour = ($timestamp2 - $timestamp1)/(60*60);

  Log::debug($hour);
  if ($hour < 0) {
    return false;
  }
  return true;
}

/**
 * @param $date
 * @return bool
 * @deprecated
 */
function validateTimeRegex($date) {
  if (!preg_match('/([01]?[0-9]|2[0-3]):[0-5][0-9]/', $date)) {
    return false;
  }
  return true;
}

/**
 * @param $time
 * @param $customStart
 * @param $customEnd
 * @param $date
 * @return bool
 * @throws \Exception
 *
 * @deprecated
 */
function validateTime($time, $customStart, $customEnd, $date) {
  $now = new \DateTime("now", new \DateTimeZone('Asia/Almaty'));
  $requestedTime = new \DateTime($date, new \DateTimeZone('Asia/Almaty'));
  $timestamp1 = strtotime($now->format('d.m.Y H:i'));
  $timestamp2 = strtotime($requestedTime->format('d.m.Y').$time);
  $minutes = ($timestamp2 - $timestamp1)/(60);
  Log::debug('$minutes');
  Log::debug($minutes);
  if ($minutes < 0) {
    return false;
  }

  $start = strtotime($customStart);
  $end = strtotime($customEnd);
  $checkTime = strtotime($time);
  Log::debug('$checkTime - $start > 0');
  Log::debug($checkTime - $start > 0);
  Log::debug('$checkTime - $end < 0');
  Log::debug($checkTime - $end < 0);
  if (($checkTime - $start > 0) && ($checkTime - $end < 0)) {
    return true;
  }
  return false;
}

/**
 * @param $date
 * @param $customEnd
 * @return bool
 *
 * @deprecated
 */
function validateTimeTill($date, $customEnd) {
  $twoPm = new \DateTime();
  $twoPm->setTime(20,0);
  if ($date < $twoPm) {
    return true;
  } else {
    return false;
  }
}

/**
 * @param $date
 * @param string $format
 * @return string
 * @throws \Exception
 *
 * @deprecated
 */
function formatDate($date, $format = 'd.m.Y') {
  $dt = new \DateTime($date);
  return $dt->format($format);
}

/**
 * @param $date
 * @return bool
 *
 * @deprecated
 * @throws \Exception
 */
function isToday($date) {
  $now = new \DateTime("now", new \DateTimeZone('Asia/Almaty'));
  $requestedTime = new \DateTime($date, new \DateTimeZone('Asia/Almaty'));
  $current = strtotime($now->format('d.m.Y'));
  $date = strtotime($requestedTime->format('d.m.Y'));

  $datediff = $date - $current;
  $difference = floor($datediff/(60*60*24));
  Log::debug('$difference');
  Log::debug($difference);
  if ($difference == 0) {
    return true;
  }
  return false;
}
