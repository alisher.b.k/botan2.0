<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;
use App\TelegramCompanyLogs;

class MenuCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "menu";
    public static $commands = "Вот, что я умею: \n" .
    "👨‍💼 Найду коллегу /facebook" .
    "\n"."🏝 Мой отпуск /vacation".
    "\n⌚️ Бронирование /booking".
    "\n"."🏦 О нас /culture".
    "\n"."💡 Заявить о проблеме /problem".
    "\n ✉️ Улучшить Ботан /feedback".
    "\n"."👩‍⚕️ Запись на прием к врачу  /doctor".
    "\n"."👩‍⚕️ Лимиты по ДМС /limits".
    "\n"."💻️ Магазин Cent.kz  /centkz".
    "\n"."📑 Внутренний рекрутинг /recruiting";
    /**
     * @var string Command Description
     */
    protected $description = "Меню";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            TelegramCompanyLogs::where('telegramId', $chatId)->delete();

            $userEmail = $result['user'] ? $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN
            : '';

            $keyboard = DefaultKeyboard::getMenuKeyboard($result['user']->WORKPLACE);

            $response = MenuCommand::$commands;

            if ($result['user'] && $result['user']->WORKPLACE === '"АО СК Коммеск-Омир"') {
                $response .= "\n🔒️   Сброс пароля /pwreset";
            }

            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);

            Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => $response,
                'reply_markup' => $reply_markup
            ]);
            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $response,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
        } catch (\Exception $e) {
            Log::debug('/menu ' . $e->getMessage());
        }
    }

    public static function returnMenuKeyboard($chatId){
        $workPlace = StaffTelegramUsers::getWorkPlaceByChatId($chatId);
        $keyboard = DefaultKeyboard::getMenuKeyboard($workPlace);

        $response = "Вот, что я умею: \n" .
            "👨‍💼 Найду коллегу /facebook" .
            "\n"."🏝 Мой отпуск /vacation".
            "\n"."🏦 О нас /culture".
            "\n"."💡 Заявить о проблеме /problem".
            "\n ✉️ Улучшить Ботан /feedback".
            // ."\n 🤑 Cенткоины /centcoins"
            "\n"."👩‍⚕️ Запись на прием к врачу  /doctor".
            "\n"."👩‍⚕️ Лимиты по ДМС /limits".
            "\n"."💻️ Магазин Cent.kz  /centkz".
            "\n"."📑 Внутренний рекрутинг /recruiting";
        // "\nМетодология /methodology".
        // "\n📚 Библиотека /library";
        if (
            $workPlace === 'АО "СК "Сентрас Иншуранс"' ||
            $workPlace === '"АО СК Коммеск-Омир"' ||
            $workPlace === "АО \"КСЖ Сентрас Коммеск Life\""

        ) {
            $response .= "\n⌚️ Бронирование /booking";
        }

        if ($workPlace === '"АО СК Коммеск-Омир"'){
            $response .= "\n🔒️   Сброс пароля /pwreset";
        }

        // "💼 Помогу продавцам в работе /forsales \n" .
        // "❓ Разбираюсь в кадровых вопросах /hr \n" .
        // "ℹ Могу рассказать много интересного о холдинге /culture \n" .
        // "Методология /methodology \n".
        // "Библиотека /library \n".
        // "Опросы /poll \n".
        // "Калькулятор /calculator \n".

        // if ($result['user']->WORKPLACE === '"АО СК Коммеск-Омир"') {
        //     $response .= "📁 Офис /office \n";
        //     array_push($keyboard[2], 'Офис');
        // }

        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => $response,
            'reply_markup' => $reply_markup
        ]);
    }

    public static function expandMenu($chatId, $username, $firstname, $lastname, $result){
        try {
            TelegramRequestLog::deleteRows($chatId);
            TelegramRequestLog::setNewLog($chatId, 'menu:expanded');
            $userEmail = $result['user'] ? $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN
                : '';

            $keyboard = DefaultKeyboard::getExpandedKeyboard($result['user']->WORKPLACE);

            $response = "Нажмите нужную кнопку";

            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);

            Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => $response,
                'reply_markup' => $keyboard
            ]);
            $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "response" => $response,
                "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
        } catch (\Exception $e) {
            Log::debug('/menu ' . $e->getMessage());
        }
    }
}
