<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\CustomCommand;


class HoldingCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "holding";

    /**
     * @var string Command Description
     */
    protected $description = "О холдинге";

    /**
     * @inheritdoc
     */
    public function handle()
    {
      $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
      $chatId = $chat->getId();
      $username = $chat->getUsername();
      $firstname = $chat->first_name;
      $lastname = $chat->last_name;
      $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
      $messageText = strtolower($message->getText());

      $result = CheckUser::index($chatId);

      $historyArr = [
        "chatId" => $chatId,
        "telegramUsername" => $username,
        "telegramFirstName" => $firstname,
        "telegramLastName" => $lastname,
        "request" => $messageText,
      ];

      // запись запроса в историю диалогов
      $historyArr["userEmail"] = $result['user']
        ? $result['user']->email
          ? $result['user']->email
          : $result['user']->ISN
        : '';
      BotanDialogHistories::create($historyArr);

      $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
    }

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

          TelegramRequestLog::where('telegramId', $chatId)->delete();
          
          $aboutUs = DB::table('about_us')->first();
          $res = $aboutUs->contentv2;

          $log = new TelegramRequestLog();
          $log->telegramId = $chatId;
          $log->command = 'holding';
          $log->save();

          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup,
              'disable_web_page_preview' => 'true',
              ]);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $res,
            "userEmail" => $userEmail,
          ];
          BotanDialogHistories::create($historyArr);

        
        }
      } catch (\Exception $e) {
          Log::debug('/holding ' . $e->getMessage());
      }
    }

    public static function sendCommandContent($chatId, $messageText, $username, $firstname, $lastname, $result) {
      try {
          if ($result['user']) {
              TelegramRequestLog::where('telegramId', $chatId)->delete();

              $reply_markup = Keyboard::make([
                  'keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true
              ]);
              $response = 'Команда не найдена';
              $command = CustomCommand::where('command', $messageText)->first();

              if ($command) {
                $token = getenv('TELEGRAM_BOT_TOKEN');
                define('BOTAPI', 'https://api.telegram.org/bot' . $token . '/');

                if ($command->content) {
                  // $data2 = [
                  //   'chat_id' => $chatId,
                  //   'document' => new \CURLFile($command->content),
                  //   'reply_markup' => $reply_markup
                  // ];
                  // $ch = curl_init(BOTAPI . 'sendDocument');
                  $response = $command->content;
                  Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,                    
                  ]);
                }
                if($command->file) {                  
                  $extension = explode(".", $command->file);                  
                  $path = public_path()."/about_us/".$command->file;
                  

                  if ($extension[count($extension)-1] === 'pdf') {
                    // send pdf  
                    $data2 = [
                      'chat_id' => $chatId,
                      'document' => new \CURLFile($path),
                      'reply_markup' => $reply_markup
                    ];
                    $ch = curl_init(BOTAPI . 'sendDocument');
                  } else {
                    // send image
                    $data2 = [
                      'chat_id' => $chatId,
                      'photo' => new \CURLFile($path),
                      'reply_markup' => $reply_markup
                    ];
                    $ch = curl_init(BOTAPI . 'sendPhoto');                    
                  }                  
                }
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data2);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                $output = curl_exec($ch);
                curl_close($ch);
              }

              

              $userEmail = $result['user']->email
                  ? $result['user']->email
                  : $result['user']->ISN;
              $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $response,
                  "userEmail" => $userEmail,
              ];
              BotanDialogHistories::create($historyArr);

              $log = new TelegramRequestLog();
              $log->telegramId = $chatId;
              $log->command = 'holding';
              $log->save();
          }
      } catch (\Exception $e) {
          Log::debug('/tech_staff sendRequest' . $e->getMessage());
      }
  }
}
