<?php
/**
 * Created by PhpStorm.
 * User: Anamaria
 * Date: 30.06.2019
 * Time: 19:40
 */

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Telegram\ConfigClass;

class MethodologyCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "methodology";

    /**
     * @var string Command Description
     */
    protected $description = "Методология";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            if ($result['user']) {
                $keyboard = [
                    ['Вопросы', 'Категории вопросов',],
                    ['Поиск вопросов'],
                    ['На главную']
                ];
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);

                $response = "❓ Все вопросы /questions \n" .
                    "❓ Категории вопросов /questionscategories \n" .
                    "❓ Поиск вопросов /searchquestions";

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup
                ]);
                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;
                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $response,
                  "userEmail" => $userEmail,
                ];
                BotanDialogHistories::create($historyArr);
              }
        } catch
        (\Exception $e) {
            Log::debug('/methodology ' . $e->getMessage());
        }
    }
}
