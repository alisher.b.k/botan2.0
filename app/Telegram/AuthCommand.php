<?php

namespace App\Telegram;

use App\BotanCodes;
use App\BotanDialogHistories;
use App\BotanStaff;
use App\Http\Controllers\BotanTelegramUsersController;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class AuthCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "auth";

    /**
     * @var string Command Description
     */
    protected $description = "Авторизация";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
        $chat = $message->getChat();
        $chatId = $chat->getId();
        $username = $chat->getUsername();
        $firstname = $chat->first_name;
        $lastname = $chat->last_name;
        $messageText = strtolower($message->getText());

        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "request" => $messageText,
        ];

        BotanDialogHistories::create($historyArr);

        $this->executeCommand($chatId, $username, $firstname, $lastname);
    }

    public static function executeCommand($chatId, $username, $firstname, $lastname)
    {
        try {
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $log = new TelegramRequestLog();
            $log->telegramId = $chatId;
            $log->command = 'auth: enter email';
            $log->save();

            $text = 'Введите email или ISN';

            Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => $text,
            ]);

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $text,
            ];
            BotanDialogHistories::create($historyArr);
        } catch (\Exception $e) {
            Log::channel('telegram')->debug('/auth ' . $e->getMessage());
        }
    }

    /**
     * @param $chatId
     * @param $messageText
     */
    public static function login($chatId, $messageText, $username, $firstname, $lastname)
    {
        try {

            $isISN = false;

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "userEmail" => $messageText,
            ];

            $user = BotanStaff::where('email', $messageText)->first();
            Log::channel('telegram')->debug($user);
            if (!$user) {
                $email = StaffEmail::where("email", $messageText)->first();
                if ($email) {
                    $user = BotanStaff::find($email->userId);
                } else {
                    $user = BotanStaff::where('ISN', $messageText)->first();
                    $isISN = true;
                }
            }
            if ($user) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                Log::channel('telegram')->debug($isISN);
                if (!$isISN) {
                    $code = BotanTelegramUsersController::generateCode();
                    session()->put('code', $code);
                    Log::channel('telegram')->debug($code);
                    Mail::send(['text' => 'mail'], ['name' => 'Код для авторизации'], function ($message) use ($user, $messageText) {
                        $message->to($messageText, $user->FIO)->subject('Код для авторизации');
                        $message->from('bot@centras.kz', 'Botan');
                    });

                    $newCode = new BotanCodes();
                    $newCode->botan_user_id = $user->id;
                    $newCode->code = $code;
                    $newCode->save();

                    $log = new TelegramRequestLog();
                    $log->telegramId = $chatId;
                    $log->command = 'auth: waiting for code verification '.$messageText;
                    $log->save();

                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => 'Письмо с кодом отправлено вам на почту',
                    ]);
                    $historyArr["response"] = 'Письмо с кодом отправлено вам на почту';
                    BotanDialogHistories::create($historyArr);

                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => 'Введите код',
                    ]);
                    $historyArr["response"] = 'Введите код';
                    BotanDialogHistories::create($historyArr);
                } else {
                    $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)->first();
                    if ($telegramUser) {
                        $telegramUser->staffEmail = $messageText;
                        $telegramUser->save();
                    } else {
                        $telegramUser = new StaffTelegramUsers();
                        $telegramUser->staffEmail = $messageText;
                        $telegramUser->telegramId = $chatId;
                        $telegramUser->save();
                    }
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => 'Авторизация прошла успешно',
                    ]);
                    $historyArr["response"] = 'Авторизация прошла успешно';
                    BotanDialogHistories::create($historyArr);

                    $reply_markup = Keyboard::make([
                        'keyboard' => DefaultKeyboard::getKeyboard(),
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true
                    ]);

                    // $response = "Вот, что я умею: \n" .
                    //     "👨‍💼 Помогу найти коллегу /facebook" .
                    //     "\n" .
                    //     "💼 Помогу продавцам в работе /forsales \n" .
                    //     "❓ Разбираюсь в кадровых вопросах /hr \n" .
                    //     "ℹ Могу рассказать много интересного о холдинге /culture \n" .
                    //     "Методология /methodology \n".
                    $response = "Вот, что я умею: \n" .
                        "👨‍💼 Найду коллегу /facebook" .
                        "\n"."🏝 Мой отпуск /vacation".
                        "\n"."🏦 О холдинге /holding".
                        "\n"."💡 Заявить о проблеме /problem".
                        "\n ✉️ Улучшить Ботан /feedback".
                        "\n"."💻️ Магазин Cent.kz  /centkz".
                        "\n"."👩‍⚕️ Лимиты по ДМС /limits"
                        // ."\n 💰 Мой сенткоины /balance"
                        ;
                        // "\nМетодология /methodology".
                        // "\n📚 Библиотека /library"
                    if ($user->WORKPLACE === 'АО "СК "Сентрас Иншуранс"' || $user->WORKPLACE === '"АО СК Коммеск-Омир"'||$user->WORKPLACE === "АО \"КСЖ Сентрас Коммеск Life\""){
                        $response .= "\n⌚️ Бронирование /booking";
                    }

                    if ($user->WORKPLACE === '"АО СК Коммеск-Омир"'){
                        $response .= "\n🔒️   Сброс пароля /pwreset";
                    }

                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $response,
                        'reply_markup' => $reply_markup
                    ]);
                    $historyArr["response"] = $response;
                    BotanDialogHistories::create($historyArr);
                }
            } else {

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => "Email или ISN введен неверно или у вас нет доступа для просмотра информации.\nПопробуйте ввести заново",
                ]);
                $historyArr["response"] = "Email или ISN введен неверно или у вас нет доступа для просмотра информации.\nПопробуйте ввести заново";
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/auth login ' . $e->getMessage());
        }
    }

    public static function checkCode($chatId, $messageText, $email, $username, $firstname, $lastname)
    {
        try {
            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "userEmail" => $messageText,
            ];

            $user = BotanStaff::where('email', strtolower($email))->first();
            if(!$user) {
                $email = StaffEmail::where("email", strtolower($email))->first();
                if ($email) {
                    $user = BotanStaff::find($email->userId);
                }
            }
            if ($user) {
                $historyArr["userEmail"] = $email;

                $code = BotanCodes::where('botan_user_id', $user->id)
                    ->where('code', $messageText)
                    ->first();

                TelegramRequestLog::where('telegramId', $chatId)->delete();
                BotanCodes::where('botan_user_id', $user->id)->delete();

                if(!$code){
                    $keyboard = [['Авторизация'],];
                    $reply_markup = Keyboard::make([
                        'keyboard' => $keyboard,
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true
                    ]);
                    $text = 'Код введен неверно. Пройдите авторизацию повторно /auth';
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $text,
                        'reply_markup' => $reply_markup
                    ]);
                    $historyArr["response"] = $text;
                    BotanDialogHistories::create($historyArr);
                } else {
                  $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)->first();
                  if ($telegramUser) {
                      $telegramUser->staffEmail = $email;
                      $telegramUser->save();
                  } else {
                      $telegramUser = new StaffTelegramUsers();
                      $telegramUser->staffEmail = $email;
                      $telegramUser->telegramId = $chatId;
                      $telegramUser->save();
                  }
                    $text = 'Авторизация прошла успешно';
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $text,
                    ]);
                    $historyArr["response"] = $text;
                    BotanDialogHistories::create($historyArr);

                    $reply_markup = Keyboard::make([
                        'keyboard' => DefaultKeyboard::getKeyboard(),
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true
                    ]);

                    // $response = "Вот, что я умею: \n" .
                    //     "👨‍💼 Помогу найти коллегу /facebook" .
                    //     "\n" .
                    //     "💼 Помогу продавцам в работе /forsales \n" .
                    //     "❓ Разбираюсь в кадровых вопросах /hr \n" .
                    //     "ℹ Могу рассказать много интересного о холдинге /culture \n" .
                    //     "Методология /methodology \n";

                    $response = "Вот, что я умею: \n" .
                            "👨‍💼 Найду коллегу /facebook" .
                            "\n"."🏝 Мой отпуск /vacation".
                            "\n"."🏦 О холдинге /holding".
                            "\n"."💡 Заявить о проблеме /problem".
                            "\n ✉️ Улучшить Ботан /feedback".
                            "\n"."💻️ Магазин Cent.kz  /centkz".
                            "\n"."👩‍⚕️ Лимиты по ДМС /limits"
                            // ."\n 💰 Мой сенткоины /balance"
                            ;
                            // "\nМетодология /methodology".
                            // "\n📚 Библиотека /library";
                    if ($user->WORKPLACE === 'АО "СК "Сентрас Иншуранс"' || $user->WORKPLACE === '"АО СК Коммеск-Омир"' || $user->WORKPLACE === "АО \"КСЖ Сентрас Коммеск Life\""){
                        $response .= "\n⌚️ Бронирование /booking";
                    }

                    if ($user->WORKPLACE === '"АО СК Коммеск-Омир"') {
                        $response .= "\n🔒️   Сброс пароля /pwreset";
                    }

                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $response,
                        'reply_markup' => $reply_markup
                    ]);
                    $historyArr["response"] = $response;
                    BotanDialogHistories::create($historyArr);
                }
            } else {
                $text = "Email введен неверно или у вас нет доступа для просмотра информации. Пройдите авторизацию повторно /auth";
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $text,
                ]);
                $historyArr["response"] = $text;
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/auth check code ' . $e->getMessage());
        }
    }
}
