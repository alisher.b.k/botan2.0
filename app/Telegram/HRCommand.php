<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;

class HRCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "hr";

    /**
     * @var string Command Description
     */
    protected $description = "HR";


    /**
     * @inheritdoc
     */
    // public function handle()
    // {
    //     $chatId = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat()->getId();
    //     $this->executeCommand($chatId);
    // }

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
        try {
            if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $log = new TelegramRequestLog();
                $log->telegramId = $chatId;
                $log->command = 'hr';
                $log->save();

                $keyboard = [['На главную'], ['/vacation', 'Категории вопросов']];
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);

                $response = "❓ Категории вопросов /questionscategories \n" .
                    "🏝 Мой отпуск /vacation \n";

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup
                ]);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;
                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $response,
                  "userEmail" => $userEmail,
                ];
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/hr ' . $e->getMessage());
        }
    }
}
