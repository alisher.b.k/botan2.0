<?php
namespace App\Telegram;
use App\BotanStaff;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Illuminate\Support\Facades\Log;
use App\TelegramRequestLog;
use App\TypeMessages;
use App\StaffTelegramUsers;

 class SubscriptionCommand extends Command
 {
     protected $name = 'subscription';

     protected $description = 'Subscription command, Get a list of commands';

     public function handle(){
         $update = Telegram::getWebhookUpdates();
         $message = $update->getMessage();
         $chatId = $message->getChat()->getId();
         $username = $message->getChat()->getUsername();
         $firstname = $message->getChat()->first_name;
         $lastname = $message->getChat()->last_name;
         $messageText = $message->getText();
         $this->executeCommand($chatId,$username,$firstname,$lastname);
     }
     public static function executeCommand($chatId,$username,$firstname,$lastname){
         try {
             TelegramRequestLog::where('telegramId', $chatId)->delete();
         $log = new TelegramRequestLog();
         $log->telegramId = $chatId;
         $log->command = 'sub:YesOrNo';
         $log->save();

         $text = "\nДобро пожаловать в раздел подписки!\n";
         $data = TypeMessages::all();
         $keyboard = DefaultKeyboard::subscription($data,$chatId);
         Telegram::sendMessage([
             'chat_id'=>$chatId,
             'text' => $text,
             'reply_markup' => $keyboard,
         ]);
             } catch (\Exception $e) {
                 Log::debug('/subscription executeCommand' . $e->getMessage());
             }
     }
     public static function yesOrNo($messageId,$callback,$chatId){
         try {
             $messages = TypeMessages::all();

             $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)->first();
             $user = BotanStaff::where('email', $telegramUser->staffEmail)->orWhere('isn', $telegramUser->staffEmail)->first();

             $types = $user->typeMessages;

             $result = 0;
             if(count($types) === 0){
                 $result = 0;
             }else{
                     foreach($types as $type){
                         if("❌".$type->name === $callback){
                             $result += 1;
                         }else{
                             $result +=0;
                         }
             }}
             if($result === 1){
                 foreach($messages as $message){
                     if("❌".$message->name === $callback){
                             $user->typeMessages()->detach($message->id);
                     }
                 }
             }else{
                     foreach($messages as $message){
                         if("✅".$message->name === $callback){
                                 $user->typeMessages()->attach($message->id);
                         }
                     }
             }

             $keyboard = DefaultKeyboard::subscription($messages,$chatId);
             $text = "\nДобро пожаловать в раздел подписки!\n";
             Telegram::editMessageText([
             'text' => $text,
             'chat_id' => $chatId,
             'message_id' => $messageId,
             'reply_markup' => $keyboard
             ]);
         }catch (\Exception $e) {
             Log::debug('/subscription yesOrNo' . $e->getMessage());
         }
     }
 }

