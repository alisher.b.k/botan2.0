<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\PollResults;
use App\Poll;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Telegram\ConfigClass;

class PollCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "poll";

    /**
     * @var string Command description
     */
    protected $description = "Опросы";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
      try {
        if ($result['user']) {
              TelegramRequestLog::where('telegramId', $chatId)->delete();

              $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                'resize_keyboard' => true,
                'one_time_keyboard' => true]);


              $pollRes = PollResults::where('chatId', $chatId)
              ->get();
              $poll = Poll::get();

              
              for ($i=0; $i< count($pollRes); $i++) {
                  foreach($poll as $key => $value) {

                      if ($value->id == $pollRes[$i]->pollId) {
                          unset($poll[$key]);
                      }
                  }
//                for ($j=0; $j< count($pollArray); $j++) {

//                }
              }
              
              $res = "";

            foreach($poll as $key => $value) {
                $res .= $value->id.'. '.$value->question."\n\n";
              }

              $res .= "\n\n Выберите номер опроса";
              
              Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup]);
              
              $logArr = [
                "command" => 'waiting poll',
                "telegramId" => $chatId,
              ];
              TelegramRequestLog::create($logArr);
              
              $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "response" => $res,
                "userEmail" => $result['user']->email,
              ];
              BotanDialogHistories::create($historyArr);

          }
      } catch (\Exception $e) {
          Log::debug('/poll execute' . $e->getMessage());
      }
    }

    public static function showPollAnswers($chatId, $messageText, $username, $firstname, $lastname, $result){
        try {
            if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                $logArr = [
                    "command" => 'poll' . $messageText,
                    "telegramId" => $chatId,
                ];
                $keyboard = [['Да', 'Нет']];

                $reply_markup = Keyboard::make(['keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);

                $poll = Poll::where('id', $messageText)->first();

                Telegram::sendMessage(['chat_id' => $chatId,
                    'text' => $poll->question,
                    'reply_markup' => $reply_markup]);

                TelegramRequestLog::create($logArr);

                $historyArr = [
                    "chatId" => $chatId,
                    "telegramUsername" => $username,
                    "telegramFirstName" => $firstname,
                    "telegramLastName" => $lastname,
                    "response" => $poll->question,
                    "userEmail" => $result['user']->email,
                ];
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/poll execute' . $e->getMessage());
        }
    }

    public static function saveResult($chatId, $messageText, $username, $firstname, $lastname, $result, $pollId)
    {
        try {
            $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                'resize_keyboard' => true,
                'one_time_keyboard' => true]);
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            $res = $result['res'];
            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;
            $res = "Спасибо за ваш ответ";

            $pollRes = PollResults::where('chatId', $chatId)
              ->where('pollId', $pollId)
              ->first();
            if ($pollRes) {
              $pollRes->answer = $messageText;
              $pollRes->save();
            } else {
              $pollRes = new PollResults();
              $pollRes->chatId = $chatId;
              $pollRes->userEmail = $userEmail;
              $pollRes->pollId = $pollId;
              $pollRes->answer = $messageText;
              $pollRes->save();
            }

            Telegram::sendMessage(['chat_id' => $chatId,
                'text' => $res,
                'reply_markup' => $reply_markup]);

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $res,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
        } catch
        (\Exception $e) {
            Log::debug('/poll saveResult' . $e->getMessage());
        }
    }
}
