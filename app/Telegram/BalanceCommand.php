<?php

namespace App\Telegram;

use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\TelegramRequestLog;
use App\BotanDialogHistories;
use App\StaffBalance;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use Telegram\Bot\FileUpload\InputFile;


class BalanceCommand extends Command {
  /**
   * @var string Command Name
   */
  protected $name = "centcoins";

  /**
   * @var string Command Description
   */
  protected $description = "";

  /**
   * @inheritdoc
   */
  public function handle() {
    $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
    $chatId = $chat->getId();
    $username = $chat->getUsername();
    $firstname = $chat->first_name;
    $lastname = $chat->last_name;
    $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
    $messageText = strtolower($message->getText());

    $result = CheckUser::index($chatId);

    $historyArr = [
      "chatId" => $chatId,
      "telegramUsername" => $username,
      "telegramFirstName" => $firstname,
      "telegramLastName" => $lastname,
      "request" => $messageText,
    ];

    // запись запроса в историю диалогов
    $historyArr["userEmail"] = $result['user']
      ? $result['user']->email
        ? $result['user']->email
        : $result['user']->ISN
      : '';
    BotanDialogHistories::create($historyArr);

    $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
  }

  public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
    try {
      if ($result['user']) {
        $userEmail = $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN;

        $reply_markup = Keyboard::make(['keyboard' => [['💰 Мой баланс', '🎁 Призы'],['На главную']],
        'resize_keyboard' => true,
        'one_time_keyboard' => true]);   

        Telegram::sendMessage(['chat_id' => $chatId,
            'text' => "💰 Мой баланс /balance \n🎁 Призы /prizes",
            'reply_markup' => $reply_markup
            ]);

        TelegramRequestLog::where('telegramId', $chatId)->delete();
        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "response" => "💰 Мой баланс /balance \n🎁 Призы /prizes",
          "userEmail" => $userEmail,
        ];
        BotanDialogHistories::create($historyArr);
      }
    } catch (\Exception $e) {
        Log::debug('/balance executeCommand' . $e->getMessage());
    }
  }

  public static function balance($chatId, $username, $firstname, $lastname, $result) {
    try {
      if ($result['user']) {
        $userEmail = $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN;

        $reply_markup = Keyboard::make(['keyboard' => [['🎁 Призы'],['На главную']],
        'resize_keyboard' => true,
        'one_time_keyboard' => true]);

        $balance = 0;
        $balanceInfo = StaffBalance::where('telegramId', $chatId)->first();  
        if ($balanceInfo && $balanceInfo->balance) {
          $balance = $balanceInfo->balance;
        }     
        
        
        // if ($result['user']->WORKPLACE === 'АО "СК "Сентрас Иншуранс"') {
        //   Telegram::sendMessage(['chat_id' => $chatId,
        //     'text' => 'Пожалуйста подождите пока я посчитаю',
        //     'reply_markup' => $reply_markup
        //     ]);
        //   try {
        //     $centcoinsResponse = Curl::to('https://my.cic.kz/api/centcoins?isn='.$result['user']->ISN)
        //       ->withTimeout(60 * 10)
        //       ->get();
        //       $arr = json_decode($centcoinsResponse);
        //     if ($arr->coins){
        //       $balance = $arr->coins;
        //     }
        //   } catch(\Exception $e){
        //     Log::debug($e);
        //   }      
        // }       

        Telegram::sendMessage(['chat_id' => $chatId,
            'text' => $balance,
            'reply_markup' => $reply_markup
            ]);

        TelegramRequestLog::where('telegramId', $chatId)->delete();
        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "response" => $balance,
          "userEmail" => $userEmail,
        ];
        BotanDialogHistories::create($historyArr);
      }
    } catch (\Exception $e) {
        Log::debug('/balance' . $e->getMessage());
    }
  }

  public static function prizes($chatId, $username, $firstname, $lastname, $result) {
    try {
      if ($result['user']) {
        $userEmail = $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN;

        $reply_markup = Keyboard::make(['keyboard' => [['💰 Мой баланс'],['На главную']],
        'resize_keyboard' => true,
        'one_time_keyboard' => true]);

        $fileName = 'prizes.jpeg';
        $dir    = public_path();
        $files1 = scandir($dir);
        foreach($files1 as $old) {
            if (preg_match("/prizes/i", $old)) {
              $fileName = $old;
            } 
        }
        $resp = Telegram::sendPhoto([
          'chat_id' => $chatId,
          'photo' => new InputFile(public_path() .'/'. $fileName, $fileName),
          'reply_markup' => $reply_markup,
        ]);


        TelegramRequestLog::where('telegramId', $chatId)->delete();
        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "response" => public_path() .$fileName,
          "userEmail" => $userEmail,
        ];
        BotanDialogHistories::create($historyArr);
      }
    } catch (\Exception $e) {
        Log::debug('/prizes' . $e->getMessage());
    }
  }
}