<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\QuestionCategories;
use App\QuestionsAnswers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;

class QuestionsCategoriesCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "questionscategories";

    /**
     * @var string Command Description
     */
    protected $description = "Категории вопросов";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result['user']) {
                $userEmail = $result['user']->email
                  ? $result['user']->email
                  : $result['user']->ISN;
                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "userEmail" => $userEmail,
                ];

                $log = TelegramRequestLog::where('telegramId', $chatId)->first();
                if ($log && $log->command === 'hr') {
                    $categories = QuestionCategories::where("parent_id", 1)->get();
                } else {
                    $categories = QuestionCategories::where("parent_id", 0)->get();
                }
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                if (count($categories) > 0) {
                    $res = "";
                    foreach ($categories as $category) {
                        $res .= $category->id . ". ";
                        $res .= $category->categoryName . "\n";
                    }
                } else {
                    $res = "Категории не найдены";
                }
                $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true]);

                Telegram::sendMessage(['chat_id' => $chatId,
                    'text' => $res,
                    'reply_markup' => $reply_markup]);
                $historyArr["response"] = $res;
                BotanDialogHistories::create($historyArr);

                $text =  "Выберите номер категории и отправьте команду 'категория номер_категории'\nНапример: категория 10";
                Telegram::sendMessage(['chat_id' => $chatId,
                    'text' => $text]);
                $historyArr["response"] = $text;
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/questionscategories ' . $e->getMessage());
        }
    }
}
