<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;

class OfficeCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "office";

    /**
     * @var string Command Description
     */
    protected $description = "Office buttons";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                $keyboard = [
                  ['Жанабай', 'Тех. персонал'],
                  ['help desk'], ['На главную'],
                ];
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);

                $response = "👨 Жанабай /zhanabay \n" .
                    "🧤 Тех. персонал /tech_staff \n" . //TODO: /tech_staff
                    "ℹ️ help desk /help_desk"; //TODO: /help_desk

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup
                ]);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;
                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $response,
                  "userEmail" => $userEmail,
                ];
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/office ' . $e->getMessage());
        }
    }
}
