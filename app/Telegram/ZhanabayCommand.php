<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;

class ZhanabayCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "zhanabay";

    /**
     * @var string Command Description
     */
    protected $description = "";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $log = new TelegramRequestLog();
                $log->telegramId = $chatId;
                $log->command = 'zhanabay';
                $log->save();

                $reply_markup = Keyboard::make([
                    'keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);

                $response = "Опишите запрос";

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup
                ]);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;
                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $response,
                  "userEmail" => $userEmail,
                ];
                BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/zhanabay ' . $e->getMessage());
        }
    }

    public static function sendRequest($chatId, $messageText, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result['user']) {
                TelegramRequestLog::where('command', 'zhanabay waiting answer')->delete();

                $reply_markup = Keyboard::make([
                  'keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true
              ]);

                $response = "Спасибо за обращение";

                Telegram::sendMessage([
                  'chat_id' => $chatId,
                  'text' => $response,
                  'reply_markup' => $reply_markup
              ]);

                $userEmail = $result['user']->email
                  ? $result['user']->email
                  : $result['user']->ISN;
                $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "response" => $response,
                "userEmail" => $userEmail,
              ];
                BotanDialogHistories::create($historyArr);

                $usernameStr = $username ? "telegram: @".$username."\n" : "";

                // отправка запроса сотруднику Жанабай
                // -381528038 - тестовый чат
                // -373887467 - группа Жанабая
                $chatIdArr = ["-381528038", "-373887467"];
                $reply_markup = Keyboard::make([
                  'keyboard' => [['Буду в течении часа'], ['Буду в течении дня'], ['Сегодня не получится, повторите запрос завтра']],
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true
              ]);
                $log = new TelegramRequestLog();
                $log->telegramId = $chatId;
                $log->command = 'zhanabay waiting answer';
                $log->save();
                foreach ($chatIdArr as $id) {
                    $text =  "Запрос к сотруднику Жанабай \n\n" .
                  $usernameStr. "" .$userEmail . "\nЗапрос: " . $messageText;
                    $resp = Telegram::sendMessage([
                    'chat_id' => $id,
                    'text' => $text,
                    'reply_markup' => $reply_markup
                ]);
                    $historyArr['chatId'] = $id;
                    if ($resp->chat) {
                      if ($resp->chat->title) {
                          $historyArr['telegramUsername'] = "";
                          $historyArr['telegramFirstName'] = $resp->chat->title;
                          $historyArr['telegramLastName'] = "";
                      } elseif ($resp->chat->first_name || $resp->chat->last_name || $resp->chat->username) {
                          $historyArr['telegramUsername'] = $resp->chat->username;
                          $historyArr['telegramFirstName'] = $resp->chat->first_name;
                          $historyArr['telegramLastName'] = $resp->chat->last_name;
                      }
                    }
                    $historyArr["response"] = $text;
                    BotanDialogHistories::create($historyArr);
                }
            }
        } catch (\Exception $e) {
            Log::debug('/zhanabay sendRequest' . $e->getMessage());
        }
    }

    public static function sendZhanabayAnswerToUser($chatId, $messageText, $username, $firstname, $lastname)
    {
        try {
            $log = TelegramRequestLog::where('command', 'zhanabay waiting answer')->first();
            $reply_markup = Keyboard::make([
                  'keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true
              ]);

            $response = "Ответ от сотрудника Жанабай \n".$messageText;

            Telegram::sendMessage([
                  'chat_id' => $log->telegramId,
                  'text' => $response,
                  'reply_markup' => $reply_markup
              ]);

            $historyArr = [
                "chatId" => $log->telegramId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "response" => $response,
              ];
            BotanDialogHistories::create($historyArr);

            $log->delete();
        } catch (\Exception $e) {
            Log::debug('/zhanabay sendZhanabayAnswerToUser' . $e->getMessage());
        }
    }
}
