<?php
/**
 * Created by PhpStorm.
 * User: Anamaria
 * Date: 30.06.2019
 * Time: 20:22
 */

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Telegram\ConfigClass;

class UpdatesSubscription extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "subscription";

    /**
     * @var string Command Description
     */
    protected $description = "Подписка-отписка от обновлений";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();

                $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)->first();
                $subscribed = $telegramUser->updatesSubscription === 1 ? 0 : 1;
                $telegramUser->updatesSubscription = $subscribed;
                $telegramUser->save();

                $reply_markup = Keyboard::make([
                    'keyboard' => DefaultKeyboard::getKeyboard(),
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
                $text = $subscribed === 1 ? 'Вы подписаны на уведомления об обновлениях' : 'Вы отписаны от уведомлений об обновлениях';
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $text,
                    'reply_markup' => $reply_markup
                ]);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;
                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $text,
                  "userEmail" => $userEmail,
                ];
                BotanDialogHistories::create($historyArr);
            }
        } catch
        (\Exception $e) {
            Log::debug('/subscription ' . $e->getMessage());
        }
    }
}
