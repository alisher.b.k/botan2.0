<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;

class SearchQuestionsCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "searchquestions";

    /**
     * @var string Command Description
     */
    protected $description = "Поиск вопросов";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
            if ($result['user']) {
              TelegramRequestLog::where('telegramId', $chatId)->delete();
              $text = "Отправьте команду 'найти вопрос ключевые_слова'\nНапример: найти вопрос документы";
              Telegram::sendMessage(['chat_id' => $chatId,
                  'text' => $text]);
              $userEmail = $result['user']->email
                  ? $result['user']->email
                  : $result['user']->ISN;
              $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "response" => $text,
                "userEmail" => $userEmail,
              ];
              BotanDialogHistories::create($historyArr);
            }
        } catch (\Exception $e) {
            Log::debug('/searchquestions ' . $e->getMessage());
        }
    }
}
