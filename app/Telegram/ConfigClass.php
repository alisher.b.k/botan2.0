<?php

namespace App\Telegram;

use Telegram\Bot\Commands\Command;
use App\BotanDialogHistories;

class ConfigClass extends Command {

  public function handle()
  {
    $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
    $chatId = $chat->getId();
    $username = $chat->getUsername();
    $firstname = $chat->first_name;
    $lastname = $chat->last_name;
    $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
    $messageText = strtolower($message->getText());

    $result = CheckUser::index($chatId);

    $historyArr = [
      "chatId" => $chatId,
      "telegramUsername" => $username,
      "telegramFirstName" => $firstname,
      "telegramLastName" => $lastname,
      "request" => $messageText,
    ];

    // запись запроса в историю диалогов
    $historyArr["userEmail"] = $result['user']
      ? $result['user']->email
        ? $result['user']->email
        : $result['user']->ISN
      : '';
    BotanDialogHistories::create($historyArr);

    $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
  }
}
