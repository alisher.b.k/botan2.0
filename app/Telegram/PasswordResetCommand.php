<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\CentMessage;
use App\Telegram\MenuCommand;
use App\ProblemDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\Problem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\CustomCommand;
use Ixudra\Curl\Facades\Curl;


class PasswordResetCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "pwreset";

    /**
     * @var string Command Description
     */
    protected $description = "";

    protected $catId = null;

    public static $getMessageTexts = [
        '/pwreset',
        '🔒️ сброс пароля',
        '🔒️ Сброс пароля',
    ];

    /**
     * @inheritdoc
     */
    public static function isPwResetMessage($messageText, $chatId, $callback){
        return in_array($messageText, self::$getMessageTexts) ||
            self::checkLog($chatId);
    }

    public static function checkLog($chatId){
        $log = TelegramRequestLog::getLog($chatId);
        return $log !== null && isset($log->command) && str_contains($log->command, 'pwreset');
    }

    public function handle()
    {
        // TODO: Implement handle() method.
    }

    public function handleMessage($messageText, $chatId, $callback){
        if(in_array($messageText, self::$getMessageTexts)){
            Telegram::sendMessage([
                'text' => 'Вы уверены что хотите сбросить пароль?',
                'chat_id' => $chatId,
                'reply_markup' => $this->getYesNoKeyboard()
            ]);
            TelegramRequestLog::setNewLog($chatId, 'pwreset', '');
        }else{
            Log::debug('Choice');
            if ($messageText === '✅ Да') {
                $workPlace = StaffTelegramUsers::getWorkPlaceByChatId($chatId);
                $ISN = StaffTelegramUsers::getUserIsnByChatId($chatId);
                Log::debug('Yes');
                if($workPlace === 'АО "СК "Сентрас Иншуранс"'){
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => 'Функционал "Сброс пароля" временно отключен',
                        'reply_markup' => Keyboard::make([
                            'keyboard' => [['На главную']],
                            'resize_keyboard' => true,
                            'one_time_keyboard' => true
                        ])
                    ]);
                    return 0;
                    //
                    if ($ISN !== ""){
                        $data = Curl::to('https://my.cic.kz/api/pwreset')
                            ->withData(['isn' => $ISN])
                            // ->withContentType('application/json')
                            ->asJson()
                            ->post();
                        if($data->success){
                            Telegram::sendMessage([
                                'chat_id' => $chatId,
                                'text' => "Ваш новый пароль :{$data->newPass}",
                                'reply_markup' => Keyboard::make([
                                    'keyboard' => [['На главную']],
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => true
                                ])
                            ]);
                        }else{
                            Telegram::sendMessage([
                                'chat_id' => $chatId,
                                'text' => "Произошла ошибка\nПрошу обратиться в тех.поддержку",
                                'reply_markup' => Keyboard::make([
                                    'keyboard' => [['На главную']],
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => true
                                ])
                            ]);
                            Log::debug($data->error);
                        }
                    }else{
                        Telegram::sendMessage([
                            'chat_id' => $chatId,
                            'text' => "Произошла ошибка\nУ пользователя не найден ISN",
                            'reply_markup' => Keyboard::make([
                                'keyboard' => [['На главную']],
                                'resize_keyboard' => true,
                                'one_time_keyboard' => true
                            ])
                        ]);
                    }
                } elseif ($workPlace === "\"АО СК Коммеск-Омир\""){
                    Log::debug('kmsk');
                    if ($ISN !== ""){
                        $data = Curl::to("http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdb.php?ISN={$ISN}&action=pwd")
                            ->withContentType('application/json')
                            ->withHeader('mypwd:313f5d4d9262f11eaa0077a02bbbf764')
                            ->withTimeout(60 * 10)
                            ->get();
                        $data = json_decode($data, false);
                        if($data->text){
                            Telegram::sendMessage([
                                'chat_id' => $chatId,
                                'text' => "$data->text",
                                'reply_markup' => Keyboard::make([
                                    'keyboard' => [['На главную']],
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => true
                                ])
                            ]);
                        }else{
                            Telegram::sendMessage([
                                'chat_id' => $chatId,
                                'text' => "Произошла ошибка\nПрошу обратиться в тех.поддержку",
                                'reply_markup' => Keyboard::make([
                                    'keyboard' => [['На главную']],
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => true
                                ])
                            ]);
                            Log::debug($data->error);
                        }
                    }else{
                        Telegram::sendMessage([
                            'chat_id' => $chatId,
                            'text' => "Произошла ошибка\nУ пользователя не найден ISN",
                            'reply_markup' => Keyboard::make([
                                'keyboard' => [['На главную']],
                                'resize_keyboard' => true,
                                'one_time_keyboard' => true
                            ])
                        ]);
                    }
                }
                elseif ($workPlace === "АО \"КСЖ Сентрас Коммеск Life\""){
                    Log::debug('ckl');
                    if ($ISN !== ""){
                        $data = Curl::to("http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdblife.php?ISN={$ISN}&action=pwd")
                            ->withContentType('application/json')
                            ->withHeader('mypwd:313f5d4d9262f11eaa0077a02bbbf764')
                            ->withTimeout(60 * 10)
                            ->get();
                        $data = json_decode($data, false);
                        if($data->text){
                            Telegram::sendMessage([
                                'chat_id' => $chatId,
                                'text' => "$data->text",
                                'reply_markup' => Keyboard::make([
                                    'keyboard' => [['На главную']],
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => true
                                ])
                            ]);
                        }else{
                            Telegram::sendMessage([
                                'chat_id' => $chatId,
                                'text' => "Произошла ошибка\nПрошу обратиться в тех.поддержку",
                                'reply_markup' => Keyboard::make([
                                    'keyboard' => [['На главную']],
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => true
                                ])
                            ]);
                            Log::debug($data->error);
                        }
                    }else{
                        Telegram::sendMessage([
                            'chat_id' => $chatId,
                            'text' => "Произошла ошибка\nУ пользователя не найден ISN",
                            'reply_markup' => Keyboard::make([
                                'keyboard' => [['На главную']],
                                'resize_keyboard' => true,
                                'one_time_keyboard' => true
                            ])
                        ]);
                    }
                }
            } else {
                TelegramRequestLog::deleteRows($chatId);
                MenuCommand::returnMenuKeyboard($chatId);
            }
        }
    }

    public function getYesNoKeyboard(){
        $keyboard = [
                ['✅ Да', '❌ Нет'],
                ['🔚 На главную']
        ];
        return Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
    }
}
