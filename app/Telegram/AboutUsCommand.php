<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class AboutUsCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "culture";

    /**
     * @var string Command description
     */
    protected $description = "О нас";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
        $chatId = $chat->getId();
        $username = $chat->getUsername();
        $firstname = $chat->first_name;
        $lastname = $chat->last_name;
        $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
        $messageText = strtolower($message->getText());

        $result = CheckUser::index($chatId);

        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "request" => $messageText,
        ];

        // запись запроса в историю диалогов
        $historyArr["userEmail"] = $result['user']
          ? $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN
          : '';
        BotanDialogHistories::create($historyArr);

        $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
    }

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if ($result['user']) {
            $reply_markup = Keyboard::make([
              'keyboard' => [['🏦 О холдинге', '😎 Шишки'],['На главную']], // ['О мероприятиях'],
                'resize_keyboard' => true,
                'one_time_keyboard' => true]);
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            $res = $result['res'];
            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;
            $res = "Я могу рассказать: \n".
              // "📆 О мероприятиях /parties \n".
              "🏦 О холдинге /holding \n".
              "😎 Показать всех больших шишек /bosses \n";
              // "Сенат /senate";

            Telegram::sendMessage(['chat_id' => $chatId,
                'text' => $res,
                'reply_markup' => $reply_markup]);

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $res,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
          }
        } catch
        (\Exception $e) {
            Log::debug('/culture ' . $e->getMessage());
        }
    }
}
