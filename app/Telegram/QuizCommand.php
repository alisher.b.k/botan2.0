<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\CustomCommand;
use App\Library;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;


class QuizCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "quiz";

    /**
     * @var string Command Description
     */
    protected $description = "";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
      $token = "519488420:AAGHxgFGCHZ_xZ5KQatoUgiR0XUbxBvTnHc";
      // $website = "https://api.telegram.org/bot".$token;
      
      // $name1 = 'WhatsApp';
      // $name2 = 'Telegram';
      // $placeId1 = '1';
      // $placeId2 = '2';
      // $keyboard = [
      //   'inline_keyboard' => [
      //     [
      //       ['text' =>  $name1, 'callback_data' => $placeId1],
      //       ['text' =>  $name2, 'callback_data' => $placeId2]
      //     ]
      //   ],
      // ];
      
      // send poll
      // $reply = json_encode($keyboard);
      // $url = $website."/sendmessage?chat_id=".$chatId."&text=What is your favorite messenger? &reply_markup=".$reply;
      // file_get_contents($url);


      $options = array(
        "Алиханов Берик",
        "Бакурова Мария",
        "Бейсекеев Талгат",
        "Зорина Марина",
        "Бахтиярова Аида",
        "Исаева Бибигуль",
        "Кадыр Жанар",
        "Карасаева Сымбат",
        "Нигай Алексей",
        "Саттаров Ахмет",
        ) ;
      $data = [
        'chat_id' => $chatId,
        'question' => 'Бюллетень для голосования на выборах в Сенат 2019-2020',
        'options' => json_encode($options),
        'allows_multiple_answers'=> 'True',
        'is_anonymous' => 'True',
        'open_period' => 600
      ];
      $response = file_get_contents("https://api.telegram.org/bot$token/sendPoll?" . http_build_query($data) );

      // send video
      // $bot_url    = "https://api.telegram.org/bot".$token;
      // $url = $bot_url . "sendVideo?chat_id=" . $chatId;      
      // $ch = curl_init(); 
      // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      //     "Content-Type:multipart/form-data"
      // ));
      // curl_setopt($ch, CURLOPT_URL, $url); 
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

      // $path = public_path()."/photos/f76975d7154ae405a9b36.mp4";

      // $filePath = public_path().'/photos/Pirates of the Caribbean - Hes a Pirate.mp3';
      // define('BOTAPI', 'https://api.telegram.org/bot' . $token . '/');
      // $data = [
      //     'chat_id' => $chatId,
      //     'audio' => "CQACAgIAAxkDAAIQeF7FWuG1NlP60Fkw-5YpU__WDxGTAALWBgACHjIxSmDBxe2z5pAmGQQ",
      // ];
      // audio file id CQACAgIAAxkDAAIQeF7FWuG1NlP60Fkw-5YpU__WDxGTAALWBgACHjIxSmDBxe2z5pAmGQQ
      // from file path new \CURLFile($filePath)
      // $ch = curl_init(BOTAPI . 'sendAudio');
      // curl_setopt($ch, CURLOPT_HEADER, false);
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      // curl_setopt($ch, CURLOPT_POST, 1);
      // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      // $output = curl_exec($ch);
      // curl_close($ch);

      // send video
      // $filePath = public_path().'/photos/f76975d7154ae405a9b36.mp4';
      // $data2 = [
      //   'chat_id' => $chatId,
      //   'video' => new \CURLFile($filePath),
      // ];
      // $ch = curl_init(BOTAPI . 'sendVideo');
      // curl_setopt($ch, CURLOPT_HEADER, false);
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      // curl_setopt($ch, CURLOPT_POST, 1);
      // curl_setopt($ch, CURLOPT_POSTFIELDS, $data2);
      // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      // $output = curl_exec($ch);
      // curl_close($ch);
    }
}
