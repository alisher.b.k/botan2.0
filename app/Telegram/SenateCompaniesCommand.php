<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Telegram\ConfigClass;

class SenateCompaniesCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "senate_companies";

    /**
     * @var string Command Description
     */
    protected $description = "Сенат комании";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
      try {
        if ($result['user']) {
          $userEmail = $result['user']->email
              ? $result['user']->email
              : $result['user']->ISN;

          $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
              'resize_keyboard' => true,
              'one_time_keyboard' => true]);

          TelegramRequestLog::where('telegramId', $chatId)->delete();
          $aboutUs = DB::table('about_us')->where('id', 2)->first();
          $res = $aboutUs->contentv2;

          Telegram::sendMessage(['chat_id' => $chatId,
              'text' => $res,
              'reply_markup' => $reply_markup]);

          $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $res,
            "userEmail" => $userEmail,
          ];
          BotanDialogHistories::create($historyArr);
        }
      } catch (\Exception $e) {
          Log::debug('/holding ' . $e->getMessage());
      }
    }
}
