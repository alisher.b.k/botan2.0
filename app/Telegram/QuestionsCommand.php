<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\QuestionsAnswers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;

class QuestionsCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "questions";

    /**
     * @var string Command Description
     */
    protected $description = "Вопросы";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if ($result['user']) {
            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;
            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "userEmail" => $userEmail,
            ];
              $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true]);
              TelegramRequestLog::where('telegramId', $chatId)->delete();
              $company = Companies::where('companyName', $result['user']->WORKPLACE)->first();
              $questions = QuestionsAnswers::where('companyId', $company->id)
                  ->orWhere('companyId', 0)
                  ->get();
              if (count($questions) > 0) {
                  $i = 0;
                  $res = "";
                  foreach ($questions as $question) {
                      $res .= $question->id;
                      $res .= ". ";
                      $res .= $question->question . "\n";
                      $i++;
                      if ($i === 10) {
                          Telegram::sendMessage(['chat_id' => $chatId,
                              'text' => $res,
                              'reply_markup' => $reply_markup]);
                          $historyArr["response"] = $res;
                          BotanDialogHistories::create($historyArr);
                          $i = 0;
                          $res = '';
                      }
                  }
              } else {
                  $res = "Вопросы не найдены";
                  $historyArr["response"] = $res;
                  BotanDialogHistories::create($historyArr);
              }
            }
            $res = "Выберите номер вопроса и отправьте команду 'ответ номер_вопроса'\nНапример: ответ 10";
            Telegram::sendMessage(['chat_id' => $chatId,
                'text' => $res]);
            $historyArr["response"] = $res;
            BotanDialogHistories::create($historyArr);
        } catch (\Exception $e) {
            Log::debug('/questions ' . $e->getMessage());
        }
    }
}
