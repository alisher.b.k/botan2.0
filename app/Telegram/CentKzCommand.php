<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\CentMessage;
use App\Telegram\MenuCommand;
use App\ProblemDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\Problem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\CustomCommand;
use Ixudra\Curl\Facades\Curl;


class CentKzCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "problem";

    /**
     * @var string Command Description
     */
    protected $description = "";

    protected $catId = null;

    public static $getMessageTexts = [
        '💻 cent.kz',
        '/centkz',
        'к категориям',
        'К категориям'
    ];

    /**
     * @inheritdoc
     */
    public static function isCentMessage($messageText, $chatId, $callback){
        return in_array($messageText, self::$getMessageTexts) ||
            self::checkLog($chatId);
    }

    public static function checkLog($chatId){
        $log = TelegramRequestLog::getLog($chatId);
        return $log !== null && isset($log->command) && str_contains($log->command, 'cent');
    }

    public function handle()
    {
        // TODO: Implement handle() method.
    }

    public function handleMessage($messageText, $chatId, $callback){
        if(in_array($messageText, self::$getMessageTexts)){
            $reply_markup = $this->getCategoryKeyboard();
            Telegram::sendMessage(
                [
                    'chat_id' => $chatId,
                    'text' => "Добро пожаловать в интернет-магазин cent.kz\nВыберите категорию",
                    'reply_markup' => $reply_markup
                ]
            );
            TelegramRequestLog::setNewLog($chatId, 'cent:category');
        }else{
            $log = TelegramRequestLog::getLog($chatId);
            if ($log !== null){
                switch ($log->command){
                    case  'cent:category' :
                        return $this->runCheckCategory($messageText, $chatId);
                    case  'cent:item' :
                        return $this->sendPartnerResponse($messageText, $chatId, $log, $callback);
                }
            }else{
                $reply_markup = $this->getCategoryKeyboard();
                Telegram::sendMessage(
                    [
                        'chat_id' => $chatId,
                        'text' => "Добро пожаловать в интернет-магазин cent.kz\nВыберите категорию",
                        'reply_markup' => $reply_markup
                    ]
                );
                TelegramRequestLog::setNewLog($chatId, 'cent:category');
            }
        }
    }

    public function getCategoryKeyboard(){
        $cats = $this->getCats();
        $keys = [];
        foreach ($cats as $cat){
            $keys[] = [
                'text' => $cat->catname,
                'callback_data' => "category_$cat->id"
            ];
        }
        $keys[] = [
            'text' => "На главную",
        ];
        return Keyboard::make([
            'keyboard' => DefaultKeyboard::makeKeyboardResponsive($keys),
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
    }

    public function findCategory($messageText){
        $cats = $this->getCats();
        $catId = null;
        foreach ($cats as $cat) {
            if ($cat->catname === $messageText){
                $catId = $cat->id;
                break;
            }
        }
        $this->catId = $catId;
        return $catId;
    }

    public function runCheckCategory($messageText, $chatId){
        $catId = $this->catId;
        if($catId === null){
            $catId = $this->findCategory($messageText);
        }
        if($catId === null){
            return $this->sendErrorMessage();
        }else{
            try {
                $items = $this->getItemsByCatId($catId);
            } catch (\Exception $exception) {
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => 'К сожалению, на данный момент в этой категории предложений нет'
                ]);
            }
            if($items === null){
                $message = "К сожалению, на данный момент в этой категории предложений нет";
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $message,
                    'reply_markup' => Keyboard::make([
                        'keyboard' => [[['text' => 'На главную'],['text' => 'К категориям']]],
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true
                    ])
                ]);

            }else {
                $message = "Выберите интересующий вас продукт\n";
                try {
                    Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => $message
                    ]);
                } catch (\Exception $exception) {
                    Log::debug($exception->getMessage());
                }
                foreach ($items as $item) {
                    [$isn, $company] = StaffTelegramUsers::getUserIsnAndWorkPlaceByChatId($chatId);
                    $linkParams = "?utm_source=botan";
                    if ($isn !== "")
                        $linkParams .= "&isn=$isn";
                    if ($company !== "") {
                        $company = urlencode($company);
                        $linkParams .= "&company=$company";
                    }
                    $reply_markup = Keyboard::make([
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true,
                        'inline_keyboard' => [
                            [[
                                'text' => $item->botanbutton,
                                'url' => $item->link.$linkParams,
                            ]]
                        ],
                    ]);
                    $fileName = explode('/', $item->fname);
                    $fileName = end($fileName);
                    try {
                        Telegram::sendPhoto([
                            'chat_id' => $chatId,
                            'caption' => $item->desk,
                            'parse_mode' => 'html',
                            'reply_markup' => $reply_markup,
                            'photo' => new InputFile($item->fname, $fileName),
                        ]);
                    } catch ( \Exception $e) {
                        Log::debug("199 ". $e->getMessage());
                    }
                }
                TelegramRequestLog::setNewLog($chatId, 'cent:item', $catId);
            }
        }
        return true;
    }

    public function runCheckItem($messageText, $chatId, $log){
        $itemId = explode('_',$messageText)[1];
        $item = $this->getItemById($itemId)[0];
        $message = "<b>$item->name</b>\n\n$item->desk";
        $fileName = explode('/', $item->fname);
        $fileName = end($fileName);
        $reply_markup = Keyboard::make([
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'inline_keyboard' => [
                [[
                    'text' => $item->botanbutton,
                    'callback_data' => "storeitem_{$itemId}"
                ]]
            ],
        ]);
        Telegram::sendPhoto([
            'chat_id' => $chatId,
            'caption' => $message,
            'parse_mode' => 'html',
            'reply_markup' => $reply_markup,
            'photo' => new InputFile($item->fname, $fileName),
        ]);
        TelegramRequestLog::setNewLog($chatId, 'cent:selected', $itemId);
        return true;
    }

    public function sendPartnerResponse($messageText, $chatId, $log, $callback){
        $this->sendCentResponse($callback, $chatId, $messageText);
        return true;
    }

    public function sendCentResponse($callback, $chatId, $messageText){
        $workplace = StaffTelegramUsers::getWorkPlaceByChatId($chatId);
        $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getMenuKeyboard($workplace),
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        $partner_id = explode("_", $callback);
        if(!array_key_exists(1, $partner_id)){
            if($this->findCategory($messageText) !== null){
                $this->runCheckCategory($messageText, $chatId);
                return;
            }
            Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => 'Произошла ошибка, попробуйте заново',
                'reply_markup' => Keyboard::make([
                    'keyboard' => [[['text' => 'На главную']]],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ])
            ]);
            Log::debug("Partner ID not found. Callback : {$callback}; Chat ID : {$chatId}");
            TelegramRequestLog::deleteRows($chatId);
            return;
        }
        $partner_id = $partner_id[1];
        $user = StaffTelegramUsers::where('telegramId', $chatId)->first();
        $userData = BotanStaff::where('email', $user->staffEmail)
            ->orWhere('isn', $user->staffEmail)
            ->first();
        $messageData = CentMessage::where('product_id', $partner_id)
            ->where('telegram_id', $chatId)
            ->where('created_at', '>', strtotime('-3 days'))
            ->first();
        if($userData !== null && $messageData === null){
            $fullname = explode(' ', $userData->FIO);
            $data = [
                'email' => $userData->email,
                'phone' => $userData->mobPhone,
                'first_name' => $fullname[1],
                'last_name' => $fullname[0],
                'product_id' => $partner_id
            ];
            $centMessage = new CentMessage();
            $centMessage->product_id = $partner_id;
            $centMessage->telegram_id = $chatId;
            $centMessage->save();
            $response = Curl::to('https://cent.kz/utls/sendorderfrombotan')
                ->withData($data)
                ->asJson()
                ->post();
            $id = (string)$response->id;
            try{
                $resp = Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => "Спасибо! \nВаша заявка успешно оформлена! Мы свяжемся с Вами в ближайшее время. \nНомер вашей заявки: $id",
                    'parse_mode' => 'html',
                    'reply_markup' => $reply_markup
                ]);
            }catch (\Exception $exception){
                Log::debug("CENT Message : Error {$exception->getMessage()}");
            }
        }else{
            $resp = Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => 'Произошла ошибка напишите в раздел сообщить о проблеме',
                'reply_markup' => $reply_markup
            ]);
            Log::debug("CENT Message : User not found {$chatId}");
        }
    }

    public function getCats(){
        return json_decode(file_get_contents('https://cent.kz/utls/getcats'));
    }

    public function getItemsByCatId($catId){
        return json_decode(file_get_contents("https://cent.kz/utls?catid=$catId"));
    }

    public function getItemById($id){
        return json_decode(file_get_contents("https://cent.kz/utls?id=$id"));
    }

    public function sendErrorMessage(){
        return false;
    }
}
