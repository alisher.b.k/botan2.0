<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\CustomCommand;
use App\Library;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use App\Telegram\ConfigClass;

class LibraryCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "library";

    /**
     * @var string Command Description
     */
    protected $description = "";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if ($result['user']) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                $library = Library::all();
                $response = "";

                for($i=0;$i<count($library);$i++) {
                    $response .= $library[$i]->description ."\n" . $library[$i]->link . "\n\n";
                }

                if(!$response) {
                    $response = "Книги еще не добавлены";
                }

              $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true]);

                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $response,
                    'reply_markup' => $reply_markup,
                ]);

                $userEmail = $result['user']->email
                    ? $result['user']->email
                    : $result['user']->ISN;

                $historyArr = [
                  "chatId" => $chatId,
                  "telegramUsername" => $username,
                  "telegramFirstName" => $firstname,
                  "telegramLastName" => $lastname,
                  "response" => $response,
                  "userEmail" => $userEmail,
                ];

                BotanDialogHistories::create($historyArr);

              $log = new TelegramRequestLog();
              $log->telegramId = $chatId;
              $log->command = 'help_desk';
              $log->save();
            }
        } catch (\Exception $e) {
            Log::debug('/library ' . $e->getMessage());
        }
    }
}
