<?php

namespace App\Telegram;
use Telegram\Bot\Commands\Command;
use App\BotanDialogHistories;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\TelegramRequestLog;
use App\Vacancy;
use App\MessageGroup;
use App\StaffTelegramUsers;
use App\BotanStaff;

class RecruitingCommand extends Command
{
    protected $name = "recruiting";

    protected $description = "Рекрутинг";

    public function handle()
    {
        $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
        $chatId = $chat->getId();
        $username = $chat->getUsername();
        $firstname = $chat->first_name;
        $lastname = $chat->last_name;
        $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
        $messageText = strtolower($message->getText());

        $result = CheckUser::index($chatId);

        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "request" => $messageText,
        ];

        // запись запроса в историю диалогов
        $historyArr["userEmail"] = $result['user']
          ? $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN
          : '';
        BotanDialogHistories::create($historyArr);

        $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
    }

    public static function executeCommand($chatId)
    {
        try{
            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $reply_markup = Keyboard::make([
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true,
                    "inline_keyboard" => [
                        [
                            ['text' => 'Коммеск', 'callback_data' => 'Коммеск'],
                            ['text' => 'Сентрас Иншуранс', 'callback_data'=>'Сентрас Иншуранс']
                        ],
                        [
                            ['text' => 'Сентрас Капитал', 'callback_data'=>'Сентрас Капитал'],
                            ['text' => 'SOS MA', 'callback_data'=>'SOS MA'],
                        ],
                        [
                            ['text' => 'Сентрас Секьюритиз', 'callback_data'=>'Сентрас Секьюритиз'],
                            ['text' => 'Centras Kommesk Life', 'callback_data'=>'Centras Kommesk Life']
                        ]
                    ]
                ]);

            Telegram::sendMessage([
                        'chat_id' => $chatId,
                        'text' => "Выберите компанию:",
                        'reply_markup' => $reply_markup,
                    ]);
            $logArr = [
                "command" => 'recruiting',
                "telegramId" => $chatId,
                'data' => '',
                ];
            TelegramRequestLog::create($logArr);
        } catch (\Exception $e) {
            Log::debug('/recruiting' . $e->getMessage());
        }
    }

    public static function chooseVacancy($сhatId, $message, $callback) {
        try{
            $callbackData = explode(":", $callback);
            if(count($callbackData) === 2){
                    $vacancy = Vacancy::find($callbackData[0]);
                    if($callbackData[1] === "detailed"){
                        Telegram::editMessageText([
                            'chat_id' => $сhatId,
                            'message_id' => $message["message_id"],
                            'text' =>  "\n"."Вакансия: ".$vacancy->name.
                                    "\n"."Компания: ".$vacancy->company.
                                    "\n"."Подразделение: ".$vacancy->department.
                                    "\n"."Город: ".$vacancy->city.
            
                                    "\n\n"."Требования: ".
                                    "\n".$vacancy->requirements.
                                    
                                    "\n\n"."Обязанности: ".
                                    "\n".$vacancy->duties,
            
                            'reply_markup' => Keyboard::make([
                                'resize_keyboard' => true,
                                'one_time_keyboard' => true,
                                "inline_keyboard" => [[['text' => 'Откликнуться', 'callback_data' => $vacancy->id.":send"]]]
                            ]),
                        ]);
                        return;
                    } else {
                        if($vacancy->company === 'Коммеск') {
                            $group = MessageGroup::find(11);
                        } elseif($vacancy->company === 'Сентрас Иншуранс') {
                            $group = MessageGroup::find(12);
                        } elseif($vacancy->company === 'Сентрас Капитал') {
                            $group = MessageGroup::find(13);
                        } elseif($vacancy->company === 'SOS MA') {
                            $group = MessageGroup::find(14);
                        } elseif($vacancy->company === 'Сентрас Секьюритиз') {
                            $group = MessageGroup::find(15);
                        } elseif($vacancy->company === 'Centras Kommesk Life') {
                            $group = MessageGroup::find(16);
                        }
    
                        $telegramUser = StaffTelegramUsers::where('telegramId', $сhatId)->first();
                        $user = BotanStaff::where('email', $telegramUser->staffEmail)->orWhere('isn', $telegramUser->staffEmail)->first();
                        $text = "\n"."Вакансия: ".$vacancy->name.
                                "\n"."Компания: ".$vacancy->company.
                                "\n"."Подразделение: ".$vacancy->department.
                                "\n"."Город: ".$vacancy->city.
    
                                "\n\n"."Требования: ".
                                "\n".$vacancy->requirements.
    
                                "\n\n"."Обязанности: ".
                                "\n".$vacancy->duties.
                                
                                "\n\n"."Кандидат: ".
                                "\n"."ФИО: ".$user->FIO.
                                "\n"."Контактный номер: ".$user->mobPhone.
                                "\n"."Почта: ".$user->email;
                        
                        Telegram::sendMessage([
                            'chat_id' => $сhatId,
                            'text' => 'Cпасибо, что откликнулись на вакансию "'.$vacancy->name.'". Прошу направить ваше резюме на почту hr@centras.kz с пометкой вакансии на которую вы откликнулись. С вами обязательно свяжутся!',
                        ]);
    
                        if($group){
                            $users = $group->getUsersArray();
                            if($users){
                                foreach ($users as $user){
                                    $telegramUser = StaffTelegramUsers::where('staffEmail', $user)->first();
                                    Telegram::sendMessage([
                                        'chat_id' => $telegramUser->telegramId,
                                        'text' => $text,
                                        'parse_mode' => 'html'
                                    ]);
                                }
                            }
                        }
                }
            } else{

                $vacancies = Vacancy::where('company', $callbackData[0])->get();

                if(count($vacancies)){
                    Telegram::sendMessage([
                        'chat_id' => $сhatId,
                        'text' => 'Вакансии: ',
                        'reply_markup' => Keyboard::make([
                            'resize_keyboard' => true,
                            'one_time_keyboard' => true,
                            'keyboard' => [['На главную']]
                        ])
                    ]);
                    foreach($vacancies as $vacancy) {
                        Telegram::sendMessage([
                            'chat_id' => $сhatId,
                            'text' =>  "\n"."Вакансия: ".$vacancy->name.
                                    "\n"."Компания: ".$vacancy->company.
                                    "\n"."Подразделение: ".$vacancy->department.
                                    "\n"."Город: ".$vacancy->city,
                            'reply_markup' => Keyboard::make([
                                'resize_keyboard' => true,
                                'one_time_keyboard' => true,
                                "inline_keyboard" => [[['text' => 'Подробнее', 'callback_data' => $vacancy->id.":detailed"]]]
                            ]),
                        ]);
                    }
                } else {
                    Telegram::sendMessage([
                        'chat_id' => $сhatId,
                        'text' => 'Нет открытых вакансий',
                        'reply_markup' => Keyboard::make([
                            'resize_keyboard' => true,
                            'one_time_keyboard' => true,
                            'keyboard' => [['На главную']]
                        ])
                    ]);
                    return;
                }
            }
        } catch(\Exception $e) {
            Log::debug('/recruiting_chooseVacancy' . $e->getMessage());
        }
    }
}