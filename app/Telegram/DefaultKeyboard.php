<?php
/**
 * Created by PhpStorm.
 * User: Anamaria
 * Date: 19.05.2019
 * Time: 12:55
 */

namespace App\Telegram;
use App\BotanStaff;
use App\StaffTelegramUsers;
use App\Clinics;
use Telegram\Bot\Keyboard\Keyboard;

class DefaultKeyboard
{
    public static function getKeyboard()
    {
        $keyboard = [['На главную']];
        return $keyboard;
    }

    public static function getMenuKeyboard($workplace)
    {
        $keyboard = [
            ['👨‍💼 Найду коллегу', '🏝 Мой отпуск'],//'Продавцам'
            ['👩‍⚕️ Лимиты по ДМС', '📑 Внутренний рекрутинг'],
//            ['🏦 О нас', '💡 Заявить о проблеме'],
            // ['🤑 Cенткоины'],
            //['✉️ Улучшить Ботан', '👩‍⚕️ Запись на прием к врачу'],
            //['📝 Подписка'],
            // ['О нас', 'Калькулятор'],
            // ['HR', 'Опросы'],
            //'Методология',
            // ['Подписка-отписка от обновлений']['📚 Библиотека'],
        ];
        if($workplace === '"АО СК Коммеск-Омир"' || $workplace === "АО \"КСЖ Сентрас Коммеск Life\""){
            $keyboard[] = ['🔒️ Сброс пароля'];
            $keyboard[] = ['Еще'];
//            $keyboard[] = ['🔒️ Сброс пароля'];
//        }else if () {
//            $keyboard[] = ['≠', '💻 Cent.kz'];
//            $keyboard[] = ['Еще'];
        }else{
            $keyboard[] = ['💻 Cent.kz'];
        }
//        array_push($keyboard, ['Авторизация']);
        return $keyboard;
    }

    public static function getExpandedKeyboard($workplace){
        $rows = ['🏦 О нас', '💡 Заявить о проблеме', '✉️ Улучшить Ботан', '👩‍⚕️ Запись на прием к врачу', '⌚️ Бронирование'];
        //TODO CENT.KZ
        if($workplace === 'АО "СК "Сентрас Иншуранс"' || $workplace === '"АО СК Коммеск-Омир"' || $workplace === "АО \"КСЖ Сентрас Коммеск Life\"")
            $rows[] = '💻 Cent.kz';

        $keyboard = Keyboard::make()->inline();
        $keys = [];
        foreach ($rows as $row) {
            $keys[] = Keyboard::inlineButton([
                'text' => $row,
                'callback_data' => $row,
            ]);
            if (count($keys) === 2) {
                $keyboard->row($keys[0], $keys[1]);
                $keys = [];
            }
        }
        if (count($keys) > 0) {
            $keyboard->row($keys[0]);
        }
        return $keyboard;
    }


    public static function getCommands()
    {
        $commands = [
            '/start', '/menu', '/vacation', '/facebook',
            '/auth', '/quiz', '/holding', '/problem',
            '/questionscategories', '/questions', '/searchquestions',
            '/library', '/methodology', '/culture', '/bosses', '/feedback',
            '/centcoins', '/booking', '/doctor','/booking', '/recruiting' // '/pwreset'
        ];
        //     '/hr', '/parties',
        //     '/forsales'
        //      '/subscription',
        // '/office', '/zhanabay', '/senate', '/senate_parties',
        // '/senate_companies', '/tech_staff', '/help_desk',
        //  '/poll', '/calculator',
        return $commands;
    }
    public static function MainKeyboard(){
        $keyboard = [
            ['🔚 На главную']
        ];
        return $keyboard;
    }
    public static function getAnswer()
    {
        $keyboard = [
            ['✅ Да', '❌ Нет'],
            ['🔚 На главную']
        ];
        return $keyboard;
    }

    public static function getClinic()
    {
        $clinics = Clinics::all();
        $keyboard = [];
        foreach ($clinics as $clinic) {
            array_push($keyboard, [
                '🏥 ' . $clinic->name
            ]);
        }
        array_push($keyboard,['🔚 На главную']);
        return $keyboard;
    }

    public static function getDoctors($clinic)
    {
        $keyboard = Keyboard::make()->inline();
        $keys = [];
        foreach ($clinic->doctors as $clc) {
            $keys[] = Keyboard::inlineButton([
                'text' => $clc->specialty,
                'callback_data' => $clc->specialty,
            ]);
            if (count($keys) === 2) {
                $keyboard->row($keys[0], $keys[1]);
                $keys = [];
            }
        }
        if (count($keys) > 0) {
            $keyboard->row($keys[0]);
        }
        return $keyboard;
    }

    public static function makeKeyboardResponsive($data){
        $keys = [];
        $tempKeys = [];
        foreach ($data as $item){
            $tempKeys[] = $item;
            if(count($tempKeys) === 2){
                $keys[] = $tempKeys;
                $tempKeys = [];
            }
        }
        if(count($tempKeys) > 0)
            $keys[] = $tempKeys;
        return $keys;
    }

    static function subscription($data,$chatId){
        $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)->first();
        $user = BotanStaff::where('email', $telegramUser->staffEmail)->orWhere('isn', $telegramUser->staffEmail)->first();
        $result = [];
        foreach($data as $d){
            $types = $user->typeMessages;
            $curr = 0;

            if(count($types) === 0){
                array_push($result,"✅".$d->name);
            }else{
                foreach($types as $type){
                    if($d->id === $type->pivot->type_messages_id){
                        $curr += 1;
                    }else{
                        $curr += 0;
                    }
                }

                if($curr == 1){
                    array_push($result,"❌".$d->name);
                }else{
                    array_push($result,"✅".$d->name);
                }
            }
        }
        $keyboard = Keyboard::make()->inline();
        $key = [];
        foreach($result as $message){
                $key[] = Keyboard::inlineButton([
                    'text'          => $message,
                    'callback_data' => $message,
                ]);
            if(count($key) === 2){
                $keyboard->row($key[0],$key[1]);
                $key = [];
            }
        }
        if(count($key) > 0){
            $keyboard->row($key[0]);
        }
        return $keyboard;
    }
}
