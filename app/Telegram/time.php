<?php

/**
 * @param int $hour
 * @param int $minutes
 * @return array
 *
 * @deprecated
 */
function getTimePicker(int $hour, int $minutes) {
    $prevHourCallback = 'timepicker-hour-';
    if ($hour == 0) {
        $prevHourCallback .= '23' . "-" . $minutes;
    } else {
        $prevHourCallback .= $hour - 1 . "-" . $minutes;
    }

    $nextHourCallback = 'timepicker-hour-';
    if ($hour == 23) {
        $nextHourCallback .= '0' . "-" . $minutes;
    } else {
        $nextHourCallback .= $hour + 1 . "-" . $minutes;
    }

    $prevMinutesCallback = 'timepicker-minutes-' . $hour . "-";
    if ($minutes == 0) {
        $prevMinutesCallback .= '50';
    } else {
        $prevMinutesCallback .= $minutes - 10;
    }

    $nextMinutesCallback = 'timepicker-minutes-'. $hour . "-";
    if ($minutes == 50) {
        $nextMinutesCallback .= '0';
    } else {
        $nextMinutesCallback .= $minutes + 10;
    }

    $timepickerMap = [
        [
            ['text' => '-', 'callback_data' => $prevHourCallback],
            ['text' => '-', 'callback_data' => $prevMinutesCallback],
        ],
        [
            ['text' => $hour, 'callback_data' => 'null_callback'],
            ['text' => $minutes, 'callback_data' => 'null_callback'],
        ],
        [
            ['text' => '+', 'callback_data' => $nextHourCallback],
            ['text' => '+', 'callback_data' => $nextMinutesCallback],
        ],
        [
            ['text' => 'ОК', 'callback_data' => 'timepicker-all-'.$hour."-".$minutes],
        ],
    ];

    return $timepickerMap;
}
