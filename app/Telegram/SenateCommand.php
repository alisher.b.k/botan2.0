<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Telegram\ConfigClass;

class SenateCommand extends ConfigClass
{
    /**
     * @var string Command Name
     */
    protected $name = "senate";

    /**
     * @var string Command description
     */
    protected $description = "Сенат";

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result)
    {
        try {
          if ($result['user']) {
            $reply_markup = Keyboard::make([
              'keyboard' => DefaultKeyboard::getKeyboard(),
                'resize_keyboard' => true,
                'one_time_keyboard' => true]);
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            $res = $result['res'];
            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;
            $res = "📆 Мероприятия /senate_parties \n".
              "🏢 Компании /senate_companies";

            Telegram::sendMessage(['chat_id' => $chatId,
                'text' => $res,
                'reply_markup' => $reply_markup]);

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $res,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
          }
        } catch
        (\Exception $e) {
            Log::debug('/senate ' . $e->getMessage());
        }
    }
}
