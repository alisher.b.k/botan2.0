<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class LimitsCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "limits";
    protected static $tokenFilePath = "app/public/token.txt";
    /**
     * @var string Command Description
     */
    protected $description = "";

    public function handle()
    {
        $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
        $chat = $message->getChat();
        $chatId = $chat->getId();
        $username = $chat->getUsername();
        $firstname = $chat->first_name;
        $lastname = $chat->last_name;
        $messageText = strtolower($message->getText());

        $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "request" => $messageText,
        ];

        BotanDialogHistories::create($historyArr);

        $this->executeCommand($chatId);
    }

    public static function getLimits($chatId, $message)
    {
        try {
            if ($message == '/limits')
            {
                return;
            }
            Log::debug("get LImits: ".$message);
            $data = LimitsCommand::search($message, $chatId);
            if ($data['status'] != false) {
                LimitsCommand::sendLimits($data['data'], $chatId);
            } else {
                TelegramRequestLog::query()->where('telegramId', $chatId)->delete();
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => 'wrong:('
                ]);
            }
        } catch (\Exception $e) {
            TelegramRequestLog::query()->where('telegramId', $chatId)->delete();
        }
    }

    public static function search($predicate, $chatId)
    {
        try {
//            if(!file_exists(storage_path(LimitsCommand::$tokenFilePath))) {
            LimitsCommand::auth($chatId);
//            }
            $token = file_get_contents(storage_path(LimitsCommand::$tokenFilePath));
            $url = env('SOS_MED_HOST') . "botan/get-limits";
            $header = ["Authorization: Bearer $token",
                'Content-Type: application/json',];
            $body = ['cardNo' => "$predicate"];
            $response = Curl::to($url)
                ->withContentType('application/json')
                ->withHeaders($header)
                ->withData(json_encode($body))
                ->post();
            $data = json_decode($response);
            try {
                if (isset($data['status']) && $data['status'] == 401) {
                    LimitsCommand::auth($chatId);
                    return LimitsCommand::search($predicate, $chatId);
                }
            } catch (\Exception $e) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();
            }
            return ['status' => true, 'data' => $data];
        } catch (\Exception $e) {
            Log::debug('getLimits');
            Log::debug($e->getMessage());
            TelegramRequestLog::where('telegramId', $chatId)->delete();
        }
    }

    public static function executeCommand($chatId)
    {
        try {
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            Telegram::sendMessage([
                'chat_id' => $chatId,
                'parse_mode' => 'html',
                'text' => "Вниманию застрахованных в «Коммеске»!\n      1. Вводить номер карточки страхования нужно на кириллице.\n      2. После «К» в номере идёт цифра 0, а не буква «О»"
            ]);
            Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => "Введите номер карточки страхования"
            ]);

            $log = new TelegramRequestLog();
            $log->telegramId = $chatId;
            $log->command = 'limits';
            $log->save();
        } catch (\Exception $e) {
            Log::debug('/bosses' . $e->getMessage());
        }
    }

    public static function auth($chatId)
    {
        try {
            $credentials = base64_encode('botan::Ed#BJ%Vc');
            $url = env('SOS_MED_HOST') . "auth/login?credentials=$credentials";
            $response = Curl::to($url)
                ->withContentType('application/json')
                ->withTimeout(60 * 10)
                ->post();
            $result = json_decode($response, true);
            file_put_contents(storage_path(LimitsCommand::$tokenFilePath), $result['jwt']);
            return true;
        } catch (\Exception $e) {
            Log::debug('auth');
            Log::debug($e->getMessage());
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            return false;
        }
    }

    public static function sendLimits($result, $chatId)
    {
        try {
            if (!is_array($result) || count($result) == 0) {
                TelegramRequestLog::where('telegramId', $chatId)->delete();
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'parse_mode' => 'html',
                    'text' => 'Карточка не найдена'
                ]);
                return false;
            }
            $sum = "";
            $remainingSum = "";
            foreach ($result as $data) {
                $sum = $sum . "$data->usluga: $data->amount тг.\n";
                $remainingSum = "$remainingSum$data->usluga: $data->remainingsum тг.\n";
            }
            $message = "Лимиты по программе страхования:\n$sum\nОстатки лимитов на дату запроса:\n$remainingSum";
            Telegram::sendMessage([
                'chat_id' => $chatId,
                'parse_mode' => 'html',
                'text' => $message
            ]);
            TelegramRequestLog::where('telegramId', $chatId)->delete();
        } catch (\Exception $e) {
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            Log::debug("sendLimits");
            Log::debug($e->getMessage());
        }
    }

    public static function curlGeneration($url, $header, $body) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        $result = curl_exec($ch);
        return json_decode($result);
    }

}
