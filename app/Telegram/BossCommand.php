<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\Telegram\MenuCommand;
use App\ProblemDialogHistories;
use App\BotanStaff;
use App\BotanTelegramUsers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\Problem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\CustomCommand;
use Ixudra\Curl\Facades\Curl;
use App\Boss;
use Telegram\Bot\FileUpload\InputFile;


class BossCommand extends Command {
  /**
   * @var string Command Name
   */
  protected $name = "bosses";

  /**
   * @var string Command Description
   */
  protected $description = "";

  /**
   * @inheritdoc
   */
  public function handle() {
    $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
    $chatId = $chat->getId();
    $username = $chat->getUsername();
    $firstname = $chat->first_name;
    $lastname = $chat->last_name;
    $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
    $messageText = strtolower($message->getText());

    $result = CheckUser::index($chatId);

    $historyArr = [
      "chatId" => $chatId,
      "telegramUsername" => $username,
      "telegramFirstName" => $firstname,
      "telegramLastName" => $lastname,
      "request" => $messageText,
    ];

    // запись запроса в историю диалогов
    $historyArr["userEmail"] = $result['user']
      ? $result['user']->email
        ? $result['user']->email
        : $result['user']->ISN
      : '';
    BotanDialogHistories::create($historyArr);

    $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
  }

  public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
    try {
      if ($result['user']) {
        $userEmail = $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN;


        TelegramRequestLog::where('telegramId', $chatId)->delete();

        $bosses = Boss::select('workplace')->distinct()->get();
        $res = "Выберите компанию";
        $keyboard = array();
        $buttons = array();
        $i = 0;
        foreach ($bosses as $boss) {
          if ($i == 2) {
            array_push($keyboard, $buttons);
            $buttons = array();
            $i = 0;
          }
          array_push($buttons, $boss->workplace);
          $i++;
        }
        if (count($buttons)) {
          array_push($keyboard, $buttons);
        }
        array_push($keyboard, ['На главную']);
        $reply_markup = Keyboard::make(['keyboard' => $keyboard,
          'resize_keyboard' => true,
          'one_time_keyboard' => true]);

        Telegram::sendMessage(['chat_id' => $chatId,
            'text' => $res,
            'reply_markup' => $reply_markup
            ]);


        $log = new TelegramRequestLog();
        $log->telegramId = $chatId;
        $log->command = 'boss_companies';
        $log->save();

        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "response" => $res,
          "userEmail" => $userEmail,
        ];
        BotanDialogHistories::create($historyArr);
      }
    } catch (\Exception $e) {
        Log::debug('/bosses' . $e->getMessage());
    }
  }

  public static function companyBosses($chatId, $messageText, $username, $firstname, $lastname, $result) {
    try {
      if ($result['user']) {
        $userEmail = $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN;


        TelegramRequestLog::where('telegramId', $chatId)->delete();

        $reply_markup = Keyboard::make(['keyboard' =>
          [
            ['На главную']
          ],
          'resize_keyboard' => true,
          'one_time_keyboard' => true]);

        $bosses = Boss::where('workplace', $messageText)->get();

        $res = "";
        $i = 0;
        foreach ($bosses as $boss) {
          if ($res) {
            $res .= "\n\n";
          }
          $res .= "ФИО: ".$boss->FIO.
          "\n"."Должность: ".$boss->position.
          "\n"."Компания: ".$boss->workplace;
          if ($boss->photo) {
            $fi = explode(" ", $boss->FIO);
            $command = str_replace(" ", "_", $fi[0]." ".$fi[1]);
            $fio = $boss->id.'_'.self::transliterate($command);
            $res .= "\n"."Фото /".$fio;
          }
          if ($i % 6 === 0 || $i === count($bosses) - 1) {
            Telegram::sendMessage(['chat_id' => $chatId,
            'text' => $res,
            'reply_markup' => $reply_markup
            ]);
            $res = "";
          }
          $i++;
        }

        if ($res) {
          Telegram::sendMessage(['chat_id' => $chatId,
            'text' => $res,
            'reply_markup' => $reply_markup
          ]);
        }


        $log = new TelegramRequestLog();
        $log->telegramId = $chatId;
        $log->command = 'boss_photo';
        $log->save();

        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "response" => $res,
          "userEmail" => $userEmail,
        ];
        BotanDialogHistories::create($historyArr);
      }
    } catch (\Exception $e) {
        Log::debug('/companyBosses' . $e->getMessage());
    }
  }

  public static function bossPhoto($chatId, $messageText, $username, $firstname, $lastname, $result) {
    try {
      if ($result['user']) {
        $userEmail = $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN;

        $reply_markup = Keyboard::make(['keyboard' =>
          [
            ['На главную'],
          ],
          'resize_keyboard' => true,
          'one_time_keyboard' => true]);

        TelegramRequestLog::where('telegramId', $chatId)->delete();

        $command = str_replace("/", "", $messageText);
        $id = explode('_', $command);
        $command = str_replace("_", " ", $command);
        $fi = self::transliterate(null, $command);
        $boss = Boss::where('id', $id[0])->first();
        if ($boss->photo) {
          Telegram::sendPhoto([
            'chat_id' => $chatId,
            'photo' => new InputFile(base_path() . '/public/bosses/' . $boss->photo),
          ]);
        } else {
          $reply_markup = Keyboard::make(['keyboard' =>
          [
            ['На главную'],
          ],
          'resize_keyboard' => true,
          'one_time_keyboard' => true]);

          Telegram::sendMessage(['chat_id' => $chatId,
            'text' => "Фото отсутствует",
            'reply_markup' => $reply_markup,
            'parse_mode'=> 'markdown']);
        }
        self::executeCommand($chatId, $username, $firstname, $lastname, $result);
      }
    } catch (\Exception $e) {
        Log::debug('/bossPhoto' . $e->getMessage());
    }
  }

  public static function transliterate($textcyr = null, $textlat = null) {
    $cyr = array(
    'ы', 'й','ж',  'ч',  'щ',   'ш',  'ю', 'я', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ь',
    'Ы', 'Й', 'Ж',  'Ч',  'Щ',   'Ш',  'Ю', 'Я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ь');
    $lat = array(
    'y', "ii", 'zh', 'ch', 'sht', 'sh', 'yu', 'ya', 'a', 'b', 'v', 'g', 'd', 'e', 'z', "i", 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', "'",
    'Y', "II", 'Zh', 'Ch', 'Sht', 'Sh', 'Yu', 'Ya', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', "'");
    if($textcyr) return str_replace($cyr, $lat, $textcyr);
    else if($textlat) return str_replace($lat, $cyr, $textlat);
    else return null;
}
}
