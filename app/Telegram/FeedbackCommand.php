<?php

namespace App\Telegram;

use App\BotanDialogHistories;
use App\BotanStaff;
use App\Companies;
use App\Feedback;
use App\QuestionsAnswers;
use App\StaffEmail;
use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Methods\Update;
use Ixudra\Curl\Facades\Curl;

class FeedbackCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "feedback";

    /**
     * @var string Command Description
     */
    protected $description = "";


    /**
     * @inheritdoc
     */
    public function handle()
    {
        $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
        $chatId = $chat->getId();
        $username = $chat->getUsername();
        $firstname = $chat->first_name;
        $lastname = $chat->last_name;
        $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
        $messageText = strtolower($message->getText());

        $result = CheckUser::index($chatId);

        $historyArr = [
          "chatId" => $chatId,
          "telegramUsername" => $username,
          "telegramFirstName" => $firstname,
          "telegramLastName" => $lastname,
          "request" => $messageText,
        ];

        // запись запроса в историю диалогов
        $historyArr["userEmail"] = $result['user']
          ? $result['user']->email
            ? $result['user']->email
            : $result['user']->ISN
          : '';
        BotanDialogHistories::create($historyArr);

        $this->executeCommand($chatId, $username, $firstname, $lastname, $result);
    }

    public static function executeCommand($chatId, $username, $firstname, $lastname, $result) {
        try {
          if ($result['user']) {
            $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getKeyboard(),
                'resize_keyboard' => true,
                'one_time_keyboard' => true]);

            TelegramRequestLog::where('telegramId', $chatId)->delete();

            $userEmail = $result['user']->email
                ? $result['user']->email
                : $result['user']->ISN;

//            $text = "Я рад, у тебя есть идеи как сделать мои функциональности более удобнее для тебя. 😁  За
//            хорошую идею ты даже можешь получить подарок. \nНапиши своё предложение вот здесь 👇";
              $text = "Я рад, что у тебя есть идеи как сделать меня еще лучше. 😁 \nНапиши своё предложение вот здесь 👇";

            Telegram::sendMessage([
              'chat_id' => $chatId,
              'text' => $text,
            ]);

            $log = new TelegramRequestLog();
            $log->telegramId = $chatId;
            $log->command = 'feedback_message';
            $log->save();

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "response" => $text,
              "userEmail" => $userEmail,
            ];
            BotanDialogHistories::create($historyArr);
          }
        } catch (\Exception $e) {
            Log::debug('/feedback 1' . $e->getMessage());
        }
    }

    public static function saveFeedback($chatId, $messageText, $username, $firstname, $lastname, $result, $message)
    {
        try {
          if ($result['user']) {
            $userEmail = $result['user']->email
              ? "email: " . $result['user']->email
              : "ISN: " . $result['user']->ISN;

            $historyArr = [
              "chatId" => $chatId,
              "telegramUsername" => $username,
              "telegramFirstName" => $firstname,
              "telegramLastName" => $lastname,
              "userEmail" => $userEmail,
            ];

            $usernameStr = $username ? "telegram: @".$username."\n" : "";
            TelegramRequestLog::where('telegramId', $chatId)->delete();

              $reply_markup = Keyboard::make([
                  'keyboard' => DefaultKeyboard::getKeyboard(),
                  'resize_keyboard' => true,
                  'one_time_keyboard' => true
              ]);

              $feedback = new Feedback();
              $feedback->email = $userEmail;
              $feedback->telegramId = $chatId;
              $feedback->message = $messageText;
              $feedback->save();

              if ($message) {
                $messageDecoded = json_decode($message, true);
                $isCaption = false;
                $isFile = false;
                if (
                  array_key_exists("document", $messageDecoded) 
                  || array_key_exists("photo", $messageDecoded) 
                  || array_key_exists("video", $messageDecoded) 
                  || array_key_exists("video_note", $messageDecoded)
                  || array_key_exists("audio", $messageDecoded)
                  || array_key_exists("voice", $messageDecoded)
                ) {
                  $isFile = true;
    
                  $key = '';
                  if (array_key_exists("document", $messageDecoded)){
                    $key = $messageDecoded["document"]["file_id"];
                  } elseif (array_key_exists("photo", $messageDecoded)) {
                    $key = $messageDecoded["photo"][1]["file_id"];
                  } elseif (array_key_exists("video", $messageDecoded)) {
                    $key = $messageDecoded["video"]["file_id"];
                  } elseif (array_key_exists("video_note", $messageDecoded)) {
                    $key = $messageDecoded["video_note"]["file_id"];
                  } elseif (array_key_exists("audio", $messageDecoded)) {
                    $key = $messageDecoded["audio"]["file_id"];
                  } elseif (array_key_exists("voice", $messageDecoded)) {
                    $key = $messageDecoded["voice"]["file_id"];
                  }
                  $token = getenv('TELEGRAM_BOT_TOKEN');
                  $fileInfo = Curl::to('https://api.telegram.org/bot'.$token.'/getFile?file_id='.$key)
                  ->withContentType('application/json')
                  ->withTimeout(60 * 10)
                  ->get();
    
                  $fileInfoDecoded = json_decode($fileInfo, true);
                  file_put_contents( public_path()."/feedback/".$fileInfoDecoded["result"]["file_path"],file_get_contents('https://api.telegram.org/file/bot'.$token.'/'.$fileInfoDecoded["result"]["file_path"]));
    
                  
                  $feedback->file = $fileInfoDecoded["result"]["file_path"];
                  $feedback->save();
                }
                if (array_key_exists("caption", $messageDecoded)) {
                  $isCaption = true;
                  $feedback->message = $messageDecoded["caption"];
                  $feedback->save();
                }    
              }  

              $resText = "Спасибо за твоё предложение по улучшению меня! Наша команда обязательно рассмотрит твою идею"; //, лови 1 сенткоин 😊
              Telegram::sendMessage(['chat_id' => $chatId,
                  'text' => $resText,
                  'reply_markup' => $reply_markup]);
              $historyArr['response'] = $resText;
              BotanDialogHistories::create($historyArr);

              
              $res = "В разделе «✉️ Улучшить Ботан» появилось новое обращение.
              \nАвтор: ".$result['user']->FIO."\nКомпания : ".$result['user']->WORKPLACE;

              // отправка обращения в групповой чат
              // -381528038 - тестовый чат
              // -333170792 - родители ботана
              // '-1001151800226' - Botan 2.0

              $chatIdArr = ['-1001151800226'];
              foreach ($chatIdArr as $id) {
                $resp = Telegram::sendMessage([
                  'chat_id' => $id,
                  'text' => $res,
                ]);
                $historyArr['response'] = $res;
                $historyArr['chatId'] = $id;
                if ($resp->chat->title) {
                  $historyArr['telegramUsername'] = "";
                  $historyArr['telegramFirstName'] = $resp->chat->title;
                  $historyArr['telegramLastName'] = "";
                } else if ($resp->chat->first_name || $resp->chat->last_name || $resp->chat->username) {
                  $historyArr['telegramUsername'] = $resp->chat->username;
                  $historyArr['telegramFirstName'] = $resp->chat->first_name;
                  $historyArr['telegramLastName'] = $resp->chat->last_name;
                }
                BotanDialogHistories::create($historyArr);
              }
            }
        } catch (\Exception $e) {
            Log::debug('/feedback 2' . $e->getMessage());
        }
    }
}
