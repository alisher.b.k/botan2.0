<?php

namespace App\Telegram;

use App\StaffTelegramUsers;
use App\TelegramRequestLog;
use App\BotanDialogHistories;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use App\Telegram\CheckUser;
use Illuminate\Support\Facades\Log;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
     public function handle()
     {
         try {
            $chat = $this->getTelegram()->getWebhookUpdates()->getMessage()->getChat();
            $chatId = $chat->getId();
            $username = $chat->getUsername();
            $firstname = $chat->first_name;
            $lastname = $chat->last_name;
            $message = $this->getTelegram()->getWebhookUpdates()->getMessage();
            $messageText = strtolower($message->getText());

            $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "request" => $messageText,
            ];

            // запись запроса в историю диалогов
            BotanDialogHistories::create($historyArr);

            $this->executeCommand($chatId, $username, $firstname, $lastname);
        } catch (\Exception $e) {
            Log::debug('/start handle ' . $e->getMessage());
        }
     }

    public function executeCommand($chatId, $username, $firstname, $lastname)
    {
        try {
            $this->replyWithChatAction(['action' => Actions::TYPING]);
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)->first();
            $text = "Привет! Я корпоративный бот группы Компаний Сентрас.";

            if (!$telegramUser) {
                $text .=  "Для доступа нужно будет авторизоваться, нажмите на /auth";
                $reply_markup = Keyboard::make([
                    'keyboard' => [['/auth', '/start']],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
            } else {
                $result = CheckUser::index($chatId);
                if ($result['user']) {
                    $text = "Привет, ".$result['user']->FIO."! Я корпоративный бот группы Компаний Сентрас.";
                }
                $text .=  "\nВот, что я умею: \n" .
                        "👨‍💼 Найду коллегу /facebook" .
                        "\n"."🏝 Мой отпуск /vacation".
                        "\n⌚️ Бронирование /booking".
                        "\n"."🏦 О холдинге /holding".
                        "\n"."💡 Заявить о проблеме /problem".
                        // ."\n 🤑 Cенткоины /centcoins"
                        "\n"."👩‍⚕️ Запись на прием к врачу  /doctor".
                        "\n"."👩‍⚕️ Лимиты по ДМС /limits".
                        "\n"."💻️ Магазин Cent.kz  /centkz".
                        "\n"."📑 Внутренний рекрутинг /recruiting";
                        //"\n"."📝 Подписка /subscription";
                        // "\nМетодология /methodology".
                        // "\n📚 Библиотека /library";
                if ($result['user']->WORKPLACE === '"АО СК Коммеск-Омир"'){
                    $text .= "\n🔒️   Сброс пароля /pwreset";
                }
                $keyboard = DefaultKeyboard::getMenuKeyboard($result['user']->WORKPLACE);
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
            }
            $this->replyWithMessage(['text' => $text, 'reply_markup' => $reply_markup]);
            $historyArr = [
            "chatId" => $chatId,
            "telegramUsername" => $username,
            "telegramFirstName" => $firstname,
            "telegramLastName" => $lastname,
            "response" => $text,
            ];
            BotanDialogHistories::create($historyArr);
        }  catch (\Exception $e) {
            Log::debug('/start execute ' . $e->getMessage());
        }
    }
}
