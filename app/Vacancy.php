<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $fillable = [
        'name', 'company', 'department', 'city', 'requirements', 'duties'
      ];
}
