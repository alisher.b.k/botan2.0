<?php

namespace App\Console;

use App\Console\Commands\SandboxCommand;
use App\Console\Commands\AddStaffCommand;
use App\Console\Commands\RemoveProblemFiles;
use App\Console\Commands\SendBookingNotificationsCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        AddStaffCommand::class,
        RemoveProblemFiles::class,
        SandboxCommand::class,
        SendBookingNotificationsCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       $schedule->command('send:bookingNotifications')->cron('* * * * *');
       $schedule->command('botan:addUsersInfo')->cron('0 0 * * *');
       $schedule->command('staff:replaceEmail')->cron('0 0 * * *');
       $schedule->command('botan:remove_problem_files')->cron('20 0 * * *');
        // $schedule->command('botan:addUsersInfo --force')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
