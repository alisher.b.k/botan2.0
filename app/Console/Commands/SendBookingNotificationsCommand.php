<?php

namespace App\Console\Commands;
use Illuminate\Support\Carbon;

use App\BookRecord;
use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class SendBookingNotificationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:bookingNotifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sendEverybody();
    }

    public function sendEverybody(){
        $bookingRecords = BookRecord::whereDate('date', Carbon::today())->get();
        if($bookingRecords != null && $bookingRecords != ""){
            foreach($bookingRecords as $bookingRecord){
                if($bookingRecord->telegramId != null){
                    $now_date = new \DateTime(date('H:i:s'));    //время сейчас
                    $record_start = new \DateTime($bookingRecord->start); //дата с которой отчитываем 
                    $record_end = new \DateTime($bookingRecord->end);
                    if($record_end >= $now_date){
                        $startBooking = $record_start->diff($now_date);
                        $endBooking = $record_end->diff($now_date);
                        if($startBooking->format("%H:%I") == '00:14'){
                            Telegram::sendMessage(['chat_id' => $bookingRecord->telegramId,
                            'text' => "$bookingRecord->fio ❗️❗️❗️".
                            "\n"."Время бронирования комнаты переговоров \"$bookingRecord->room\" начнется через 15 минут"]);
                        }
                        
                        if($endBooking->format("%H:%I") == '00:14'){
                            Telegram::sendMessage(['chat_id' => $bookingRecord->telegramId,
                            'text' => "$bookingRecord->fio ❗️❗️❗️".
                            "\n"."Время бронирования комнаты переговоров \"$bookingRecord->room\" завершится через 15 минут"]);
                        }
                    }else{
                        $bookingRecord->delete();
                        continue;
                    }
                }
            }
        }
    }
}
