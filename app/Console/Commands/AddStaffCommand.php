<?php

namespace App\Console\Commands;

use App\BotanStaff;
use App\StaffEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;
use Tests\Unit\ExampleTest;
use App\Http\Controllers\SimpleXLSX;

class AddStaffCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'botan:addUsersInfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug("Crontab is begin");
        $this->addCommeskOmirStaff();
        $this->addSentrasInsuranceStaff();
        $this->addCommeskLifeStaff();
        $this->changeUser();
        Log::debug("Crontab is end");
    }

    public function addCommeskOmirStaff()
    {
        $getStaffInfo_request = Curl::to('http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdb.php')
            ->withContentType('application/json')
            ->withHeader('mypwd:313f5d4d9262f11eaa0077a02bbbf764')
            ->withTimeout(60 * 10)
            ->get();
        $arr = json_decode($getStaffInfo_request);
        if ($getStaffInfo_request && count($arr)) {
            BotanStaff::where('WORKPLACE', '"АО СК Коммеск-Омир"')->delete();
            StaffEmail::truncate();

            for ($i = 0, $iMax = count($arr); $i < $iMax; $i++) {
                $user = new BotanStaff();
                $user->ISN = $arr[$i]->ISN;
                $user->FIO = $arr[$i]->LASTNAME . " " . $arr[$i]->FIRSTNAME . " " . $arr[$i]->PARENTNAME;
                $user->NAMELAT = $arr[$i]->NAMELAT;
                $user->WORKPLACE = $arr[$i]->WORKPLACE;
                $user->MAINDEPT = $arr[$i]->MAINDEPT;
                $user->SUBDEPT = $arr[$i]->SUBDEPT;
                $user->position = $arr[$i]->DUTY;
                $user->workPhone = $arr[$i]->WORKPHONE;
                $user->intPhone = $arr[$i]->INNPHONE;
                $user->mobPhone = $arr[$i]->MOBILEPHONE;
                if (stristr($arr[$i]->EMAIL, ',') === FALSE) {
                    $user->email = strtolower($arr[$i]->EMAIL);
                } else {
                    $emailArr = explode(",", $arr[$i]->EMAIL);
                    $user->email = strtolower($emailArr[0]);
                }
                $user->bday = $arr[$i]->BIRTHDAY;
                $user->VACATIONBEG = $arr[$i]->VACATIONBEG;
                $user->VACATIONEND = $arr[$i]->VACATIONEND;
                $user->TRIPBEG = $arr[$i]->TRIPBEG;
                $user->TRIPEND = $arr[$i]->TRIPEND;
                $user->workEx = $arr[$i]->WORKEXP;
                $user->LEAVEDATE = $arr[$i]->LEAVEDATE;
                if ($arr[$i]->img) {
                    $user->photo = "http://92.46.39.10:1611/bot/" . $arr[$i]->img;
                }
                $user->save();
                if (stristr($arr[$i]->EMAIL, ',') != FALSE) {
                    $emailArr = explode(",", $arr[$i]->EMAIL);
                    for ($k = 1, $kMax = count($emailArr); $k < $kMax; $k++) {
                        $staffEmail = new StaffEmail();
                        $staffEmail->userId = $user->id;
                        $staffEmail->email = strtolower(str_replace(" ", "", $emailArr[$k]));
                        $staffEmail->save();
                    }
                }
            }
        }
    }

    public function addCommeskLifeStaff()
    {
        $getStaffInfo_request = Curl::to('http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdblife.php')
            ->withContentType('application/json')
            ->withHeader('mypwd:313f5d4d9262f11eaa0077a02bbbf764')
            ->withTimeout(60 * 10)
            ->get();
        $arr = json_decode($getStaffInfo_request);
        if ($getStaffInfo_request && count($arr)) {
            BotanStaff::where('WORKPLACE', "АО \"КСЖ Сентрас Коммеск Life\"")->delete();
            StaffEmail::truncate();

            for ($i = 0, $iMax = count($arr); $i < $iMax; $i++) {
                $user = new BotanStaff();
                $user->ISN = $arr[$i]->ISN;
                $user->FIO = $arr[$i]->LASTNAME . " " . $arr[$i]->FIRSTNAME . " " . $arr[$i]->PARENTNAME;
                $user->NAMELAT = $arr[$i]->NAMELAT;
                $user->WORKPLACE = $arr[$i]->WORKPLACE;
                $user->MAINDEPT = $arr[$i]->MAINDEPT;
                $user->SUBDEPT = $arr[$i]->SUBDEPT;
                $user->position = $arr[$i]->DUTY;
                $user->workPhone = $arr[$i]->WORKPHONE;
                $user->intPhone = $arr[$i]->INNPHONE;
                $user->mobPhone = $arr[$i]->MOBILEPHONE;
                if (stristr($arr[$i]->EMAIL, ',') === FALSE) {
                    $user->email = strtolower($arr[$i]->EMAIL);
                } else {
                    $emailArr = explode(",", $arr[$i]->EMAIL);
                    $user->email = strtolower($emailArr[0]);
                }
                $user->bday = $arr[$i]->BIRTHDAY;
                $user->VACATIONBEG = $arr[$i]->VACATIONBEG;
                $user->VACATIONEND = $arr[$i]->VACATIONEND;
                $user->TRIPBEG = $arr[$i]->TRIPBEG;
                $user->TRIPEND = $arr[$i]->TRIPEND;
                $user->workEx = $arr[$i]->WORKEXP;
                $user->LEAVEDATE = $arr[$i]->LEAVEDATE;
                if ($arr[$i]->img) {
                    $user->photo = "http://92.46.39.10:1611/bot/" . $arr[$i]->img;
                }
                $user->save();
                if (stristr($arr[$i]->EMAIL, ',') != FALSE) {
                    $emailArr = explode(",", $arr[$i]->EMAIL);
                    for ($k = 1, $kMax = count($emailArr); $k < $kMax; $k++) {
                        $staffEmail = new StaffEmail();
                        $staffEmail->userId = $user->id;
                        $staffEmail->email = strtolower(str_replace(" ", "", $emailArr[$k]));
                        $staffEmail->save();
                    }
                }
            }
        }
    }

    public function addSentrasInsuranceStaff()
    {
        $getStaffInfo_request = Curl::to('https://api.kupipolis.kz/bot')
            ->withHeader('authcode: k5DYwMNtGvahEN7s')
            ->withTimeout(60 * 10)
            ->get();

        $a = json_decode($getStaffInfo_request);
        if ($getStaffInfo_request && count($a)) {
            BotanStaff::where('WORKPLACE', 'АО "СК "Сентрас Иншуранс"')->delete();
            $i = 0;
            foreach ($a as $item) {
                $i++;

                $test = (array)$item;
                $user = new BotanStaff();
                if (gettype($test['ISN']) != "object") {
                    $user->ISN = $test['ISN'];
                    $user->photo = "http://api.kupipolis.kz/bot/image?isn=" . $test['ISN'];
                }
                if (gettype($test['LASTNAME']) != "object") {
                    $user->FIO .= $test['LASTNAME'];
                }
                if (gettype($test['FIRSTNAME']) != "object") {
                    $user->FIO .= " " . $test['FIRSTNAME'];
                }
                if (gettype($test['PARENTNAME']) != "object") {
                    $user->FIO .= " " . $test['PARENTNAME'];
                }
                if (gettype($test['NAMELAT']) != "object") {
                    $user->NAMELAT = $test['NAMELAT'];
                }
                if (gettype($test['WORKPLACE']) != "object") {
                    $user->WORKPLACE = $test['WORKPLACE'];
                }
                if (gettype($test['MAINDEPT']) != "object") {
                    $user->MAINDEPT = $test['MAINDEPT'];
                }
                if (gettype($test['SUBDEPT']) != "object") {
                    $user->SUBDEPT = $test['SUBDEPT'];
                }
                if (gettype($test['DUTY']) != "object") {
                    $user->position = $test['DUTY'];
                }
                if (gettype($test['WORKPHONE']) != "object") {
                    $user->workPhone = $test['WORKPHONE'];
                }
                if (gettype($test['INNPHONE']) != "object") {
                    $user->intPhone = $test['INNPHONE'];
                }
                if (gettype($test['MOBILEPHONE']) != "object") {
                    $user->mobPhone = $test['MOBILEPHONE'];
                }
                if (gettype($test['EMAIL']) != "object") {
                    if (stristr($test['EMAIL'], ',') === FALSE) {
                        $user->email = strtolower($test['EMAIL']);
                    } else {
                        $emailArr = explode(",", $test['EMAIL']);
                        $user->email = strtolower($emailArr[0]);
                    }
                }
                if (gettype($test['BIRTHDAY']) != "object") {
                    $user->bday = $test['BIRTHDAY'];
                }
                if (gettype($test['VACATIONBEG']) != "object") {
                    $user->VACATIONBEG = $test['VACATIONBEG'];
                }
                if (gettype($test['VACATIONEND']) != "object") {
                    $user->VACATIONEND = $test['VACATIONEND'];
                }
                if (gettype($test['TRIPBEG']) != "object") {
                    $user->TRIPBEG = $test['TRIPBEG'];
                }
                if (gettype($test['TRIPEND']) != "object") {
                    $user->TRIPBEG = $test['TRIPEND'];
                }
                if (gettype($test['WORKEXP']) != "object") {
                    $user->workEx = $test['WORKEXP'];
                }
                if (gettype($test['LEAVEDATE']) != "object") {
                    $user->workEx = $test['LEAVEDATE'];
                }
                $user->save();
                if (gettype($test['EMAIL']) != "object") {
                    if (stristr($test['EMAIL'], ',') != FALSE) {
                        $emailArr = explode(",", $test['EMAIL']);
                        for ($k = 1, $kMax = count($emailArr); $k < $kMax; $k++) {
                            $staffEmail = new StaffEmail();
                            $staffEmail->userId = $user->id;
                            $staffEmail->email = strtolower(str_replace(" ", "", $emailArr[$k]));
                            $staffEmail->save();
                        }
                    }
                }
            }
        }
    }

    public function changeUser(){
        $staff = BotanStaff::where('ISN', 3921599)
            ->where('WORKPLACE','"АО СК Коммеск-Омир"')
            ->first();
        if($staff !== null){
            $staff->ISN = 39215990000;
            $staff->save();
        }
    }
}
