<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Problem;
use Illuminate\Support\Facades\File;

class RemoveProblemFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'botan:remove_problem_files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $problems = Problem::where('areFilesRemoved', false)->where('status', 'closed')
            ->get();

        $currentDate = new \DateTime();

        foreach ($problems as $problem) {
            $date = new \DateTime($problem->updated_at);
            $diff = $date->diff($currentDate);
            $months = $diff->y * 12 + $diff->m + $diff->d / 30;
            if ($months >= 3) {
                if ($problem->questionFile) {
                    File::delete(public_path() . '/problem/' .$problem->questionFile);
                }
                if ($problem->answerFile) {
                    File::delete(public_path() . '/problem/' .$problem->answerFile);
                }
                $problem->areFilesRemoved = true;
                $problem->save();
            }
        }
    }
}