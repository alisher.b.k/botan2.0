<?php

namespace App\Console\Commands;

use App\CentPartner;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class getCentProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cent:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            $data = json_decode(file_get_contents('https://cent.kz/utls'));
            CentPartner::truncate();
            foreach ($data as $product){
//            if(($partner = CentPartner::where('partner_id', $product->id)->first()) === null){
                $partner = new CentPartner();
//            }
                $partner->partner_id = $product->id;
                $partner->partner_name = $product->name;
                $partner->title = $product->name;
                $partner->message_text = $product->desk;
                $partner->image_url = $product->fname;
                $partner->save();
            }
        }catch (\Exception $exception){
            DB::rollBack();
        }
        DB::commit();
    }
}
