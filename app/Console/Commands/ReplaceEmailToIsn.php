<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ixudra\Curl\Facades\Curl;
use App\BotanStaff;
use App\StaffTelegramUsers;
use Illuminate\Support\Facades\Log;


class replaceEmailToIsn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'staff:replaceEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        self::replaceCentras();
        self::replaceKommesk();
    }

    public function replaceKommesk()
    {
        $getStaffInfo_request = Curl::to('http://92.46.39.10:1611/bot/a133dc9b0e07efd33ac4da64235bcbdb.php')
            ->withContentType('application/json')
            ->withHeader('mypwd:313f5d4d9262f11eaa0077a02bbbf764')
            ->withTimeout(60 * 10)
            ->get();
        $users = [];
        $arr = json_decode($getStaffInfo_request);
        if ($getStaffInfo_request && count($arr)) {
            for ($i = 0; $i < count($arr); $i++) {
                if (stristr($arr[$i]->EMAIL, ',') === FALSE) {
                    $users[strtolower($arr[$i]->EMAIL)] = $arr[$i]->ISN;
                } else {
                    $emailArr = explode(",", $arr[$i]->EMAIL);
                    foreach ($emailArr as $email){
                        $users[strtolower($email)] = $arr[$i]->ISN;
                    }
                }
            }
            $userData = StaffTelegramUsers::whereIn('staffEmail', array_keys($users))->get();
            foreach ($userData as $user) {
                try {
                    Log::debug('Kommesk : '.$user->id . ' ' . $user->staffEmail . ' ' . $users[strtolower($user->staffEmail)]);
                    $user->staffEmail = $users[strtolower($user->staffEmail)];
                    $user->save();
                } catch (\Exception $ex) {
                    Log::debug('Error : ' . $ex->getMessage());
                }
            }
        }
    }

    public function replaceCentras()
    {
        $getStaffInfo_request = Curl::to('https://api.kupipolis.kz/bot')
            ->withHeader('authcode: k5DYwMNtGvahEN7s')
            ->withTimeout(60 * 10)
            ->get();
        $users = [];
        $a = json_decode($getStaffInfo_request);
        if ($getStaffInfo_request && count($a)) {
            $i = 0;
            foreach ($a as $item) {
                $i++;
                $test = (array)$item;
                if (gettype($test['EMAIL']) != "object") {
                    if (stristr($test['EMAIL'], ',') === FALSE) {
                        $users[strtolower($test['EMAIL'])] = $test['ISN'];
                    } else {
                        $emailArr = explode(",", $test['EMAIL']);
                        foreach ($emailArr as $email){
                            $users[strtolower($email)] = $test['ISN'];
                        }
                    }
                }
            }
            $userData = StaffTelegramUsers::whereIn('staffEmail', array_keys($users))->get();
            foreach ($userData as $user) {
                try {
                    Log::debug('Centras : '.$user->id . ' ' . $user->staffEmail . ' ' . $users[strtolower($user->staffEmail)]);
                    $user->staffEmail = $users[strtolower($user->staffEmail)];
                    $user->save();
                } catch (\Exception $ex) {
                    Log::debug('Error : ' . $ex->getMessage());
                }
            }
        }
    }
}
