<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinics extends Model
{
    protected $table = 'clinics';
    
    public function doctors()
    {
        return $this->belongsToMany('App\Doctors');
    }
}
