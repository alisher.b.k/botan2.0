<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsReactions extends Model
{
    protected $table = 'news_reactions';
}
