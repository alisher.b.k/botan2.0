<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\FileUpload\InputFile;
use App\Telegram\DefaultKeyboard;
use App\Telegram\MenuCommand;
use Illuminate\Support\Facades\Log;
use App\StaffTelegramUsers;
use App\BotanDialogHistories;


class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $fileNames;
    public $message;
    public $telegramUser;
    public $bookingButton;
    public $messageType;

    public $tries = 1;
    public $timeout = 60;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fileNames, $message, $messageType, $telegramUser, $bookingButton = false)
    {
        $this->fileNames = $fileNames;
        $this->message = $message;
        $this->telegramUser = $telegramUser;
        $this->bookingButton = $bookingButton;
        $this->messageType = $messageType;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $resp = null;
        $reply_markup = Keyboard::make([
            'keyboard' => [['На главную']],
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        if ($this->bookingButton) {
            $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getMenuKeyboard(StaffTelegramUsers::getWorkPlaceByChatId($this->telegramUser->telegramId)),
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
        }
        
        try {
            if ($this->fileNames) {
                foreach($this->fileNames as $fileName){
                    if(strpos($fileName, 'xlsx')
                        || strpos($fileName, 'lsx')
                        || strpos($fileName, 'docx')
                        || strpos($fileName, 'doc')
                        || strpos($fileName, 'pdf')){
                        Telegram::sendDocument([
                            'chat_id' => $this->telegramUser->telegramId,
                            'document' => new InputFile(base_path() . '/public/pictures/' . $fileName, $fileName),
                        ]);
                    } else {
                        Telegram::sendPhoto([
                            'chat_id' => $this->telegramUser->telegramId,
                            'photo' => new InputFile(base_path() . '/public/pictures/' . $fileName, $fileName),
                        ]);
                    }
                }
            }
        } catch (\Exception $e) {
            Log::debug('Выброшено исключение fileNames: ' . $e->getMessage() . " " . $this->telegramUser->telegramId);
        }
        

        try {
            if ($this->message) {
                $resp = Telegram::sendMessage([
                    'chat_id' => $this->telegramUser->telegramId,
                    'text' => $this->message,
                    'reply_markup' => $reply_markup
                ]);
                Log::info($resp);
            }
        } catch (\Exception $e) {
            Log::debug('Выброшено исключение message: ' . $e->getMessage() . " " . $this->telegramUser->telegramId);
        }


        try {
            if ($this->messageType === 'poll') {
                TelegramRequestLog::where('telegramId', $this->telegramUser->telegramId)->delete();
                $logArr["telegramId"] = $this->telegramUser->telegramId;
                TelegramRequestLog::create($logArr);
            }
        } catch (\Exception $e) {
            Log::debug('Выброшено исключение poll: ' . $e->getMessage() . " " . $this->telegramUser->telegramId);
        }

        $historyArr = [
            "chatId" => $this->telegramUser->telegramId,
            "response" => $this->messageType === 'poll'
                ? "Опрос \n" . $this->message
                : $this->message,
            "userEmail" => $this->telegramUser->staffEmail,
            "telegramUsername" => $resp->chat->username,
            "telegramFirstName" => $resp->chat->first_name,
            "telegramLastName" => $resp->chat->last_name,
        ];
        BotanDialogHistories::create($historyArr);
    }
}
