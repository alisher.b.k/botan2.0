<?php

namespace App\Jobs;

use App\BotanDialogHistories;
use App\QuizResults;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\FileUpload\InputFile;

class SendQuizJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileName = false;
    protected $pollData = [];
    protected $quizId = null;
    protected $resp = null;
    protected $telegramUserId = null;
    protected $telegramUserStaffEmail = null;
    private $token;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fileName, $pollData, $quizId, $resp,$telegramUserId, $telegramUserStaffEmail)
    {
        $this->fileName = $fileName;
        $this->pollData = $pollData;
        $this->quizId = $quizId;
        $this->resp = $resp;
        $this->telegramUserId = 357253401;//$telegramUserId;
        $this->telegramUserStaffEmail = $telegramUserStaffEmail;
        $this->token = env('TELEGRAM_BOT_TOKEN');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->fileName) {
            $resp = Telegram::sendPhoto([
                'chat_id' => $this->telegramUserId,
                'photo' => new InputFile(public_path() . '/quiz/'. $this->fileName, $this->fileName),
            ]);
        }
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, "https://api.telegram.org/bot$this->token/sendPoll?" . http_build_query($this->pollData));
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl_handle);
        curl_close($curl_handle);

        $respObject = json_decode($response, true);
        $quizResult = new QuizResults();
        $quizResult->chatId = $this->telegramUserId;
        $quizResult->quizId = $this->quizId;
        $quizResult->pollId = $respObject["result"]["poll"]["id"];
        $quizResult->save();

        $historyArr = [
            "chatId" => $this->telegramUserId,
            "response" => $this->resp,
            "userEmail" => $this->telegramUserStaffEmail,
        ];
        BotanDialogHistories::create($historyArr);
    }
}
