<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rooms extends Model
{
    public function bookingBuilding(){
        return $this->belongsTo('App\Rooms');
    }
}
