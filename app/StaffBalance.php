<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffBalance extends Model
{
  protected $table = 'staff_balance';
}
