<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{

  protected $table = 'problem';

  protected $fillable = [
    'email', 'telegramId', 'question', 'questionFile', 'answer', 'answerFile', 'category', 'areFilesRemoved', 
  ];

    public $company = '';

    public function getUserFullName(){
        $email = $this->email;
        $type = 'email';
        if(is_numeric($email)){
            $type = 'ISN';
        }
        if(($userData = BotanStaff::where($type, $email)->first()) === null){
            return $email;
        }
        $this->company = $userData->WORKPLACE;
        return $userData->FIO;
    }

    public function messages(){
        return $this->hasMany('App\ProblemDialogHistories', 'problemId', 'id');
    }
}
