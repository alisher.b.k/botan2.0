<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramRequestLog extends Model
{
    protected $fillable = [
      'telegramId', 'command', 'data',
    ];

    protected $table = "telegram_request_logs";

    public static function setNewLog($chatId, $command, $data = ''){
        DB::beginTransaction();
        try{
            self::where('telegramId', $chatId)->delete();
            self::create([
                "command" => $command,
                "telegramId" => $chatId,
                "data" => $data ?? '',
            ]);
            DB::commit();
            return true;
        }catch (\Exception $exception){
            Log::debug("Exception when : $command");
            Log::debug($exception);
            DB::rollBack();
            return false;
            //TODO MAKE SORRY TEXT and stop app
        }

    }

    public static function getLog($chatId){
        return self::where('telegramId', $chatId)->first();
    }

    public static function deleteRows($chatId){
        self::where('telegramId', $chatId)->delete();
    }
}
