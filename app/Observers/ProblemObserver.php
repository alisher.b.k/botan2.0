<?php

namespace App\Observers;

use App\Problem;
use App\StaffBalance;
use App\StaffTelegramUsers;
use App\Transaction;
use App\User;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\DB;

class ProblemObserver
{
    /**
     * Handle the problem "created" event.
     *
     * @param  \App\Problem  $problem
     * @return void
     */
    public function created(Problem $problem)
    {
        $this->problemCoinGift($problem);
    }

    public function problemCoinGift($problem){
        DB::beginTransaction();
        if(($userBalance = StaffBalance::where('telegramId', (string)$problem->telegramId)->first()) === null) {
            $userBalance = new StaffBalance();
            $userBalance->telegramId = $problem->telegramId;
            $userBalance->balance = 0;
            $userBalance->isn = is_numeric($problem->email) ? $problem->email : User::getIdentityByEmail($problem->email);
        }
        $userBalance->balance += 2;

        $transaction = new Transaction();
        $transaction->telegramId = $userBalance->telegramId;
        $transaction->isn = $userBalance->isn;
        $transaction->balanceBefore = $userBalance->balance - 2;
        $transaction->balanceAfter = $userBalance->balance;
        $transaction->description = 'Награда за обращение по проблеме';
        try{
            $userBalance->save();
            $transaction->save();
            DB::commit();
        }catch (\Exception $ex){
            DB::rollBack();
            abort(500, 'Произошла ошибка при начислении Сенткоинов');
        }
    }

    /**
     * Handle the problem "updated" event.
     *
     * @param  \App\Problem  $problem
     * @return void
     */
    public function updated(Problem $problem)
    {
        //
    }

    /**
     * Handle the problem "deleted" event.
     *
     * @param  \App\Problem  $problem
     * @return void
     */
    public function deleted(Problem $problem)
    {
        //
    }

    /**
     * Handle the problem "restored" event.
     *
     * @param  \App\Problem  $problem
     * @return void
     */
    public function restored(Problem $problem)
    {
        //
    }

    /**
     * Handle the problem "force deleted" event.
     *
     * @param  \App\Problem  $problem
     * @return void
     */
    public function forceDeleted(Problem $problem)
    {
        //
    }
}
