<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionsAnswers extends Model
{
    protected $table = 'questions-_answers';

    public function category()
    {
        return $this->belongsTo('App\QuestionCategories', 'categoryId', 'id');
    }
    public function company()
    {
        return $this->belongsTo('App\Companies', 'companyId', 'id');
    }
}
