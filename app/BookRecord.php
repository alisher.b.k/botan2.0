<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookRecord extends Model
{

  protected $table = 'book_record';

  protected $fillable = [
        'date',
        'start',
        'end',
        'room', 
        'fio',
        'telegramId',
        'outsystemsUserId',
        'buildingName'
  ];

}
