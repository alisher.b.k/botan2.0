<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeMessages extends Model
{
    public function botanStaff(){
        return $this->belongsToMany("App\BotanStaff");
    }
}