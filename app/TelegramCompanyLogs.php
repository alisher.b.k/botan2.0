<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelegramCompanyLogs extends Model
{
    protected $fillable = [
        'telegramId', 'company'
      ];
}
