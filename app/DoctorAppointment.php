<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorAppointment extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'phone_number',
        'clinic',
        'doctor',
        'reason',
        'insured',
        'card_number',
        'date'
    ];
}
