<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TemplateCategory
 * @package App
 * @property  int $id
 * @property  string $category
 * @property  string $description
 * @property  $created_at
 * @property  $updated_at
 */
class TemplateCategory extends Model
{
    public function templates(){
        return $this->hasMany('App\Template', 'category', 'id');
    }
}
