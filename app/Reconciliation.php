<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reconciliation extends Model {
    protected $table = 'reconciliation';
    protected $fillable = [
        'telegram_id',
        'message_id',
        'isn',
        'reconciliation_isn',
        'message',
        'fio',
        'contract_number',
        'date',
        'kurator',
        'company',
        'participant',
        'note'
    ];

    public const COMPANY_INSURANCE = 'insurance';
    public const COMPANY_KOMMESK = 'kommesk';
    public const COMPANY_KOMMESK_LIFE = 'kcj';

    public const COMPANIES = [
        'ksj' => self::COMPANY_KOMMESK_LIFE,
        'centras' => self::COMPANY_INSURANCE,
        'kommesk' => self::COMPANY_KOMMESK
    ];

    public static function getMessage($reconciliation, $withMessage = false): string
    {
        $message = 'Здравствуйте!'. "\n" .$reconciliation->message ." "
            .$reconciliation->contract_number.'" от "'.$reconciliation->date.'."'. "\n"
            .'Инициатор либо Куратор СЗ: '.$reconciliation->kurator;
        if ($withMessage) {
            $message = $message."\nКонтрагент: ".$reconciliation->participant.". Примечание: ".$reconciliation->note;
        }
        return $message;
    }
}
