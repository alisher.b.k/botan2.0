<?php

namespace App;

use App\QuizResults;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * Class QuizQuestion
 * @package App
 *
 * @property-read QuizResults[] $results
 */
class QuizQuestion extends Model {
    protected $table = 'quiz_questions';

    public function optionResults($id, $optionId) {
        $results = QuizResults::where('quizId', $id)->where('answer', $optionId)->count();
        return $results;
    }

    public function results() {
        return $this->hasMany(QuizResults::class, 'quizId');
    }
}
