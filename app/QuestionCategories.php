<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionCategories extends Model
{
    public function parentCategory()
    {
        return $this->belongsTo('App\QuestionCategories', 'parent_id', 'id');
    }
}
