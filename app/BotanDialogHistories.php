<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BotanDialogHistories extends Model
{
    protected $fillable = [
      'chatId', 'telegramUsername', 'telegramFirstName',
      'telegramLastName', 'request', 'response', 'userEmail',
    ];

    public function user()
    {
        return $this->belongsTo('App\BotanStaff', 'userEmail', 'email');
    }
}
