<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProblemDialogHistories extends Model
{

  protected $table = 'problem_dialog_histories';

  protected $fillable = [
    'email', 'telegramId', 'question', 'questionFile', 'answer', 'answerFile', 'problemId',
  ];
}
