<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/notification', 'BotanApiController@sendNotification');
Route::post('/serviceCenterNotify', 'BotanApiController@sendServiceNotification');
Route::post('/booking/data', 'BotanApiController@getBookingData');
Route::post('/ogpovts/notification', 'BotanApiController@sendOgpoVtsNotification');
Route::post('/aida/notification/doctor', 'BotanApiController@sendDoctorNotificationAida');
Route::post('/api/get-ol-data', 'BotanApiController@getOlData');

Route::get('/help-desk', 'HomeController@helpDesk')->name('helpDesk');
Route::get('/help-desk/{id}/edit', 'HomeController@editHelpDesk');
Route::post('/help-desk/save', 'HomeController@saveCommand')->name('saveCommand');
Route::get('/help-desk/{id}/delete', 'HomeController@deleteCommand');

Route::get('botan/about-us/{id}/edit', 'HomeController@editAboutUsCommand');
Route::get('botan/prizes', function() {
    return view('prizes');
})->name('prizes');

Route::get('botan/about-us/{id}/delete', 'HomeController@deleteAboutCommand');
Route::post('botan/about-us/save', 'HomeController@saveAboutUsCommand')->name('saveAboutUsCommand');
Route::get('botan/about-us', 'HomeController@aboutUs')->name('aboutUs');
Route::get('botan/about-us/view', 'BotanStaffController@aboutUsView')->name('aboutUsView');
Route::get('botan/about-us/view/get-content', 'BotanStaffController@aboutUsViewGetContent')->name('aboutUsViewGetContent');
Route::post('botan/about-us', 'HomeController@saveAboutUs')->name('saveAboutUs');
Route::post('botan/about-us-senate', 'HomeController@saveSenateAboutUs')->name('saveSenateAboutUs');

Route::get('botan/problems', 'HomeController@problems')->name('problems');
Route::get('botan/problems/{id}/answer', 'HomeController@answerOnProblem');
Route::post('botan/problems/answer/save', 'HomeController@answerOnProblemSave')->name("answerOnProblemSave");
Route::get('botan/problems/{id}/close', 'HomeController@closeProblem')->name("closeProblem");
Route::get('botan/problems/{uuid}/download', 'HomeController@downloadProblemFile')->name('problem.download');
Route::get('botan/problems/{uuid}/download/answer', 'HomeController@downloadProblemAnswerFile')->name('problem.download.answer');
Route::get('botan/dialog/{dialog}/download/', 'HomeController@downloadProblemFileDialog')->name('dialog.download');
Route::get('botan/dialog/{dialog}/download/answer', 'HomeController@downloadProblemAnswerFileDialog')->name('dialog.download.answer');

Route::get('/library', 'HomeController@library')->name('library');
Route::get('/library/{id}/edit', 'HomeController@editLibrary');
Route::post('/library/save', 'HomeController@saveLibrary')->name('saveLibrary');
Route::get('/library/{id}/delete', 'HomeController@deleteLibrary');

Route::get('/export-excel', 'BotanDialogHistoriesController@exportToExcel')->name('export-excel');
Route::post('staff/save', 'HomeController@saveStaff')->name('saveStaff');
Route::post('import-excel', 'HomeController@importExcel')->name('importExcel');
Route::get('/', 'BotanDialogHistoriesController@showHistory')->name('home');
Route::get('/assign/role', 'UserRolesController@assignRole');
Route::get('/user/{id}/edit', 'UserRolesController@userInfo');
Route::get('/question/{id}/edit', 'QuestionsAnswersController@editQuestion');
Route::get('/staff/{id}/edit', 'HomeController@editStaff');
Route::get('/question/{id}/delete', 'QuestionsAnswersController@deleteQuestion');

Route::get('/categories/get', 'QuestionCategoriesController@getCategories')->name('questionsCategories');
Route::get('/categories/{id}/edit', 'QuestionCategoriesController@editCategory')->name('editCategory');
Route::post('/category/save', 'QuestionCategoriesController@saveCategory')->name('saveCategory');
Route::get('/categories/{id}/delete', 'QuestionCategoriesController@deleteCategory')->name('deleteCategory');

Route::get('/botan/bosses', 'BossController@list')->name('bosses');
Route::post('/botan/bosses/import-excel', 'BossController@importExcel')->name('importExcelBosses');
Route::get('/botan/bosses/create', 'BossController@create')->name('createBosses');
Route::post('/botan/bosses/store', 'BossController@store')->name('bossesStore');
Route::get('/botan/bosses/edit/{id}', 'BossController@edit')->name('bossesEdit');
Route::post('/botan/bosses/update', 'BossController@update')->name('bossesUpdate');
Route::post('/botan/bosses/upload-photo', 'BossController@uploadPhoto')->name('uploadPhotoBosses');
Route::get('botan/bosses/{dialog}/download', 'BossController@downloadPhoto')->name('downloadPhotoBosses');

Route::get('/botan/staff/{id}/delete', 'HomeController@deleteStaff')->name('deleteStaff');
Route::get('botan/categories/get', 'BotanStaffController@getCategories');
Route::get('botan/subcategories/get', 'BotanStaffController@getSubcategories');
Route::get('botan/questions/search', 'BotanStaffController@searchQuestions');
Route::get('botan/staff/vacation', 'BotanStaffController@getVacationRemainingDays');
Route::get('/categories/{id}/edit', 'QuestionCategoriesController@editCategory')->name('editCategory');

Route::post('botan/users/search', 'BotanStaffController@search');
Route::post('botan/telegram/calc', 'BotanStaffController@calculateInsurance');
Route::post('botan/user/save', 'UserRolesController@userSaveInfo')->name('saveUserInfo');
Route::post('botan/question/save', 'QuestionsAnswersController@saveQuestion')->name('saveQuestion');

Route::get('botan/polls', 'PollController@showPolls')->name('polls');

Route::get('botan/quizzes', 'QuizController@showQuizzes')->name('quizzes');
Route::get('botan/quizzes/{id}/downloadExcel', 'QuizController@downloadExcel')->name('downloadExcel');

Route::post('botan/transactions/rechargeBalance', 'TransactionsController@rechargeBalance')->name('rechargeBalance');
Route::post('botan/transactions/withdrawBalance', 'TransactionsController@withdrawBalance')->name('withdrawBalance');
Route::get('botan/transactions/{id}','TransactionsController@showTransactions')->name('showTransactions');

Route::get('staff/photo/{fileName}', 'BotanCodesController@showImage');
Route::post('botan/login', 'BotanTelegramUsersController@login');
Route::post('botan/login/code', 'BotanTelegramUsersController@checkCode');
Route::get('botan/dialogues/history/{chatId}', 'BotanDialogHistoriesController@showHistoryForChat')->name('chatDialogHistory');
Route::get('botan/dialogues/history', 'BotanDialogHistoriesController@showHistory')->name('dialogHistory');
Route::get('botan/telegram/questions/get', 'BotanStaffController@getQuestions');

Route::get('botan/users/roles', 'UserRolesController@getAllUsersRoles')->name('usersRoles');
Route::get('botan/create/user', 'UserRolesController@createUserView' )->name('create.user');
Route::post('botan/create/user', 'UserRolesController@createUser');
Route::get('botan/delete/user/{user_id}', 'UserRolesController@deleteUser')->name('delete.user');

Route::get('botan/questions/get', 'QuestionsAnswersController@getAllQuestionsAndAnswers')->name('questionsAnswers');
Route::post('botan/answer/get', 'BotanStaffController@getAnswer');
Route::get('botan/category/questions/get', 'BotanStaffController@getCategoryQuestions');
Route::get('botan/staff/get', 'BotanDialogHistoriesController@showStaff')->name('showStaff');
Route::get('botan/staff/prelogin', 'BotanTelegramUsersController@prelogin')->name('prelogin');
Route::get('botan/test', 'BotanCodesController@test')->name('test');
Route::get('botan/events', 'HomeController@getEventCalendar')->name('events');
Route::get('botan/api/events/get', 'BotanStaffController@getEvents');
Route::post('botan/events/save', 'HomeController@saveEvent')->name('saveEvent');
Route::post('botan/events/delete', 'HomeController@deleteEvent')->name('deleteEvent');
Route::get('botan/events/edit/{eventId}', 'HomeController@editEvent')->name('editEvent');
Route::get('botan/products', 'ProductsController@getProducts')->name('getProducts');
Route::get('botan/products/edit/{productsId}', 'ProductsController@editProduct')->name('editProduct');
Route::post('botan/products/save', 'ProductsController@saveProduct')->name('saveProduct');
Route::get('botan/products/delete/{productsId}', 'ProductsController@deleteProduct')->name('deleteProduct');
Route::get('botan/api/products/view/{productId}', 'BotanApiController@viewProduct')->name('viewProduct');
Route::get('botan/api/products', 'BotanApiController@getProducts');
Route::get('botan/schedule', 'BotanApiController@schedule');

Route::get('botan/feedback', 'FeedbackController@index')->name('feedback');
Route::get('botan/feedback/{feedback}/download/file', 'HomeController@downloadFeedbackFile')->name('feedback.download.file');


Route::get('botan/templates', 'TemplateController@index')->name('templates');
Route::get('botan/templates/category/add', 'TemplateController@addCategory')->name('templates.category.add');
Route::post('botan/templates/category/add', 'TemplateController@saveCategory')->name('templates.category.save');
Route::get('botan/templates/{category}', 'TemplateController@getTemplatesByCategory')->name('templates.category.get');
Route::get('botan/templates/delete/{category}', 'TemplateController@deleteCategory')->name('templates.category.delete');
Route::get('botan/template/add', 'TemplateController@addTemplate')->name('templates.add');
Route::post('botan/template/add', 'TemplateController@saveTemplate')->name('templates.save');
Route::get('botan/template/edit/{template}', 'TemplateController@editTemplate')->name('templates.edit');
Route::get('botan/template/delete/{template}', 'TemplateController@deleteTemplate')->name('templates.delete');
Route::post('botan/template/edit/{template}', 'TemplateController@saveEditedTemplate')->name('templates.saveEdited');
Route::get('botan/template/deleteFile/{template}', 'TemplateController@deleteTemplateFile')->name('templates.deleteFile');
Route::get('botan/template/download/{template}', 'TemplateController@downloadFile')->name('templates.download');
Route::get('botan/template/send/{template}', 'TemplateController@sendTemplateView')->name('templates.sendView');
Route::post('botan/template/send/{template}', 'TemplateController@sendTemplate')->name('templates.send');

Route::get('/botan/group/list', 'GroupController@index')->name('group');
Route::get('/botan/group/add', 'GroupController@addView')->name('group.add');
Route::post('/botan/group/add', 'GroupController@add');
Route::get('/botan/group/edit/{group}', 'GroupController@editView');
Route::post('/botan/group/edit/{group}', 'GroupController@edit');
Route::get('/botan/group/delete/{group}', 'GroupController@delete');

Route::get('/botan/recruiting/getVacancy', 'RecruitingController@getVacancy')->name('getVacancy');
Route::get('/botan/recruiting/createVacancy', 'RecruitingController@createVacancyView')->name('createVacancyView');
Route::post('/botan/recruiting/createVacancy', 'RecruitingController@createVacancy')->name('createVacancy');
Route::get('/botan/recruiting/vacancy/{id}/delete', 'RecruitingController@deleteVacancy')->name('deleteVacancy');
Route::get('/botan/recruiting/vacancy/{id}/edit', 'RecruitingController@editVacancyView' )->name('editVacancyView');
Route::post('/botan/recruiting/vacancy/{id}/edit', 'RecruitingController@editVacancy')->name('editVacancy');
Route::post('/botan/recruiting/vacancy/import-excel', 'RecruitingController@importVacancyExcel')->name('importVacancyExcel');

Route::get('booking/building', 'BookingController@buildingList')->name('booking.building');
Route::get('booking/building/add', 'BookingController@buildingAddView')->name('booking.building.add');
Route::post('booking/building/add', 'BookingController@buildingAdd');
Route::get('booking/building/edit/{building}', 'BookingController@buildingEditView')->name('booking.building.edit');
Route::post('booking/building/edit/{building}', 'BookingController@buildingEdit');
Route::get('booking/building/delete/{building}', 'BookingController@buildingDelete')->name('booking.building.delete');

Route::get('booking/room/{building}', 'BookingController@roomList')->name('booking.room');
Route::get('booking/room/add/{building}', 'BookingController@roomAddView')->name('booking.room.add');
Route::post('booking/room/add/{building}', 'BookingController@roomAdd');
Route::get('booking/room/edit/{room}/{building}', 'BookingController@roomEditView')->name('booking.room.edit');
Route::post('booking/room/edit/{room}/{building}', 'BookingController@roomEdit');
Route::get('booking/room/delete/{room}/{building}', 'BookingController@roomDelete')->name('booking.room.delete');

Route::middleware(['auth'])->namespace('Backend')->group(function () {
    Route::get('botan/send-messages', 'SettingController@sendMessagesFormView')->name('sendMessagesForm');
    Route::get('botan/send-cent-messages', 'SettingController@sendCentMessagesFormView')->name('sendCentMessagesForm');
    Route::get('botan/send-quiz-messages', 'SettingController@sendQuizMessagesFormView')->name('sendQuizMessagesForm');
});
Route::middleware(['auth'])->prefix('admin')->namespace('Backend')->name('admin.')->group(function () {
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('/setting', 'SettingController@index')->name('setting.index');
    Route::post('/setting/store', 'SettingController@store')->name('setting.store');
    Route::post('/setting/setwebhook', 'SettingController@setwebhook')->name('setting.setwebhook');
    Route::post('/setting/getwebhookinfo', 'SettingController@getwebhookinfo')->name('setting.getwebhookinfo');
    Route::post('/setting/sendNotifications', 'SettingController@sendNotifications')->name('setting.sendNotifications');
    Route::post('/setting/sendCentNotifications', 'SettingController@sendCentNotifications')->name('setting.sendCentNotifications');
    Route::post('/setting/sendQuiz', 'SettingController@sendQuiz')->name('setting.sendQuiz');
    Route::post('/setting/uploadPrizes', 'SettingController@uploadPrizes')->name('setting.uploadPrizes');
    Route::get('/setting/cent/products/update', 'SettingController@updateCentProducts')->name('update.cent.products');
    Route::get('/setting/logs', 'SettingController@logs')->name('logs');
    Route::get('/setting/log/{path}', 'SettingController@read')->name('read.log');
    Route::post('/setting/add-user-info', 'SettingController@addUsersInfo')->name('addUsersInfo');
});

Route::post(Telegram::getAccessToken(), 'TelegramHandlerController@index')->name('setting.store');
Route::get('/test/limit', function() {
//    \App\Telegram\LimitsCommand::executeCommand(  '316204431', 'К0025626');
//    \App\Telegram\LimitsCommand::$token();
});
Auth::routes();
Route::post('/updateStaff', 'StaffController@updateStaff');
Route::post('/updateStaffSOSMA', 'StaffController@updateStaffSOSMA');
Route::get('/logout', 'HomeController@logout')->name('logout');
Route::view('/phpini', 'phpini');

Route::get('/export/users', 'ExportController@getNewUsersView')->name('export.newusers.view');
Route::post('/export/users', 'ExportController@exportNewUsers')->name('export.newusers');

Route::get('/export/centcoins', 'ExportController@exportCentcoins')->name('export.centcoins');

Route::get('/export/clinics', 'ExportController@getClinicsView')->name('export.clinics.view');
Route::post('/export/clinics', 'ExportController@exportClinics')->name('export.clinics');
Route::get('/export/reactions', 'ExportController@getReactionsView')->name('export.reactions.view');
Route::post('/export/get_reactions', 'ExportController@getReactionsForExport')->name('export.get.reactions');
Route::post('/export/reactions', 'ExportController@exportReaction')->name('export.reaction');

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/report/userlist', 'DataLakeController@getUsers');
Route::get('/report/activity', 'DataLakeController@getUserActivity');
Route::get('/report/reaction', 'DataLakeController@getUserReactions');
Route::get('/report/problem', 'DataLakeController@getProblemReport');
Route::get('/report/booking', 'DataLakeController@getBookingReport');
Route::get('/report/doctor', 'DataLakeController@getDoctorReport');

Route::get('/change/{company}', 'Backend\SettingController@changeCompanyVVP');
Route::get('/test', 'StaffController@getVacation');

Route::post('/botan/send-messages/type','Backend\SettingController@sendTypeOfMessage')->name('message.type');
Route::post('/api/send-reconciliation', 'NotificationController@storeAndSend');
Route::get('/admin/command', 'CommandController@command')->name('admin.command');
Route::post('/send/mail', 'NotificationController@sendMail');
