<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/booking/buildings', 'BotanApiController@getBuildings');
Route::post('/booking/building', 'BotanApiController@getBuildingByIdWithRooms');
Route::post('/booking/records', 'BotanApiController@getBookingRecords');
Route::post('/booking/createRecord', 'BotanApiController@createRecord');
Route::post('/booking/userRecords', 'BotanApiController@getUserRecords');
Route::post('/booking/deleteRecord', 'BotanApiController@deleteRecordById');
Route::get('/appointment/clinics', 'BotanApiController@getClinics');
Route::post('/appointment/doctors', 'BotanApiController@getDoctors');
Route::post('/appointment/coordinators', 'BotanApiController@sendToCoordinators');
Route::get('/outsystems/employees', 'BotanApiController@getEmployees');
Route::post('/outsystems/vacation', 'BotanApiController@getOutsystemsVacation');