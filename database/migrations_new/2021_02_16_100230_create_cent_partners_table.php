<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cent_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('partner_name');
            $table->string('title');
            $table->string('image_url');
            $table->unsignedBigInteger('partner_id');
            $table->text('message_text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cent_partners');
    }
}
