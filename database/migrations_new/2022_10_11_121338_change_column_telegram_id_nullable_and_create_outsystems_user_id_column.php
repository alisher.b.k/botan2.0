<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTelegramIdNullableAndCreateOutsystemsUserIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_record', function (Blueprint $table) {
            $table->integer('telegramId')->nullable()->change();
            $table->integer('outsystemsUserId')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_record', function (Blueprint $table) {
            $table->integer('telegramId')->nullable(false)->change();
            $table->dropColumn('outsystemsUserId');
        });
    }
}
