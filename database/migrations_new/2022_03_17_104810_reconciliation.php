<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reconciliation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reconciliation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('telegram_id')->index('telegram_id');
            $table->string('message_id')->index('message_id')->nullable();
            $table->string('isn')->index('isn');
            $table->string('reconciliation_isn');
            $table->string('message')->nullable();
            $table->string('fio')->index('fio');
            $table->string('contract_number')->index();
            $table->string('date');
            $table->string('kurator')->nullable();
            $table->string('company')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reconciliation');
    }
}
