<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToBotanStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('botan_staff', function (Blueprint $table) {
            $table->string('ISN', 20)->nullable()->default(null);
            $table->string('NAMELAT', 100)->nullable()->default(null);
            $table->string('WORKPLACE', 100)->nullable()->default(null);
            $table->string('MAINDEPT', 100)->nullable()->default(null);
            $table->string('SUBDEPT', 100)->nullable()->default(null);
            $table->string('VACATIONBEG', 100)->nullable()->default(null);
            $table->string('VACATIONEND', 100)->nullable()->default(null);
            $table->string('TRIPBEG', 100)->nullable()->default(null);
            $table->string('TRIPEND', 100)->nullable()->default(null);
            $table->string('LEAVEDATE', 100)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('botan_staff', function (Blueprint $table) {
            //
        });
    }
}
