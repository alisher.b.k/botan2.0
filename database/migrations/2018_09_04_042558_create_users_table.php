<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('botan_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('FIO', 100)->index('FIO');
            $table->string('position', 50)->index('position');
            $table->string('photo');
            $table->string('workPhone',100)->index('workPhone');
            $table->string('intPhone',100)->index('intPhone');
            $table->string('mobPhone',100);
            $table->string('email')->index('email');
            $table->date('bday');
            $table->boolean('Holiday');
            $table->integer('workEx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
