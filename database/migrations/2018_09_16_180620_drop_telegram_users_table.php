<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTelegramUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('botan_telegram_users');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('botan_telegram_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname',50)->index('fullname')->nullable();
            $table->string('email',30)->index('email');
            $table->timestamps();
        });
    }
}
