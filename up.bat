docker-compose run --rm app chmod 0777 -R /app/storage/
docker-compose run --rm app composer install
docker-compose run --rm app php artisan migrate
docker-compose run --rm app php artisan db:seed
docker-compose up -d
