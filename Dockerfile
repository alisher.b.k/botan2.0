ARG PREFIX=reg.cic.kz/centras

# FROM ${PREFIX}/php-fpm:7-oci as botan-laravel-vendor
#
# WORKDIR /app
#
# COPY composer.* ./
#
# RUN rm vendor -rf
#
# RUN composer install \
#     --no-autoloader \
#     --no-interaction \
#     --no-scripts
#
#

# Configure api
FROM ${PREFIX}/php-fpm:7-oci
ARG CI_COMMIT_REF_NAME=unknown
ARG ENV=prod
WORKDIR /app

# RUN echo "CACHE_DRIVER=file" >> /app/.env

# COPY --from=botan-laravel-vendor /app/vendor /app/vendor
# COPY --from=botan-build-front /app/public /app/public
COPY ./app /app/app
COPY ./bootstrap /app/bootstrap
COPY ./config /app/config
COPY ./database /app/database
COPY ./artisan /app/artisan
# COPY ./storage /app/storage
COPY ./routes /app/routes
COPY ./public /app/public
COPY ./vendor /app/vendor

 # TODO remove and use view:cache
COPY ./resources /app/resources
COPY ./storage/framework /app/storage/framework
COPY ./storage/logs /app/storage/logs
COPY ./storage/app /app/storage/app
COPY composer.* ./

RUN chmod 0777 /app -R

RUN chmod 0777 /app/storage/app -R
RUN chmod 0777 /app/storage/logs -R
RUN chmod 0777 /app/storage/framework -R

# RUN composer dump-autoload

#RUN php artisan storage:link

# Set Nginx config
COPY .deploy/.conf/nginx/default.conf /opt/docker/etc/nginx/vhost.conf


# Print current branch name into a file
RUN echo "CI_PIPELINE_ID=${CI_PIPELINE_ID}" >> /app/public/branch.txt
RUN echo "CI_PIPELINE_IID=${CI_PIPELINE_IID}" >> /app/public/branch.txt
RUN echo "CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}" >> /app/public/branch.txt
RUN echo "CI_PROJECT_PATH=${CI_PROJECT_PATH}" >> /app/public/branch.txt
RUN echo "CI_JOB_ID=${CI_JOB_ID}" >> /app/public/branch.txt
RUN echo "CI_JOB_NAME=${CI_JOB_NAME}" >> /app/public/branch.txt
RUN echo "CI_JOB_STAGE=${CI_JOB_STAGE}" >> /app/public/branch.txt
RUN echo "CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}" >> /app/public/branch.txt

RUN ln -s /shared/.env /app/.env
# COPY .env /app/.env

COPY .deploy/.conf/supervisor/botan-worker.conf /opt/docker/etc/supervisor.d/botan-worker.conf

COPY .deploy/.conf/cron/crontab /crontab
RUN crontab -l | { cat; cat /crontab; } | crontab -
RUN rm -f /crontab


# COPY .deploy/.conf/php-fpm/application.conf /opt/docker/etc/php/fpm/pool.d/application.conf

# Push useful bash aliases
# https://www.atatus.com/blog/14-useful-bash-aliases-that-make-shell-less-complex-and-more-fun/
RUN echo 'alias a="php /app/artisan"' >> ~/.bashrc
RUN echo 'alias ll="ls -la"' >> ~/.bashrc
RUN echo 'alias tin="php /app/artisan tinker"' >> ~/.bashrc

RUN mkdir -p /app/storage/logs/nginx

RUN touch /var/log/cron.log && chmod 0777 /var/log/cron.log
