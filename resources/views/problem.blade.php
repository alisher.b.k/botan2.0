@extends('layouts.default_light')

@section('content')
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <style>
        .filterable {
            margin-top: 15px;
        }
        .filterable .panel-heading .pull-right {
            margin-top: -20px;
        }
        .filterable .filters input[disabled] {
            background-color: transparent;
            border: none;
            cursor: auto;
            box-shadow: none;
            padding: 0;
            height: auto;
        }
        .filterable .filters input[disabled]::-webkit-input-placeholder {
            color: #333;
        }
        .filterable .filters input[disabled]::-moz-placeholder {
        .filterable .filters input[disabled]::-moz-placeholder {
            color: #333;
        }
        .filterable .filters input[disabled]:-ms-input-placeholder {
            color: #333;
        }


    </style>

    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div>
                                <i class="fas fa-table"></i>
                                Проблемы
                            </div>
                        </div>
                    </div>
                    <div class="card-body filterable">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="problems" width="100%" cellspacing="0">
                                <thead>
                                <tr class="filters">
                                    <th><input type="text" class="form-control" placeholder="№" disabled></th>
                                    <th><input type="text" class="form-control" placeholder="ФИО" disabled></th>
                                    <th><input type="text" class="form-control" placeholder="Компания" disabled></th>
                                    <th><input type="text" class="form-control" placeholder="Категория" disabled></th>
                                    <th><input type="text" class="form-control" placeholder="Сообщение / Файл" disabled></th>
                                    <th><input type="text" class="form-control" placeholder="Ответ / Файл" disabled></th>
                                    <th><input type="text" class="form-control" placeholder="Дата" disabled></th>
                                    <th><input type="text" class="form-control" placeholder="Статус" disabled></th>
                                    <th></th>
                                    <th><button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>№</th>
                                    <th>ФИО</th>
                                    <th>Компания</th>
                                    <th>Категория</th>
                                    <th>Сообщение / Файл</th>
                                    <th>Ответ / Файл</th>
                                    <th>Дата</th>
                                    <th>Статус</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($problems as $problem)
                                    <tr>
                                        <td>{{$problem->id}}</td>
                                        <td>{{$problem->getUserFullName()}}</td>
                                        <td>{{$problem->company}}</td>
                                        <td>{{$problem->category}}</td>
                                        <td>{{$problem->question}}
                                            @if ($problem->questionFile)
                                            <div> <a href="{{ route('problem.download', $problem->id) }}">Скачать</a></div>
                                            @endif
                                        </td>
                                        <td>{{$problem->answer}} 
                                            @if ($problem->answerFile)
                                            <div> <a href="{{ route('problem.download.answer', $problem->id) }}">Скачать</a></div>
                                            @endif
                                        </td>
                                        <td>{{$problem->created_at}}</td> 
                                        <td>
                                            @if ($problem->status == "active")
                                                Активная
                                            @elseif ($problem->status == "closed")
                                                Закрыта
                                            @endif
                                        </td>
                                        <td>
                                            @if ($problem->areFilesRemoved)
                                                Файлы удалены
                                            @endif
                                        </td>  
                                        <td>
                                            @if($problem->status != "closed")
                                            <div class="d-flex flex-row">
                                                <a title="Ответить" href="/botan/problems/{{$problem->id}}/answer" class="mx-1 btn btn-primary btn-xs">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <a title="Закрыть" href="/botan/problems/{{$problem->id}}/close" class="mx-1 btn btn-danger btn-xs">
                                                    <i class="far fa-window-close"></i>
                                                </a>
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif


        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>


        <!-- /.content-wrapper -->
    </div>
    <script>
        /*
Please consider that the JS part isn't production ready at all, I just code it to show the concept of merging filters and titles together !
*/
        $(document).ready(function(){
            $('.filterable .btn-filter').click(function(){
                var $panel = $(this).parents('.filterable'),
                    $filters = $panel.find('.filters input'),
                    $tbody = $panel.find('.table tbody');
                if ($filters.prop('disabled') == true) {
                    $filters.prop('disabled', false);
                    $filters.first().focus();
                } else {
                    $filters.val('').prop('disabled', true);
                    $tbody.find('.no-result').remove();
                    $tbody.find('tr').show();
                }
            });

            $('.filterable .filters input').keyup(function(e){
                /* Ignore tab key */
                var code = e.keyCode || e.which;
                if (code == '9') return;
                /* Useful DOM data and selectors */
                var $input = $(this),
                    inputContent = $input.val().toLowerCase(),
                    $panel = $input.parents('.filterable'),
                    column = $panel.find('.filters th').index($input.parents('th')),
                    $table = $panel.find('.table'),
                    $rows = $table.find('tbody tr');
                /* Dirtiest filter function ever ;) */
                var $filteredRows = $rows.filter(function(){
                    var value = $(this).find('td').eq(column).text().toLowerCase();
                    return value.indexOf(inputContent) === -1;
                });
                /* Clean previous no-result if exist */
                $table.find('tbody .no-result').remove();
                /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
                $rows.show();
                $filteredRows.hide();
                /* Prepend no-result row if all rows are filtered */
                if ($filteredRows.length === $rows.length) {
                    $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
                }
            });
        });
    </script>
@endsection
