@extends('layouts.default_light')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.5/dist/css/select2.min.css" rel="stylesheet" />
@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Рассылка
                    </div>
                    <div class="card-body">
                        <a href="{{route('admin.update.cent.products')}}">
                            <button type="button" class="btn btn-outline-info">Обновить список продуктов</button>
                        </a>
                        <form id="sendNotifications" action="{{route('admin.setting.sendCentNotifications')}}"
                              method="post" enctype="multipart/form-data" >
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="partner">Продукт</label>
                                <select class="form-control" id="partner" name="partner">
                                    @foreach($partners as $partner)
                                        <option value="{{$partner->partner_id}}">{{$partner->partner_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="company">Получатели</label>
                                <select class="form-control" id="company" name="company"  onchange="checkType('m')">
                                    <option value="user">Нескольким пользователям</option>
                                    <option value="companies">Нескольким компаниям</option>
                                    <option value="" selected>Всем пользователям</option>
                                </select>
                            </div>
                            <div class="form-group" id="usersSelectMailing" style="display: none;">
                                <label for="company">Получатели</label>
                                <select class="multiple-2" id="users" name="users[]" style="width: 100%;" multiple>
                                    @foreach ($users as $user)
                                        @if($user->getFullName() !== null)
                                            <option value="{{$user->telegramId}}">{{$user->FIO}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="companiesSelectMailing" style="display: none">
                                <label for="company">Получатели</label>
                                <select id="users" class="multiple-1" name="companies[]" style="width: 100%;" multiple>
                                    @foreach($companies as $company)
                                        <option value="{{$company->companyName}}">{{$company->companyName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Сообщение</label>
                                <div class="input-group">
                                    <textarea class="form-control" name="message" id="message"></textarea>
                                </div>
                            </div>
                            <div class="input-group mt-2">
                                <button type="submit" class="btn btn-primary">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <!-- <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer> -->

    </div>
    <!-- /.content-wrapper -->

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://rawgit.com/select2/select2/4.0.5/dist/js/select2.full.min.js"></script>
    <script>
        $(".multiple-1").select2();
        $(".multiple-2").select2();
        function checkType(elem){
            let type = $('#company').val();
            if(type === 'user'){
                $('#usersSelectMailing').show();
                $('#companiesSelectMailing').hide();
            }else if(type === 'companies'){
                $('#usersSelectMailing').hide();
                $('#companiesSelectMailing').show();
            }else{
                $('#usersSelectMailing').hide();
                $('#companiesSelectMailing').hide();
            }
        }
    </script>
    <script>
        var texts = {
            @foreach($partnersArray as $key => $value)
                "{{$key}}" : {!! json_encode($value) !!},
            @endforeach
        };

        console.log(texts);
        window.addEventListener('load', mExternalJsLoadFunc, false);

        function mExternalJsLoadFunc (){
            function partnerChanged(){
                let val = $('#partner').val();
                $('#message').val(texts[val])
            }
            $('#partner')[0].addEventListener('change', function (){
                partnerChanged();
            })
            partnerChanged();
        }
    </script>

@endsection
