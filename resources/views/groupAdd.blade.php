@extends('layouts.default_light')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Добавить группу
                </div>
                <div class="card-body">
                    <form id="sendNotifications" action="/botan/group/add"
                          method="post" enctype="multipart/form-data" >
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>Название</label>
                            <div class="input-group">
                                <input class="form-control" name="name" type="text">
                            </div>
                        </div>
                        <div class="form-group" id="usersSelectMailing">
                            <label for="company">Получатели</label>
                            <select class="js-example-basic-multiple multiple-js" id="users" name="users[]" style="width: 100%;" multiple>
                                @foreach ($users as $user)
                                    @if($user->getFullName() !== null)
                                        <option value="{{$user->staffEmail}}">{{$user->FIO}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group mt-2">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Sticky Footer -->
        <!-- <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer> -->

    </div>
    <!-- /.content-wrapper -->

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(".multiple-js").select2();
        function checkType(elem){
            if(elem === 'm'){
                let type = $('#company').val();
                if(type === 'user'){
                    $('#usersSelectMailing').show();
                }else{
                    $('#usersSelectMailing').hide();
                }
            }else{
                let type = $('#companyQuiz').val();
                if(type === 'user'){
                    $('#usersSelectQuiz').show();
                }else{
                    $('#usersSelectQuiz').hide();
                }
            }
        }
    </script>
    <script>
    function addFields(){
            // Number of inputs to create
            var number = document.getElementById("questionsNum").value;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("container");

            for (i=0;i<number;i++){
                // Append a node with a random text
                container.appendChild(document.createTextNode("Вопрос " + (i+1)));
                // Create an <input> element, set its type and name attributes
                var div = document.createElement("div");
                div.innerHTML = `<div class="form-group">
                                    <label>Сообщение</label>
                                    <div class="input-group">
                                        <textarea class="form-control" name="message${i}" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="photoUpload">Загрузить картинку</label>
                                    <input class="form-control-file" id="photoUpload${i}" name="photoUpload${i}" type="file">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 1</label>
                                    <input class="form-control-file" id="option1${i}" name="option1${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 2</label>
                                    <input class="form-control-file" id="option2${i}" name="option2${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 3</label>
                                    <input class="form-control-file" id="option3${i}" name="option3${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 4</label>
                                    <input class="form-control-file" id="option4${i}" name="option4${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="correctAnswer">Правильный ответ</label>
                                    <select class="form-control" id="correctAnswer${i}" name="correctAnswer${i}">
                                        <option value="0">Вариант 1</option>
                                        <option value="1">Вариант 2</option>
                                        <option value="2">Вариант 3</option>
                                        <option value="3">Вариант 4</option>
                                    </select>
                                </div>`;
                container.appendChild(div);
                // Append a line break
                container.appendChild(document.createElement("br"));
            }
        }
    </script>

@endsection
