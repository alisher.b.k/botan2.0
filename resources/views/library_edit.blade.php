@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Редактирование книги
                    </div>
                    <div class="card-body">
                        <form action="{{route('saveLibrary')}}" method="post">
                            @csrf
                            <input type="hidden" class="form-control" name="libraryId" value="
                                @if($library)
                            {{$library->id}}
                            @endif">
                            <div class="form-group">
                                <label for="description">Описание</label>
                                <input class="form-control" id="description" name="description" value="@if($library){{$library->description}}@endif" required>
                            </div>
                            <div class="form-group">
                                <label for="link">Ссылка</label>
                                <input class="form-control" id="link" name="link" value="@if($library){{$library->link}}@endif" required>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
