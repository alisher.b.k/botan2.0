@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Ответ на проблему
                    </div>
                    <div class="card-body">
                        <form action="{{route('answerOnProblemSave')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" class="form-control" name="id" value="@if($problem){{$problem->id}}@endif">
                            <div class="form-group">
                                <label for="fio">ФИО</label>
                                <input class="form-control" id="fio" name="fio" value="{{$problem->getUserFullName()}}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="company">Компания</label>
                                <input class="form-control" id="company" name="company" value="{{$problem->company}}" disabled>
                            </div>
                            @if($problem && $problem->question)
                                <div class="form-group">
                                    <label for="question">Обращение</label>
                                    <input class="form-control" id="question" name="question" value="{{$problem->question}}" disabled>
                                </div>
                            @endif
                            @if ($problem->questionFile)
                                <div class="form-group">
                                    <label for="questionFile">Файл</label>
                                    <a href="{{ route('problem.download', $problem->id) }}">Скачать</a>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="answer">Ответ</label>
                                <input class="form-control" id="answer" name="answer" required>
                            </div>
                            <div class="form-group">
                                <label for="commandFile">Файл</label>
                                <input type="file" id="file" name="file" />
                            </div>
                            <button class="btn btn-success">Отправить</button>
                        </form>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Диалог
                    </div>
                    <div class="card-body">
                        @php
                            $answer = false;
                        @endphp
                        @foreach($problem->messages->sortByDesc('id') as $message)
                            @if(!$answer && count($problem->messages) && $message->created_at < $problem->updated_at)
                                <div class="message mt-3 answer">
                                    <span class="font-weight-bold">Ботан</span>
                                    <p>{{$problem->answer}}</p>
                                    @if ($problem->answerFile)
                                        <a href="{{ route('problem.download', $problem->id) }}">Скачать</a>
                                    @endif
                                    <p class="message-date">{{date('d.m.Y H:i:s', strtotime($problem->messages[0]->created_at))}}</p>
                                </div>
                                @php
                                    $answer = true;
                                @endphp
                            @endif
                            @if($message->question || $message->questionFile)
                                <div class="message mt-3">
                                    <span class="font-weight-bold">{{$problem->getUserFullName()}}</span>
                                    <p>{{$message->question}}</p>
                                    @if ($message->questionFile)
                                        <a href="{{ route('dialog.download', $message->id) }}">Скачать</a>
                                    @endif
                                    <p class="message-date">{{date('d.m.Y H:i:s', strtotime($message->created_at))}}</p>
                                </div>
                            @elseif($message->answer || $message->answerFile)
                                <div class="message mt-3 answer">
                                    <span class="font-weight-bold">Ботан</span>
                                    <p>{{$message->answer}}</p>
                                    @if ($message->answerFile)
                                        <a href="{{ route('dialog.download.answer', $message->id) }}">Скачать</a>
                                    @endif
                                    <p class="message-date">{{date('d.m.Y H:i:s', strtotime($message->created_at))}}</p>
                                </div>
                            @endif
                            @if(!$answer && $problem->updated_at > $message->created_at)
                                <div class="message mt-3">
                                    <span class="font-weight-bold">{{$problem->getUserFullName()}}</span>
                                    <p>{{$message->question}}</p>
                                    <p class="message-date">{{date('d.m.Y H:i:s', strtotime($message->created_at))}}</p>
                                </div>
                            @endif
                        @endforeach
                        @if(!$answer)
                            <div class="message mt-3 answer">
                                <span class="font-weight-bold">Ботан</span>
                                <p>{{$problem->answer}}</p>
                                @if ($problem->answerFile)
                                    <a href="{{ route('problem.download', $problem->id) }}">Скачать</a>
                                @endif
                                <p class="message-date">{{date('d.m.Y H:i:s', strtotime($problem->updated_at))}}</p>
                            </div>
                        @endif
                        <div class="message mt-3">
                            <span class="font-weight-bold">{{$problem->getUserFullName()}}</span>
                            <p>{{$problem->question}}</p>
                            @if ($problem->questionFile)
                                <a href="{{ route('problem.download', $problem->id) }}">Скачать</a>
                            @endif
                            <p class="message-date">{{date('d.m.Y H:i:s', strtotime($problem->created_at))}}</p>
                        </div>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
