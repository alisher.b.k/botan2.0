@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                @php
                    $categories = \App\QuestionCategories::all();
                @endphp
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Редактирование категории
                    </div>
                    <div class="card-body">
                        <form action="{{route('saveCategory')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="category">Название категории</label>
                                <input class="form-control" id="category" name="category" value=" @if($category){{$category->categoryName}}@endif ">
                                <input type="hidden" class="form-control" name="categoryId" value="
                                @if($category)
                                {{$category->id}}
                                @else
                                {{$last}}
                                @endif
                                        ">
                            </div>
                            <div class="form-group">
                                <label for="parent_id">Название родительской категории</label>
                                <select name="parent_id" id="parent_id" class="form-control">
                                    <option value="0">Выберите родительскую категорию</option>
                                    @foreach($categories as $parentCategory)
                                        <option value="{{$parentCategory->id}}"
                                                @if($category && $category->parent_id == $parentCategory->id) selected @endif>
                                            {{$parentCategory->categoryName}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Описание</label>
                                <input class="form-control" id="description" name="description"
                                       value=" @if($category){{$category->description}}@endif ">
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
