@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Редактирование категории
                    </div>
                    <div class="card-body">
                        <form action="{{route('templates.save')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="template">Название шаблона</label>
                                <input class="form-control" id="template" name="template">
                            </div>
                            <div class="form-group">
                                <label for="category">Категория</label>
                                <select name="category" id="category" class="form-control">
                                    @foreach($templates as $template)
                                        <option value="{{$template->id}}">
                                            {{$template->category}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Описание</label>
                                <input class="form-control" id="description" name="description">
                            </div>
                            <div class="form-group">
                                <label for="commandFile">Файл</label>
                                <input type="file" id="file" name="file" />
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
