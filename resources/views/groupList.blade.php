@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Рассылка заявок ЦО</div>
                <div class="card-body">
                    <a href="{{route('group.add')}}" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Название группы</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>№</th>
                                <th>Название группы</th>
                                <th></th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($groups as $group)
                                <tr>
                                    <td>{{$group->id}}</td>
                                    <td>{{$group->name}}</td>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <a title="Редактировать" href="/botan/group/edit/{{$group->id}}" class="mx-1 btn btn-primary btn-xs">
                                                <i class="far fa-edit"></i>
                                            </a>
                                            <a title="Удалить" href="/botan/group/delete/{{$group->id}}" class="mx-1 btn btn-danger btn-xs">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
