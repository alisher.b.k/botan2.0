@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8 card">
                <form action="{{route('admin.addUsersInfo')}}" method="post">
                    @csrf
                    <button class="btn btn-primary">Запустить команду</button>
                </form>
                @foreach($files as $file)
                    @if($file != '.gitignore' && $file != '..' && $file != '.')
                        <a href="{{route('admin.read.log', $file)}}">{{$file}}</a>
                    @endif
                @endforeach
            </div>
            @if ($errors->any())
                <div class="col-md-5 card">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection
