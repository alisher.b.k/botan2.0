@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <a href="{{route('admin.logs')}}" style="width: 180px; margin-left: 10px; margin-top: 10px" class="btn btn-warning">Назад</a>
                 <div style="background-color: black; padding: 15px; margin-top: 10px">
                    @foreach($result as $data)
                        <p style="color:green; line-height: 1 !important;">{{$data}}</p>
                    @endforeach
                 </div>
            </div>
        </div>
    </div>
@endsection
