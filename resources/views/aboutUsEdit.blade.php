@extends('layouts.default_light')

@section('content')

    <div id="content-wrapper">
        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        О холдинге
                    </div>
                    <div class="card-body">
                        <form action="{{route('saveAboutUs')}}" method="post">
                            @csrf
                            {{--<div class="form-group">--}}
                                {{--<textarea name="aboutUsContent" id="content" class="form-control" cols="10" rows="10">--}}
                                    {{--@if ($aboutUs && count($aboutUs) > 0)--}}
                                        {{--{{$aboutUs[0]->content}}--}}
                                    {{--@endif--}}
                                {{--</textarea>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <textarea name="aboutUsContentV2" id="contentV2" class="form-control" cols="10" rows="8">@if ($aboutUs && count($aboutUs) > 0){{$aboutUs[0]->contentv2}}@endif</textarea>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                        <div class="mt-5">
                            <div class="d-flex flex-row">
                                <div class="p-2"><h5 class="mt-2">Команды</h5></div>
                                <div class="p-2"><a href="/botan/about-us/0/edit" class="btn btn-primary mx-1 mb-3">+ Добавить</a></div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Команда</th>
                                        <th>Описание</th>
                                        <th>Содержание</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>№</th>
                                        <th>Команда</th>
                                        <th>Описание</th>
                                        <th>Содержание</th>
                                        <th>Файл</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($commands as $command)
                                        <tr>
                                            <td>{{$command["id"]}}</td>
                                            <td>{{$command["command"]}}</td>
                                            <td>{{$command["commandDescription"]}}</td>
                                            <td>{{$command["content"]}}</td>
                                            @php
                                                $filePath = explode("/", $command["file"]);
                                            @endphp
                                            <td>{{$filePath[count($filePath)-1]}}</td>
                                            <td>
                                                <div class="d-flex flex-row">
                                                    <a title="Редактировать" href="/botan/about-us/{{$command->id}}/edit" class="mx-1 btn btn-primary btn-xs">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <a title="Удалить" href="/botan/about-us/{{$command->id}}/delete" class="mx-1 btn btn-danger btn-xs">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Сенат - компании
                    </div>
                    <div class="card-body">
                        <form action="{{route('saveSenateAboutUs')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <textarea name="senateCompanies" id="senateCompanies" class="form-control" cols="10" rows="8">@if ($aboutUs && count($aboutUs) > 1 && $aboutUs[1]){{$aboutUs[1]->contentv2}}@endif
                                </textarea>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
