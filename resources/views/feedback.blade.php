@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div>
                                <i class="fas fa-table"></i>
                                Улучшить ботан
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="feedback" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Email</th>
                                    <th>Сообщение</th>
                                    <th>Дата</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>№</th>
                                    <th>Email</th>
                                    <th>Сообщение</th>
                                    <th>Дата</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($feedbackMessages as $feedback)
                                    <tr>
                                        <td>{{$feedback->id}}</td>
                                        <td>
                                            @if($feedback->email)
                                                {{$feedback->email}}
                                            @else
                                                {{$feedback->telegramId}}
                                            @endif
                                        </td>
                                        <td>
                                            {{$feedback->message}}
                                            <br />
                                            @if ($feedback->file)
                                                <div> <a href="{{ route('feedback.download.file', $feedback->id) }}">Скачать</a></div>
                                            @endif
                                        </td>
                                        <td>{{$feedback->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif


        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>


        <!-- /.content-wrapper -->
    </div>

@endsection
