@extends('layouts.default_light')

@section('content')

    <div id="content-wrapper">
        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Выгрузка заявок по функционалу "Запись на прием к врачу"
                    </div>
                    <div class="card-body">
                        <form action="{{route('export.clinics')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="example-date-input" class="offset-1 col-form-label">Период с :</label>
                                    <div class="col-10">
                                        <input class="form-control" type="date" value="{{date('Y-01-01')}}" name="dateStart" id="example-date-input">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="example-date-input" class="offset-1 col-form-label">Период по :</label>
                                    <div class="col-10">
                                        <input class="form-control" type="date" value="{{date('Y-m-d')}}" name="dateEnd" id="example-date-input">
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-success">Выгрузить</button>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
