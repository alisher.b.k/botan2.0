@extends('layouts.default_light')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Новый пользователь
                </div>
                <div class="card-body">
                    <form id="createUser" action="{{ route('create.user') }}"
                          method="post" enctype="multipart/form-data" >
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Имя</label>
                            <div class="input-group">
                                <input class="form-control" name="name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Почта</label>
                            <div class="input-group">
                                <input class="form-control" name="email" type="email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">Пароль</label>
                            <div class="input-group">
                                <input class="form-control" name="password" type="password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role">Роль</label>
                            <select class="form-control" name="role" >
                                @foreach ($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group mt-2">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Sticky Footer -->
        <!-- <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer> -->

    </div>
@endsection
