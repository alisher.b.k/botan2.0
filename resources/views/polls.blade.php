@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Опросы</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Вопрос</th>
                                    <th>Ответ 1</th>
                                    <th>Ответ 2</th>
                                    <th>Дата</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>№</th>
                                    <th>Вопрос</th>
                                    <th>Ответ 1</th>
                                    <th>Ответ 2</th>
                                    <th>Дата</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($polls as $poll)
                                    <tr>
                                        <td>{{$poll["id"]}}</td>
                                        <td>{{$poll["question"]}}</td>
                                        <td>{{$poll["option1"]}} - {{$poll["option1Res"]}}</td>
                                        <td>{{$poll["option2"]}} - {{$poll["option2Res"]}}</td>
                                        <td>{{$poll["created_at"]}}</td>                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
