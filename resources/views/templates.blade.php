@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Категории вопросов</div>
                    <div class="card-body">
                        <a href="{{route('templates.category.add')}}" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Название категории</th>
                                    <th>Описание</th>
                                    <th>Операции</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i = 1)
                                @foreach($templates as $template)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td><a href="{{route('templates.category.get', $template->id)}}">{{$template->category}}</a></td>
                                        <td>{{$template->description}}</td>
                                        <td><a href="{{route('templates.category.delete', $template->id)}}">Удалить</a></td>
                                    </tr>
                                @php($i++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
