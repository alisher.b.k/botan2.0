@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Шишки
                    </div>
                    <div class="card-body">
                        <div style="display: flex; justify-content: space-between">
                            <form role="form" action="{{route('importExcelBosses')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                   id="excelFile" name="excelFile" />
                                <button class="btn btn-success" type="submit">Загрузить</button>
                            </form>
                            <a class="btn btn-primary" href="{{route('createBosses')}}">Создать</a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="bosses" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>ФИО</th>
                                    <th>Должность</th>
                                    <th>Компания</th>
                                    <th>Фото</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>№</th>
                                    <th>ФИО</th>
                                    <th>Должность</th>
                                    <th>Компания</th>
                                    <th>Фото</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($bosses as $boss)
                                    <tr>
                                        <td>{{$boss->id}}</td>
                                        <td>{{$boss->FIO}}</td>
                                        <td>{{$boss->position}}</td>
                                        <td>{{$boss->workplace}}</td>
                                        <td>
                                            @if ($boss->photo)
                                                <a href="{{ route('downloadPhotoBosses', $boss->id) }}">Скачать</a>
                                            @else
                                                <form role="form" action="{{route('uploadPhotoBosses')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="file" accept="image/*" id="photo" name="photo" />
                                                    <input type="hidden" id="id" name="id" value="{{$boss->id}}"/>
                                                    <button class="btn btn-success" type="submit">Загрузить</button>
                                                </form>
                                            @endif
                                        </td>
                                        <td><a href="{{route('bossesEdit', $boss->id)}}">Ред.</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
