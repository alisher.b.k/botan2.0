@extends('layouts.default_light')

@section('content')
    <div id="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                Обновить
            </div>
            <div class="card-body">
                <form action="{{route('bossesUpdate')}}" method="post">
                    @csrf
                    <input type="number" name="id" hidden value="{{$boss->id}}">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="FIO">ФИО</label>
                            <input type="text" id="FIO" class="form-control" name="FIO" value="{{$boss->FIO}}">
                        </div>
                        <div class="col-md-6">
                            <label for="position">Должность</label>
                            <input type="text" id="position" class="form-control" name="position" value="{{$boss->position}}">
                        </div>
                        <div class="col-md-6">
                            <label for="workplace">Компания</label>
                            <input type="text" id="workplace" class="form-control" name="workplace" value="{{$boss->workplace}}">
                        </div>
                        <div class="col-md-3 mt-3">
                            <button class="btn btn-primary">Обновить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
