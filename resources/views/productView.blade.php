<head>
  <meta property="og:site_name" content='<?php echo $product->companyName ?>'>
</head>
<body>
  <h2 id="title">
    <?php echo $product->title ?>
  </h2>
  <div id="content">
    <?php echo $product->productContent ?>
  </div>
</body>
