@extends('layouts.default_light')

@section('content')
    <div id="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                Добавить
            </div>
            <div class="card-body">
                <form action="{{route('bossesStore')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <label for="FIO">ФИО</label>
                            <input type="text" id="FIO" class="form-control" name="FIO">
                        </div>
                        <div class="col-md-6">
                            <label for="workplace">Должность</label>
                            <input type="text" id="workplace" class="form-control" name="workplace">
                        </div>
                        <div class="col-md-6">
                            <label for="company">Компания</label>
                            <input type="text" id="company" class="form-control" name="company">
                        </div>
                        <div class="col-md-3 mt-3">
                            <button class="btn btn-primary">Создать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
