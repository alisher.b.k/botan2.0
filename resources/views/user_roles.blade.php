@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Роли пользователей
                    </div>
                    <br>
                    <div class="ml-3">
                        <a href="{{ route('create.user') }}"><button type="button" class="btn btn-primary">Создать нового</button></a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Email</th>
                                    <th>Роль</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>№</th>
                                    <th>Email</th>
                                    <th>Роль</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->role->roleName->name}}</td>
                                        <td>
                                            <a title="Редактировать" href="/user/{{$user->id}}/edit" class="btn btn-primary btn-xs">
                                                <i class="fas fa-user-edit"></i>
                                            </a>
                                            <a  title="Удалить" href="{{route('delete.user', $user->id)}}" class="btn btn-danger text-white">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <a href="/change/insurance"><button class="btn btn-success">Иншуранс</button></a>
                        <a href="/change/kommesk"><button class="btn btn-success">Коммеск</button></a>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
