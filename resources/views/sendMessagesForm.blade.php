@extends('layouts.default_light')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.5/dist/css/select2.min.css" rel="stylesheet" />
@section('content')

    <div id="content-wrapper">

        <div class="container-fluid">
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Рассылка
                    </div>
                    <div class="card-body">
                        <form id="sendNotifications" action="{{route('admin.setting.sendNotifications')}}"
                              method="post" enctype="multipart/form-data" onsubmit="myButton.disabled = true; return true;">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="messageType">Тип</label>
                                <select class="form-control" id="messageType" name="messageType" onchange="typeMessages('d')">
                                  <option value="message" selected>Сообщение</option>
                                  <option value="message-reactions">Сообщение c реакциями со ссылкой</option>
                                  {{-- <option value="poll">Опрос</option> --}}
                                  <option value="reactions-without-links">Сообщение c реакциями без ссылки</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" id="title" name="title" required>
                            </div>
                            <div class="form-group">
                                <label for="company">Получатели</label>
                                <select class="form-control" id="company" name="company"  onchange="checkType('m')">
                                    <option value="user">Нескольким пользователям</option>
                                    <option value="companies">Нескольким компаниям</option>
                                    <option value="" selected>Всем пользователям</option>
                                </select>
                            </div>
                            <div class="form-group" id="usersSelectMailing" style="display: none">
                                <label for="company">Получатели</label>
                                <select id="users" class="multiple-1" name="users[]" style="width: 100%;" multiple>
                                    @foreach ($users as $user)
                                        @if($user->getFullName() !== null)
                                            <option value="{{$user->telegramId}}">{{$user->FIO}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="companiesSelectMailing" style="display: none">
                                <label for="company">Получатели</label>
                                <select id="users" class="multiple-1" name="companies[]" style="width: 100%;" multiple>
                                    @foreach($companies as $company)
                                        <option value="{{$company->companyName}}">{{$company->companyName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{-- <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="check" value="yes" id="buttonWithoutUnsubscribe">
                                <label class="form-check-label" for="buttonWithoutUnsubscribe">
                                  Принудительное получение данной рассылки
                                </label>
                              </div> --}}
                              
                              <div class="mt-2">
                                  <label>Количество подписанных пользователей :</label>
                                  <span id="count"></span>
                              </div>
                            <div class="form-group">
                                <label>Сообщение</label>
                                <div class="input-group">
                                    <textarea class="form-control" name="message"></textarea>
                                </div>
                            </div>
                            <div class="form-group" id ="link">
                                <label>Ссылка</label>
                                <div class="input-group">
                                    <textarea class="form-control" name="link"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="photoUpload">Загрузить картинку</label>
                                <input class="form-control-file" id="photoUpload" name="photoUpload[]" type="file" multiple>
                                <span style="color:red">Для выбора несколько файлов нажмите кнопку "Ctrl"</span>
                            </div>
                                <div class="input-group mt-2">
                                    <button type="submit" id="sendForm" name="myButton" class="btn btn-primary">Отправить</button>
                                </div>
                        </form>
                    </div>
                </div>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <!-- <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer> -->

    </div>
    <!-- /.content-wrapper -->

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://rawgit.com/select2/select2/4.0.5/dist/js/select2.full.min.js"></script>
    <script>
        function typeMessages(elem){
            let type = $('#messageType').val();
            if(type === 'reactions-without-links'){
                $('#link').hide();
            }else if(type === 'message'){
                $('#link').hide();
            }else if(type === 'poll'){
                $('#link').hide();
            }else{
                $('#link').show();
            }
        }
    </script>
    <script>
        $(".multiple-1").select2();
        $(".multiple-2").select2();
        function checkType(elem){
            let type = $('#company').val();
            if(type === 'user'){
                $('#usersSelectMailing').show();
                $('#companiesSelectMailing').hide();
            }else if(type === 'companies'){
                $('#usersSelectMailing').hide();
                $('#companiesSelectMailing').show();
            }else{
                $('#usersSelectMailing').hide();
                $('#companiesSelectMailing').hide();
            }
        }

        function getCountUsers(){
            const type = $('#messageType').val();
            $.ajax({
                type: "POST",
                url: "{{route('message.type')}}",
                data: {
                    type : type,
                    _token : '{{csrf_token()}}'
                },
                success:function(data){
                    $('#count').text(data.success)
                },
            });
        }
        window.onload = getCountUsers();

        function getTypeOfMessages(){
            $('#messageType').val('message').change();
        }

        window.onload = getTypeOfMessages();

       


    </script>

    <script>
         $('#sendForm').click(function(){
            $(this).text('Подождите пожалуйста');
        });
    </script>
    <script>
    function addFields(){
            // Number of inputs to create
            var number = document.getElementById("questionsNum").value;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("container");

            for (i=0;i<number;i++){
                // Append a node with a random text
                container.appendChild(document.createTextNode("Вопрос " + (i+1)));
                // Create an <input> element, set its type and name attributes
                var div = document.createElement("div");
                div.innerHTML = `<div class="form-group">
                                    <label>Сообщение</label>
                                    <div class="input-group">
                                        <textarea class="form-control" name="message${i}" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="photoUpload">Загрузить картинку</label>
                                    <input class="form-control-file" id="photoUpload${i}" name="photoUpload${i}" type="file">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 1</label>
                                    <input class="form-control-file" id="option1${i}" name="option1${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 2</label>
                                    <input class="form-control-file" id="option2${i}" name="option2${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 3</label>
                                    <input class="form-control-file" id="option3${i}" name="option3${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 4</label>
                                    <input class="form-control-file" id="option4${i}" name="option4${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="correctAnswer">Правильный ответ</label>
                                    <select class="form-control" id="correctAnswer${i}" name="correctAnswer${i}">
                                        <option value="0">Вариант 1</option>
                                        <option value="1">Вариант 2</option>
                                        <option value="2">Вариант 3</option>
                                        <option value="3">Вариант 4</option>
                                    </select>
                                </div>`;
                container.appendChild(div);
                // Append a line break
                container.appendChild(document.createElement("br"));
            }
        }
    </script>

@endsection



