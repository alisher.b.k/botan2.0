@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Категории вопросов</div>
                    <div class="card-body">
                        <a href="{{route('templates.add')}}" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Шаблоны</th>
                                    <th>Формат</th>
                                    <th>Описание</th>
                                    <th>Операции</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i = 1)
                                @foreach($templates as $template)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$template->template}}</td>
                                        <td>{{$template->description ? 'Текст' : ($template->file ? 'Файл' : 'Нет данных')}}</td>
                                        <td>{{$template->description}}<br>
                                            @if($template->file)
                                                <a href="{{route('templates.download', $template->id)}}">Скачать файл</a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('templates.sendView', $template->id)}}">Разослать</a><br>
                                            <a href="{{route('templates.edit', $template->id)}}">Изменить</a><br>
                                            <a href="{{route('templates.delete', $template->id)}}">Удалить</a>
                                        @if($template->file)
                                                <br><a href="{{route('templates.deleteFile', $template->id)}}">Удалить файл</a>
                                        @endif
                                        </td>
                                    </tr>
                                    @php($i++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
