@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Здания
                    </div>
                <div class="card-body">
                    <a href="{{route('booking.building.add')}}" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Здание</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Здание</th>
                                <th></th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($list as $building)
                                <tr>
                                    <td>{{$building->building}}</td>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <a title="Комнаты" href="{{ route('booking.room', [$building->id]) }}" class="btn btn-success">
                                                Комнаты
                                            </a>
                                            <a title="Редактировать" href="{{ route('booking.building.edit', [$building->id]) }}" class="mx-1 btn btn-primary btn-xs">
                                                <i class="far fa-edit"></i>
                                            </a>
                                            <a title="Удалить" href="{{ route('booking.building.delete', [$building->id]) }}" class="mx-1 btn btn-danger btn-xs">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
