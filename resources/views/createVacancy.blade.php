@extends('layouts.default_light')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@section('content')
  @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2 || Illuminate\Support\Facades\Auth::user()->role->role_id == 4)
    <div id="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Новая вакансия
                </div>
                <div class="card-body">
                    <form action="{{ route('createVacancy') }}"
                          method="post" enctype="multipart/form-data" >
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Название</label>
                            <div class="input-group">
                                <input class="form-control" name="name" type="text" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role">Компания</label>
                            <select class="form-control" name="company" required>
                                    <option value="Коммеск">Коммеск</option>
                                    <option value="Сентрас Иншуранс">Сентрас Иншуранс</option>
                                    <option value="Сентрас Капитал">Сентрас Капитал</option>
                                    <option value="SOS MA">SOS MA</option>
                                    <option value="Сентрас Секьюритиз">Сентрас Секьюритиз</option>
                                    <option value="Centras Kommesk Life">Centras Kommesk Life</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="department">Подразделение</label>
                            <div class="input-group">
                                <input class="form-control" name="department" type="text" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city">Город</label>
                            <div class="input-group">
                                <input class="form-control" name="city" type="text" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="requirements">Требования</label>
                            <textarea class="form-control" name="requirements" style="height: 200px" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="duties">Обязанности</label>
                            <textarea class="form-control" name="duties" style="height: 200px" required></textarea>
                        </div>
                        <div class="input-group mt-2">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
    @else
        У вас нет доступа для просмотра данной страницы
    @endif
@endsection
