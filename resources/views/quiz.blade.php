@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Викторина</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="quizDataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Название</th>
                                    <th>Дата</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>№</th>
                                    <th>Вопрос</th>
                                    <th>Дата</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($quizzes as $quiz)
                                    <tr>
                                        <td>{{$quiz["id"]}}</td>
                                        <td>
                                            {{$quiz["name"]}}
                                            <a class="btn btn-default" href="{{route('downloadExcel', ['id' => $quiz->id])}}">Экспорт Excel</a>                                   
                                        </td>
                                        <td>{{$quiz["created_at"]}}</td>                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
