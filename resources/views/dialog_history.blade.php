@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Диалоги</div>
                    <a class="btn btn-default" href="{{route('export-excel')}}">Экспорт Excel</a>
                    <div class="card-body">
                        <div class="d-flex justify-content-center">
                          <div class="d-inline-flex p-2 bd-highlight">
                            <table class="table table-borderless table-sm">
                              <!-- <thead>
                                <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">First</th>
                                  <th scope="col">Last</th>
                                  <th scope="col">Handle</th>
                                </tr>
                              </thead> -->
                              <tbody>
                                @foreach($histories as $history)
                                <?php if ($history->chatId) { ?>
                                  <tr>
                                    <td>
                                      @if($history->telegramFirstName)
                                          {{$history->telegramFirstName}}
                                      @endif
                                      @if($history->telegramLastName)
                                        {{$history->telegramLastName}}
                                      @endif
                                      <div><small class="text-muted">{{$history->date}}</small></div>
                                    </td>
                                    <td>
                                      <a href="/botan/dialogues/history/{{$history->chatId}}">
                                        <span style="font-size: 1.5em;">
                                          <i class="fab fa-telegram"></i>
                                        </span>
                                      </a>
                                    </td>
                                  </tr>
                                <?php } ?>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                            <!-- <table class="table table-bordered" id="history" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Дата</th>
                                    <th>Email или ФИО</th>
                                    <th>Запрос пользователя</th>
                                    <th>Ответ бота</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Дата</th>
                                    <th>Email или ФИО</th>
                                    <th>Запрос пользователя</th>
                                    <th>Ответ бота</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($histories as $history)
                                    <tr>
                                        <td>{{$history->updated_at}}</td>
                                        <td>
                                            @if($history->userEmail)
                                                {{$history->userEmail}}
                                            @elseif($history->telegramFirstName && $history->telegramLastName)
                                                {{$history->telegramFirstName}} {{$history->telegramLastName}}
                                            @elseif($history->telegramUsername)
                                                {{$history->telegramUsername}}
                                            @else
                                                Неизвестный пользователь
                                            @endif
                                        </td>
                                        <td>{{$history->request}}</td>
                                        <td>{{$history->response}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table> -->
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif


        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>


        <!-- /.content-wrapper -->
    </div>

@endsection
