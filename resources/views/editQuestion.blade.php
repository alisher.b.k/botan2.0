@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Редактирование вопроса
                    </div>
                    <div class="card-body">
                        <form action="{{route('saveQuestion')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="question">Вопрос</label>
                                <textarea class="form-control" id="question" name="question" rows="2">@if($question){{$question->question}}@endif </textarea>
                                <input type="hidden" class="form-control" name="questionId" value="
                                @if($question)
                                {{$question->id}}
                                @else
                                {{$last}}
                                @endif
                                        ">
                            </div>
                            <div class="form-group">
                                <label for="answer">Ответ</label>
                                <textarea class="form-control" id="answer" name="answer" rows="8">@if($question){{$question->answer}}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="category">Категория</label>
                                <select class="form-control" id="category" name="category">
                                    @foreach($categories as $category)
                                        @if($question && $question->categoryId!=0 && $category->id == $question->category->id)
                                            <option value="{{$category->id}}" selected>{{$category->categoryName}}</option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->categoryName}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="companyId">Доступ</label>
                                <select class="form-control" id="companyId" name="companyId">
                                    <option value="0"
                                            @if($question && $question->companyId && $question->companyId == 0)
                                            selected
                                            @endif
                                    >Для всех сотрудников</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}"
                                                @if($question && $question->companyId && $question->companyId ==  $company->id) selected @endif
                                        >{{$company->companyName}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
