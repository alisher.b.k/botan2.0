@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div>
                                <i class="fas fa-table"></i>
                                Редактирование мероприятия
                            </div>
                            <form action="{{route('deleteEvent')}}" method="post">
                                @csrf
                                <input class="form-control" type="text" name="id"
                                       value="@if($event){{$event->id}}@endif"
                                       hidden>
                                <button class="btn btn-danger btn-sm" @if(!$event) disabled @endif><i class="fas fa-trash"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('saveEvent')}}" method="post">
                            @csrf
                            <input class="form-control" type="text" name="id" value="@if($event){{$event->id}}@endif"
                                   hidden>
                            <div class="form-group">
                                <label for="title">Заголовок *</label>
                                <input class="form-control" type="text" name="title"
                                       value="@if($event){{$event->title}}@endif" required>
                            </div>
                            <div class="form-group">
                                <label for="eventType">Тип</label>
                                <select class="form-control" id="eventType" name="eventType">
                                  <option value="basic" selected>Обычное</option>
                                  <option value="senate">Сенат</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="eventContent">Содержание</label>
                                <textarea class="form-control"
                                          name="eventContent">@if($event){{$event->content}}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="eventDate">Дата *</label>
                                <input class="form-control" type="date" name="eventDate"
                                       value="@if($event){{$event->eventDate}}@endif" required>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
