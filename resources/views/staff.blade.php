@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Сотрудники</div>
                    <div class="card-body">
                        <a href="/staff/{{$last}}/edit" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                        <div>
                            <form role="form" action="{{route('importExcel')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                   id="excelFile" name="excelFile" />
                                <button class="btn btn-success" type="submit">Загрузить</button>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>ISN</th>
                                    <th>ФИО</th>
                                    <th>Должность</th>
                                    <th>Рабочий телефон</th>
                                    <th>Мобильный телефон</th>
                                    <th>Email</th>
                                    <th>Опыт</th>
                                    <th>Компания</th>
                                    <th>Сенткоины</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ISN</th>
                                    <th>ФИО</th>
                                    <th>Должность</th>
                                    <th>Рабочий телефон</th>
                                    <th>Мобильный телефон</th>
                                    <th>Email</th>
                                    <th>Опыт</th>
                                    <th>Компания</th>
                                    <th>Сенткоины</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($staff as $person)
                                    <tr>
                                        <td>{{$person->ISN}}</td>
                                        <td>{{$person->FIO}}</td>
                                        <td>{{$person->position}}</td>
                                        <td>{{$person->workPhone}}
                                            @if($person->intPhone)
                                                (вн. {{$person->intPhone}})
                                            @endif
                                        </td>
                                        <td>{{$person->mobPhone}}</td>
                                        <td>{{$person->email}}</td>
                                        <td>{{$person->workEx}}</td>
                                        <td>{{$person->WORKPLACE}}</td>
                                        <td>
                                            @if(!empty($person->balanceByIsn))
                                                {{$person->balanceByIsn->balance}}
                                            @elseif(!empty($person->balanceByEmail))
                                                {{$person->balanceByEmail->balance}}
                                            @else
                                                0
                                            @endif
                                            <br> 
                                            <a href="{{route('showTransactions',['id'=>$person->id])}}">Транзакции</a>
                                        <td>
                                            <div class="d-flex flex-row">
                                                <a title="Редактировать" href="/staff/{{$person->id}}/edit" class="mx-1 btn btn-primary btn-xs">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <a title="Удалить" href="/botan/staff/{{$person->id}}/delete" class="mx-1 btn btn-danger btn-xs">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
