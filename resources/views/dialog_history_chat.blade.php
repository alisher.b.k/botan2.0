@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                      {{$histories[0]->telegramFirstName}}
                      {{$histories[0]->telegramLastName}}
                    </div>
                    <div class="card-body d-flex justify-content-center" style="font-size: 0.8rem">
                      <div class="col-8" style="overflow-y: scroll; height:23rem;">
                        @foreach($histories as $history)
                            <div class="d-flex  @if($history->request)flex-row @elseif($history->response) flex-row-reverse @endif bd-highlight">
                                <div class="d-flex flex-column bd-highlight border rounded px-2 py-1" style="max-width: 30rem;">
                                    <div class="bd-highlight @if($history->request)text-primary @elseif($history->response) text-success @endif">
                                        @if($history->request)
                                          {{$history->telegramFirstName}} {{$history->telegramLastName}}
                                        @elseif($history->response)
                                          Ботан
                                        @endif
                                    </div>
                                    <div class="p-1 bd-highlight">
                                        @if($history->request)
                                            {{$history->request}}
                                        @elseif($history->response)
                                            {{$history->response}}
                                        @endif
                                    </div>
                                    <div class="p-1 bd-highlight"><small class="text-muted">{{$history->created_at}}</small></div>
                                </div>
                            </div>
                        @endforeach
                      </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif


        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>


        <!-- /.content-wrapper -->
    </div>

@endsection
