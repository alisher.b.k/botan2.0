@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Редактирование команды
                    </div>
                    <div class="card-body">
                        <form action="{{route('saveCommand')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="command">Команда</label>
                                <input class="form-control" id="command" name="command" value="@if($command){{$command->command}}@endif" required>
                                <input type="hidden" class="form-control" name="commandId" value="
                                @if($command)
                                {{$command->id}}
                                @endif">
                            </div>
                            <div class="form-group">
                                <label for="commandDescription">Описание</label>
                                <input class="form-control" id="commandDescription" name="commandDescription" value="@if($command){{$command->commandDescription}}@endif" required>
                            </div>
                            <div class="form-group">
                                <label for="commandContent">Содержание</label>
                                <input class="form-control" id="commandContent" name="commandContent" value="@if($command){{$command->content}}@endif" required>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
