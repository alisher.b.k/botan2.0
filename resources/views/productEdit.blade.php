@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Редактирование продукта
                    </div>
                    <div class="card-body">
                        <form action="{{route('saveProduct')}}" method="post">
                            @csrf
                            <input name="id" type="text" value="@if($product) {{$product->id}} @endif" hidden>
                            <div class="form-group">
                                <label>Заголовок</label>
                                <input name="title" type="text" class="form-control" value="@if($product) {{$product->title}} @endif"required>
                            </div>
                            <div class="form-group">
                                <label>Компания</label>
                                <select class="form-control" name="companyName">
                                    @foreach($companies as $company)
                                        @if($product && $product->companyName === $company->companyName)
                                            <option value="{{$company->companyName}}"
                                                    selected>{{$company->companyName}}</option>
                                        @else
                                            <option value="{{$company->companyName}}">{{$company->companyName}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Содержание</label>
                                <textarea name="productContent" id="productContent" class="form-control" cols="10"
                                          rows="10" required>
                                    @if ($product)
                                        {{$product->productContent}}
                                    @endif
                                </textarea>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection