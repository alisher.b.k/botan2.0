@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Вопросы-ответы</div>
                    <div class="card-body">
                        <a href="/question/{{$last}}/edit" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Вопрос</th>
                                    <th>Ответ</th>
                                    <th>Доступ</th>
                                    <th>Категория</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>№</th>
                                    <th>Вопрос</th>
                                    <th>Ответ</th>
                                    <th>Доступ</th>
                                    <th>Категория</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($questions as $question)
                                    <tr>
                                        <td>{{$question->id}}</td>
                                        <td>{{$question->question}}</td>
                                        <td>{{$question->answer}}</td>
                                        <td>@if($question->companyId !=0)
                                                {{$question->company->companyName}}
                                            @else
                                                Для всех сотрудников
                                            @endif
                                        </td>
                                        <td>
                                            @if($question->category)
                                                {{$question->category->categoryName}}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="d-flex flex-row">
                                                <a title="Редактировать" href="/question/{{$question->id}}/edit" class="mx-1 btn btn-primary btn-xs">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <a title="Удалить" href="/question/{{$question->id}}/delete" class="mx-1 btn btn-danger btn-xs">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
