@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Библиотека</div>
                    <div class="card-body">
                        <a href="/library/0/edit" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Описание</th>
                                    <th>Ссылка</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>№</th>
                                    <th>Описание</th>
                                    <th>Ссылка</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($library as $book)
                                    <tr>
                                        <td>{{$book["id"]}}</td>
                                        <td>{{$book["description"]}}</td>
                                        <td style="max-width: 100px;word-break: break-all;">{{$book["link"]}}</td>
                                        <td>
                                            <div class="d-flex flex-row">
                                                <a title="Редактировать" href="/library/{{$book->id}}/edit" class="mx-1 btn btn-primary btn-xs">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <a title="Удалить" href="/library/{{$book->id}}/delete" class="mx-1 btn btn-danger btn-xs">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
