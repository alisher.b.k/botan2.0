@extends('layouts.default_light')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.5/dist/css/select2.min.css" rel="stylesheet" />
@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Викторина
                    </div>
                    <div class="card-body">
                        <form id="sendQuiz" action="{{route('admin.setting.sendQuiz')}}"
                              method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                    <label for="quizName">Название викторины</label>
                                    <input class="form-control-file" id="quizName" name="quizName" type="text" required>
                                </div>
                            <label for="questionsNum">Кол-во вопросов в викторине:</label>
                            <input type="text" id="questionsNum" name="questionsNum" value="">
                            <a href="#" id="filldetails" onclick="addFields()">Добавить</a>
                            <div class="form-group">
                                <label for="company">Получатели</label>
                                <select class="form-control" id="companyQuiz" name="company" onchange="checkType('q')">
                                    <option value="user">Нескольким пользователям</option>
                                    <option value="companies">Нескольким компаниям</option>
                                    <option value="" selected>Всем пользователям</option>
                                </select>
                            </div>
                            <div class="form-group" id="usersSelectQuiz" style="display: none;">
                                <label for="company">Получатели</label>
                                <select id="users" class="multiple-1" name="users[]" style="width: 100%;" multiple>
                                    @foreach ($users as $user)
                                        @if($user->getFullName() !== null)
                                            <option value="{{$user->telegramId}}">{{$user->FIO}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="companiesSelectMailing" style="display: none;">
                                <label for="company">Получатели</label>
                                <select id="users" class="multiple-2" name="companies[]" style="width: 100%;" multiple>
                                    @foreach($companies as $company)
                                        <option value="{{$company->companyName}}">{{$company->companyName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="container"></div>
                                <!-- <div class="form-group">
                                    <label>Сообщение</label>
                                    <div class="input-group">
                                        <textarea class="form-control" name="message" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="photoUpload">Загрузить картинку</label>
                                    <input class="form-control-file" id="photoUpload" name="photoUpload" type="file">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 1</label>
                                    <input class="form-control-file" id="option1" name="option1" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 2</label>
                                    <input class="form-control-file" id="option2" name="option2" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 3</label>
                                    <input class="form-control-file" id="option3" name="option3" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 4</label>
                                    <input class="form-control-file" id="option4" name="option4" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="correctAnswer">Правильный ответ</label>
                                    <select class="form-control" id="correctAnswer" name="correctAnswer">
                                        <option value="0">Вариант 1</option>
                                        <option value="1">Вариант 2</option>
                                        <option value="2">Вариант 3</option>
                                        <option value="3">Вариант 4</option>
                                    </select>
                                </div> -->
                                <div class="input-group mt-2">
                                    <button type="submit" class="btn btn-primary">Отправить</button>
                                </div>
                            {{--</div>--}}
                        </form>

                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <!-- <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer> -->

    </div>
    <!-- /.content-wrapper -->

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://rawgit.com/select2/select2/4.0.5/dist/js/select2.full.min.js"></script>
    <script>
        $(".multiple-1").select2();
        $(".multiple-2").select2();
        function checkType(elem){
            let type = $('#companyQuiz').val();
            if(type === 'user'){
                $('#usersSelectQuiz').show();
                $('#companiesSelectMailing').hide();
            }else if(type === 'companies'){
                $('#usersSelectQuiz').hide();
                $('#companiesSelectMailing').show();
            }else{
                $('#usersSelectQuiz').hide();
                $('#companiesSelectMailing').hide();
            }
        }
    </script>
    <script>
    function addFields(){
            // Number of inputs to create
            var number = document.getElementById("questionsNum").value;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("container");

            for (i=0;i<number;i++){
                // Append a node with a random text
                container.appendChild(document.createTextNode("Вопрос " + (i+1)));
                // Create an <input> element, set its type and name attributes
                var div = document.createElement("div");
                div.innerHTML = `<div class="form-group">
                                    <label>Сообщение</label>
                                    <div class="input-group">
                                        <textarea class="form-control" name="message${i}" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="photoUpload">Загрузить картинку</label>
                                    <input class="form-control-file" id="photoUpload${i}" name="photoUpload${i}" type="file">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 1</label>
                                    <input class="form-control-file" id="option1${i}" name="option1${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 2</label>
                                    <input class="form-control-file" id="option2${i}" name="option2${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 3</label>
                                    <input class="form-control-file" id="option3${i}" name="option3${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="options">Вариант 4</label>
                                    <input class="form-control-file" id="option4${i}" name="option4${i}" type="text">
                                </div>
                                <div class="form-group">
                                    <label for="correctAnswer">Правильный ответ</label>
                                    <select class="form-control" id="correctAnswer${i}" name="correctAnswer${i}">
                                        <option value="0">Вариант 1</option>
                                        <option value="1">Вариант 2</option>
                                        <option value="2">Вариант 3</option>
                                        <option value="3">Вариант 4</option>
                                    </select>
                                </div>`;
                container.appendChild(div);
                // Append a line break
                container.appendChild(document.createElement("br"));
            }
        }
    </script>

@endsection
