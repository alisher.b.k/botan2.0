@extends('layouts.default_light')
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@section('content')
    <!-- Font Awesome -->

    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Рассылка
                    </div>
                    <div class="card-body">
                        <form id="sendNotifications" action="{{route('templates.send', $template->id)}}"
                              method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="template" value="{{$template->id}}">
                            <div class="form-group">
                                <label for="company">Получатели</label>
                                <select class="form-control" id="company" name="company" onchange="checkType()">
                                    @foreach($companies as $company)
                                        <option value="{{$company->companyName}}">{{$company->companyName}}</option>
                                    @endforeach
                                    <option value="user">Отдельному пользователю</option>
                                    <option value="" selected>Всем пользователям</option>
                                </select>
                            </div>
                            <div class="form-group" id="usersSelect" style="display: none;">
                                <label for="company">Получатели</label>
                                <select class="js-example-disabled-results" id="users" name="users[]" style="width: 100%;" multiple>
                                    @foreach ($users as $user)
                                        @if($user->getFullName() !== null)
                                           <option value="{{$user->telegramId}}">{{$user->FIO}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-group mt-2">
                                <button type="submit" class="btn btn-primary">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->


<script>
    var $disabledResults = $(".js-example-disabled-results");
    $disabledResults.select2();

    function checkType(){
        var type = $('#company').val();
        console.log(type);
        if(type === 'user'){
            $('#usersSelect').show();
        }else{
            $('#usersSelect').hide();
        }
    }
</script>
@endsection
