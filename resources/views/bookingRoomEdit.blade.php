@extends('layouts.default_light')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                </div>
                <div class="card-body">
                    <form id="bookingRoom" action="{{ route('booking.room.edit', [$room->id, $buildingId]) }}"
                          method="post" enctype="multipart/form-data" >
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>Название зданий</label>
                            <div class="input-group">
                                <input class="form-control" name="building" type="text" value="{{$room->room}}">
                            </div>
                            <br>
                            <div class="input-group">
                                <label>Цвет:</label> 
                                <input class="ml-2" type="color" value="{{ $room->color }}" name="color">
                            </div>
                            <span style="color:red">Для изменения цвет комнаты щелкните левой кнопкой мыши на поле с цветом</span>
                        </div>
                        <div class="input-group mt-2">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Sticky Footer -->
        <!-- <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer> -->

    </div>
    <!-- /.content-wrapper -->
@endsection
