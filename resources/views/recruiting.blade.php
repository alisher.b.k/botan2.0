@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2 || Illuminate\Support\Facades\Auth::user()->role->role_id == 4)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-tasks"></i>
                        Вакансии</div>
                    <div class="card-body">
                        <a href="{{ route('createVacancyView') }}" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                        <div>
                            <form role="form" action="{{ route('importVacancyExcel') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                   id="excelFile" name="excelFile" />
                                <button class="btn btn-success" type="submit">Загрузить</button>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Вакансия</th>
                                    <th>Компания</th>
                                    <th>Подразделение</th>
                                    <th>Город</th>
                                    <th>Требования</th>
                                    <th>Обязанности</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Вакансия</th>
                                    <th>Компания</th>
                                    <th>Подразделение</th>
                                    <th>Город</th>
                                    <th>Требования</th>
                                    <th>Обязанности</th>
                                    <th>Действия</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($vacancies as $vacancy)
                                    <tr>
                                        <td>{{ $vacancy->name }}</td>
                                        <td>{{ $vacancy->company }}</td>
                                        <td>{{ $vacancy->department }}</td>
                                        <td>{{ $vacancy->city }}</td>
                                        <td>{{ $vacancy->requirements }}</td>
                                        <td>{{ $vacancy->duties }}</td>
                                        <td>
                                            <div class="d-flex flex-row">
                                                <a title="Редактировать" href="{{ route('editVacancyView',['id' => $vacancy->id]) }}" class="mx-1 btn btn-primary btn-xs">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <a title="Удалить" href="{{ route('deleteVacancy',['id' => $vacancy->id]) }}" class="mx-1 btn btn-danger btn-xs">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->
@endsection
