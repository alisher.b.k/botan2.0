<!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex,nofollow">

    <title>Botan</title>
    <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <script src="{{asset('js/ru.js')}}"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body id="page-top">
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="">Botan</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>
    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        {{--<div class="input-group">--}}
        {{--<input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">--}}
        {{--<div class="input-group-append">--}}
        {{--<button class="btn btn-primary" type="button">--}}
        {{--<i class="fas fa-search"></i>--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
        {{--<li class="nav-item dropdown no-arrow mx-1">--}}
        {{--<a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
        {{--<i class="fas fa-bell fa-fw"></i>--}}
        {{--<span class="badge badge-danger">9+</span>--}}
        {{--</a>--}}
        {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">--}}
        {{--<a class="dropdown-item" href="#">Action</a>--}}
        {{--<a class="dropdown-item" href="#">Another action</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a class="dropdown-item" href="#">Something else here</a>--}}
        {{--</div>--}}
        {{--</li>--}}
        {{--<li class="nav-item dropdown no-arrow mx-1">--}}
        {{--<a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
        {{--<i class="fas fa-envelope fa-fw"></i>--}}
        {{--<span class="badge badge-danger">7</span>--}}
        {{--</a>--}}
        {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">--}}
        {{--<a class="dropdown-item" href="#">Action</a>--}}
        {{--<a class="dropdown-item" href="#">Another action</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a class="dropdown-item" href="#">Something else here</a>--}}
        {{--</div>--}}
        {{--</li>--}}
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                {{--<a class="dropdown-item" href="#">Settings</a>--}}
                {{--<a class="dropdown-item" href="#">Activity Log</a>--}}
                {{--<div class="dropdown-divider"></div>--}}
                {{--<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Выход</a>--}}
                <a class="dropdown-item">{{\Illuminate\Support\Facades\Auth::user()->email}}</a>
                <a class="dropdown-item" href="{{ route('logout') }}">Выход</a>
            </div>
        </li>
    </ul>

</nav>

<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav" style="font-size: 0.9rem">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1)
            <li class="nav-item">
                <a class="nav-link" href="{{route('usersRoles')}}">
                    <i class="fas fa-fw fa-user-tie"></i>
                    <span style="font-size: 0.9rem">Доступ</span></a>
            </li>
        @endif
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 2 || Illuminate\Support\Facades\Auth::user()->role->role_id == 1)
            <li class="nav-item">
                <a class="nav-link" href="{{route('dialogHistory')}}">
                    <i class="fas fa-fw fa-history"></i>
                    <span style="font-size: 0.9rem">История диалогов</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('questionsCategories')}}">
                    <i class="fas fa-fw fa-list"></i>
                    <span style="font-size: 0.9rem">Категории вопросов</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('questionsAnswers')}}">
                    <i class="fas fa-fw fa-table"></i>
                    <span style="font-size: 0.9rem">Вопросы-Ответы</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('showStaff')}}">
                    <i class="fas fa-fw fa-users"></i>
                    <span style="font-size: 0.9rem">Сотрудники</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('aboutUs')}}">
                    <i class="fas fa-info-circle" aria-hidden="true"></i>
                    <span style="font-size: 0.9rem">О нас</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('events')}}">
                    <i class="fas fa-calendar" aria-hidden="true"></i>
                    <span style="font-size: 0.9rem">Меропиятия</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('getProducts')}}">
                    <i class="fas fa-briefcase"></i>
                    <span style="font-size: 0.9rem">Продукты</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('sendMessagesForm')}}">
                    <i class="fas fa-envelope"></i>
                    <span style="font-size: 0.9rem">Рассылка</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('quizzes')}}">                   
                    <span style="font-size: 0.9rem">Результаты викторины</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('feedback')}}">
                    <i class="fas fa-comment-alt"></i>
                    <span style="font-size: 0.9rem">Улучшить Ботан</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('problems')}}">
                    <i class="fas fa-comment-alt"></i>
                    <span style="font-size: 0.9rem">Проблемы</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('prizes')}}">
                    <i class="fas fa-gift"></i>
                    <span style="font-size: 0.9rem">Призы</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('polls')}}">
                    <i class="fas fa-comment-alt"></i>
                    <span style="font-size: 0.9rem">Опросы</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('library')}}">
                    <i class="fas fa-comment-alt"></i>
                    <span style="font-size: 0.9rem">Библиотека</span></a>
            </li>
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="{{route('showStaff')}}">--}}
            {{--<i class="fas fa-fw fa-users"></i>--}}
            {{--<span>Рассылка</span></a>--}}
            {{--</li>--}}
        @endif
    </ul>
    @yield('content')
</div>


</body>
</html>
