@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">

        <div class="container-fluid">
        @if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
            <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Редактирование информации о сотруднике
                    </div>
                    <div class="card-body">
                        <form role="form" action="{{route('saveStaff')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @php
                                if($staff){
                                    $name = explode(" ", $staff->FIO);
                                }
                            @endphp
                            <div class="form-group">
                                <label for="ISN">ISN</label>
                                <input class="form-control" id="ISN" name="ISN"
                                       @if($staff) value="{{$staff->ISN}}" @endif required>
                            </div>
                            <div class="form-group">
                                <label for="lastname">Фамилия</label>
                                <input class="form-control" id="lastname" name="lastname"
                                       @if($staff && count($name)>0) value="{{$name[0]}}" @endif required>
                                <input type="hidden" class="form-control" name="id" value="
                                @if($staff)
                                {{$staff->id}}
                                @else
                                {{$last}}
                                @endif
                                        ">
                            </div>
                            <div class="form-group">
                                <label for="firstname">Имя</label>
                                <input class="form-control" id="firstname" name="firstname"
                                       @if($staff && count($name)>1) value="{{$name[1]}}" @endif required>
                            </div>
                            <div class="form-group">
                                <label for="parentname">Отчество</label>
                                <input class="form-control" id="parentname" name="parentname"
                                       @if($staff && count($name)>2) value="{{$name[2]}}" @endif required>
                            </div>
                            <div class="form-group">
                                <label for="NAMELAT">ФИО на латинском</label>
                                <input class="form-control" id="NAMELAT" name="NAMELAT"
                                       @if($staff) value="{{$staff->NAMELAT}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="position">Должность</label>
                                <input class="form-control" id="position" name="position"
                                       @if($staff) value="{{$staff->position}}" @endif required>
                            </div>
                            <div class="form-group">
                                <label for="WORKPLACE">Компания</label>
                                @if($staff)
                                    <input class="form-control" id="WORKPLACE" name="WORKPLACE"
                                           value="{{$staff->WORKPLACE}}" disabled>
                                @else
                                    <select class="form-control" id="WORKPLACE" name="WORKPLACE">
                                        <option value="АО Сентрас Секьюритиз" selected>АО «Сентрас Секьюритиз»</option>
                                        <option value="ТОО «Сентрас Капитал»">ТОО «Сентрас Капитал»</option>
                                        <option value="Test company">Test company</option>
                                    </select>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="MAINDEPT">Департамент</label>
                                <input class="form-control" id="MAINDEPT" name="MAINDEPT"
                                       @if($staff) value="{{$staff->MAINDEPT}}" @endif>
                            </div>
                            <div class="form-group">
                                <label for="SUBDEPT">Отделение</label>
                                <input class="form-control" id="SUBDEPT" name="SUBDEPT"
                                       @if($staff) value="{{$staff->SUBDEPT}}" @endif>
                            </div>
                            <div class="form-group">
                                <label for="photo">Ссылка на фото</label>
                                <input class="form-control" id="photo" name="photo"
                                       @if($staff) value="{{$staff->photo}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="photoUpload">Загрузить фото</label>
                                <input class="form-control-file" id="photoUpload" name="photoUpload" type="file">
                            </div>
                            <div class="form-group">
                                <label for="workPhone">Рабочий телефон</label>
                                <input class="form-control" id="workPhone" name="workPhone"
                                       @if($staff) value="{{$staff->workPhone}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="intPhone">Внутренний номер</label>
                                <input class="form-control" id="intPhone" name="intPhone"
                                       @if($staff) value="{{$staff->intPhone}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="mobPhone">Мобильный номер</label>
                                <input class="form-control" id="mobPhone" name="mobPhone"
                                       @if($staff) value="{{$staff->mobPhone}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input class="form-control" id="email" name="email"
                                       @if($staff) value="{{$staff->email}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="bday">Дата рождения</label>
                                <input type="date" class="form-control" id="bday" name="bday"
                                       @if($staff) value="{{$staff->bday}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="workEx">Стаж</label>
                                <input type="text" class="form-control" id="workEx" name="workEx"
                                       @if($staff) value="{{$staff->workEx}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="VACATIONBEG">Дата начала отпуска</label>
                                <input type="text" class="form-control" id="VACATIONBEG" name="VACATIONBEG"
                                       @if($staff) value="{{$staff->VACATIONBEG}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="VACATIONEND">Дата окончания отпуска</label>
                                <input type="text" class="form-control" id="VACATIONEND" name="VACATIONEND"
                                       @if($staff) value="{{$staff->VACATIONEND}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="TRIPBEG">Дата начала командировки</label>
                                <input type="text" class="form-control" id="TRIPBEG" name="TRIPBEG"
                                       @if($staff) value="{{$staff->TRIPBEG}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="TRIPEND">Дата окончания командировки</label>
                                <input type="text" class="form-control" id="TRIPEND" name="TRIPEND"
                                       @if($staff) value="{{$staff->TRIPEND}}" @endif >
                            </div>
                            <div class="form-group">
                                <label for="LEAVEDATE">Дата уволнения</label>
                                <input type="text" class="form-control" id="LEAVEDATE" name="LEAVEDATE"
                                       @if($staff) value="{{$staff->LEAVEDATE}}" @endif >
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>

                </div>
            @else
                У вас нет доступа для просмотра данной страницы
            @endif
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->



@endsection
