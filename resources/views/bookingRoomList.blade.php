@extends('layouts.default_light')

@section('content')


    <div id="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Комнаты
                    </div>
                <div class="card-body">
                    <a href="{{route('booking.room.add', [$building->id])}}" class="btn btn-primary mx-1 mb-3">+ Добавить</a>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Комната</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Комната</th>
                                <th></th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($rooms as $room)
                                <tr>
                                    <td><span style="background:{{ $room->color }}; display:inline-block; width:15px; height:15px;  border-radius:15px; vertical-align:middle;"></span> {{$room->room}}</td>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <a title="Редактировать" href="{{ route('booking.room.edit', [$room->id, $building->id]) }}" class="mx-1 btn btn-primary btn-xs">
                                                <i class="far fa-edit"></i>
                                            </a>
                                            <a title="Удалить" href="{{ route('booking.room.delete', [$room->id, $building->id]) }}" class="mx-1 btn btn-danger btn-xs">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Botan</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->
@endsection
