@extends('layouts.default_light')
@section('content')

<div id="content-wrapper">
<div class="container-fluid">
@if(Illuminate\Support\Facades\Auth::user()->role->role_id == 1 || Illuminate\Support\Facades\Auth::user()->role->role_id == 2)
<!-- DataTables Example -->
<div class="card mb-3">
<div class="card-header">
<i class="fas fa-table"></i>
Транзакции</div>
</div>
<div>
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#rechageBalance">
    Пополнить
</button>
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#withdrawBalance">
    Списать
</button>
</div>
    <!-- The Modal -->
    <div class="modal fade" id="rechageBalance">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title">Пополнение баланса</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body --> 
        <form method="post" action="{{route('rechargeBalance')}}">
        <div class="modal-body">
            
                @csrf
                <input name="id" value="{{$user->id}}" hidden/>
                <div class="form-group">
                    <label for="balance">Сумма пополнения</label>
                    <input type="number" class="form-control" id="balance" name="balance" min="1" required>
                </div>
                <div class="form-group">
                    <label for="description">Опиcание</label>
                    <input type="text" class="form-control" id="description" name="description" required>
                </div>
            
        </div>

<!-- Modal footer -->
<div class="modal-footer">
<button class="btn btn-primary">Пополнить</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
</div>
</form>
</div>
</div>
</div>
<!-- The Modal -->
<div class="modal fade" id="withdrawBalance">
<div class="modal-dialog modal-dialog-centered">
<div class="modal-content">

<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title">Списание баланса</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<form method="post" action="{{route('withdrawBalance')}}">
<div class="modal-body">
    
        @csrf
        <input name="id" value="{{$user->id}}" hidden/>
        <div class="form-group">
            <label for="balance">Сумма списания</label>
            <input type="number" class="form-control" id="balance" name="balance" min="1" required>
        </div>
        <div class="form-group">
            <label for="description">Опиcание</label>
            <input type="text" class="form-control" id="description" name="description" required>
        </div>
    
</div>

<!-- Modal footer -->
<div class="modal-footer">
<button class="btn btn-primary">Списать</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
</div>
</form>
</div>
</div>
</div>
<br/>
<div class="table-responsive">
<table class="table table-bordered table table-striped table-hover" id="history" width="100%" cellspacing="0">
<thead class='table-dark'>
<tr>
<th>Дата и время</th>
<th>Баланс до транзакций</th>
<th>Текущий баланс</th>
<th>Операции</th>
<th>Комментарии</th>
</tr>
</thead>
<tfoot class='table-dark' >
<tr>
<th>Дата и время</th>
<th>Баланс до транзакций</th>
<th>Текущий баланс</th>
<th>Операции</th>
<th>Комментарии</th>
</tr>
</tfoot>
<tbody>
@foreach($data as $person)
<tr>
<td>{{$person->created_at}}</td>
<td>{{$person->balanceBefore}}</td>
<td>{{$person->balanceAfter}}</td>
<td><span class="{{ $person->style }}">{{ $person->symbol }} {{ $person->result }}</span></td>
<td style="word-break: break-all">{{$person->description}}</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
@else
У вас нет доступа для просмотра данной страницы
@endif
</div>
<!-- /.container-fluid -->
<!-- Sticky Footer -->
<footer class="sticky-footer">
<div class="container my-auto">
<div class="copyright text-center my-auto">
<span>Copyright © Botan</span>
</div>
</div>
</footer>
</div>
<!-- /.content-wrapper -->
@endsection